// #pragma semicolon 1

#include <amxmodx>
#include <time>
#include <playerban>

#define PLUGIN "Player Screenshot"
#define VERSION "1.0"
#define AUTHOR "F@nt0M"

#if (AMXX_VERSION_NUM < 183) || defined NO_NATIVE_COLORCHAT
#include <colorchat>
#else
#define DontChange print_team_default
#endif

enum _BanData {
	BanTime[64],
	BanReason[128],
	BanDate[30]
};
new g_BanData[MAX_PLAYERS+1][_BanData];

new g_SyncHud;

new g_ServerAddr[32];
new g_ServerName[256];

new pcvar_Count;
new pcvar_Enable;

public plugin_init()
{
	register_plugin(PLUGIN, VERSION, AUTHOR);
	register_dictionary("player_screnshot.txt");
	register_dictionary("time.txt");
	g_SyncHud = CreateHudSyncObj();

	pcvar_Enable = register_cvar("amx_ps_enable", "1");
	pcvar_Count = register_cvar("amx_ps_count", "2");
}

public plugin_cfg()
{
	get_cvar_string("playerban_server_address", g_ServerAddr, 31);
	if (!g_ServerAddr[0])  {
		get_user_ip(0, g_ServerAddr, 31);
	}

	get_cvar_string("playerban_server_nick", g_ServerName, 255);
	if (!g_ServerName[0]) {
		get_pcvar_string(get_cvar_pointer("hostname"), g_ServerName, 255);
	}
}

public playerban_banned(id, ban_time, ban_reason[])
{
	if (is_user_connected(id) && get_pcvar_num(pcvar_Enable)) {
		g_BanData[id][BanTime] = ban_time;
		copy(g_BanData[id][BanReason], 127, ban_reason);

		if (ban_time > 0)  {
			get_time_length(id, ban_time, timeunit_minutes, g_BanData[id][BanTime], 63);
		} else {
			format(g_BanData[id][BanTime], 63, "%L", id, "PS_PERMANENTLY");
		}

		new Time[15], Date[15];
		get_time("%H:%M:%S", Time, 14);
		get_time("%d.%m.%Y", Date, 14);
		format(g_BanData[id][BanDate], 29, "%s %s", Date, Time);

		showChatMessage(id);
		showHudMessage(id);
		client_cmd(id, "stop");

		new count = get_pcvar_num(pcvar_Count);
		if (count < 1) {
			count = 1;
		}

		new data[1];
		data[0] = id;
		set_task(1.0, "task_snapshot", _, data, 1, "a", count);
	}
}

public task_snapshot(data[1], id)
{
	new pid = data[0];
	if (is_user_connected(pid)) {
		client_cmd(pid, "snapshot");
		showHudMessage(pid);
	}
}

showChatMessage(id)
{
	new name[32];
	get_user_name(id, name, 31);

	client_print_color(id, print_team_red, "^3%L. ^1%L: ^3%s.", id, "PS_BANNED", id, "PS_SERVER_IP", g_ServerAddr);
	client_print_color(id, print_team_red, "^3%L. ^1%L: ^3%s. ^1%L: ^3%s. ^1%L: ^3%s.", id, "PS_BANNED", id, "PS_PLAYER_NAME", name, id, "PS_BAN_REASON", g_BanData[id][BanReason], id, "PS_BAN_LENGTH", g_BanData[id][BanTime]);
	client_print_color(id, print_team_red, "^3%L. ^1%L: ^3%s.", id, "PS_BANNED", id, "PS_DATETIME", g_BanData[id][BanDate]);
}

showHudMessage(id)
{
	new message[512], len;

	new name[32];
	get_user_name(id, name, 31);

	len = format(message, 511, "%L^n^n", id, "PS_BANNED");

	len += format(message[len], 511 - len, "%L: %s^n", id, "PS_SERVER_NAME", g_ServerName);
	len += format(message[len], 511 - len, "%L: %s^n", id, "PS_SERVER_IP", g_ServerAddr);
	len += format(message[len], 511 - len, "%L: %s^n", id, "PS_PLAYER_NAME", name);
	len += format(message[len], 511 - len, "%L: %s^n", id, "PS_BAN_REASON", g_BanData[id][BanReason]);
	len += format(message[len], 511 - len, "%L: %s^n", id, "PS_BAN_LENGTH", g_BanData[id][BanTime]);
	len += format(message[len], 511 - len, "%L: %s^n", id, "PS_DATETIME", g_BanData[id][BanDate]);
	set_hudmessage(255, 100, 100, 0.4, 0.1, 0, 0.0, 1.0, 0.0, 0.0, 4);
	ShowSyncHudMsg(id, g_SyncHud, message);
}