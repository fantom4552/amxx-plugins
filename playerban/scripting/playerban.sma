#pragma semicolon 1

#define PLUGIN "Player Ban"
#define VERSION "1.9"
#define AUTHOR "F@nt0M"

// #define NOTIFY_SYSTEM
#define MAX_PLAYERS 32
#define MAX_BANTIMES 14
#define MAX_DISCONNECTED 5

#include <amxmodx>
#include <amxmisc>
#include <fakemeta>
#include <time>
#include <sqlx>

#include <adminload>

#if (AMXX_VERSION_NUM < 183) || defined NO_NATIVE_COLORCHAT
#include <colorchat>
#else
#define DontChange print_team_default
#endif

#if AMXX_VERSION_NUM == 183
#define client_has_disconnect client_disconnected
#define create_new_cvar create_cvar
#else
#define client_has_disconnect client_disconnect
#define create_new_cvar register_cvar
#endif

#include "playerban/global_vars.inl"
#include "playerban/init_functions.inl"
#include "playerban/cmd_ban.inl"
#include "playerban/menu_ban.inl"
#include "playerban/menu_history.inl"
#include "playerban/check_player.inl"
#include "playerban/web_handshake.inl"

public plugin_init()
{
	register_plugin(PLUGIN, VERSION, AUTHOR);
	register_cvar("playerban_version", VERSION, FCVAR_SERVER | FCVAR_EXTDLL | FCVAR_UNLOGGED| FCVAR_SPONLY);
	
	register_dictionary("playerban.txt");
	register_dictionary("common.txt");
	register_dictionary("time.txt");

	g_banReasons = ArrayCreate(128, 7);
	g_banReasons_Bantime = ArrayCreate(1, 7);

	register_concmd("amx_ban", "cmdBan", ADMIN_BAN, "<time in mins> <steamID or nickname or #authid or IP> <reason>");
	register_clcmd("amx_banmenu", "cmdBanMenu", ADMIN_BAN, "- displays ban menu");
	register_clcmd("amx_banhistorymenu", "cmdBanhistoryMenu", ADMIN_BAN, "- displays banhistorymenu");
	// register_clcmd("amx_bandisconnectedmenu", "cmdBanDisconnectedMenu", ADMIN_BAN, "- displays bandisconnectedmenu");

	register_srvcmd("amx_list", "cmdLst", ADMIN_RCON, "sends playerinfos to web");

	register_concmd("amx_reloadreasons", "cmdReload", ADMIN_CFG);

	/*#if defined COOKIE_CHECK_BAN
	register_message(get_user_msgid("MOTD"),"start_motd");
	register_concmd("amx_ban_cookie", "cmdBanCookie", ADMIN_CFG);
	#endif*/

	for (new i = 1; i <= MAX_PLAYERS; i++) {
		set_user_state(i, PDATA_DISCONNECTED);
	}

	g_TimeOffset = create_new_cvar("playerban_time_offset", "0");

	create_new_cvar("playerban_server_address", "");
	create_new_cvar("playerban_server_name", "");
	create_new_cvar("playerban_kick_url", "");
	create_new_cvar("playerban_kick_delay", "10.0");
	create_new_cvar("playerban_history_url", "");
	create_new_cvar("playerban_complain_url", "");
	create_new_cvar("playerban_ban_type", "0");
	create_new_cvar("playerban_server_nick", "");
	create_new_cvar("playerban_ban_times", "5 30 60 120 1440 10080 0");

	new configsDir[64];
	get_configsdir(configsDir, 63);
	
	server_cmd("exec %s/playerban.cfg", configsDir);
}

public plugin_cfg()
{
	create_forwards();

	g_ShowActivity = get_cvar_pointer("amx_show_activity");

	get_cvar_string("playerban_server_address", g_ServerAddr, 31);
	if (!g_ServerAddr[0])  {
		get_user_ip(0, g_ServerAddr, 31);
	}

	get_cvar_string("playerban_server_name", g_ServerName, 255);
	if (!g_ServerName[0]) {
		get_pcvar_string(get_cvar_pointer("hostname"), g_ServerName, 255);
	}

	g_KickDelay = get_cvar_float("playerban_kick_delay");

	get_cvar_string("playerban_kick_url", g_KickUrl, 255);
	get_cvar_string("playerban_history_url", g_HistoryUrl, 255);

	get_cvar_string("playerban_complain_url", g_ComplainUrl, 255);
	replace_all(g_ComplainUrl, 255, "http://", "");

	g_BanType = get_cvar_num("playerban_ban_type");

	// new mega_flag[5];
	// get_pcvar_string(register_cvar("playerban_mega_flag", "n"), mega_flag, 4);
	// g_MegaFlag = read_flags(mega_flag);

	new ban_times_cvar[128];
	get_cvar_string("playerban_ban_times", ban_times_cvar, 127);
	loadBanTimes(ban_times_cvar);

	//#if defined COOKIE_CHECK_BAN
	//get_pcvar_string(register_cvar("playerban_motd_url", ""), g_MotdUrl, 255);
	//replace_all(g_MotdUrl, 255, "http://", "");
	//#endif

	get_cvar_string("playerban_server_nick", g_PlayerData[0][PlayerName], 31);
	if (!g_PlayerData[0][PlayerName][0]) {
		copy(g_PlayerData[0][PlayerName], 31, g_ServerName);
	}
	copy(g_PlayerData[0][PlayerSteamid], 33, "STEAM_ID_SERVER");
	get_user_ip(0, g_PlayerData[0][PlayerIp], 21, 1);
}

public plugin_natives() 
{
	register_library("PlayerBanCore");
}

public adminload_sql_initialized(Handle:DBTuple, const dbPrefix[])
{
	if (DBTuple == Empty_Handle) {
		log_amx("[SQL Error] DB Info Tuple from adminload is empty");
		return PLUGIN_CONTINUE;
	}

	g_DBTuple = DBTuple;

	copy(g_DbPrefix, 9, dbPrefix);
	
	set_task(0.1, "server_info_load");

	return PLUGIN_CONTINUE;
}

public adminload_changename(player, name[])
{
	copy(g_PlayerData[player][PlayerName], 31, name);
}

public adminload_connect(id, flags)
{
	if (flags & ADMIN_BAN) {
		add_user_state(id, PDATA_ADMIN);
	}

	if (flags & ADMIN_IMMUNITY) {
		add_user_state(id, PDATA_IMMUNITY);
	}

	// if (flags & g_MegaFlag) {
	// 	add_user_state(id, PDATA_MEGA_ADMIN);
	// }
}

public adminload_disconnect(id)
{
	remove_user_state(id, PDATA_ADMIN);
	remove_user_state(id, PDATA_IMMUNITY);
	remove_user_state(id, PDATA_MEGA_ADMIN);
}

create_forwards() 
{
	MFHandle[Playerban_Banned] = CreateMultiForward("playerban_banned", ET_IGNORE, FP_CELL, FP_CELL, FP_STRING);
}

public client_connect(id) 
{
	set_user_state(id, PDATA_CONNECTING);
}

public client_authorized(id) 
{
	get_user_name(id, g_PlayerData[id][PlayerName], 31);
	get_user_authid(id, g_PlayerData[id][PlayerSteamid], 33);
	get_user_ip(id, g_PlayerData[id][PlayerIp], 21, 1);

	if (g_DbInit) {
		check_player(id);
	}
}

public client_putinserver(id) 
{
	remove_user_state(id, PDATA_CONNECTING);
	
	if (is_user_hltv(id)) {
		add_user_state(id, PDATA_HLTV);
	} else if (is_user_bot(id)) {	
		add_user_state(id, PDATA_BOT);
	}

	add_user_state(id, PDATA_CONNECTED);
}

public client_has_disconnect(id) 
{
	// if (!get_user_state(id, PDATA_BOT) && !get_user_state(id, PDATA_HLTV) && !get_user_state(id, PDATA_IMMUNITY) && !get_user_state(id, PDATA_IMMUNITY)) {
	// 	g_PlayerDisconnectedNum++;
	// 	if (g_PlayerDisconnectedNum > MAX_DISCONNECTED) {
	// 		g_PlayerDisconnectedNum = 0;
	// 	}

	// 	g_PlayerDisconnectedCount++;
	// 	if (g_PlayerDisconnectedCount > MAX_DISCONNECTED) {
	// 		g_PlayerDisconnectedCount = MAX_DISCONNECTED;
	// 	}
	// 	copy(g_PlayerDisconnected[g_PlayerDisconnectedNum][PlayerSteamid], 33, g_PlayerData[id][PlayerSteamid]);
	// 	copy(g_PlayerDisconnected[g_PlayerDisconnectedNum][PlayerName], 31, g_PlayerData[id][PlayerName]);
	// 	copy(g_PlayerDisconnected[g_PlayerDisconnectedNum][PlayerIp], 321, g_PlayerData[id][PlayerIp]);
	// }

	set_user_state(id, PDATA_DISCONNECTED);
}

public cmdReload(id, level, cid)
{
	if (!cmd_access(id, level, cid, 1)) {
		return PLUGIN_HANDLED;
	}

	if (g_DBTuple != Empty_Handle) {
		server_info_load();
		log_amx("[PlayerBan] Reload reasons success!");
	} else {
		log_amx("[SQL Error] DB Info Tuple from adminload is empty");
	}

	return PLUGIN_HANDLED;
}

public delayed_kick(id) 
{
	id -= KICK_TASK_ID;
	
	if (!is_user_disconnected(id)) {
		// add_user_state(id, PDATA_KICKED);
		kick_player(id);
	}
}

kick_player(id)
{
	server_cmd("kick #%d  %L", get_user_userid(id), id, "KICK_MESSAGE");
}

loadBanTimes(ban_times[])
{
	new ban_time_str[10];
	trim(ban_times);
	while(contain(ban_times, " ") != -1){
		strtok(ban_times, ban_time_str, 9, ban_times, 127, ' ', 1);
		g_BanTimes[g_BanTimesCount++] = str_to_num(ban_time_str);
	}
	g_BanTimes[g_BanTimesCount++] = str_to_num(ban_times);
}

stock SQL_Error(Handle:query, const error[], errornum, failstate)
{
	new qstring[1024];
	SQL_GetQueryString(query, qstring, 1023);
	
	if (failstate == TQUERY_CONNECT_FAILED) {
		log_amx("[SQL Error] Connection failed");
	} else if (failstate == TQUERY_QUERY_FAILED) {
		log_amx("[SQL Error] Query failed");
	}

	log_amx("[SQL Error] Message: %s (%d)", error, errornum);
	log_amx("[SQL Error] Query: %s", qstring);

	return SQL_FreeHandle(query);
}

stock mysql_escape_string(dest[], len)
{
	replace_all(dest, len, "\\", "\\\\");
	replace_all(dest, len, "\0", "\\0");
	replace_all(dest, len, "\n", "\\n");
	replace_all(dest, len, "\r", "\\r");
	replace_all(dest, len, "\x1a", "\Z");
	replace_all(dest, len, "'", "\'");
	replace_all(dest, len, "^"", "\^"");
}

stock mysql_get_username(id, dest[], len) 
{
	copy(dest, len, g_PlayerData[id][PlayerName]);
	mysql_escape_string(dest, len);
}

stock mysql_get_servername(dest[], len) 
{
	new mapname[64];
	get_mapname(mapname, 63);
	format(dest, len, "%s (%s)", g_ServerName, mapname);
	mysql_escape_string(dest, len);
}