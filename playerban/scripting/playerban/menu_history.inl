#if defined _menu_history_included
    #endinput
#endif

#define _menu_history_included

public cmdBanhistoryMenu(id, level, cid) 
{
	if(!cmd_access(id, level, cid, 1)) {
		return PLUGIN_HANDLED;
	}

	new menu = menu_create("menu_player", "actionBanhistoryMenu");
	
	MenuSetProps(id, menu, "BAN_MENU");

	MenuGetPlayers(menu, -1);
	
	menu_display(id, menu, 0);

	return PLUGIN_HANDLED;
}

public actionBanhistoryMenu(id, menu, item)
{
	if(item == MENU_EXIT) {
		menu_destroy(menu);
		return PLUGIN_HANDLED;
	}

	new szData[6], mapName[64], item_access, item_callback;
	menu_item_getinfo(menu, item, item_access, szData, 5, mapName,63, item_callback);
		
	new pid = str_to_num(szData);

	// if (!get_user_state(pid, PDATA_CONNECTED)) {
	// 	client_print_color(0, print_team_red,  "%L", id, "PLAYER_LEAVED");
	// 	return PLUGIN_HANDLED;
	// }

	new motd_url[256];
	copy(motd_url, 255, g_HistoryUrl);
	replace(motd_url, 255, "{PLAYER_ID}", g_PlayerData[pid][PlayerSteamid]);
	replace(motd_url, 255, "{PLAYER_IP}", g_PlayerData[pid][PlayerIp]);

	show_motd(id, motd_url, g_ServerName);

	return PLUGIN_HANDLED;
}

/*public cmdBanDisconnectedMenu(id, level, cid)
{
	if(!cmd_access(id, level, cid, 1)) {
		return PLUGIN_HANDLED;
	}

	new menu = menu_create("menu_player", "actionBanDisconnectedMenu");
	
	MenuSetProps(id, menu, "BAN_MENU");

	new i, pid[3];
	for (i = 0; i < g_PlayerDisconnectedCount; i++) {
		num_to_str(i, pid, 2);
		menu_additem(menu, g_PlayerDisconnected[i][PlayerName], pid);
	}

	menu_display(id, menu, 0);

	return PLUGIN_HANDLED;
}

public actionBanDisconnectedMenu(id, menu, item)
{
	if(item == MENU_EXIT) {
		menu_destroy(menu);
		return PLUGIN_HANDLED;
	}

	new szData[6], mapName[64], item_access, item_callback;
	menu_item_getinfo(menu, item, item_access, szData, 5, mapName,63, item_callback);
		
	new pid = str_to_num(szData);

	banPlayer(pid, true);

	return PLUGIN_HANDLED;
}*/