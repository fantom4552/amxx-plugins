#if defined _menu_ban_included
    #endinput
#endif

#define _menu_ban_included


public cmdBanMenu(id, level, cid) 
{
	if(!cmd_access(id, level, cid, 1)) {
		return PLUGIN_HANDLED;
	}

	g_BanData[id][BanMenu] = -1;
	banMenu(id, 0);

	return PLUGIN_HANDLED;
}

banMenu(id, page = 0)
{
	clear_ban(id);
	
	new menu = menu_create("menu_player", "actionBanMenu");
	
	MenuSetProps(id, menu, "BAN_MENU");

	new callback = menu_makecallback("callback_MenuGetPlayers");
	MenuGetPlayers(menu, callback);
	
	menu_display(id, menu, page);

	g_BanData[id][BanMenu] = menu;
	g_BanData[id][BanPage] = page;
}

public actionBanMenu(id, menu, item)
{
	if(item == MENU_EXIT) {
		menu_destroy(menu);
		g_BanData[id][BanMenu] = -1;
		g_BanData[id][BanPage] = 0;
		return PLUGIN_HANDLED;
	}

	new szData[6], mapName[64], item_access, item_callback;
	menu_item_getinfo(menu, item, item_access, szData, 5, mapName,63, item_callback);
		
	new pid = str_to_num(szData);
	
	if (!get_user_state(pid, PDATA_CONNECTED)) {
		client_print_color(0, print_team_red,  "%L", id, "PLAYER_LEAVED");
		return PLUGIN_HANDLED;
	}

	g_BanData[id][BanPlayer] = pid;

	new oldmenu, newmenu, page;
	player_menu_info(id, oldmenu, newmenu, page);
	g_BanData[id][BanPage] = page;

	if (adminload_get_static_bantime(id)) {
		cmdReasonMenu(id, 1);
	} else {
		cmdBantimeMenu(id);
	}

	return PLUGIN_HANDLED;
}

public cmdReasonMenu(id, static_bantime) 
{
	new menu = menu_create("menu_banreason", "actionReasonMenu");
	
	MenuSetProps(id, menu, "REASON_MENU");
	MenuGetReason(id, menu, static_bantime);

	menu_display(id, menu, 0);

	return PLUGIN_HANDLED;
}

public actionReasonMenu(id, menu, item) 
{
	if(item == MENU_EXIT) {
		menu_destroy(menu);
		remove_user_state(g_BanData[id][BanPlayer], PDATA_BEING_BANNED);
		g_BanData[id][BanPlayer] = 0;
		menu_display(id, g_BanData[id][BanMenu], g_BanData[id][BanPage]);
		return PLUGIN_HANDLED;
	}

	new szData[6], mapName[64], item_access, item_callback;
	menu_item_getinfo(menu, item, item_access, szData, 5, mapName,63, item_callback);
	
	new rid = str_to_num(szData);

	menu_destroy(menu);

	ArrayGetString(g_banReasons, rid, g_BanData[id][BanReason], 127);
	if (adminload_get_static_bantime(id)) {
		g_BanData[id][BanTime] = ArrayGetCell(g_banReasons_Bantime, rid);
	}

	g_BanData[id][BanType] = get_ban_type(g_BanData[id][BanPlayer]);

	menu_destroy(g_BanData[id][BanMenu]);
	g_BanData[id][BanMenu] = -1;
	g_BanData[id][BanPage] = 0;

	if(get_user_state(g_BanData[id][BanPlayer], PDATA_BEING_BANNED)) {
		client_print_color(0, print_team_red,  "%L", id, "BLOCKING_DOUBLEBAN");
		return PLUGIN_HANDLED;
	}

	banPlayer(id);

	return PLUGIN_HANDLED;
}

public cmdBantimeMenu(id) 
{
	new menu = menu_create("menu_bantime", "actionBantimeMenu");
	
	MenuSetProps(id, menu, "BANTIME_MENU");
	MenuGetBantime(id, menu);

	menu_display(id, menu, 0);

	return PLUGIN_HANDLED;
}

public actionBantimeMenu(id, menu, item) 
{
	if (item == MENU_EXIT) {
		menu_destroy(menu);
		remove_user_state(g_BanData[id][BanPlayer], PDATA_BEING_BANNED);
		g_BanData[id][BanPlayer] = 0;
		menu_display(id, g_BanData[id][BanMenu], g_BanData[id][BanPage]);

		return PLUGIN_HANDLED;
	}
	
	new szData[6], mapName[64], item_access, item_callback;
	menu_item_getinfo(menu, item, item_access, szData, 5, mapName,63, item_callback);
	
	g_BanData[id][BanTime] = str_to_num(szData);
	
	menu_destroy(menu);

	cmdReasonMenu(id, 0);

	return PLUGIN_HANDLED;
}

stock MenuSetProps(id, menu, const title[]) 
{
	new szText[64];

	menu_setprop(menu, MPROP_PERPAGE, 7);

	formatex(szText, 63, "\y%L\w", id, title);
	menu_setprop(menu, MPROP_TITLE, szText);

	formatex(szText, 63, "%L", id, "BACK");
	menu_setprop(menu, MPROP_BACKNAME, szText);
	
	formatex(szText, 63, "%L", id, "MORE");
	menu_setprop(menu, MPROP_NEXTNAME, szText);
	
	formatex(szText, 63, "%L^n^n\y%s by %s", id, "EXIT", PLUGIN, AUTHOR);
	menu_setprop(menu, MPROP_EXITNAME, szText);
	
	return 1;
}

stock MenuGetPlayers(menu, callback) 
{
	new i, pid, playerId[3], players[32], num;

	get_players(players, num, "ch");

	for (i = 0; i < num; i++) {
		pid = players[i];
		g_PlayersMenuTeams[pid] = get_user_team(pid);
	}

	SortCustom1D(players, num, "_PlayersMenuSortTeam");

	for (i = 0; i <num; i++) {
		pid = players[i];
		num_to_str(pid, playerId, 2);
		menu_additem(menu, g_PlayerData[pid][PlayerName], playerId, 0, callback);
	}
}

public _PlayersMenuSortTeam(const elem1, const elem2)
{
	if (elem1 == 0 || elem2 == 0) {
		return 0;
	}
		
	if (elem1 == elem2) {
		return 0;
	}

	if (g_PlayersMenuTeams[elem1] > g_PlayersMenuTeams[elem2]) {
		return 1;
	} else if (g_PlayersMenuTeams[elem1] < g_PlayersMenuTeams[elem2]) {
		return -1;
	}

	return 0;
}

public callback_MenuGetPlayers(id, menu, item) 
{
	new acc, szInfo[3], szText[128], callb;
	menu_item_getinfo(menu, item, acc, szInfo, 2, szText, 127, callb);
	
	new pid = str_to_num(szInfo);
	
	new szStatus[64];	
	get_player_status(pid, szStatus);
	
	switch (g_PlayersMenuTeams[pid]) {
		case 1: {
			format(szText, 63, "\r%s %s \w (T)", g_PlayerData[pid][PlayerName], szStatus);
		}
		case 2: {
			format(szText, 63, "\y%s %s \w(CT)", g_PlayerData[pid][PlayerName], szStatus);
		}

		default: {
			format(szText, 63, "\d%s %s \w(SPEC)", g_PlayerData[pid][PlayerName],  szStatus);
		}
	}

	menu_item_setname(menu, item, szText);
	
	if (get_user_state(pid, PDATA_IMMUNITY) || get_user_state(pid, PDATA_BOT) || get_user_state(pid, PDATA_BEING_BANNED) || is_user_disconnected(pid)) {
		return ITEM_DISABLED;
	}
	
	return ITEM_ENABLED;
}

stock MenuGetReason(id, menu, staticBantime = 0) 
{
	new rnum = ArraySize(g_banReasons);
	new szDisplay[128], szArId[3], szTime[64];
	
	for (new i; i < rnum; i++) {
		ArrayGetString(g_banReasons, i ,szDisplay, 127);
		num_to_str(i, szArId, 2);
		if (staticBantime) {
			get_bantime_string(id, ArrayGetCell(g_banReasons_Bantime, i), szTime, 63);
			format(szDisplay, 127, "%s (%s)", szDisplay, szTime);
		} 
		menu_additem(menu, szDisplay, szArId);
	}
	
	return 1;
}

stock get_player_status(pid, szStatus[64])
{
	if (!get_user_state(pid, PDATA_CONNECTED)) {
		format(szStatus, 63, "%s \r(NOT CONNECTED)\w", szStatus);
	}

	if (get_user_state(pid, PDATA_ADMIN)) {
		format(szStatus, 63, "%s \r*\w", szStatus);
	}

	if (get_user_state(pid, PDATA_BOT)) {
		format(szStatus, 63, "%s \r(BOT)\w", szStatus);
	}

	if (get_user_state(pid, PDATA_HLTV)) {
		format(szStatus, 63, "%s \r(HLTV)\w", szStatus);
	}

	if (get_user_state(pid, PDATA_BEING_BANNED)) {
		format(szStatus, 63, "%s \r(Banning...)\w", szStatus);
	}
}

stock get_bantime_string(id, btime, text[], len) 
{
	if (btime <=0)  {
		formatex(text, len, "%L", id, "BAN_PERMANENT");
	}  else {
		new szTime[64];
		get_time_length(id, btime, timeunit_minutes, szTime, 63);
		formatex(text, len, "%L", id, "BAN_FOR_MINUTES", szTime);
	}
	
	return 1;
}

stock MenuGetBantime(id, menu) 
{
	new ban_time_str[10], ban_time_display[64];
	
	for (new i = 0; i < g_BanTimesCount; i++) {
		// if (!get_user_state(id, PDATA_MEGA_ADMIN) && g_BanTimes[i] == 0) {
		// 	continue;
		// }
		num_to_str(g_BanTimes[i], ban_time_str, 9);
		get_bantime_string(id, g_BanTimes[i], ban_time_display, 63);
		menu_additem(menu, ban_time_display, ban_time_str);
	}
}