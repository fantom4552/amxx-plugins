#if defined _init_functions_included
    #endinput
#endif

#define _init_functions_included

public server_info_load()
{
	new error[128], errno;
	new pquery[1024];
	new Handle:sql = SQL_Connect(g_DBTuple, errno, error, 127);
	new Handle:query;
	
	if (sql == Empty_Handle) {
		log_amx("[SQL Error] Can't connect: %s", error);
		return PLUGIN_HANDLED;
	}

	SQL_QueryAndIgnore(sql, "SET NAMES UTF8");
	//SQL_SetCharset(g_DBTuple, "utf8");

	formatex(pquery, 255, "SELECT `id`, (UNIX_TIMESTAMP(NOW()) - %d) `offset` FROM `%s_serverinfo` WHERE address = '%s' LIMIT 1", get_systime(0), g_DbPrefix, g_ServerAddr);
	query = SQL_PrepareQuery(sql, pquery);
	if (!SQL_Execute(query)) {
		new error[512];
		new errornum = SQL_QueryError(query, error, 511);
		
		SQL_Error(query, error, errornum, TQUERY_QUERY_FAILED);
		return PLUGIN_HANDLED;
	}

	if (!SQL_NumResults(query)) {
		log_amx("[Error] Server not found in Database");
		return PLUGIN_HANDLED;
	}

	g_ServerId = SQL_ReadResult(query, 0);
	if (!get_pcvar_num(g_TimeOffset)) {
		set_pcvar_num(g_TimeOffset, SQL_ReadResult(query, 1));
	}

	SQL_FreeHandle(query);
	formatex(pquery, 1023, "SELECT re.`reason`, re.`static_bantime` ^n\
							FROM `%s_reasons` re ^n\
							JOIN `%s_reasons_to_set` rs ON re.`id` = rs.`reasonid` ^n\
							JOIN `%s_serverinfo` si ON rs.`setid` = si.`reasons` ^n\
							WHERE `si`.`id` = %d ^n\
							ORDER BY `re`.`id`", g_DbPrefix, g_DbPrefix, g_DbPrefix, g_ServerId);
	query = SQL_PrepareQuery(sql, pquery);
	if (!SQL_Execute(query)) {
		new error[512];
		new errornum = SQL_QueryError(query, error, 511);
		
		SQL_Error(query, error, errornum, TQUERY_QUERY_FAILED);
		return PLUGIN_HANDLED;
	}

	ArrayClear(g_banReasons);
	ArrayClear(g_banReasons_Bantime);

	new reason[128];
	new reason_time;

	new aNum = SQL_NumResults(query);

	if (!aNum) {
		formatex(reason, 127, "%L", LANG_SERVER, "REASON_1");
		ArrayPushReasons(reason, 0);
		formatex(reason, 127, "%L", LANG_SERVER, "REASON_2");
		ArrayPushReasons(reason, 0);
		formatex(reason, 127, "%L", LANG_SERVER, "REASON_3");
		ArrayPushReasons(reason, 0);
		formatex(reason, 127, "%L", LANG_SERVER, "REASON_4");
		ArrayPushReasons(reason, 0);
		formatex(reason, 127, "%L", LANG_SERVER, "REASON_5");
		ArrayPushReasons(reason, 0);
		formatex(reason, 127, "%L", LANG_SERVER, "REASON_6");
		ArrayPushReasons(reason, 0);
		formatex(reason, 127, "%L", LANG_SERVER, "REASON_7");
		ArrayPushReasons(reason, 0);
	
		log_amx("[PlayerBan] No Reasons found in Database. Static reasons were loaded instead.");

		return PLUGIN_HANDLED;
	} else {
		while (SQL_MoreResults(query)) {
			SQL_ReadResult(query, 0, reason, 127);
			reason_time = SQL_ReadResult(query, 1);
			ArrayPushReasons(reason, reason_time);
			SQL_NextRow(query);
		}

		log_amx("[PlayerBan] 7 Reasons loaded from Database", aNum);
	}
	
	SQL_FreeHandle(query);
	SQL_FreeHandle(sql);

	check_players();

	g_DbInit = true;

	return PLUGIN_CONTINUE;
}

stock ArrayPushReasons(const reason[], bantime) 
{
	ArrayPushString(g_banReasons, reason);
	ArrayPushCell(g_banReasons_Bantime, bantime);
}