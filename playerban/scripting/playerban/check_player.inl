#if defined _check_player_included
    #endinput
#endif

#define _check_player_included

public check_players()
{
	for (new id = 1; id <= MAX_PLAYERS; id++) {
		if (get_user_state(id, PDATA_CONNECTING) || get_user_state(id, PDATA_CONNECTED)) {
			check_player(id);
		}
	}
}

public check_player(id) 
{	
	if (is_user_disconnected(id) || get_user_state(id, PDATA_IMMUNITY)) {
		return PLUGIN_HANDLED;
	}

	new data[1], pquery[1024];
	formatex(pquery, 1023, "SELECT bid, ban_created, ban_length, ban_reason, admin_nick, player_nick, player_id, player_ip FROM `%s_bans` WHERE ((player_id = '%s' AND ban_type = 'S') OR (player_ip = '%s' AND ban_type = 'SI')) AND expired = 0 ORDER BY ban_created DESC LIMIT 1", g_DbPrefix, g_PlayerData[id][PlayerSteamid], g_PlayerData[id][PlayerIp]);
	// log_amx(pquery)

	//SQL_SetCharset(g_DBTuple, "utf8");

	data[0] = id;
	SQL_ThreadQuery(g_DBTuple, "action_check_player", pquery, data, 1);

	return PLUGIN_HANDLED;
}

public action_check_player(failstate, Handle:query, const error[], errornum, const data[], size, Float:queuetime)
{
	if (failstate) {
		SQL_Error(query, error, errornum, failstate);
		return PLUGIN_HANDLED;
	}
	
	new pid = data[0];
	new pquery[256];
	
	if (!SQL_NumResults(query) || is_user_disconnected(pid)) {
		SQL_FreeHandle(query);
		return PLUGIN_HANDLED;
	}

	new ban_reason[128], admin_nick[100], player_nick[50], player_steamid[50], player_ip[30];

	new bid = SQL_ReadResult(query, 0);
	new ban_created = SQL_ReadResult(query, 1);
	new ban_length = SQL_ReadResult(query, 2) * 60;
	SQL_ReadResult(query, 3, ban_reason, 127);
	SQL_ReadResult(query, 4, admin_nick, 99);
	SQL_ReadResult(query, 5, player_nick, 49);
	SQL_ReadResult(query, 6, player_steamid, 49);
	SQL_ReadResult(query, 7, player_ip, 29);

	SQL_FreeHandle(query);
	
	if ( (ban_length == 0) || (ban_created == 0) || (ban_created + ban_length) > get_systime(get_pcvar_num(g_TimeOffset)) ) {

		print_console_ban(pid, 0, true, ban_length, ban_reason);
		set_task(0.5, "delayed_kick", pid + KICK_TASK_ID);

		/*formatex(pquery, 255, "UPDATE `%s_bans` SET `ban_kicks` = `ban_kicks` + 1 WHERE `bid` = %d LIMIT 1", g_DbPrefix, bid);
		SQL_ThreadQuery(g_DBTuple, "empty_sql", pquery);*/

	} else {
		formatex(pquery, 255, "UPDATE `%s_bans` SET `expired` = 1 WHERE `bid` = %d LIMIT 1", g_DbPrefix, bid);
		SQL_ThreadQuery(g_DBTuple, "empty_sql", pquery);
	}

	return PLUGIN_HANDLED;
}

/*public load_ban(id, bid) 
{
	if(is_user_disconnected(id) || get_user_state(id, PDATA_IMMUNITY))
	{
		return PLUGIN_HANDLED;
	}

	new data[1], pquery[1024];
	formatex(pquery, 1023, "SELECT `bid`, `ban_created`, `ban_length`, `ban_reason`, `admin_nick`, `player_nick`, `player_id`, `player_ip` FROM `%s_bans` WHERE bid=%d LIMIT 1", g_DbPrefix, bid);

	data[0] = id;
	SQL_ThreadQuery(g_DBTuple, "action_check_player", pquery, data, 1);

	return PLUGIN_HANDLED;
}*/

public empty_sql(failstate, Handle:query, const error[], errornum, const data[], size, Float:queuetime)
{
	if (failstate) {
		return SQL_Error(query, error, errornum, failstate);
	}
	
	SQL_FreeHandle(query);
	return PLUGIN_HANDLED;
}

// #if defined COOKIE_CHECK_BAN
// public start_motd(const msg, const dest, const id) 
// {
//         if(get_user_state(id, PDATA_MOTD_SHOW)) 
// 		{
// 			return PLUGIN_CONTINUE;
// 		}
		
//         new buffer[256];
//         format(buffer, 255, g_MotdUrl, get_user_userid(id));
//         show_motd(id, buffer, g_ServerName);
//         add_user_state(id, PDATA_MOTD_SHOW);

//         return PLUGIN_HANDLED;
// }
// #endif