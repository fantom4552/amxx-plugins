#if defined _global_vars_included
	#endinput
#endif

#define _global_vars_included

#define KICK_TASK_ID 2015
// #define FILTER_PLAYERS "" // "ch"

#define PDATA_DISCONNECTED 0
#define PDATA_CONNECTING 1
#define PDATA_CONNECTED 2
#define PDATA_BOT 3
#define PDATA_HLTV 4
#define PDATA_ADMIN 5
#define PDATA_IMMUNITY 6
#define PDATA_MEGA_ADMIN 7
#define PDATA_BEING_BANNED 8
// #define PDATA_MOTD_SHOW 11

#define set_user_state(%1,%2) (g_PlayerData[%1][PlayerState] = (1<<%2))
#define add_user_state(%1,%2) (g_PlayerData[%1][PlayerState] |= (1<<%2))
#define get_user_state(%1,%2) (g_PlayerData[%1][PlayerState] & (1<<%2))
#define remove_user_state(%1,%2) (g_PlayerData[%1][PlayerState] &= ~(1<<%2))
#define is_user_disconnected(%1) (get_user_state(%1, PDATA_DISCONNECTED) || (!is_user_connecting(%1) && !get_user_state(%1, PDATA_CONNECTED)))

enum _PlayerData {
	PlayerSteamid[34],
	PlayerName[32],
	PlayerIp[22],
	PlayerState
};

new g_PlayerData[MAX_PLAYERS+1][_PlayerData];


// new g_PlayerDisconnected[MAX_DISCONNECTED+1][_PlayerData];
// new g_PlayerDisconnectedNum;
// new g_PlayerDisconnectedCount;

new const g_BanIpSteamId[] = {
	"HLTV",
	"STEAM_ID_LAN",
	"VALVE_ID_LAN",
	"VALVE_ID_PENDING",
	"STEAM_ID_PENDING"
};

enum MFHandles {
	Playerban_Banned
};

new MFHandle[MFHandles];
new MFReturn;

new Handle:g_DBTuple;
new g_DbPrefix[10];
new bool:g_DbInit = false;

new g_ServerAddr[32];
new g_ServerName[256];
new g_ServerId;
new g_ShowActivity = 0;
new g_KickUrl[256];
new Float:g_KickDelay = 10.0;
new g_ComplainUrl[256];
new g_HistoryUrl[256];
new g_BanType = 0;
// new g_MegaFlag;

new g_BanTimes[MAX_BANTIMES];
new g_BanTimesCount = 0;

// #if defined COOKIE_CHECK_BAN
// new g_MotdUrl[256];
// #endif

new g_TimeOffset;

new Array:g_banReasons;
new Array:g_banReasons_Bantime;

enum _BanData {
	BanMenu,
	BanPage,
	BanType,
	BanPlayer,
	BanReason[128],
	BanTime
};

new g_BanData[MAX_PLAYERS+1][_BanData];

new g_PlayersMenuTeams[MAX_PLAYERS+1];