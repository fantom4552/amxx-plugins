#if defined _web_handshake_included
    #endinput
#endif
#define _web_handshake_included


public cmdLst(id, level, cid)
{
	if (!cmd_access(id, level, cid, 1)) {
		return PLUGIN_HANDLED;
	}
	
	new pid, status, immun, userid;
	
	for (pid = 1; pid <= MAX_PLAYERS; pid++)	{
		if (get_user_state(pid, PDATA_CONNECTED)) {
			userid = get_user_userid(pid);
			
			if (get_user_state(pid, PDATA_BOT)) {
				status = 1;
			} else if (get_user_state(pid, PDATA_HLTV)) {
				status = 2;
			} else {
				status = 0;
			}
			
			immun = get_user_state(pid, PDATA_IMMUNITY) ? 1 : 0;
			
			console_print(id, "%s%c%d%c%s%c%s%c%d%c%d", g_PlayerData[pid][PlayerName], -4, userid, -4, g_PlayerData[pid][PlayerSteamid], -4, g_PlayerData[pid][PlayerIp], -4, status, -4, immun);
		}
	}
	
	return PLUGIN_HANDLED;
}
