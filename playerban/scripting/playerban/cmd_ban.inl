#if defined _cmd_ban_included
    #endinput
#endif

#define _cmd_ban_included

public cmdBan(id, level, cid)
{
	if (!cmd_access(id, level, cid, 3)) {
		return PLUGIN_HANDLED;
	}
	
	new tmp[192], tmp_time[6], tmp_player[52], ban_reason[128], ban_player, ban_time;
	
	read_args(tmp, 191);
	parse(tmp, tmp_time, 5, tmp_player, 51, ban_reason, 127);
	remove_quotes(tmp_time);
	remove_quotes(tmp_player);
	remove_quotes(ban_reason);

	if (!is_str_num(tmp_time) || read_argc() < 3) {
		console_print(id, "[Ban Error] amx_ban <Time in Minutes> <SteamID | Nickname> <Reason>");
		return PLUGIN_HANDLED;
	}

	ban_player = locate_player(tmp_player);

	if (!ban_player) {
		console_print(id, "[Ban Error] Player %s could not be found", tmp_player);
		return PLUGIN_HANDLED;
	}

	if (get_user_state(ban_player, PDATA_IMMUNITY)) {
		console_print(id, "[Ban Error] Player %s has imumnity", tmp_player);
		return PLUGIN_HANDLED;
	}

	if(get_user_state(ban_player, PDATA_BEING_BANNED)) {
		console_print(id, "This player has already banning by other admin");
		return PLUGIN_HANDLED;
	}

	ban_time = abs(str_to_num(tmp_time));

	// if (!get_user_state(id, PDATA_MEGA_ADMIN) && ban_time == 0) {
	// 	console_print(id, "[Ban Error] You are not authorized to ban via Console.");
	// 	return PLUGIN_HANDLED;
	// }

	g_BanData[id][BanPlayer] = ban_player;
	g_BanData[id][BanTime] = ban_time;
	copy(g_BanData[id][BanReason], 127, ban_reason);
	g_BanData[id][BanType] = get_ban_type(ban_player);

	banPlayer(id);

	return PLUGIN_HANDLED;
}

banPlayer(id)
{
	new pid = g_BanData[id][BanPlayer];

	add_user_state(pid, PDATA_BEING_BANNED);

	print_console_ban(pid, id, false, g_BanData[id][BanTime], g_BanData[id][BanReason]);
	set_pev(pid, pev_flags, pev(pid, pev_flags) | FL_FROZEN);

	new admin_nick[32];
	mysql_get_username(id, admin_nick, 31);

	new server_name[256];
	mysql_get_servername(server_name, 255);
	
	new current_time_int = get_systime(get_pcvar_num(g_TimeOffset));

	new ban_type[3];
	if (g_BanData[id][BanType]) {
		copy(ban_type, 2, "SI");
	} else {
		copy(ban_type, 2, "S");
	}

	log_ban(pid, id);

	//SQL_SetCharset(g_DBTuple, "utf8");

	new pquery[512];
	format(pquery, 511, "SET NAMES UTF8; INSERT INTO %s_bans ^n\
		(player_id, player_ip, player_nick, admin_ip, admin_id, admin_nick, ban_type, ban_reason, cs_ban_reason, ban_created, ban_length, server_name, server_ip, expired) ^n\
		VALUES('%s', '%s', '%s', '%s', '%s', '%s', '%s', '%s', '%s', %d, %d, '%s', '%s', 0)",
		g_DbPrefix, g_PlayerData[pid][PlayerSteamid], g_PlayerData[pid][PlayerIp], g_PlayerData[pid][PlayerName], g_PlayerData[id][PlayerIp], g_PlayerData[id][PlayerSteamid], admin_nick, ban_type, g_BanData[id][BanReason], "See banlist", current_time_int, g_BanData[id][BanTime], server_name, g_ServerAddr);
	
	//log_amx(pquery)

	new data[2];
	data[0] = id;
	data[1] = pid;
	SQL_ThreadQuery(g_DBTuple, "insert_ban", pquery, data, 2);
}

public insert_ban(failstate, Handle:query, const error[], errornum, const data[], size, Float:queuetime)
{
	new id = data[0];
	new pid = data[1];

	if (failstate) {
		set_task(g_KickDelay, "delayed_kick", pid + KICK_TASK_ID);
		SQL_Error(query, error, errornum, failstate);
	} else {
		ExecuteForward(MFHandle[Playerban_Banned], MFReturn, pid, g_BanData[id][BanTime], g_BanData[id][BanReason]);

		new pquery[256];
		format(pquery, 255, "SELECT bid FROM %s_bans WHERE player_id='%s' AND server_ip='%s' ORDER BY ban_created DESC LIMIT 1",
			g_DbPrefix, g_PlayerData[pid][PlayerSteamid], g_ServerAddr);
		SQL_ThreadQuery(g_DBTuple, "select_ban", pquery, data, 2);
	}
}

public select_ban(failstate, Handle:query, const error[], errornum, const data[], size, Float:queuetime)
{
	new id = data[0];
	new pid = data[1];

	if (failstate) {
		set_task(g_KickDelay, "delayed_kick", pid + KICK_TASK_ID);
		SQL_Error(query, error, errornum, failstate);
	} else if(!SQL_NumResults(query)) {
		set_task(g_KickDelay, "delayed_kick", pid + KICK_TASK_ID);
	} else {
		new bid = SQL_ReadResult(query, 0);

		SQL_FreeHandle(query);

		notify_ban(pid, id);
		clear_ban(id);

		new motd_url[256], bidstr[10], lang[5];
		if (g_KickUrl[0]) {
			copy(motd_url, 255, g_KickUrl);

			formatex(bidstr, 9, "%d", bid);
			replace(motd_url, 255, "{BID}", bidstr);

			get_user_info(pid, "lang", lang, 9);
			if (!lang[0]) {
				get_cvar_string("amx_language", lang, 9);
			}

			replace(motd_url, 255, "{LANG}", lang);
			replace(motd_url, 255, "{ADMIN}", ((g_ShowActivity == 2) ? "1" : "0"));
		}

		if (!is_user_disconnected(pid)) {
			set_task(g_KickDelay, "delayed_kick", pid + KICK_TASK_ID);

			if (motd_url[0] && get_user_state(pid, PDATA_CONNECTED)) {
				show_motd(pid, motd_url, g_ServerName);
			}
		}
	}
}

/*#if defined COOKIE_CHECK_BAN
public cmdBanCookie(id, level, cid)
{
	if(!cmd_access(id, level, cid, 3, true))
	{
		return PLUGIN_HANDLED;
	}
	
	new tmp[192], uid_str[10], bid_str[10], uid, bid, pid;
	read_args(tmp, 191);
	parse(tmp, uid_str, 9, bid_str, 9);
	remove_quotes(uid_str);
	remove_quotes(bid_str);
	uid = str_to_num(uid_str);
	bid = str_to_num(bid_str);
	
	pid = find_player("k", uid);

	server_print("User ID %d Ban ID %d ID %d", uid, bid, pid);
	
	load_ban(pid, bid);
	
	return PLUGIN_HANDLED
}
#endif*/

stock get_ban_type(id)
{
	if (g_BanType == 1) {
		return 1;
	} else if(g_BanType == 2) {
		return 0;
	} else if(!equal(g_PlayerData[id][PlayerIp], "127.0.0.1")) {
		new i, n = sizeof(g_BanIpSteamId);
		for (i = 0; i < n; i++) {
			if (equal(g_BanIpSteamId[i], g_PlayerData[id][PlayerSteamid])) {
				return 1;
			}
		}
	}

	return 0;
}

stock print_console_ban(id, admin_id, bool:forse, ban_time, ban_reason[])
{
	new ban_length[64];
	if (ban_time > 0)  {
		get_time_length(id, ban_time, timeunit_minutes, ban_length, 63);
	} else {
		format(ban_length, 63, "%L", id, "TIME_ELEMENT_PERMANENTLY");
	}
	
	console_info(id, forse, "=====================================");
	console_info(id, forse, "%L", id, "BAN_MSG_1");
	console_info(id, forse, "%L", id, "BAN_MSG_2", ban_reason);
	console_info(id, forse, "%L", id, "BAN_MSG_3", ban_length);
	console_info(id, forse, "%L", id, "BAN_MSG_4", g_PlayerData[id][PlayerName]);
	console_info(id, forse, "%L", id, "BAN_MSG_5", g_PlayerData[id][PlayerSteamid]);
	console_info(id, forse, "%L", id, "BAN_MSG_6", g_PlayerData[id][PlayerIp]);
	if (admin_id) {
		console_info(id, forse, "%L", id, "BAN_MSG_7", g_PlayerData[admin_id][PlayerName]);
	}
	console_info(id, forse, "%L", id, "BAN_MSG_8", g_ComplainUrl);
	console_info(id, forse, "=====================================");
}

stock console_info(id, bool:forse, fmt[], any:...)
{
	new msg[192];
 	vformat(msg, 191, fmt, 4);

 	if (forse) {
 		client_cmd(id, "echo ^"[Banned] %s^"", msg);
 	} else {
 		client_print(id, print_console, "[Banned] %s", msg);
 	}
}

stock notify_ban(id, admin_id)
{
	new ban_time, ban_length[64], ban_reason[128];

	ban_time = g_BanData[admin_id][BanTime];
	copy(ban_reason, 127, g_BanData[admin_id][BanReason]);

	if (ban_time > 0)  {
		get_time_length(id, ban_time, timeunit_minutes, ban_length, 63);
	}

	if (!ban_reason[0]) {
		format(ban_reason, 127, "%L", id, "NO_REASON");
	}

	new message[192];

	switch (get_pcvar_num(g_ShowActivity)) {
		case 1: {
			if (ban_time > 0) {
				client_print_color(0, print_team_default, "%L", LANG_SERVER, "PUBLIC_BAN_ANNOUNCE", g_PlayerData[id][PlayerName], ban_length, ban_reason);
			} else {
				client_print_color(0, print_team_default, "%L", LANG_SERVER, "PUBLIC_BAN_ANNOUNCE_PERM", g_PlayerData[id][PlayerName], ban_reason);
			}
		}

		case 2: {
			if (ban_time > 0) {
				client_print_color(0, print_team_default, "%L", LANG_SERVER, "PUBLIC_BAN_ANNOUNCE_2", g_PlayerData[id][PlayerName], ban_length, ban_reason, g_PlayerData[admin_id][PlayerName]);
			} else {
				client_print_color(0, print_team_default, "%L", LANG_SERVER, "PUBLIC_BAN_ANNOUNCE_2_PERM", g_PlayerData[id][PlayerName], ban_reason, g_PlayerData[admin_id][PlayerName]);
			}
		}

		case 3: {
			if (ban_time > 0) {
				format(message, 191, "%L", LANG_SERVER, "PUBLIC_BAN_ANNOUNCE_2", g_PlayerData[id][PlayerName], ban_length, ban_reason, g_PlayerData[admin_id][PlayerName]);
				notify_ban_ex(true, message);
				format(message, 191, "%L", LANG_SERVER, "PUBLIC_BAN_ANNOUNCE", g_PlayerData[id][PlayerName], ban_length, ban_reason);
				notify_ban_ex(false, message);
			} else {
				format(message, 191, "%L", LANG_SERVER, "PUBLIC_BAN_ANNOUNCE_2_PERM", g_PlayerData[id][PlayerName], ban_reason, g_PlayerData[admin_id][PlayerName]);
				notify_ban_ex(true, message);
				format(message, 191, "%L", LANG_SERVER, "PUBLIC_BAN_ANNOUNCE_PERM", g_PlayerData[id][PlayerName], ban_reason);
				notify_ban_ex(false, message);
			}
		}

		case 4: {
			if (ban_time > 0) {
				format(message, 191, "%L", LANG_SERVER, "PUBLIC_BAN_ANNOUNCE_2", g_PlayerData[id][PlayerName], ban_length, ban_reason, g_PlayerData[admin_id][PlayerName]);
				notify_ban_ex(true, message);
			} else {
				format(message, 191, "%L", LANG_SERVER, "PUBLIC_BAN_ANNOUNCE_2_PERM", g_PlayerData[id][PlayerName], ban_reason, g_PlayerData[admin_id][PlayerName]);
				notify_ban_ex(true, message);
			}
		}

		case 5: {
			if (ban_time > 0) {
				format(message, 191, "%L", LANG_SERVER, "PUBLIC_BAN_ANNOUNCE", g_PlayerData[id][PlayerName], ban_length, ban_reason);
				notify_ban_ex(true, message);
			} else {
				format(message, 191, "%L", LANG_SERVER, "PUBLIC_BAN_ANNOUNCE_PERM", g_PlayerData[id][PlayerName], ban_reason);
				notify_ban_ex(true, message);
			}
		}
	}
}

stock notify_ban_ex(bool:showAdmins, message[192])
{
	new players[32], num;
	get_players(players, num, "ch");
	for (new i = 0; i < num; i++) {
		if (showAdmins && get_user_state(players[i], PDATA_ADMIN)) {
			client_print_color(players[i], print_team_default, message);
		} else if(!showAdmins && !get_user_state(players[i], PDATA_ADMIN)) {
			client_print_color(players[i], print_team_default, message);
		}
	}
}

stock log_ban(id, admin_id)
{
	new ban_time, ban_length[64], ban_reason[128];

	ban_time = g_BanData[admin_id][BanTime];
	copy(ban_reason, 127, g_BanData[admin_id][BanReason]);

	if (ban_time > 0)  {
		get_time_length(id, ban_time, timeunit_minutes, ban_length, 63);
	}

	if (!ban_reason[0]) {
		format(ban_reason, 127, "%L", id, "NO_REASON");
	}

	log_amx("[BAN] Admin ^"%s^" banned ^"%s^" (authid %s) (ip %s) for %s (Reason ^"%s^")", g_PlayerData[admin_id][PlayerName], g_PlayerData[id][PlayerName], g_PlayerData[id][PlayerSteamid], g_PlayerData[id][PlayerIp], ban_length, ban_reason);
}

stock locate_player(const identifier[]) 
{
	new player = 0;

	if (identifier[0]=='#' && identifier[1]) {
		player = find_player("k", str_to_num(identifier[1]));
	}

	if (!player) {
		player = find_player("c", identifier);
	}

	if (!player) {
		player = find_player("bl", identifier);
	}
	
	if (!player) {
		player = find_player("d", identifier);
	}
	
	return player;
}

stock clear_ban(id)
{
	g_BanData[id][BanMenu] = -1;
	g_BanData[id][BanPage] = 0;
	
	g_BanData[id][BanPlayer] = 0;
	g_BanData[id][BanReason][0] = '^0';
	g_BanData[id][BanTime] = 0;
	g_BanData[id][BanType] = 0;
}