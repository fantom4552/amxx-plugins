#if defined _playerban_included
	#endinput
#endif

#define _playerban_included

#pragma reqlib PlayerBanCore

#if !defined AMXMODX_NOAUTOLOAD
	#pragma loadlib PlayerBanCore
#endif

// PlayerBanCore v1.8

forward playerban_banned(id, ban_time, ban_reason[]);