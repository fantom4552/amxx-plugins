#pragma semicolon 1

#include <amxmodx>
#include <amxmisc>
#include <engine>
#include <hamsandwich>
#include <fakemeta>
#include <fun>
#include <cstrike>

#define PLUGIN "Admin ESP"
#define VERSION "1.1"
#define AUTHOR "F@nt0M"

#define REQUIRED_ADMIN_LEVEL ADMIN_BAN

#define MAX_PLAYERS 32
#define ESP_TASK_ID 13896

new spec[MAX_PLAYERS+1];
new bool:ducking[MAX_PLAYERS+1];
new bool:first_person[MAX_PLAYERS+1];
new bool:connected[MAX_PLAYERS+1];
new bool:admin[MAX_PLAYERS+1];
new bool:enabled[MAX_PLAYERS+1];

new laser; // precached model

const m_afButtonPressed = 246;
const m_afButtonReleased = 247;

new Float:VecNull[3]={0.0,0.0,0.0};

public plugin_init()
{
	register_plugin(PLUGIN, VERSION, AUTHOR);

	register_event("StatusValue", "status_value", "bd", "1=2");
	register_event("TextMsg", "spec_mode", "b", "2&#Spec_Mode");
	RegisterHam(Ham_Player_Duck, "player", "player_duck");
	//RegisterHam(Ham_ObjectCaps, "player", "UseButton");
	set_task(0.3, "esp_timer", ESP_TASK_ID, _, _, "b");
	register_clcmd("esp_enable", "esp_enable");
}

public plugin_precache()
{
	laser = precache_model("sprites/laserbeam.spr");
}

public client_putinserver(id)
{
	connected[id] = true;
	if(get_user_flags(id) & REQUIRED_ADMIN_LEVEL)
	{
		admin[id] = true;
	}
	else
	{
		admin[id] = false;
	}

	enabled[id] = true;
}

public client_disconnect(id)
{
	connected[id] = false;
	admin[id] = false;
}

public esp_enable(id)
{
	if(connected[id] && admin[id])
	{
		enabled[id] = !enabled[id];
		if (enabled[id]) {
			client_print(id, print_console, "[Admin ESP] Enabled");
		} else {
			client_print(id, print_console, "[Admin ESP] Disabled");
		}
	}

	return PLUGIN_HANDLED;
}

public status_value(id)
{
	if (id>0)
	{
		new target = read_data(2);

		if(target != 0){
			spec[id] = target;
		}
	}

	return PLUGIN_CONTINUE;
}

public spec_mode(id)
{
	// discover if in first_person_view
	static specMode[12];
	read_data(2, specMode, 11);
	
	if(equal(specMode,"#Spec_Mode4"))
	{
		first_person[id] = true;
	}
	else
	{
		first_person[id] = false;
	}
}

public player_duck(id)
{
	if(!is_user_alive(id))
		return HAM_IGNORED;

	if(get_pdata_int(id, m_afButtonPressed, 5) & IN_DUCK)
	{
		ducking[id] = true;
	}
	else if(get_pdata_int(id, m_afButtonReleased, 5) & IN_DUCK)
	{
		ducking[id] = false;
	}

	return HAM_IGNORED;
} 

public esp_timer()
{
	static id, player, spec_id;

	static Float:spec_origin[3], Float:target_origin[3];
	
	static origins[MAX_PLAYERS+1][3];
	static names[MAX_PLAYERS+1][32];
	static CsTeams:team[MAX_PLAYERS+1];

	static Float:distance;
	static Float:v_middle[3], Float:offset_vector[3];
	static Float:v_hitpoint[3];

	static Float:v_bone_start[3], Float:v_bone_end[3];
	static Float:distance_to_hitpoint;
	static Float:vecLen;
	static Float:scaled_bone_len, Float:scaled_bone_width;

	static actual_bright;

	for(id = 1; id <= MAX_PLAYERS; id++)
	{
		if(!connected[id])
			continue;
			
		entity_get_vector(id, EV_VEC_origin, Float:origins[id]);
		team[id] = cs_get_user_team(id);
		get_user_name(id, names[id], 31);
	}

	for(id = 1; id <= MAX_PLAYERS; id++)
	{
		if(!admin[id] || !connected[id] || !enabled[id] || is_user_alive(id) || !first_person[id] || spec[id] == 0)
			continue;

		spec_id = spec[id];

		for(player = 1; player <= MAX_PLAYERS; player++)
		{
			if(spec_id == player)
				continue;

			if(!connected[id] || !is_user_alive(player))
				continue;
				
			if(team[spec_id] == team[player])
				continue;

			copyVec(Float:origins[spec_id], spec_origin);
			copyVec(Float:origins[player], target_origin);

			distance = vector_distance(Float:origins[spec_id], Float:origins[player]);

			// trace from me to target, getting hitpoint
			trace_line(-1, spec_origin, target_origin, v_hitpoint);
			
			// get distance from me to hitpoint (nearest wall)
			distance_to_hitpoint = vector_distance(spec_origin, v_hitpoint);

			if(distance_to_hitpoint == distance)
				continue;
			
			v_middle[0] = target_origin[0] - spec_origin[0];
			v_middle[1] = target_origin[1] - spec_origin[1];
			v_middle[2] = target_origin[2] - spec_origin[2];

			vecLen = vector_distance(v_middle, VecNull);
			
			// get the point 10.0 units away from wall
			offset_vector[0] = v_middle[0] / vecLen * (distance_to_hitpoint - 10.0);
			offset_vector[1] = v_middle[1] / vecLen * (distance_to_hitpoint - 10.0);
			offset_vector[2] = v_middle[2] / vecLen * (distance_to_hitpoint - 10.0);

			scaled_bone_len = distance_to_hitpoint / distance * 50.0;
			scaled_bone_width = distance_to_hitpoint / distance * 150.0;

			offset_vector[0] += spec_origin[0];
			offset_vector[1] += spec_origin[1];
			offset_vector[2] += spec_origin[2];

			if(ducking[id])
			{
				offset_vector[2] += 2.3;
			}
			else
			{
				offset_vector[2] += 17.5;
			}

			copyVec(offset_vector, v_bone_start);
			copyVec(offset_vector, v_bone_end);
			v_bone_end[2] -= scaled_bone_len;

			if((distance - distance_to_hitpoint) < 2040.0)
			{
				actual_bright = (255 - floatround((distance - distance_to_hitpoint) / 12.0));
			}
			else
			{
				actual_bright = 85;
			}	

			make_TE_BEAMPOINTS(id, v_bone_start, v_bone_end, floatround(scaled_bone_width), actual_bright);
		}
	}
}

stock copyVec(Float:Vec[3],Float:Ret[3])
{
	Ret[0]=Vec[0];
	Ret[1]=Vec[1];
	Ret[2]=Vec[2];
}

stock make_TE_BEAMPOINTS(id, Float:Vec1[3], Float:Vec2[3], width, brightness)
{
	message_begin(MSG_ONE_UNRELIABLE ,SVC_TEMPENTITY, {0,0,0}, id); //message begin
	write_byte(0);
	write_coord(floatround(Vec1[0])); // start position
	write_coord(floatround(Vec1[1]));
	write_coord(floatround(Vec1[2]));
	write_coord(floatround(Vec2[0])); // end position
	write_coord(floatround(Vec2[1]));
	write_coord(floatround(Vec2[2]));
	write_short(laser); // sprite index
	write_byte(3); // starting frame
	write_byte(0); // frame rate in 0.1's
	write_byte(3); // life in 0.1's
	write_byte(width); // line width in 0.1's
	write_byte(0); // noise amplitude in 0.01's
	write_byte(0);
	write_byte(255);
	write_byte(0);
	write_byte(brightness); // brightness)
	write_byte(0); // scroll speed in 0.1's
	message_end();
}