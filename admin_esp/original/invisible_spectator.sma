/* http://forums.alliedmods.net/showthread.php?p=551999#post551999
*
* Changelog
* added cvar amx_inv_dead_percent <value>
*
*/

#include <amxmodx>
#include <fakemeta>

#if AMXX_VERSION_NUM < 180
	#define charsmax(%1)	sizeof(%1) - 1
#endif

#define MAX_PLAYERS	32

#define DEAD_FLAG   (1<<0)

#define OFFSET_TEAM 	114

enum {
 	CS_TEAM_UNASSIGNED,
 	CS_TEAM_T,
 	CS_TEAM_CT,
 	CS_TEAM_SPECTATOR
}

new bool:g_roundend
new pcvar_percent
new g_invisible[MAX_PLAYERS+1][2]
new gmsgScoreAttrib, gmsgTeamInfo

public plugin_init() {
	register_plugin("Invisible Spectator", "0.2", "ConnorMcLeod")

	pcvar_percent = register_cvar("amx_inv_dead_percent", "40")

	register_clcmd("amx_spectate", "make_invis", ADMIN_RCON)

	gmsgScoreAttrib = get_user_msgid("ScoreAttrib")
	gmsgTeamInfo = get_user_msgid("TeamInfo")

	register_message( gmsgScoreAttrib, "msg_ScoreAttrib")
	register_message( gmsgTeamInfo, "msg_TeamInfo")

	register_event("HLTV", "eNewRound", "a", "1=0", "2=0")
	register_logevent("eRoundEnd", 2, "1=Round_End")
	register_event("ResetHUD", "eResetHUD", "be")
	register_event("DeathMsg", "eDeathMsg", "a")
}

public make_invis(id, level) {
	if( ~get_user_flags(id) & level )
		return PLUGIN_CONTINUE

	if(g_invisible[id][0])
	{
		client_print(id, print_console, "You're not invisible anymore")
		g_invisible[id][0] = 0
		return PLUGIN_HANDLED
	}
		
	if( is_user_alive(id) )
	{
		client_print(id, print_console, "You have to be dead first to be an invisible spectator !")
		return PLUGIN_HANDLED
	}

	g_invisible[id][0] = 1
	client_print(id, print_console, "You're now an invisible spectator")

	new team = get_pdata_int(id, OFFSET_TEAM)
	if( CS_TEAM_T <= team <= CS_TEAM_CT )
	{
		g_invisible[id][1] = team
		set_pdata_int(id, OFFSET_TEAM, CS_TEAM_SPECTATOR)
	}
	else
	{
		new players[MAX_PLAYERS], tnum, ctnum
		get_players(players, tnum, "e", "TERRORIST")
		get_players(players, ctnum, "e", "CT")
		g_invisible[id][1] = ctnum > tnum ? 1 : 2
	}

	send_ScoreAttrib(id, 0)

	new teamname[12]
	switch( g_invisible[id][1] )
	{
		case 1:formatex(teamname, charsmax(teamname), "TERRORIST")
		case 2:formatex(teamname, charsmax(teamname), "CT")
	}
	send_TeamInfo(id, teamname)

	return PLUGIN_HANDLED
}

public eDeathMsg() {
	if(g_roundend)
		return

	new players[MAX_PLAYERS], dead, inum, player, Float:percent = get_pcvar_float(pcvar_percent) / 100.0
	get_players(players, dead, "bh")
	get_players(players, inum, "h")

	if( float(dead) / float(inum) < percent) 
		return

	for(new i; i < inum; i++)
	{
		player = players[i]
		if( g_invisible[player][0] )
			send_ScoreAttrib(player, DEAD_FLAG)
	}
}

public eNewRound() {
	g_roundend = false
	new players[MAX_PLAYERS], inum, player
	get_players(players, inum)
	for(new i; i < inum; i++)
	{
		player = players[i]
		if( g_invisible[player][0] )
			send_ScoreAttrib(player, 0)
	}
}

public eRoundEnd() {
	g_roundend = true
	new players[MAX_PLAYERS], inum, player
	get_players(players, inum)
	for(new i; i < inum; i++)
	{
		player = players[i]
		if( g_invisible[player][0] )
			send_ScoreAttrib(player, DEAD_FLAG)
	}
}

public eResetHUD(id) {
	if( g_invisible[id][0] )
		g_invisible[id][0] = 0
}

// Doesn't seem to work so set flag to 0 at NewRound event.
public msg_ScoreAttrib(msg_type, msg_dest, target) {
	if(!g_invisible[get_msg_arg_int(1)][0])
		return PLUGIN_CONTINUE

	new flags = get_msg_arg_int(2)
	if(flags & DEAD_FLAG)
		set_msg_arg_int(2, 0, flags & ~DEAD_FLAG)

	return PLUGIN_CONTINUE 
}

public msg_TeamInfo(msg_type, msg_dest, target) {
	new id = get_msg_arg_int(1)
	if(!g_invisible[id][0])
		return PLUGIN_CONTINUE

	new teamname[12]
	get_msg_arg_string(2, teamname, charsmax(teamname))
	if( g_invisible[id][1] == CS_TEAM_T && strcmp(teamname, "TERRORIST") != 0 )
		set_msg_arg_string(2, "TERRORIST")
	else if( g_invisible[id][1] == CS_TEAM_CT && strcmp(teamname, "CT") != 0 )
		set_msg_arg_string(2, "CT")

	return PLUGIN_CONTINUE
}

send_ScoreAttrib(id, flags)
{
	message_begin(MSG_ALL, gmsgScoreAttrib, _, 0)
	write_byte(id)
	write_byte(flags)
	message_end()
}

send_TeamInfo(id, teamname[])
{
	message_begin(MSG_ALL, gmsgTeamInfo, _, 0)
	write_byte(id)
	write_string(teamname)
	message_end()
}