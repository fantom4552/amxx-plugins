#pragma semicolon 1

#define ENABLE_MYSQL
#define ENABLE_BACKUP

#define MAX_PLAYERS 32
#define USERS_FILENAME "users.ini"
#define BACKUP_FILENAME "adminload_bak.ini"

#include <amxmodx>

#if defined ENABLE_MYSQL
#include <sqlx>
#endif // ENABLE_MYSQL

#if AMXX_VERSION_NUM == 183
#define client_has_disconnect client_disconnected
#define create_new_cvar create_cvar
#else
#define client_has_disconnect client_disconnect
#define create_new_cvar register_cvar
#endif

#if !defined charsmax
#define charsmax(%1) (sizeof %1 - 1)
#endif

#define PLUGIN "Admin Load"
#define VERSION "2.5"
#define AUTHOR "F@nt0M"

// ------ BEGIN DEFINE FUNCTIONS ------

#define PDATA_DISCONNECTED 0
#define PDATA_CONNECTING 1
#define PDATA_CONNECTED 2
#define PDATA_ADMIN 3
#define PDATA_CASESENSITIVE 4
#define PDATA_STATICBANTIME 5
#define PDATA_BOT 6
#define PDATA_HLTV 7

#define set_user_state(%1,%2) (g_PlayersState[%1] = (1<<%2))
#define add_user_state(%1,%2) (g_PlayersState[%1] |= (1<<%2))
#define get_user_state(%1,%2) (g_PlayersState[%1] & (1<<%2))
#define remove_user_state(%1,%2) (g_PlayersState[%1] &= ~(1<<%2))

new g_PlayersState[MAX_PLAYERS+1];

// ------ END DEFINE FUNCTIONS ------

// ------ BEGIN GLOBAL VARS ------

new g_AdminNick[MAX_PLAYERS+1][32];

enum MFHANDLE_TYPES {
	Adminload_Sql_Initialized = 0,
	Adminload_Connect,
	Adminload_Disconnect,
	Adminload_ChangeName
};

new MFReturn;
new MFHandle[MFHANDLE_TYPES];

new g_ConfigsDir[128];
new g_DataDir[128];

#if defined ENABLE_MYSQL
new g_ServerAddr[32];

new Handle:g_DBTuple;
new g_DbPrefix[10];
#endif // ENABLE_MYSQL

new bool:g_AdminLoaded = false;
new bool:g_Md5 = true;

new Trie:g_AdminsAuthId;
new Trie:g_AdminsIp;
new Trie:g_AdminsNames;
new Trie:g_AdminsCaseNames;

new Array:g_AdminUseStaticBantime;
new Array:g_AdminExpired;
new Array:g_AdminNicks;

new g_PasswordField[32];
new g_AdminNum;
new g_DefaultAccess;

new g_OldFlags[MAX_PLAYERS+1];
new g_Expired[MAX_PLAYERS+1];

new amx_mode;

// ------ END GLOBAL VARS ------

// ------ BEGIN PLUGIN INIT ------
public plugin_init()
{
	register_plugin(PLUGIN, VERSION, AUTHOR);

	register_concmd("amx_reloadadmins", "cmdReload", ADMIN_CFG);

	#if defined ENABLE_MYSQL && defined ENABLE_BACKUP
	register_concmd("amx_backupdmins", "cmdBackup", ADMIN_CFG);
	#endif // ENABLE_MYSQL && ENABLE_BACKUP

	get_localinfo("amxx_configsdir", g_ConfigsDir, charsmax(g_ConfigsDir));
	get_localinfo("amxx_datadir", g_DataDir, charsmax(g_DataDir));

	g_AdminsAuthId = TrieCreate();
	g_AdminsIp = TrieCreate();
	g_AdminsNames = TrieCreate();
	g_AdminsCaseNames = TrieCreate();

	g_AdminUseStaticBantime = ArrayCreate(1, 32);
	g_AdminExpired = ArrayCreate(1, 32);
	g_AdminNicks = ArrayCreate(32, 32);

	create_new_cvar("adminload_server_address", "");

	amx_mode = create_new_cvar("amx_mode", "1");

	register_default_cvars();

	remove_user_flags(0, read_flags("z"));
}

public plugin_cfg()
{
	create_forwards();

	server_cmd("exec %s/amxx.cfg", g_ConfigsDir);
	server_exec();

	#if defined ENABLE_MYSQL
	server_cmd("exec %s/sql.cfg", g_ConfigsDir);
	server_exec();
	#endif // ENABLE_MYSQL
	
	set_task(0.25, "delayed_plugin_cfg");
}

public delayed_plugin_cfg()
{
	get_cvar_string("amx_password_field", g_PasswordField, 31);

	new default_access[25];
	get_cvar_string("amx_default_access", default_access, 24);

	g_DefaultAccess = read_flags(default_access);

	#if defined ENABLE_MYSQL
	get_cvar_string("amx_sql_prefix", g_DbPrefix, 31);
	get_cvar_string("adminload_server_address", g_ServerAddr, 31);

	if (strlen(g_ServerAddr) < 9) {
		get_user_ip(0, g_ServerAddr, 31);
	}

	log_amx("[AdminLoad] Server address is ^"%s^"", g_ServerAddr);

	db_init();
	#else // ENABLE_MYSQL
	file_load(USERS_FILENAME);
	g_Md5 = false;
	#endif // ENABLE_MYSQL
}

public plugin_end() 
{
	#if defined ENABLE_MYSQL
	if (g_DBTuple != Empty_Handle) {
		SQL_FreeHandle(g_DBTuple);
	}
	#endif // ENABLE_MYSQL

	g_AdminLoaded = false;
	g_Md5 = true;
	g_AdminNum = 0;

	TrieDestroy(g_AdminsAuthId);
	TrieDestroy(g_AdminsIp);
	TrieDestroy(g_AdminsNames);
	TrieDestroy(g_AdminsCaseNames);
	
	ArrayDestroy(g_AdminUseStaticBantime);
	ArrayDestroy(g_AdminExpired);
	ArrayDestroy(g_AdminNicks);
	
	return PLUGIN_CONTINUE;
}

public plugin_natives() 
{
	register_library("AdminLoadCore");

	register_native("adminload_get_db_prefix", "native_get_prefix");
	register_native("adminload_get_admin_nick", "native_get_nick");
	register_native("adminload_get_static_bantime", "native_static_bantime");
	register_native("adminload_get_expired", "native_get_expired");
}

create_forwards() 
{
	#if defined ENABLE_MYSQL
	MFHandle[Adminload_Sql_Initialized] = CreateMultiForward("adminload_sql_initialized", ET_IGNORE, FP_CELL, FP_STRING);
	#endif // ENABLE_MYSQL
	MFHandle[Adminload_Connect] = CreateMultiForward("adminload_connect", ET_IGNORE, FP_CELL, FP_CELL);
	MFHandle[Adminload_Disconnect] = CreateMultiForward("adminload_disconnect", ET_IGNORE, FP_CELL);
	MFHandle[Adminload_ChangeName] = CreateMultiForward("adminload_changename", ET_IGNORE, FP_CELL, FP_STRING, FP_CELL);
}

// ------ END PLUGIN INIT ------

// ------ BEGIN SERVER COMMANDS ------

public cmdReload(id, level, cid)
{
	if (!(get_user_flags(id) && level)) {
		console_print(id, "%L", id, "NO_ACC_COM");
		return PLUGIN_HANDLED;
	}

	remove_user_flags(0, read_flags("z"));
   
	#if defined ENABLE_MYSQL
	g_Md5 = true;
	set_task(0.5, "db_load");
	#else // ENABLE_MYSQL
	file_load(USERS_FILENAME);
	g_Md5 = false;
	#endif // ENABLE_MYSQL

	return PLUGIN_HANDLED;
}

#if defined ENABLE_MYSQL && defined ENABLE_BACKUP
public cmdBackup(id, level, cid)
{
	if (!(get_user_flags(id) && level)) {
		console_print(id, "%L", id, "NO_ACC_COM");
		return PLUGIN_HANDLED;
	}

	backup_admins();

	return PLUGIN_HANDLED;
}
#endif

// ------ END SERVER COMMANDS ------

// ------ BEGIN SQL ------
#if defined ENABLE_MYSQL
public db_init()
{
	new host[64], user[32], pass[32], db[128];
	new get_type[12], set_type[12];
	
	get_cvar_string("amx_sql_host", host, 63);
	get_cvar_string("amx_sql_user", user, 31);
	get_cvar_string("amx_sql_pass", pass, 31);
	get_cvar_string("amx_sql_type", set_type, 11);
	get_cvar_string("amx_sql_db", db, 127);
	
	SQL_GetAffinity(get_type, 12);
	
	if (!equali(get_type, set_type)) {
		if (!SQL_SetAffinity(set_type)) {
			log_amx("Failed to set affinity from %s to %s.", get_type, set_type);
		}
	}
	
	g_DBTuple = SQL_MakeDbTuple(host, user, pass, db, 0);

	#if AMXX_VERSION_NUM == 183
	SQL_SetCharset(g_DBTuple, "utf8");
	#endif // AMXX_VERSION_NUM
	
	new error[128], errno;

	new Handle:g_Sql = SQL_Connect(g_DBTuple, errno, error, 127);
	
	if (g_Sql == Empty_Handle) {
		log_amx("[AdminLoad] SQL error: can't connect: '%s'", error);
		log_amx("[AdminLoad] SQL error: connect arguments '%s', '%s', '%s', '%s', '%s'", host, user, pass, db, set_type);
		#if defined ENABLE_BACKUP
		g_Md5 = true;
		file_load(BACKUP_FILENAME);
		#else // ENABLE_BACKUP
		g_Md5 = false;
		file_load(USERS_FILENAME);
		#endif // ENABLE_BACKUP
	} else {
		SQL_FreeHandle(g_Sql);
		log_amx("[AdminLoad] Connected to MySQL server success");
		set_task(0.5, "db_load");
	}
}

public db_load()
{
	new pquery[512];

	format(pquery, 511, "SELECT aa.`steamid`, aa.`password`, aa.`access`, ads.`custom_flags`, aa.`flags`, aa.`nickname`, ^n\
						IF(ads.`use_static_bantime` = 'yes', 1, 0) `use_static_bantime`, aa.`expired` ^n\
						FROM `PREFIX_amxadmins` aa ^n\
						JOIN `PREFIX_admins_servers` ads ON aa.id = ads.admin_id ^n\
						JOIN `PREFIX_serverinfo` si ON ads.server_id = si.id ^n\
						WHERE (aa.`days` = '0' OR aa.`expired` > UNIX_TIMESTAMP(NOW()))	AND	si.`address` = '%s'", g_ServerAddr);

	replace_all(pquery, 511, "PREFIX", g_DbPrefix);
	
	// log_amx("Query: ^"%s^"", pquery);
	SQL_ThreadQuery(g_DBTuple, "db_load_post", pquery);
}

public db_load_post(failstate, Handle:query, const error[], errornum, const data[], size, Float:queuetime)
{
	if (failstate) {
		SQL_Error(query, error, errornum, failstate);
		return;
	}
	
	flush_admins();

	if (SQL_NumResults(query)) {
		new qcolAuth = SQL_FieldNameToNum(query, "steamid");
		new qcolPass = SQL_FieldNameToNum(query, "password");
		new qcolAccess = SQL_FieldNameToNum(query, "access");
		new qcolCustomFlags = SQL_FieldNameToNum(query, "custom_flags");
		new qcolFlags = SQL_FieldNameToNum(query, "flags");
		new qcolNick = SQL_FieldNameToNum(query, "nickname");
		new qcolStatic = SQL_FieldNameToNum(query, "use_static_bantime");
		new qcolExpired = SQL_FieldNameToNum(query, "expired");

		new AuthData[44];
		new Password[34];
		new Access[25];
		new CustomFlags[25];
		new Flags[10];
		new Nick[32];
		new Static;
		new Expired;
	   
		while (SQL_MoreResults(query)) {
			SQL_ReadResult(query, qcolAuth, AuthData, 43);
			SQL_ReadResult(query, qcolPass, Password, 33);
			SQL_ReadResult(query, qcolAccess, Access, 24);
			SQL_ReadResult(query, qcolCustomFlags, CustomFlags, 24);
			SQL_ReadResult(query, qcolNick, Nick, 31);
			SQL_ReadResult(query, qcolFlags, Flags, 9);
			Static = SQL_ReadResult(query, qcolStatic);
			Expired = SQL_ReadResult(query, qcolExpired);

			if (!equal(CustomFlags, "")) {
				copy(Access, 24, CustomFlags);
			}

			push_admin(AuthData, Password, Access, Flags, Nick, Static, Expired);

			SQL_NextRow(query);
		}
	}

	SQL_FreeHandle(query);

	log_amx("[AdminLoad] Loaded %d admins from database", g_AdminNum);

	g_Md5 = true;
	
	checkPlayers();
	
	if (!g_AdminLoaded) {
		g_AdminLoaded = true;

		ExecuteForward(MFHandle[Adminload_Sql_Initialized], MFReturn, g_DBTuple, g_DbPrefix);
	}

	#if defined ENABLE_BACKUP
	if(get_backup_num() != admins_num()) {
		backup_admins();
	}
	#endif // ENABLE_BACKUP

	return;
}
#endif
// ------ END SQL ------

// ------ BEGIN FILE ------
file_load(configName[])
{
	new Now = get_systime(0);
	flush_admins();

	new Filename[64];
	format(Filename, 63, "%s/%s", g_ConfigsDir, configName);
	new File = fopen(Filename, "r");
	
	if (File) {
		new Text[512];

		new AuthData[44];
		new Password[34];
		new Access[25];
		new Flags[10];
		new Nick[32];
		new Static[2];
		new ExpiredStr[32];
		new Expired;
		
		while (!feof(File)) {
			fgets(File, Text, 511);
			
			trim(Text);
			
			if (!Text[0] || Text[0]==';') {
				continue;
			}
			
			AuthData[0] = '^0';
			Password[0] = '^0';
			Access[0] = '^0';
			Flags[0] = '^0';
			Nick[0] = '^0';
			Static[0] = '^0';
			ExpiredStr[0] = '^0';
			
			if (parse(Text, AuthData, 43, Password, 33, Access, 24, Flags, 9, Nick, 31, Static, 1, ExpiredStr, 31) < 2) {
				continue;
			}

			Expired = str_to_num(ExpiredStr);

			if (Expired > 0 && Expired < Now) {
				continue;
			}

			push_admin(AuthData, Password, Access, Flags, Nick, str_to_num(Static), Expired);
		}
		
		fclose(File);
	}

	log_amx("[AdminLoad] Loaded %d admins from file", g_AdminNum);

	checkPlayers();

	g_AdminLoaded = true;
}
// ------ END SQL ------

// ------ BEGIN CLIENT ------

public client_authorized(id)
{
	set_user_state(id, PDATA_CONNECTING);
	g_Expired[id] = 0;
	g_OldFlags[id] = 0;

	if (g_AdminLoaded && get_pcvar_num(amx_mode)) {
		checkAccess(id);
	} else {
		removeAccess(id);
	}
}

public client_putinserver(id)
{
	remove_user_state(id, PDATA_CONNECTING);
	
	if (is_user_hltv(id)) {
		add_user_state(id, PDATA_HLTV);
	}
	
	if (is_user_bot(id)) {	
		add_user_state(id, PDATA_BOT);
	}

	add_user_state(id, PDATA_CONNECTED);
}

public client_has_disconnect(id) 
{
	if (get_user_state(id, PDATA_ADMIN)) {
		ExecuteForward(MFHandle[Adminload_Disconnect], MFReturn, id);
		log_admin_logout(id);
	}

	set_user_state(id, PDATA_DISCONNECTED);
	set_user_flags(id, g_DefaultAccess);
	g_Expired[id] = 0;
	g_OldFlags[id] = 0;
}

public client_infochanged(id)
{
	if (!g_AdminLoaded || !get_user_state(id, PDATA_CONNECTED) || !get_pcvar_num(amx_mode)) {
		return PLUGIN_CONTINUE;
	}

	new newname[32], oldname[32];
   
	get_user_name(id, oldname, 31);
	get_user_info(id, "name", newname, 31);

	if (!get_user_state(id, PDATA_CASESENSITIVE)) {
		if (!equal(newname, oldname)) {
			checkAccess(id, newname);
			ExecuteForward(MFHandle[Adminload_ChangeName], MFReturn, id, newname, get_user_flags(id));
		}
	} else {
		if (!equali(newname, oldname)) {
			checkAccess(id, newname);
			ExecuteForward(MFHandle[Adminload_ChangeName], MFReturn, id, newname, get_user_flags(id));
		}
	}
	
	return PLUGIN_CONTINUE;
}

// ------ END CLIENT ------

// ------ BEGIN PRIATE FUNCTIONS ------

flush_admins()
{
	admins_flush();

	TrieClear(g_AdminsAuthId);
	TrieClear(g_AdminsIp);
	TrieClear(g_AdminsNames);
	TrieClear(g_AdminsCaseNames);

	ArrayClear(g_AdminExpired);
	ArrayClear(g_AdminUseStaticBantime);
	ArrayClear(g_AdminNicks);

	g_AdminNum = 0;
}

push_admin(AuthData[44], Password[34], Access[25], Flags[10], Nick[32], Static, Expired)
{
	// server_print("Push: ^"%s^" ^"%s^" ^"%s^" ^"%s^" ^"%s^" ^"%d^" ^"%d^"", AuthData, Password, Access, Flags, Nick, Static, Expired);
	new AccessFlags = read_flags(Flags);
	admins_push(AuthData, Password, read_flags(Access), AccessFlags);

	ArrayPushCell(g_AdminUseStaticBantime, Static);
	ArrayPushCell(g_AdminExpired, Expired);
	ArrayPushString(g_AdminNicks, Nick);

	if (AccessFlags & FLAG_AUTHID) {
		TrieSetCell(g_AdminsAuthId, AuthData, g_AdminNum);
	} else if (AccessFlags & FLAG_IP) {
		TrieSetCell(g_AdminsIp, AuthData, g_AdminNum);
	} else {
		if (AccessFlags & FLAG_CASE_SENSITIVE) {
			strtolower(AuthData);
			if (AccessFlags & FLAG_TAG) {
				// Disable Clan Tag
			} else {
				TrieSetCell(g_AdminsCaseNames, AuthData, g_AdminNum);
			}
		} else {
			if(AccessFlags & FLAG_TAG) {
				// Disable Clan Tag
			} else {
				TrieSetCell(g_AdminsNames, AuthData, g_AdminNum);
			}
		}
	}

	g_AdminNum++;
}

checkPlayers()
{
	new mode = get_pcvar_num(amx_mode);
	for (new id = 1; id <= MAX_PLAYERS; id++) {
		if (get_user_state(id, PDATA_CONNECTING) || get_user_state(id, PDATA_CONNECTED)) {
			if (mode) {
				checkAccess(id);
			} else {
				removeAccess(id);
			}
		}
	}
}

checkAccess(id, const name[] = "")
{
	remove_user_flags(id);
	
	new userauthid[32], userip[32], username[32], usercasename[32];
	new admin_id, bool:case_sensitive, bool:static_bantime, expired;
	
	get_user_authid(id, userauthid, 31);
	get_user_ip(id, userip, 31, 1);
	
	if (name[0]) {
		copy(username, 31, name);
	} else {
		get_user_name(id, username, 31);
	}

	copy(usercasename, 31, username);
	strtolower(usercasename);
	
	new infopassword[40];
	get_user_info(id, g_PasswordField, infopassword, 39);

	new Flags;
	new Access;
	new AuthData[44];
	new Password[44];
	new MD5Password[34];
	
	admin_id = -1;
	case_sensitive = false;

	if (TrieKeyExists(g_AdminsAuthId, userauthid)) {
		TrieGetCell(g_AdminsAuthId, userauthid, admin_id);
	} else if (TrieKeyExists(g_AdminsIp, userip)) {
		TrieGetCell(g_AdminsIp, userip, admin_id);
	} else if (TrieKeyExists(g_AdminsNames, username)) {
		TrieGetCell(g_AdminsNames, username, admin_id);
	} else if (TrieKeyExists(g_AdminsCaseNames, usercasename)) {
		TrieGetCell(g_AdminsCaseNames, usercasename, admin_id);
		case_sensitive = true;
	}
	
	if (admin_id != -1) {
		static_bantime = ArrayGetCell(g_AdminUseStaticBantime, admin_id) ? true : false;
		expired = ArrayGetCell(g_AdminExpired, admin_id);

		ArrayGetString(g_AdminNicks, admin_id, g_AdminNick[id], 31);
		
		Flags = admins_lookup(admin_id, AdminProp_Flags);
		admins_lookup(admin_id, AdminProp_Auth, AuthData, 43);
		Access = admins_lookup(admin_id, AdminProp_Access);
		
		if (Flags & FLAG_NOPASS) {
			setAccess(id, username, userauthid, userip, AuthData, Access, static_bantime, case_sensitive, expired);
		} else {
			admins_lookup(admin_id, AdminProp_Password, Password, 43);
			Password[32] = 0;

			if (g_Md5) {
				#if AMXX_VERSION_NUM == 183
				hash_string(infopassword, Hash_Md5, MD5Password, 33);
				#else
				md5(infopassword, MD5Password);
				#endif
			} else {
				copy(MD5Password, 31, infopassword);
			}

			if (equal(MD5Password, Password)) {
				setAccess(id, username, userauthid, userip, AuthData, Access, static_bantime, case_sensitive, expired);
			} else if (Flags & FLAG_KICK) {
				client_cmd(id, "echo ^"* Invalid Password!^"");
				kickPlayer(id);
				log_amx("Login: <%s><%s><%s> kicked due to invalid password (account ^"%s^")", username, userauthid, userip, AuthData);
			} else {
				removeAccess(id);
			}
		}
	} else {
		if (get_pcvar_num(amx_mode) == 2) {
			kickPlayer(id);
		} else {
			removeAccess(id);
		}
	}
}

setAccess(id, username[32], userauthid[32], userip[32], AuthData[44], flags, bool:static_bantime, bool:case_sensitive, expired)
{
	set_user_flags(id, flags);

	if (static_bantime) {
		add_user_state(id, PDATA_STATICBANTIME);
	}

	if (case_sensitive) {
		add_user_state(id, PDATA_CASESENSITIVE);
	}

	g_Expired[id] = expired;

	if (!get_user_state(id, PDATA_ADMIN)) {
		add_user_state(id, PDATA_ADMIN);
		ExecuteForward(MFHandle[Adminload_Connect], MFReturn, id, flags);
	} else if (flags != g_OldFlags[id]) {
		ExecuteForward(MFHandle[Adminload_Disconnect], MFReturn, id);
		ExecuteForward(MFHandle[Adminload_Connect], MFReturn, id, flags);
	}

	g_OldFlags[id] = flags;

	new access[32];
	get_flags(flags, access, 31);
	client_print(id, print_console, "* Privileges set");
	
	new sflags[32];
	get_flags(flags, sflags, 31);

	log_amx("Login: <%s><%s><%s> became an admin (account ^"%s^") (access ^"%s^") (nick ^"%s^") (static %s) (expired %d)", \
		username, userauthid, userip, AuthData, sflags, g_AdminNick[id], (static_bantime ? "yes" : "no"), expired);
}

removeAccess(id)
{
	if (get_user_state(id, PDATA_ADMIN)) {
		ExecuteForward(MFHandle[Adminload_Disconnect], MFReturn, id);
		remove_user_state(id, PDATA_ADMIN);
	}

	set_user_flags(id, g_DefaultAccess);
	new access[32];
	get_flags(g_DefaultAccess, access, 31);
	client_print(id, print_console, "* Privileges set");
}

kickPlayer(id)
{
	server_cmd("kick #%d  ^"You have no entry to the server...^"", get_user_userid(id));
}

#if defined ENABLE_MYSQL && defined ENABLE_BACKUP
get_backup_num()
{
	new Filename[64], Text[32];
	format(Filename, 63, "%s/%s", g_DataDir, BACKUP_FILENAME);

	new File = fopen(Filename, "r");
	if (!File) {
		return 0;
	}

	fgets(File, Text, 31);

	fclose(File);

	if (Text[0] != ';' && Text[1] != '#') {
		return 0;
	}

	return str_to_num(Text[2]);
}

backup_admins()
{
	new AuthData[44];
	new Password[44];
	new Access[25];
	new Flags[10];
	new Nick[32];
	new Static;
	new Expired;

	new admin_id;
	new Count = admins_num();

	new Filename[64];
	format(Filename, 63, "%s/%s", g_DataDir, BACKUP_FILENAME);

	new File = fopen(Filename, "w");
	
	if (File) {

		fprintf(File, ";#%d^n", Count);

		for (admin_id = 0; admin_id < Count; admin_id++) {
			AuthData[0] = 0;
			Password[0] = 0;
			Access[0] = 0;
			Flags[0] = 0;
			Nick[0] = 0;
			Static = 0;
			Expired = 0;

			admins_lookup(admin_id, AdminProp_Auth, AuthData, 43);
			admins_lookup(admin_id, AdminProp_Password, Password, 43);
			Password[32] = 0;
			
			get_flags(admins_lookup(admin_id, AdminProp_Access), Access, 24);
			get_flags(admins_lookup(admin_id, AdminProp_Flags), Flags, 9);

			ArrayGetString(g_AdminNicks, admin_id, Nick, 31);

			Static = ArrayGetCell(g_AdminUseStaticBantime, admin_id);
			Expired = ArrayGetCell(g_AdminExpired, admin_id);

			fprintf(File, "^"%s^" ^"%s^" ^"%s^" ^"%s^" ^"%s^" ^"%d^" ^"%d^"^n", AuthData, Password, Access, Flags, Nick, Static, Expired);
		}

		fclose(File);

		log_amx("[AdminLoad] Backup %d admins to file %s", Count, Filename);
	}
}
#endif // ENABLE_MYSQL && ENABLE_BACKUP

// ------ END PRIATE FUNCTIONS ------

// ------ BEGIN STOCK FUNCTIONS ------
#if defined ENABLE_MYSQL
stock SQL_Error(Handle:query, const error[], errornum, failstate)
{
	new qstring[1024];
	SQL_GetQueryString(query, qstring, 1023);
	
	if (failstate == TQUERY_CONNECT_FAILED) {
		log_amx("[SQLError] Connection failed!");
	} else if (failstate == TQUERY_QUERY_FAILED) {
		log_amx("[SQLError] Query failed!");
	}

	log_amx("[SQLError] Message: %s (%d)", error, errornum);
	log_amx("[SQLError] Query statement: %s", qstring);

	return SQL_FreeHandle(query);
}
#endif // ENABLE_MYSQL

stock log_admin_logout(id)
{
	new sflags[32], username[32], userauthid[32], userip[32];
	get_flags(get_user_flags(id), sflags, 31);
	get_user_authid(id, userauthid, 31);
	get_user_ip(id, userip, 31, 1);
	get_user_name(id, username, 31);

	log_amx("Logout: <%s><%s><%s> exit admin (access ^"%s^") (nick ^"%s^")", username, userauthid, userip, sflags, g_AdminNick[id]);
}

stock register_default_cvars()
{
	create_new_cvar("amx_password_field", "_pw");
	create_new_cvar("amx_default_access", "z");

	create_new_cvar("amx_vote_ratio", "0.02");
	create_new_cvar("amx_vote_time", "10");
	create_new_cvar("amx_vote_answers", "1");
	create_new_cvar("amx_vote_delay", "60");
	create_new_cvar("amx_last_voting", "0");
	create_new_cvar("amx_show_activity", "2");
	create_new_cvar("amx_votekick_ratio", "0.40");
	create_new_cvar("amx_voteban_ratio", "0.40");
	create_new_cvar("amx_votemap_ratio", "0.40");

	set_cvar_float("amx_last_voting", 0.0);

	create_new_cvar("amx_sql_host", "127.0.0.1");
	create_new_cvar("amx_sql_user", "root");
	create_new_cvar("amx_sql_pass", "");
	create_new_cvar("amx_sql_db", "amx");
	create_new_cvar("amx_sql_type", "mysql");
	create_new_cvar("amx_sql_prefix", "amx");
}
// ------ END STOCK FUNCTIONS ------

// ------ BEGIN NATIVE FUNCTIONS ------

public native_get_prefix() 
{
	#if defined ENABLE_MYSQL
	return set_array(1, g_DbPrefix, get_param(2));
	#else // ENABLE_MYSQL
	return 0;
	#endif // ENABLE_MYSQL
}

public native_get_nick() 
{
	return set_array(2, g_AdminNick[get_param(1)], get_param(3));
}

public native_static_bantime() 
{
	return get_user_state(get_param(1), PDATA_STATICBANTIME);
}

public native_get_expired() 
{
	new id = get_param(1);
	return (0 < id <= MAX_PLAYERS) ? g_Expired[id] : 0;
}

// ------ END NATIVE FUNCTIONS ------