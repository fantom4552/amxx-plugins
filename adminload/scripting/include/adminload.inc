#if defined _adminload_included
	#endinput
#endif

#define _adminload_included

#pragma reqlib AdminLoadCore

#if !defined AMXMODX_NOAUTOLOAD
	#pragma loadlib AdminLoadCore
#endif

// AdminLoad v2.2

// get db prefix
// standalone native, see forward amxbans_sql_initialized
native adminload_get_db_prefix(output[], len = 32);

// get admin nick from web
native adminload_get_admin_nick(id, output[], len = 32);

// returns 1 if admin has to use static bantime
// param id: the player id
native adminload_get_static_bantime(id);

// returns expired time
// param id: the player id
native adminload_get_expired(id);

// executed if the db is initialized
// value sqlTuble: the db info tuble Handle which you can use to connect, don�t free it!!
// value dbPrefix: the db Prefix
forward adminload_sql_initialized(Handle:sqlTuple, const dbPrefix[]);

// executed if a player gets admin
// value id: the player id
forward adminload_connect(id, flags);

// executed if a admin disconnects
// value id: the player id
forward adminload_disconnect(id);

forward adminload_changename(id, name[]);