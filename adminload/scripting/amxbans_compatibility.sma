#pragma semicolon 1

#include <amxmodx>
#include <adminload>

#define PLUGIN "AmxBans Compatibility"
#define VERSION "0.1"
#define AUTHOR "F@nt0M"

#define DISABLE_STANDART_PLUGINS
#define MAX_OTUPUT_SIZE 128

enum MFHANDLE_TYPES {
	Amxbans_Sql_Initialized = 0,
	Amxbans_Connect,
	Amxbans_Disconnect
};

new MFHandle[MFHANDLE_TYPES];

public plugin_init()
{
	register_plugin(PLUGIN, VERSION, AUTHOR);
}

public plugin_cfg()
{
	#if defined DISABLE_STANDART_PLUGINS
	if (find_plugin_byfile("admin.amxx") != INVALID_PLUGIN_ID) {
		log_amx("[AdminLoadCompatibility] Plugin admin.amxx stopped!");
		pause("acd", "admin.amxx");
	}

	if (find_plugin_byfile("admin_sql.amxx") != INVALID_PLUGIN_ID) {
		log_amx("[AdminLoadCompatibility] Plugin admin_sql.amxx stopped!");
		pause("acd", "admin_sql.amxx");
	}
	#endif

	create_forwards();
}

public plugin_natives() 
{
	register_library("AMXBansCore");

	register_native("amxbans_get_db_prefix", "native_get_db_prefix");
	register_native("amxbans_get_admin_nick", "native_get_nick");
	register_native("amxbans_get_static_bantime", "native_static_bantime");

	return PLUGIN_CONTINUE;
}

create_forwards() 
{
	MFHandle[Amxbans_Sql_Initialized] = CreateMultiForward("amxbans_sql_initialized", ET_IGNORE, FP_CELL, FP_STRING);
	MFHandle[Amxbans_Connect] = CreateMultiForward("amxbans_admin_connect", ET_IGNORE, FP_CELL);
	MFHandle[Amxbans_Disconnect] = CreateMultiForward("amxbans_admin_disconnect", ET_IGNORE, FP_CELL);
}

public adminload_sql_initialized(Handle:sqlTuple, const dbPrefix[])
{
	new ret;
	ExecuteForward(MFHandle[Amxbans_Sql_Initialized], ret, sqlTuple, dbPrefix);
}

public adminload_connect(id, flags)
{
	new ret;
	ExecuteForward(MFHandle[Amxbans_Connect], ret, id);
}

public adminload_disconnect(id)
{
	new ret;
	ExecuteForward(MFHandle[Amxbans_Disconnect], ret, id);
}

public native_get_db_prefix() 
{
	new output[MAX_OTUPUT_SIZE];
	new len = get_param(2);
	if (len > MAX_OTUPUT_SIZE) {
		len = MAX_OTUPUT_SIZE - 1;
	}
	adminload_get_db_prefix(output, len);
	return set_array(1, output, len);
}

public native_get_nick() 
{
	new output[MAX_OTUPUT_SIZE];
	new len = get_param(3);
	if (len > MAX_OTUPUT_SIZE) {
		len = MAX_OTUPUT_SIZE - 1;
	}
	adminload_get_admin_nick(get_param(1), output, len);
	return set_array(2, output, len);
}

public native_static_bantime() 
{
	return adminload_get_static_bantime(get_param(1));
}