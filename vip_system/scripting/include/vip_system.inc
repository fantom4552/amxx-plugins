#if defined _vip_ststem_included
	#endinput
#endif

#define _vip_ststem_included

#pragma reqlib VipSystem

#if !defined AMXMODX_NOAUTOLOAD
	#pragma loadlib VipSystem
#endif

// get vip status
// @param id - Player ID
// @return 1/0
native get_vip(id);

// set vip status
// @param id - Player ID
// @param status - 1/0
// @return 1/0
native set_vip(id, status);