#pragma semicolon 1

#define ADMINLOAD
#define AMX_WARMING
#define ENABLE_WEAPON_MODEL
#define ENABLE_VAMPIRE
#define ENABLE_PARACHUTE
// #define DROP_SHIELD

// #define BRAMMO_ONLY_VIP_WEAPONS
#define VAMPIRE_MAX_HP 100.0

#include <amxmodx>
#include <cstrike>
#include <engine>
#include <fakemeta>
#include <hamsandwich>

#if (AMXX_VERSION_NUM < 183) || defined NO_NATIVE_COLORCHAT
	#include <colorchat>
#else
	#define DontChange print_team_default
#endif

#if AMXX_VERSION_NUM == 183
	#define client_has_disconnect client_disconnected
	#define create_new_cvar create_cvar
#else
	#define client_has_disconnect client_disconnect
	#define create_new_cvar register_cvar
#endif

#if defined ADMINLOAD
#include <adminload>
#endif

#if defined AMX_WARMING
#include <amx_warming>
#endif

#define PLUGIN "VIP System"
#define VERSION "4.1"
#define AUTHOR "F@nt0M"

#define BONUS_DEAGLE 		(1<<0)  /* flag "a" */
#define BONUS_ARMOR			(1<<1)  /* flag "b" */
#define BONUS_DEFUSE		(1<<2)  /* flag "c" */
#define BONUS_HEGRENADE		(1<<3)  /* flag "d" */
#define BONUS_FLASHBANG		(1<<4)  /* flag "e" */
#define BONUS_SMOKEGRENADE	(1<<5)  /* flag "f" */
#define BONUS_WEAPON		(1<<6)  /* flag "g" */
#define BONUS_VAMPIRE		(1<<7)  /* flag "h" */
#define BONUS_PARACHUTE		(1<<8)  /* flag "i" */
#define BONUS_WEAPON_MODEL	(1<<9)  /* flag "j" */

#define MAX_PLAYERS 32
#define MAX_WEAPONS 30

#define CheckBit(%1,%2)		(%1 &	(1 << (%2 & 31)))
#define SetBit(%1,%2)		(%1 |=	(1 << (%2 & 31)))
#define ClearBit(%1,%2)		(%1 &= ~(1 << (%2 & 31)))
#define ResetBit(%1)		(%1 = 0)

const PRIMARY_WEAPONS_BIT_SUM = (1 << CSW_SCOUT) | (1<< CSW_XM1014) | (1 << CSW_M3) | (1<< CSW_MAC10)|(1<<CSW_AUG)|(1<<CSW_UMP45)|(1<<CSW_SG550)|(1<<CSW_GALIL)|(1<<CSW_FAMAS)|(1<<CSW_AWP)|(1<<CSW_MP5NAVY)|(1<<CSW_M249)|(1<<CSW_M4A1)|(1<<CSW_TMP)|(1<<CSW_G3SG1)|(1<<CSW_SG552)|(1<<CSW_AK47)|(1<<CSW_P90);
const SECONDARY_WEAPONS_BIT_SUM = (1<<CSW_P228)|(1<<CSW_DEAGLE)|(1<<CSW_ELITE)|(1<<CSW_FIVESEVEN)|(1<<CSW_USP)|(1<<CSW_GLOCK18);

new const g_WeaponsBrammo[] = {
	0, 52, 0, 90, 0, 32, 0, 100, 90, 0, 120, 100, 100, 90, 90, 90, 100, 120, 30, 120, 200, 32, 90, 120, 90, 0, 35, 90, 90, 0, 100
};

new g_VipFlag;
new g_Bonuses;

new g_RoundMin;

new pcvar_Enable;
new pcvar_Bonus;
new pcvar_Flags;

enum _:BonusFlags {
	BonusFlagDeagle,
	BonusFlagArmor,
	BonusFlagDefuse,
	BonusFlagHegrenade,
	BonusFlagFlashbang,
	BonusFlagSmokegrenade,
	BonusFlagWeapon,
	BonusFlagVampire,
	BonusFlagParachute,
	BonusFlagWeaponModel
};

new g_BonusFlags[BonusFlags];

#if defined BRAMMO_ONLY_VIP_WEAPONS
new g_WeaponsFwd;
#endif

#if defined ENABLE_VAMPIRE
new pcvar_VampireHp;
new pcvar_VampireHs;
#endif

#if defined ENABLE_PARACHUTE
new HamHook:g_ParachuteHook;
#endif

new g_ConfigDir[128];

new bool:g_StartGame = false;
new bool:g_DefuseInMap = false;
new g_RoundNum = 0;

new g_WeaponUsed;
#if defined BRAMMO_ONLY_VIP_WEAPONS
new g_WeaponUsedWid[MAX_PLAYERS+1];
#endif

new g_MsgScoreAttrib;
new g_VipMenu;

#if defined BRAMMO_ONLY_VIP_WEAPONS
new g_max_clients, g_max_entities;
#endif

#if defined ENABLE_WEAPON_MODEL
new g_IsModelShow;
new g_IsModelEnable;
enum _:WeaponModel {
	v_Model[32],
	p_Model[32]
};
new g_WeaponsModels[MAX_WEAPONS+1][WeaponModel];
#endif

public plugin_init()
{
	register_plugin(PLUGIN, VERSION, AUTHOR);

	register_dictionary("vip_system.txt");

	register_event("HLTV", "event_round_start", "a", "1=0", "2=0");

	RegisterHam(Ham_Spawn, "player", "player_spawn", 1);

	#if !defined AMX_WARMING
	register_event("TextMsg", "event_start_game", "a", "2=#Game_Commencing", "2=#Game_will_restart_in");
	#endif

	#if defined ENABLE_VAMPIRE
	register_event("DeathMsg", "event_death", "a", "1>0");
	#endif

	#if defined ENABLE_WEAPON_MODEL
	register_clcmd("vipmodels", "vip_models");
	#endif

	register_clcmd("vipmenu", "vip_menu");
	register_clcmd("say /vipmenu", "vip_menu");

	if(engfunc(EngFunc_FindEntityByString, FM_NULLENT, "classname", "func_vip_safetyzone")) {
		register_message(get_user_msgid("ScoreAttrib"), "message_scoreattribute");
	}

	if(engfunc(EngFunc_FindEntityByString, FM_NULLENT, "classname", "func_bomb_target")) {
		g_DefuseInMap = true;
	}

	g_MsgScoreAttrib = get_user_msgid("ScoreAttrib");

	#if defined BRAMMO_ONLY_VIP_WEAPONS
	g_max_clients = global_get(glb_maxClients);
	g_max_entities = global_get(glb_maxEntities);
	#endif

	#if defined ENABLE_PARACHUTE
	g_ParachuteHook = RegisterHam(Ham_ObjectCaps, "player", "FwdHamObjectCaps");
	DisableHamForward(g_ParachuteHook);
	#endif

	pcvar_Enable = create_new_cvar("amx_vip", "1");
	create_new_cvar("amx_vip_flag", "t");
	pcvar_Bonus = create_new_cvar("amx_vip_bonus", "abcdefghijk");
	pcvar_Flags = create_new_cvar("amx_vip_flags", "tttttttrrta");

	#if defined ENABLE_VAMPIRE
	pcvar_VampireHp = create_new_cvar("amx_vip_vampire_hp", "5");
	pcvar_VampireHs = create_new_cvar("amx_vip_vampire_hs", "10");
	#endif

	server_cmd("exec %s/vip_system/config.cfg", g_ConfigDir);
}

public plugin_cfg()
{
	new flag[2], title[128];
	
	get_cvar_string("amx_vip_flag", flag, 1);
	g_VipFlag = read_flags(flag);

	format(title, 127, "%L", LANG_SERVER, "VIP_MENU_TITLE");
	g_VipMenu = menu_create(title, "vipmenu_handler");

	if (loadConfigWeapons()) {
		menu_addblank(g_VipMenu, 0);
		format(title,127, "%L^n^n\y%s by %s", LANG_SERVER, "VIP_MENU_EXIT", PLUGIN, AUTHOR);
		menu_additem(g_VipMenu, title, "EXIT");
		menu_setprop(g_VipMenu, MPROP_EXIT, MEXIT_NEVER);
	} else {
		log_amx("[VipSystem] Error load weapons faild");
		set_pcvar_num(pcvar_Enable, 0);
	}

	loadConfigMaps();

	bonus_cvar_change();
	flags_cvar_change();

	#if defined hook_cvar_change
	hook_cvar_change(pcvar_Bonus, "bonus_cvar_change");
	hook_cvar_change(pcvar_Flags, "flags_cvar_change");
	#endif
}

public plugin_end()
{
	menu_destroy(g_VipMenu);
}

public plugin_natives()
{
	register_library("VipSystem");
	register_native("get_vip", "native_get_vip", 1);
	register_native("set_vip", "native_set_vip", 1);
}

public plugin_precache()
{
	get_localinfo("amxx_configsdir", g_ConfigDir, 127);

	#if defined ENABLE_WEAPON_MODEL
	ResetBit(g_IsModelShow);
	if (loadConfigModels()) {
		for (new wid = 0; wid <= MAX_WEAPONS; wid++) {
			if (CheckBit(g_IsModelEnable, wid)) {
				precache_model(g_WeaponsModels[wid][v_Model]);
				precache_model(g_WeaponsModels[wid][p_Model]);
			}
		}
	} else {
		log_amx("Error load weapon models");
	}
	#endif
}

public bonus_cvar_change()
{
	new flags[15];
	get_pcvar_string(pcvar_Bonus, flags, 14);
	g_Bonuses = read_flags(flags);

	#if defined BRAMMO_ONLY_VIP_WEAPONS
	if (CheckBonus(BONUS_WEAPON)) {
		g_WeaponsFwd = register_forward(FM_SetModel, "forward_set_model", 1);
	} else {
		unregister_forward(FM_SetModel, g_WeaponsFwd, 1);
	}
	#endif

	#if defined ENABLE_PARACHUTE
	if (CheckBonus( BONUS_PARACHUTE)) {
		EnableHamForward(g_ParachuteHook);
	} else {
		DisableHamForward(g_ParachuteHook);
	}
	#endif
}

public flags_cvar_change()
{
	new i, l, flag[2], flags[15];
	get_pcvar_string(pcvar_Flags, flags, 14);
	l = strlen(flags);
	if (l > sizeof g_BonusFlags) {
		l = sizeof g_BonusFlags;
	}
	for (i = 0; i < l; i++) {
		copy(flag, 1, flags[i]);
		g_BonusFlags[i] = read_flags(flag);
	}
}

public client_putinserver(id)
{
	new name[32];

	if (CheckFlag(id, g_VipFlag)) {
		get_user_name(id, name, 31);
		client_print_color(0, print_team_default,  "%L", LANG_PLAYER, "VIP_CONNECTED", name);
		client_cmd(id, "bind ^"F4^" ^"vipmenu^"");
	}

	#if defined ENABLE_WEAPON_MODEL
	SetBit(g_IsModelShow, id);
	new info[2];
	get_user_info(id, "vipmodels", info, 1);
	if (str_to_num(info) == 0) {
		SetBit(g_IsModelShow, id);
	}
	#endif
}

#if defined ENABLE_WEAPON_MODEL
public client_has_disconnect(id)
{
	ClearBit(g_IsModelShow, id);
}
#endif

#if defined AMX_WARMING
public warming_finish()
#else
public event_start_game()
#endif
{
	g_StartGame = true;
}

public event_round_start()
{
	if (g_StartGame) {
		g_RoundNum = 0;
		g_StartGame = false;
	} else {
		g_RoundNum++;
	}
}

public player_spawn(id)
{
	if (get_pcvar_num(pcvar_Enable) == 1 && is_user_alive(id)) {
		new flags = get_user_flags(id);

		if (CheckBonus(BONUS_DEAGLE) && CheckFlag(id, g_BonusFlags[BonusFlagDeagle], flags)) {
			give_weapon_ex(id, "weapon_deagle", true);
		}

		if (CheckBonus(BONUS_ARMOR) && CheckFlag(id, g_BonusFlags[BonusFlagArmor], flags)) {
			cs_set_user_armor(id, 100, CS_ARMOR_VESTHELM);
		}

		if (g_DefuseInMap && CheckBonus(BONUS_DEFUSE) && CheckFlag(id, g_BonusFlags[BonusFlagDefuse], flags) && cs_get_user_team(id) == CS_TEAM_CT) {
			cs_set_user_defuse(id, 1);
		}

		if (CheckBonus(BONUS_HEGRENADE) && CheckFlag(id, g_BonusFlags[BonusFlagHegrenade], flags)) {
			give_item_ex(id, "weapon_hegrenade");
		}

		if (CheckBonus(BONUS_FLASHBANG) && CheckFlag(id, g_BonusFlags[BonusFlagFlashbang], flags)) {
			give_item_ex(id, "weapon_flashbang");
			cs_set_user_bpammo(id, CSW_FLASHBANG, 2);
		}

		if (CheckBonus(BONUS_SMOKEGRENADE) && CheckFlag(id, g_BonusFlags[BonusFlagSmokegrenade], flags)) {
			give_item_ex(id, "weapon_smokegrenade");
		}

		if (CheckBonus(BONUS_WEAPON) && CheckFlag(id, g_BonusFlags[BonusFlagWeapon], flags) && g_RoundNum >= g_RoundMin) {
			ClearBit(g_WeaponUsed, id);
			#if defined BRAMMO_ONLY_VIP_WEAPONS
			if (g_WeaponUsedWid[id] > 0) {
				cs_set_user_bpammo(id, g_WeaponUsedWid[id], g_WeaponsBrammo[ g_WeaponUsedWid[id] ]);
			} else if(!cs_get_user_hasprim(id) && !displayed_menu(id)) {
				menu_display(id, g_VipMenu, 0);
			}
			#else
			static wid;
			if(!cs_get_user_hasprim(id) && !displayed_menu(id)) {
				menu_display(id, g_VipMenu, 0);
			} else {
				wid = get_user_weapon_ex(id, PRIMARY_WEAPONS_BIT_SUM);
				if (wid >= 0) {
					cs_set_user_bpammo(id, wid, g_WeaponsBrammo[wid]);
				}
			}
			#endif
		}

		if (CheckFlag(id, g_VipFlag)) {
			message_begin(MSG_ALL, g_MsgScoreAttrib);
			write_byte(id);
			write_byte(4);
			message_end();
		}
	}
}

public vip_menu(id)
{
	if (get_pcvar_num(pcvar_Enable) == 0 || !CheckBonus(BONUS_WEAPON)) {
		client_print_color(id, print_team_default,  "%L", id, "VIP_WEAPONS_DISABLED");
		return PLUGIN_HANDLED;
	}

	if (!CheckFlag(id, g_BonusFlags[BonusFlagWeapon])) {
		return PLUGIN_HANDLED;
	}

	#if defined ADMINLOAD
	static expired, title[128];
	expired = get_expired_days(id);
	if (expired > 0) {
		format(title, 127, "%L^n%L", LANG_SERVER, "VIP_MENU_TITLE", LANG_SERVER, "VIP_EXPIRED", expired);
	} else{
		format(title, 127, "%L", LANG_SERVER, "VIP_MENU_TITLE");		
	}
	menu_setprop(g_VipMenu, MPROP_TITLE, title);
	#endif

	menu_display(id, g_VipMenu, 0);

	return PLUGIN_CONTINUE;
}

public vipmenu_handler(id, menu, item)
{
	if (item == MENU_EXIT) {
		return PLUGIN_HANDLED;
	}

	new round, weapon[32], title[64], callback;
	menu_item_getinfo(menu, item, round, weapon, 31, title, 63, callback);

	if (equal(weapon, "EXIT")) {
		return PLUGIN_HANDLED;
	}

	if(!is_user_alive(id)) {
		client_print_color(id, print_team_default,  "%L", id, "VIP_WEAPONS_DEAD");
		return PLUGIN_HANDLED;
	}

	if (CheckBit(g_WeaponUsed, id)) {
		client_print_color(id, print_team_default,  "%L", id, "VIP_ALREADY_USED");
		return PLUGIN_HANDLED;
	}

	if (round > 0 && round > g_RoundNum) {
		client_print_color(id, print_team_default,  "%L", id, "VIP_ALLOW_AFTER_ROUND", round);
		return PLUGIN_HANDLED;
	}

	give_weapon_ex(id, weapon, false);

	SetBit(g_WeaponUsed, id);
	#if defined BRAMMO_ONLY_VIP_WEAPONS
	g_WeaponUsedWid[id] = get_weaponid(weapon);
	#endif

	return PLUGIN_CONTINUE;
}

public vipmenu_callback()
{
	return ITEM_ENABLED;
}

#if defined ENABLE_WEAPON_MODEL
public vip_models(id)
{
	if (!CheckBonus(BONUS_WEAPON_MODEL) || !CheckFlag(id, g_BonusFlags[BonusFlagWeaponModel])) {
		return PLUGIN_HANDLED;
	}

	if (CheckBit(g_IsModelShow, id)) {
		ClearBit(g_IsModelShow, id);
		resetModel(id);
		set_user_info(id, "vipmodels", "1");
		client_print_color(id, print_team_default,  "%L", id, "VIP_MODEL_DISABLE");
	} else {
		SetBit(g_IsModelShow, id);
		checkModel(id, get_user_weapon(id, _, _));
		set_user_info(id, "vipmodels", "0");
		client_print_color(id, print_team_default,  "%L", id, "VIP_MODEL_ENABLE");
	}

	return PLUGIN_HANDLED;
}
#endif

public message_scoreattribute(msg_id, dest, receiver)
{	
	static id; 

	id = get_msg_arg_int(1);
	if (CheckFlag(id, g_VipFlag) && !get_msg_arg_int(2)) {
		set_msg_arg_int(2, ARG_BYTE, 4);
	}
}

#if defined BRAMMO_ONLY_VIP_WEAPONS
public forward_set_model(ent, const model[]) 
{
	if (!pev_valid(ent) || !equali(model, "models/w_", 8) || equali(model, "models/w_weaponbox.mdl")) {
		return FMRES_IGNORED;
	}

	new id = pev(ent, pev_owner);
	if (!(1 <= id <= g_max_clients)) {
		return FMRES_IGNORED;
	}

	static class[32];
	pev(ent, pev_classname, class, 31);
	if (!equal(class, "weaponbox")) {
		return FMRES_IGNORED;
	}

	for (new i = g_max_clients + 1; i < g_max_entities; ++i) {
		if (!pev_valid(i) || ent != pev(i, pev_owner)) {
			continue;
		}

		if (g_WeaponUsedWid[id] > 0 && g_WeaponUsedWid[id] == cs_get_weapon_id(i)) {
			g_WeaponUsedWid[id] = 0;
		}

		return FMRES_IGNORED;
	}

	return FMRES_IGNORED;
}
#endif

#if defined ENABLE_VAMPIRE
public event_death()
{
	static id, hp;

	if (get_pcvar_num(pcvar_Enable) == 0 || !CheckBonus(BONUS_VAMPIRE)) {
		return PLUGIN_CONTINUE;
	}

	id = read_data(1);
	
	if (id == read_data(2)) {
		return PLUGIN_CONTINUE;
	}

	if (!CheckFlag(id, g_BonusFlags[BonusFlagVampire])) {
		return PLUGIN_CONTINUE;
	}

	hp =  ((read_data(3) == 1) && (read_data(5) == 0)) ? get_pcvar_num(pcvar_VampireHs) : get_pcvar_num(pcvar_VampireHp);

	set_pev(id, pev_health, floatmin(pev(id, pev_health) + float(hp), VAMPIRE_MAX_HP));

	return PLUGIN_CONTINUE;
}
#endif

#if defined ENABLE_PARACHUTE
public FwdHamObjectCaps(id)
{
	static Float:velocity[3];

 	if (!CheckFlag(id, g_BonusFlags[BonusFlagParachute])) {
		return HAM_IGNORED;
	}

	if (!is_user_alive(id) || get_entity_flags(id) & FL_ONGROUND) {
		return HAM_IGNORED;
	}

	entity_get_vector(id, EV_VEC_velocity, velocity);
	if (velocity[2] < 0) {
		velocity[2] = (velocity[2] + 40.0 < -100) ? velocity[2] + 40.0 : -100.0;
		entity_set_vector(id, EV_VEC_velocity, velocity);
	}

	return HAM_IGNORED;
}
#endif

#if defined ENABLE_WEAPON_MODEL
public WeaponDeploy(weapon) 
{
	new id = get_pdata_cbase(weapon, 41, 4);
	new wid = cs_get_weapon_id(weapon);

	if(CheckBonus(BONUS_WEAPON_MODEL) && is_valid_player(id) && is_valid_weapon(wid) && CheckFlag(id, g_BonusFlags[BonusFlagWeaponModel])) {
 		checkModel(id, wid);
	}

	return HAM_IGNORED;
}

checkModel(id, wid)
{
	#if defined BRAMMO_ONLY_VIP_WEAPONS
 	if(wid == CSW_DEAGLE && CheckBit(g_IsModelEnable, wid) && CheckBit(g_IsModelShow, id)) {
 		set_pev(id, pev_viewmodel2, g_WeaponsModels[wid][v_Model]);
 		set_pev(id, pev_weaponmodel2, g_WeaponsModels[wid][p_Model]);
 		return PLUGIN_CONTINUE;
 	}

 	if (g_WeaponUsedWid[id] == 0 || g_WeaponUsedWid[id] != wid) {
 		return PLUGIN_CONTINUE;
 	}
 	#endif

 	if (CheckBit(g_IsModelEnable, wid)) {
 		if (CheckBit(g_IsModelShow, id)) {
 			set_pev(id, pev_viewmodel2, g_WeaponsModels[wid][v_Model]);
 		}
		set_pev(id, pev_weaponmodel2, g_WeaponsModels[wid][p_Model]);
 	}

 	return PLUGIN_CONTINUE;
}

resetModel(id)
{
	new weapon[32];
	new wid = get_user_weapon(id, _, _);
	get_weaponname(wid, weapon, 31);
	client_cmd(id, "weapon_knife");
	client_cmd(id, weapon);
}

bool:loadConfigModels()
{
	ResetBit(g_IsModelEnable);

	new configFile[128];
	format(configFile, 127, "%s/vip_system/models.ini", g_ConfigDir);

	if (!file_exists(configFile)) {
		log_amx("Config file '%s' not found", configFile);
		return false;
	}

	new wid, text[512], weapon[32], v_model[32], p_model[32];

	new file = fopen(configFile, "r");
	if (!file) {
		return false;
	}

	while (!feof(file)) {
		fgets(file, text, 127);
		trim(text);

		if (!text[0] || text[0] == ';') {
			continue;
		}

		weapon[0] = '^0';
		v_model[0] = '^0';
		p_model[0] = '^0';

		parse(text, weapon, 31, v_model, 31, p_model, 31);

		if (!equal(weapon, "weapon_", 7)) {
			log_amx("Bad weapon name '%s'", weapon);
			continue;
		}


		wid = get_weaponid(weapon);
		// log_amx("Read model for %s (#%d): %s (%s)", weapon, wid, v_model, p_model);
		if (is_valid_weapon(wid)) {
			SetBit(g_IsModelEnable, wid);
			copy(g_WeaponsModels[wid][v_Model], 31, v_model);
			copy(g_WeaponsModels[wid][p_Model], 31, p_model);

			RegisterHam(Ham_Item_Deploy, weapon, "WeaponDeploy", 1);

			// log_amx("Add model for %s (#%d): %s (%s)", weapon, wid, v_model, p_model);
		}
	}

	fclose(file);
	return true;
}
#endif

bool:loadConfigWeapons()
{
	new configFile[128];
	format(configFile, 127, "%s/vip_system/menu.ini", g_ConfigDir);

	if (!file_exists(configFile)) {
		return false;
	}

	new text[256], title[128], weapon[32], round_str[3], round;
	new min_round = -1;

	new callback = menu_makecallback("vipmenu_callback");

	new file = fopen(configFile, "r");
	if (!file) {
		return false;
	}

	while (!feof(file)) {
		fgets(file, text, 255);
		trim(text);

		if (!text[0] || text[0] == ';') {
			continue;
		}

		title[0] = '^0';
		weapon[0] = '^0';
		round_str[0] = '^0';

		if (parse(text, title, 127, weapon, 31, round_str, 2) < 2) {
			continue;
		}

		if (!equal(weapon, "weapon_", 7)) {
			continue;
		}

		round = round_str[0] ? str_to_num(round_str) : 0;

		if (round > 0) {
			format(title, 127, "%s \r[\d %d \r]", title, round);
		}

		menu_additem(g_VipMenu, title, weapon, round, callback);

		if (min_round == -1 || min_round > round) {
			min_round = round;
		}

		// log_amx("[VIP] Add to menu %s wapon %s round %s (%d)", title, weapon, round_str, round);
	}

	g_RoundMin = min_round;
	fclose(file);
	return true;
}

bool:loadConfigMaps()
{
	new configFile[128];
	format(configFile, 127, "%s/vip_system/maps.ini", g_ConfigDir);

	if (!file_exists(configFile)) {
		return false;
	}

	new text[256], map[64], bonuses[15], flags[15];

	new mapname[64], prefix[64];
	new bool:checkPrefix = false;
	new bool:mapChanged = false;

	get_mapname(mapname, 63);
	new prefix_pos = contain(mapname, "_");
	if (prefix_pos >= 0) {
		copy(prefix, prefix_pos , mapname);
		checkPrefix = true;
	}

	new file = fopen(configFile, "r");
	if (!file) {
		return false;
	}

	while (!feof(file)) {
		fgets(file, text, 255);
		trim(text);

		if (!text[0] || text[0] == ';') {
			continue;
		}

		map[0] = '^0';
		bonuses[0] = '^0';
		flags[0] = '^0';

		if (parse(text, map, 63, bonuses, 14, flags, 14) < 2) {
			continue;
		}

		if (equal(mapname, map)){
			mapChanged = true;
			set_pcvar_string(pcvar_Bonus, bonuses);
			if (flags[0]) {
				set_pcvar_string(pcvar_Flags, flags);
			}
			log_amx("[VIP] Change cvars for map %s (%s) (%s)", map, bonuses, flags);
		} else if (checkPrefix && !mapChanged && equal(prefix, map)) {
			set_pcvar_string(pcvar_Bonus, bonuses);
			if (flags[0]) {
				set_pcvar_string(pcvar_Flags, flags);
			}
			log_amx("[VIP] Change cvars for prefix %s (%s) (%s)", map, bonuses, flags);
		}
	}

	fclose(file);
	return true;
}

public native_get_vip(id)
{	
	if (!is_valid_player(id)) {
		return 0;
	}

	return CheckFlag(id, g_VipFlag) ? 1 : 0;
}

public native_set_vip(id, status)
{
	if (!is_valid_player(id)) {
		return 0;
	}

	if (status) {
		set_user_flags(id, get_user_flags(id) | g_VipFlag);
		#if defined ENABLE_WEAPON_MODEL
		SetBit(g_IsModelShow, id);
		#endif
	} else {
		remove_user_flags(id, g_VipFlag);
		#if defined ENABLE_WEAPON_MODEL
		ClearBit(g_IsModelShow, id);
		#endif
	}

	return 1;
}

#if defined ADMINLOAD
stock get_expired_days(id)
{
	static expired;
	expired = adminload_get_expired(id);

	if (expired == 0) {
		return 0;
	}

	expired -= get_systime(0);

	return (expired / 86400);
}
#endif

stock give_item_ex(index, const item[]) 
{
	if (!equal(item, "weapon_", 7) && !equal(item, "ammo_", 5) && !equal(item, "item_", 5) && !equal(item, "tf_weapon_", 10)) {
		return 0;
	}

	new ent = engfunc(EngFunc_CreateNamedEntity, engfunc(EngFunc_AllocString, item));
	if (!pev_valid(ent)) {
		return 0;
	}

	new Float:origin[3];
	pev(index, pev_origin, origin);
	set_pev(ent, pev_origin, origin);
	set_pev(ent, pev_spawnflags, pev(ent, pev_spawnflags) | SF_NORESPAWN);
	dllfunc(DLLFunc_Spawn, ent);

	new save = pev(ent, pev_solid);
	dllfunc(DLLFunc_Touch, ent, index);
	if (pev(ent, pev_solid) != save) {
		return ent;
	}

	engfunc(EngFunc_RemoveEntity, ent);

	return -1;
}

stock give_weapon_ex(id, weapon[], bool:forse = false)
{
	static weapons[32], num, wid, wtype, wname[32], bool:exists, ent, ent_box, i;

	wid = get_weaponid(weapon);
	if ((1 << wid) & PRIMARY_WEAPONS_BIT_SUM) {
		wtype = PRIMARY_WEAPONS_BIT_SUM;
	} else if ((1 << wid) & SECONDARY_WEAPONS_BIT_SUM) {
		wtype = SECONDARY_WEAPONS_BIT_SUM;
	}
	
	num = 0;
	get_user_weapons(id, weapons, num);
	
	exists = false;
	for (i = 0; i < num; i++) {
		if (!forse && wid == weapons[i]) {
			exists = true;
			break;
		}

		if ((1 << weapons[i]) & wtype) {
			get_weaponname(weapons[i], wname, 31);

			ent = -1;
			while ((ent = engfunc(EngFunc_FindEntityByString, ent, "classname", wname)) && pev(ent, pev_owner) != id) {}
			if(!ent) continue;

			engclient_cmd(id, "drop", wname);

			ent_box = pev(ent, pev_owner);
			if (!ent_box || ent_box == id) continue;

			dllfunc(DLLFunc_Think, ent_box);
		}
	}

	if (is_valid_weapon(wid)) {
		if (!exists) {
			#if defined DROP_SHIELD
			engclient_cmd(id, "drop", "weapon_shield");
			#endif
			give_item_ex(id, weapon);
		}

		cs_set_user_bpammo(id, wid, g_WeaponsBrammo[wid]);
	}
}

stock get_user_weapon_ex(id, wtype)
{
	static weapons[32], num, i;

	num = 0;
	get_user_weapons(id, weapons, num);

	for (i = 0; i < num; i++) {
		if ((1 << weapons[i]) & wtype) {
			return weapons[i];
		}
	}

	return -1;
}

stock bool:displayed_menu(id)
{
	new oldmenu, newmenu;
	player_menu_info(id, oldmenu, newmenu);

	return (newmenu != -1 || oldmenu > 0) ? true : false;
}

stock bool:is_valid_player(id)
{
	return (1 <= id <= MAX_PLAYERS);
}

stock bool:is_valid_weapon(wid)
{
	return (1 <= wid <= MAX_WEAPONS);
}

stock bool:CheckFlag(id, flag, userFlags = -1)
{
	if (userFlags == -1) {
		userFlags = get_user_flags(id);
	}
	return (userFlags & flag) ? true : false;
}

stock bool:CheckBonus(flag)
{
	return (g_Bonuses & flag) ? true : false;
}