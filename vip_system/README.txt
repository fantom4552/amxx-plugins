Всем привет.
Решил выложить свою версию VIP плагина:) Да да еще одну

Все почти стандартное

	Менюшка вызывается через чат /vipmenu или через консоль vipmenu
	Поддержка разминочного раунда и рестартов
	Оружие в меню можно полностю настраивать
	Deagle, Armor, Defuse, HE, 2 Flash выдаются в начале раунда
	VIP в TAB
	Вампиризм для супервипов
	Возможность моделей ВИП оружия

CVAR-ы (Конфиг vip_system.cfg)
	amx_vip 1 					// Вкл/Выкл. Де
	amx_vip_flag "t"			// Флаг для випов
	amx_vip_superflag "r"		// Флаг для супервипов
	amx_vip_round 3				// Weapon available round
	amx_vip_bonus 1				// Вкл/Выкл Deagle + Armor + Defuse
	amx_vip_grenades 1			// Вкл/Выкл 1 HE + 2 Flash
	amx_vip_weapons 1			// Вкл/Выкл меню оружия
	amx_vip_vampire 1			// Вкл/Выкл вампиризм
	amx_vip_vampire_hp 10		// Бонус HP за убийство
	amx_vip_vampire_hs 15		// Бонус HP за убийство при хедшоте

Константы
	#define ADMINLOAD // Использовать мою загрузку админов (может когда-то поделюсь)
	#define ENABLE_MODELS // Вкл/Вык модели ВИП оружия (Конфиг vip_models.ini)
	#define ENABLE_VAMPIRE // Вкл/Вык вампиризм
	#define VAMPIRE_MAX_HP 100 // Максимальное количество HP для вампиризма

Нстройка меню (Конфиг vip_menu.ini)
	"<text>" "<weapon>" "<round>"
	
	<text> - текст меню
		Доступные цвета в заголовке
			White - \w
			Yellow - \y
			Red - \r
			Gray - \d
	<weapon> - название оружия
		Список оружия
			weapon_p228
			weapon_scout
			weapon_xm1014
			weapon_mac10
			weapon_aug
			weapon_elite
			weapon_fiveseven
			weapon_ump45
			weapon_sg550
			weapon_galil
			weapon_famas
			weapon_usp
			weapon_glock18
			weapon_awp
			weapon_mp5navy
			weapon_m249
			weapon_m3
			weapon_m4a1
			weapon_tmp
			weapon_g3sg1
			weapon_deagle
			weapon_sg552
			weapon_ak47
			weapon_p90

	<round> - в каком ранде станет доступно

Локализации: EN, RU, UA

Нужные модули: fun, engine, fakemeta, cstrike, hamsandwich

В архиве инклюд colorchat и adminload. Поэтому не компилируйте веб-компилятором, только локально!

Проверено на 1.8.2 и 1.8.3. Багов не замечено

P.S. У меня 2 ника, один форумный, другой игровой