#pragma semicolon 1

#include <amxmodx>
#include <nvault>
#include <server_ranks>

#define PLUGIN "Server Ranks NVault"
#define VERSION "1.0"
#define AUTHOR "F@nt0M"

#define isValidePlayer(%1) (1 <= %1 <= MAX_PLAYERS)

new g_NVault;
new cvar_Filename;

public plugin_init()
{
	register_plugin(PLUGIN, VERSION, AUTHOR);

	cvar_Filename = register_cvar("sr_nvault_filename", "server_ranks");
}

public plugin_cfg()
{
	new filename[32];
	get_pcvar_string(cvar_Filename, filename, charsmax(filename));
	g_NVault = nvault_open(filename);
	if (g_NVault == INVALID_HANDLE) {
		server_ranks_inited(0);
		set_fail_state("[Error] Error open nvault file");
	} else {
		server_ranks_inited(1);
	}
}

public server_ranks_player_load(id, const authid[])
{
	new data[70], timestamp, experience[32], points[32];

	if (nvault_lookup(g_NVault, authid, data, 31, timestamp)) {
		strtok(data, experience, 31, points, 31, ' ', 1);
		if (is_str_num(experience) && is_str_num(points)) {
			server_ranks_player_loaded(id, abs(str_to_num(experience)), abs(str_to_num(points)));
		} else {
			server_ranks_player_loaded(id, 0, 0);
		}
	} else {
		server_ranks_player_loaded(id, 0, 0);
	}
}

public server_ranks_player_save(id, experience, points, const authid[])
{
	new data[70];

	format(data, 69, "%d %d", experience, points);
	nvault_set(g_NVault, authid, data);
}