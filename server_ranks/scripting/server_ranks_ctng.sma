#pragma semicolon 1

#include <amxmodx>
#include <colored_translit_ng>
#include <server_ranks>

#define STATUS_VALUE

#define PLUGIN "Server Ranks Colored Translit NG"
#define VERSION "1.0"
#define AUTHOR "F@nt0M"

new bool:g_ServerRanksInitialized = false;

public plugin_init()
{
	register_plugin(PLUGIN, VERSION, AUTHOR);
}

public server_ranks_initialized()
{
	g_ServerRanksInitialized = true;
}

public ctng_message_format(id, say_team)
{
	#pragma unused say_team
	static rank[64];

	if (g_ServerRanksInitialized) {
		server_ranks_get_user_rankname(id, rank, 63);
		ctng_add_prefix(0, "[^x04%s^x01]", rank);
	}
}