#pragma semicolon 1

#include <amxmodx>
#include <sqlx>
#include <server_ranks>

#define PLUGIN "Server Ranks Mysql"
#define VERSION "1.0"
#define AUTHOR "F@nt0M"

#define isValidePlayer(%1) (1 <= %1 <= MAX_PLAYERS)

new Handle:g_DBTuple;
new cvar_TableName;

new g_PlayersId[MAX_PLAYERS+1];

public plugin_init()
{
	register_plugin(PLUGIN, VERSION, AUTHOR);

	register_cvar("sr_sql_host", "");
	register_cvar("sr_sql_user", "");
	register_cvar("sr_sql_pass", "");
	register_cvar("sr_sql_type", "");
	register_cvar("sr_sql_db", "");
	cvar_TableName = register_cvar("sr_sql_table", "server_ranks");

	new configsDir[128];
	get_localinfo("amxx_configsdir", configsDir, charsmax(configsDir));
	server_cmd("exec %s/sql.cfg", configsDir);
}

public server_ranks_started()
{
	new host[64], user[32], pass[32], db[128];
	new get_type[12], set_type[12];
	
	get_cvar_string("sr_sql_host", host, 63);
	if (!host[0]) {
		get_cvar_string("amx_sql_host", host, 63);
	}
	get_cvar_string("sr_sql_user", user, 31);
	if (!user[0]) {
		get_cvar_string("amx_sql_user", user, 63);
	}
	get_cvar_string("sr_sql_pass", pass, 31);
	if (!pass[0]) {
		get_cvar_string("amx_sql_pass", pass, 63);
	}
	get_cvar_string("sr_sql_type", set_type, 11);
	if (!set_type[0]) {
		get_cvar_string("amx_sql_type", set_type, 63);
	}
	get_cvar_string("sr_sql_db", db, 127);
	if (!db[0]) {
		get_cvar_string("amx_sql_db", db, 63);
	}
	
	SQL_GetAffinity(get_type, 12);
	
	if (!equali(get_type, set_type)) {
		if (!SQL_SetAffinity(set_type)) {
			log_amx("Failed to set affinity from %s to %s.", get_type, set_type);
		}
	}
	
	g_DBTuple = SQL_MakeDbTuple(host, user, pass, db, 0);

	#if AMXX_VERSION_NUM == 183
	SQL_SetCharset(g_DBTuple, "utf8");
	#endif // AMXX_VERSION_NUM
	
	new error[128], errno;

	new Handle:g_Sql = SQL_Connect(g_DBTuple, errno, error, 127);
	
	if (g_Sql == Empty_Handle) {
		log_amx("[ServerRanks] SQL error: can't connect: '%s'", error);
		log_amx("[ServerRanks] SQL error: connect arguments '%s', '%s', '%s', '%s', '%s'", host, user, pass, db, set_type);
		server_ranks_inited(0);
		set_fail_state("[Error] Cannot connect to MYSQL");
	} else {
		SQL_FreeHandle(g_Sql);
		log_amx("[ServerRanks] Connected to MySQL server success");
		server_ranks_inited(1);
	}
}

public server_ranks_player_load(id, const authid[])
{
	new tableName[32], pquery[128], authidEscaped[64];

	g_PlayersId[id] = 0;

	mysql_escape_string(authidEscaped, 63, authid);

	get_pcvar_string(cvar_TableName, tableName, charsmax(tableName));
	format(pquery, charsmax(pquery), "SELECT id, experience, points FROM %s WHERE name = '%s' LIMIT 1", tableName, authid);
	// log_amx(pquery);

	new data[1];
	data[0] = id;
	SQL_ThreadQuery(g_DBTuple, "PlayerLoadPost", pquery, data, 1);
}

public PlayerLoadPost(failstate, Handle:query, const error[], errornum, const data[], size, Float:queuetime)
{
	new id = data[0];

	if (failstate) {
		SQL_Error(query, error, errornum, failstate);
	} else if (SQL_NumResults(query) > 0) {
		g_PlayersId[id]= SQL_ReadResult(query, 0);
		server_ranks_player_loaded(id, SQL_ReadResult(query, 1), SQL_ReadResult(query, 2));
	} else {
		g_PlayersId[id] = 0;
		server_ranks_player_loaded(id, 0, 0);
	}

	SQL_FreeHandle(query);
}

public server_ranks_player_save(id, experience, points, const authid[])
{
	new tableName[32], pquery[128], authidEscaped[64];
	get_pcvar_string(cvar_TableName, tableName, charsmax(tableName));

	if (g_PlayersId[id] > 0) {
		format(pquery, charsmax(pquery), "UPDATE `%s` SET `experience` = %d, `points` = %d WHERE `id` = %d LIMIT 1", tableName, experience, points, g_PlayersId[id]);
	} else {
		mysql_escape_string(authidEscaped, 63, authid);
		format(pquery, charsmax(pquery), "INSERT INTO %s (`name`,`experience`, `points`) VALUES('%s', %d, %d)", tableName, authid, experience, points);
	}
	// log_amx(pquery);

	SQL_ThreadQuery(g_DBTuple, "PlayerSavePost", pquery);
}

public PlayerSavePost(failstate, Handle:query, const error[], errornum, const data[], size, Float:queuetime)
{
	if (failstate) {
		SQL_Error(query, error, errornum, failstate);
	}

	SQL_FreeHandle(query);
}

stock mysql_escape_string(dest[], len, const source[])
{
	copy(dest, len, source);
	replace_all(dest, len, "\\", "\\\\");
	replace_all(dest, len, "\0", "\\0");
	replace_all(dest, len, "\n", "\\n");
	replace_all(dest, len, "\r", "\\r");
	replace_all(dest, len, "\x1a", "\Z");
	replace_all(dest, len, "'", "\'");
	replace_all(dest, len, "^"", "\^"");
}

stock SQL_Error(Handle:query, const error[], errornum, failstate)
{
	new qstring[1024];
	SQL_GetQueryString(query, qstring, 1023);
	
	if (failstate == TQUERY_CONNECT_FAILED) {
		log_amx("[SQL Error] Connection failed");
	} else if (failstate == TQUERY_QUERY_FAILED) {
		log_amx("[SQL Error] Query failed");
	}

	log_amx("[SQL Error] Message: %s (%d)", error, errornum);
	log_amx("[SQL Error] Query: %s", qstring);
}