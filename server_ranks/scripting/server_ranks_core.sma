#pragma semicolon 1

#include <amxmodx>
#include <fakemeta>
#include <server_ranks>

#define STATUS_VALUE

#define PLUGIN "Server Ranks Core"
#define VERSION "1.0"
#define AUTHOR "F@nt0M"

#if AMXX_VERSION_NUM == 183
	#define client_has_disconnect client_disconnected
#else
	#define client_has_disconnect client_disconnect
#endif

#if (AMXX_VERSION_NUM < 183) || defined NO_NATIVE_COLORCHAT
	#include <colorchat>
#else
	#define DontChange print_team_default
#endif

#define isValidPlayer(%1) (1 <= %1 <= g_MaxPlayers)

#define CheckBit(%1,%2)		(%1 &	(1 << (%2 & 31)))
#define SetBit(%1,%2)		(%1 |=	(1 << (%2 & 31)))
#define ClearBit(%1,%2)		(%1 &= ~(1 << (%2 & 31)))
#define ResetBit(%1)		(%1 = 0)

#if defined STATUS_VALUE
enum _:MAX_TYPES {
	TYPE_NONE,
	TYPE_RELATION,
	TYPE_PLAYER
};

enum {
	LOOK_FRIEND = 1,
	LOOK_ENEMY,
	LOOK_HOSTAGE
};

enum _:MAX_REPLACES {
	RE_HP,
	RE_RANK,
	RE_NAME
};

new const g_Replaces[MAX_REPLACES][] = {
	"%hp%",
	"%rank%",
	"%name%"
};

new g_StatusValues[MAX_TYPES];
new g_StatusText;
#endif

new g_ConfigsDir[128];

new bool:g_Inited = false;
new bool:g_Bonus = true;

new g_MaxPlayers;
new g_MsgHud;

new g_IsLoaded;
new g_ShowInfo[MAX_PLAYERS + 1];

enum _:Cvars {
	cvarExpKill,
	cvarExpHead,
	cvarExpKnife,
	cvarExpPlant,
	cvarExpDefuse,
	cvarPointsLevel,
	cvarMinPlayers,
	cvarShowTime,
	cvarShowRound,
	cvarShowLoad,
	cvarAuthType
};

new g_Cvars[Cvars];

enum _:Forwards {
	fwdStarted,
	fwdInitialized,
	fwdPlayerLoad,
	fwdPlayerSave,
	fwdPlayerLevelUp
}

new g_Return;
new g_Forwards[Forwards];

enum _:LevelData {
	gLevelExp,
	gLevelBonus,
	gLevelTitle[64]
}

new g_Level[LevelData];

new Array:g_LevelsData;
new g_LevelsNum;

enum _:UserData {
	gExp,
	gLevel,
	gPoints,
	gNextLevelExp,
	gTitle[64]
};

new g_PlayersData[MAX_PLAYERS+1][UserData];

public plugin_init()
{
	register_plugin(PLUGIN, VERSION, AUTHOR);

	register_dictionary("server_ranks.txt");

	register_concmd("amx_server_rank", "cmdRanks", ADMIN_CFG, "<steamID or nickname or #authid or IP> <experience> <points>");

	register_logevent("EventRoundStart", 2, "1=Round_Start");

	register_event("DeathMsg", "EventDeath", "a");
	register_logevent("EventPlanted", 3, "2=Planted_The_Bomb");
	register_logevent("EventDefused", 3, "2=Defused_The_Bomb");

	#if defined STATUS_VALUE
	register_event("StatusValue", "StatusValueShow", "be");
	g_StatusText = get_user_msgid("StatusText");
	#endif

	register_srvcmd("sr_lockmap", "CmdLockmap");

	set_task(1.0, "InfoPlayers", _, _, _, "b");

	register_forward(FM_Sys_Error, "fw_ServerDown");
	register_forward(FM_GameShutdown, "fw_ServerDown");
	register_forward(FM_ServerDeactivate, "fw_ServerDown");

	get_localinfo("amxx_configsdir", g_ConfigsDir, charsmax(g_ConfigsDir));

	g_Cvars[cvarExpKill] = register_cvar("sr_exp_kill", "1");
	g_Cvars[cvarExpHead] = register_cvar("sr_exp_head", "2");
	g_Cvars[cvarExpKnife] = register_cvar("sr_exp_knife", "3");
	g_Cvars[cvarExpPlant] = register_cvar("sr_exp_plant", "4");
	g_Cvars[cvarExpDefuse] = register_cvar("sr_exp_defuse", "4");
	g_Cvars[cvarPointsLevel] = register_cvar("sr_points_level", "1");

	g_Cvars[cvarMinPlayers] = register_cvar("sr_min_players", "4");
	g_Cvars[cvarShowTime] = register_cvar("sr_show_time", "5");
	g_Cvars[cvarShowRound] = register_cvar("sr_show_round", "15");
	g_Cvars[cvarShowLoad] = register_cvar("sr_show_load", "15");

	g_Cvars[cvarAuthType] = register_cvar("sr_auth_type", "0");

	g_Forwards[fwdStarted] = CreateMultiForward("server_ranks_started", ET_IGNORE);
	g_Forwards[fwdInitialized] = CreateMultiForward("server_ranks_initialized", ET_IGNORE);
	g_Forwards[fwdPlayerLoad] = CreateMultiForward("server_ranks_player_load", ET_IGNORE, FP_CELL, FP_STRING);
	g_Forwards[fwdPlayerSave] = CreateMultiForward("server_ranks_player_save", ET_IGNORE, FP_CELL, FP_CELL, FP_CELL, FP_STRING);
	g_Forwards[fwdPlayerLevelUp] = CreateMultiForward("server_ranks_player_levelup", ET_IGNORE, FP_CELL, FP_CELL, FP_CELL);

	g_LevelsData = ArrayCreate(LevelData);
	g_MsgHud = CreateHudSyncObj();
	g_MaxPlayers = get_maxplayers();

	ResetBit(g_IsLoaded);

	server_cmd("exec %s/server_ranks/config.cfg", g_ConfigsDir);
}

public plugin_cfg()
{
	if (!loadLevels()) {
		set_fail_state("Bad config file");
	} else {
		ExecuteForward(g_Forwards[fwdStarted], g_Return);
	}
}

public fw_ServerDown()
{
	new players[32], num;
	get_players(players, num, "ch");

	for (new i = 0; i < num; i++) {
		playerSave(players[i]);
	}
}

public CmdLockmap()
{
	new map[64];
	read_args(map, charsmax(map));
	remove_quotes(map);

	new mapname[64], prefix[64];
	new bool:checkPrefix = false;

	get_mapname(mapname, charsmax(mapname));
	new prefix_pos = contain(mapname, "_");
	if (prefix_pos >= 0) {
		copy(prefix, prefix_pos , mapname);
		checkPrefix = true;
	}

	if (equal(mapname, map) || (checkPrefix && equal(prefix, map))) {
		g_Bonus = false;
	}
}

public client_putinserver(id)
{
	playerLoad(id);
}

public client_has_disconnect(id)
{
	playerSave(id);
	ClearBit(g_IsLoaded, id);
}

public client_infochanged(id)
{
	if (g_Inited && get_pcvar_num(g_Cvars[cvarAuthType]) == 0 && is_user_connected(id) && CheckBit(g_IsLoaded, id)) {
		new newname[32], oldname[32];
		get_user_name(id, oldname, 31);
		get_user_info(id, "name", newname, 31);

		if (!equal(newname, oldname)) {
			playerSave(id, oldname);
			playerLoad(id, newname);
		}
	}
}

public cmdRanks(id, level, cid)
{
	if (!cmd_access_ex(id, level, cid, 3)) {
		return PLUGIN_HANDLED;
	}

	new args[100], playerStr[32], expStr[32], pointsStr[32];
	read_args(args, charsmax(args));
	parse(args, playerStr, charsmax(playerStr), expStr, charsmax(expStr), pointsStr, charsmax(pointsStr));

	
	new player = locate_player(playerStr);
	
	new adminName[32], playerName[32];	

	if (!player || (!is_user_connecting(player) && !is_user_connected(player))) {
		console_print(id, "[Server Ranks] Player %s not found", playerStr);
		return PLUGIN_HANDLED;
	}

	get_user_name(player, playerName, 31);

	if (!expStr[0] && !pointsStr[0] && g_Inited && !is_user_bot(id) && !is_user_hltv(id)) {
		console_print(id, "[Server Ranks] Player ^"%s^" has %d experience and %d points edited", playerName, g_PlayersData[player][gExp], g_PlayersData[player][gPoints]);
		return PLUGIN_HANDLED;
	}

	if (!is_str_num(expStr) || !is_str_num(pointsStr)) {
		console_print(id, "[Server Ranks] Experience or Points is not num");
		return PLUGIN_HANDLED;
	}

	new exp = str_to_num(expStr);
	new points = str_to_num(pointsStr);

	g_PlayersData[player][gExp] = exp;
	g_PlayersData[player][gPoints] = points;

	g_PlayersData[player][gLevel] = 0;
	g_PlayersData[player][gNextLevelExp] = 0;
	g_PlayersData[player][gTitle][0] = '^0';

	playerLevel(player, true);

	get_user_name(id, adminName, 31);
	
	log_amx("[Server Ranks] Admin ^"%s^" change %d experience and %d points for ^"%s^"", adminName, exp, points, playerName);
	console_print(id, "[Server Ranks] Player ^"%s^" successfully edited", playerName);

	return PLUGIN_HANDLED;
}

#if defined STATUS_VALUE
public StatusValueShow(id)
{
	static message[192], num, player, live;

	num = read_data(1);
	if (0 < num < MAX_TYPES) {
		player = g_StatusValues[num] = read_data(2);
		live = is_user_alive(id);

		if (num == TYPE_RELATION && !player && live) {
			show_status_text(id, player, false, "");
		} else if (num == TYPE_PLAYER && live && CheckBit(g_IsLoaded, player)) {
			if (g_StatusValues[TYPE_RELATION] == LOOK_FRIEND) {
				format(message, 191, "%L: %%name%% %L: %%hp%% %L: %%rank%%", id, "SR_FRIEND", id, "SR_HP", id, "SR_RANK");
				show_status_text(id, player, true, message);
			} else if (g_StatusValues[TYPE_RELATION] == LOOK_ENEMY) {
				format(message, 191, "%L: %%name%% %L: %%rank%%", id, "SR_ENEMY", id, "SR_RANK");
				show_status_text(id, player, true, message);
			}
		}
	}
}
#endif

public EventRoundStart()
{
	arrayset(g_ShowInfo, get_pcvar_num(g_Cvars[cvarShowRound]), MAX_PLAYERS+1);
}

public EventDeath()
{
	new killer = read_data(1);
	new victim = read_data(2);
	new head = read_data(3);

	if (killer != victim && is_user_connected(killer) && is_user_connected(victim) && CheckBit(g_IsLoaded, killer)) {
		if (get_user_weapon(killer) == CSW_KNIFE) {
			addExp(killer,  get_pcvar_num(g_Cvars[cvarExpKnife]));
		} else if (head) {
			addExp(killer, get_pcvar_num(g_Cvars[cvarExpHead]));
		} else {
			addExp(killer, get_pcvar_num(g_Cvars[cvarExpKill]));
		}

		g_ShowInfo[killer] = get_pcvar_num(g_Cvars[cvarShowTime]);
		g_ShowInfo[victim] = get_pcvar_num(g_Cvars[cvarShowTime]);
	}

	return PLUGIN_CONTINUE;
}

public EventPlanted()
{
	new id = get_loguser_index();
	
	if(get_playersnum() < get_pcvar_num(g_Cvars[cvarMinPlayers])) {
		client_print_color(id, print_team_default, "^4[^3%s^4] %L", PLUGIN, LANG_PLAYER, "SR_MIN_PLAYERS");
	} else if (CheckBit(g_IsLoaded, id)) {
		addExp(id, get_pcvar_num(g_Cvars[cvarExpPlant]));
		client_print_color(id, print_team_default, "^4[^3%s^4]%L", PLUGIN, id, "SR_PLANT_EXP", get_pcvar_num(g_Cvars[cvarExpDefuse]));
	}
}

public EventDefused()
{
	new id = get_loguser_index();

	if(get_playersnum() < get_pcvar_num(g_Cvars[cvarMinPlayers])) {
		client_print_color(id, print_team_default, "^4[^3%s^4] %L", PLUGIN, LANG_PLAYER, "SR_MIN_PLAYERS");
	} else if (CheckBit(g_IsLoaded, id)) {
		addExp(id, get_pcvar_num(g_Cvars[cvarExpDefuse]));
		client_print_color(id, print_team_default, "^4[^3%s^4] %L", PLUGIN, id, "SR_DEFUSE_EXP", get_pcvar_num(g_Cvars[cvarExpDefuse]));
	}
}

public InfoPlayers()
{
	static i, players[32], num;

	if (g_Inited) {
		get_players(players, num, "ch");

		set_hudmessage(100, 100, 100, 0.01, 0.28, 0, 1.0, 1.0, _, _, -1);
		for (i = 0; i < num; i++) {
			if (pev(players[i], pev_iuser1) == 4) {
				infoPlayer(players[i], pev(players[i], pev_iuser2), true);
			} else if(g_ShowInfo[players[i]] > 0) {
				infoPlayer(players[i], players[i], false);
			}

			if (g_ShowInfo[players[i]]) {
				g_ShowInfo[players[i]]--;
			}
		}
	}
}

playerLoad(id, const name[] = "")
{
	new authid[32];

	g_PlayersData[id][gExp] = 0;
	g_PlayersData[id][gLevel] = 0;
	g_PlayersData[id][gPoints] = 0;
	g_PlayersData[id][gNextLevelExp] = 0;
	g_PlayersData[id][gTitle][0] = '^0';

	ClearBit(g_IsLoaded, id);

	if (g_Inited && !is_user_bot(id) && !is_user_hltv(id)) {
		get_user_auth(id, authid, 31, name);
		ExecuteForward(g_Forwards[fwdPlayerLoad], g_Return, id, authid);
	}
}

playerSave(id, const name[] = "")
{
	new authid[32];
	if (g_Inited && CheckBit(g_IsLoaded, id) && g_PlayersData[id][gExp] > 0) {
		get_user_auth(id, authid, 31, name);
		ExecuteForward(g_Forwards[fwdPlayerSave], g_Return, id, g_PlayersData[id][gExp], g_PlayersData[id][gPoints], authid);
	}
}

infoPlayer(id, player, bool:user2)
{
	static message[256], len, name[32];

	if (CheckBit(g_IsLoaded, player)) {
		message[0] = '^0';
		len = 0;

		if (user2) {
			get_user_name(player, name, 31);
			len += format(message[len], charsmax(message) - len, "%L: %s^n", id, "SR_NICK", name);
		}

		len += format(message[len], charsmax(message) - len, "%L: %s^n", id, "SR_RANK", g_PlayersData[player][gTitle]);

		if (g_PlayersData[id][gLevel] >= g_LevelsNum - 1) {
			len += format(message[len], charsmax(message) - len, "%L", id, "SR_EXP_MAX");
		} else {
			len += format(message[len], charsmax(message) - len, "%L: [ %d / %d ]", id, "SR_EXP", g_PlayersData[player][gExp], g_PlayersData[player][gNextLevelExp]);
		}

		if (!user2 && get_pcvar_num(g_Cvars[cvarPointsLevel]) > 0) {
			len += format(message[len], charsmax(message) - len, "^n%L: %d", id, "SR_POINTS", g_PlayersData[player][gPoints]);
		}
		
		ShowSyncHudMsg(id, g_MsgHud, message);
	}
}

addExp(id, exp)
{
	static name[32];

	g_PlayersData[id][gExp] += exp;

	if (playerLevel(id, false)) {
		get_user_name(id, name, 31);
		client_print_color(0, print_team_default, "^4[^3%s^4] %L", PLUGIN, id, "SR_LEVEL_UP", name, g_PlayersData[id][gTitle]);

		g_PlayersData[id][gPoints] += get_pcvar_num(g_Cvars[cvarPointsLevel]);

		ExecuteForward(g_Forwards[fwdPlayerLevelUp], g_Return, id, g_PlayersData[id][gExp], g_PlayersData[id][gPoints]);
	}
}

get_user_auth(id, authid[], len, const name[] = "")
{
	switch (get_pcvar_num(g_Cvars[cvarAuthType])) {
		case 1: {
			get_user_authid(id, authid, len);
		}

		case 2: {
			get_user_ip(id, authid, len, 1);
		}

		default: {
			if (name[0]) {
				copy(authid, len, name);
			} else {
				get_user_name(id, authid, len);
			}
		}
	}
}

bool:playerLevel(id, bool:load = false)
{
	static bool:levelUp;

	if (load) {
		ArrayGetArray(g_LevelsData, g_PlayersData[id][gLevel], g_Level);
		copy(g_PlayersData[id][gTitle], 63, g_Level[gLevelTitle]);

		if (g_PlayersData[id][gLevel] < g_LevelsNum - 1) {
			ArrayGetArray(g_LevelsData, g_PlayersData[id][gLevel] + 1, g_Level);
			g_PlayersData[id][gNextLevelExp] = g_Level[gLevelExp];
		}
	}

	levelUp = false;
	while (g_PlayersData[id][gLevel] < g_LevelsNum - 1 && g_PlayersData[id][gExp] >= g_PlayersData[id][gNextLevelExp]) {
		g_PlayersData[id][gLevel]++;

		ArrayGetArray(g_LevelsData, g_PlayersData[id][gLevel], g_Level);
		copy(g_PlayersData[id][gTitle], 63, g_Level[gLevelTitle]);

		if (g_PlayersData[id][gLevel] < g_LevelsNum - 1) {
			ArrayGetArray(g_LevelsData, g_PlayersData[id][gLevel] + 1, g_Level);
			g_PlayersData[id][gNextLevelExp] = g_Level[gLevelExp];
		}

		levelUp = true;
	}

	// log_amx("Player %d. Exp %d, Level %d (%s). Bonus %d, Next exp %d", id, g_PlayersData[id][gExp], g_PlayersData[id][gLevel], g_PlayersData[id][gTitle], g_PlayersData[id][gBonus], g_PlayersData[id][gNextLevelExp]);

	return !load ? levelUp : false;
}

bool:loadLevels()
{
	new configFile[128];
	new line[256], title[64], exp[32], bonus[32];

	format(configFile, charsmax(configFile), "%s/server_ranks/levels.ini", g_ConfigsDir);

	if (!file_exists(configFile)) {
		log_amx("[Error] Could not found config file %s", configFile);
		return false;
	}

	new file = fopen(configFile, "r");
	if (!file) {
		log_amx("[Error] Could not found config file %s", configFile);
		return false;
	}

	ArrayClear(g_LevelsData);
	g_LevelsNum = 0;

	while (!feof(file)) {
		fgets(file, line, charsmax(line));
		trim(line);

		if (!line[0] || line[0]==';') {
			continue;
		}

		if (parse(line, title, charsmax(title), exp, charsmax(exp), bonus, charsmax(bonus)) < 2) {
			continue;
		}

		g_Level[gLevelExp] = str_to_num(exp);
		g_Level[gLevelBonus] = bonus[0] ? read_flags(bonus) : 0;
		copy(g_Level[gLevelTitle], charsmax(g_Level[gLevelTitle]), title);
		ArrayPushArray(g_LevelsData, g_Level);

		// log_amx("Add level: Exp %d, Bonus %d (%s), Title %s", g_Level[gLevelExp], g_Level[gLevelBonus], bonus, g_Level[gLevelTitle]);

		g_LevelsNum++;
	}

	fclose(file);

	return true;
}

stock get_loguser_index()
{
	new loguser[80], name[32];
	read_logargv(0, loguser, 79);
	parse_loguser(loguser, name, 31);

	return get_user_index(name);
}

stock bool:cmd_access_ex(id, level, cid, num)
{
	if (id == (is_dedicated_server() ? 0 : 1)) {
		return true;
	} else if (get_user_flags(id) & level) {
		return true;
	} else if (level == ADMIN_ALL) {
		return true;
	} else if (read_argc() < num) {
		new hcmd[32], hinfo[128], hflag;
		get_concmd(cid, hcmd, charsmax(hcmd), hflag, hinfo, charsmax(hinfo), level);
		console_print(id, "%L:  %s %s", id, "USAGE", hcmd, hinfo);
		return false;
	} else {
		console_print(id, "%L", id, "NO_ACC_COM");
		return false;
	}
}

stock locate_player(const identifier[]) 
{
	new player = 0;

	if (identifier[0]=='#' && identifier[1]) {
		player = find_player("k", str_to_num(identifier[1]));
	}

	if (!player)	{
		player = find_player("c", identifier);
	}

	if (!player) {
		player = find_player("bl", identifier);
	}
	
	if (!player) {
		player = find_player("d", identifier);
	}
	
	return player;
}

stock bool:is_authid(authid[])
{
	return (equal(authid, "STEAM_", 6) || equal(authid, "VALVE_", 6));
}

#if defined STATUS_VALUE
stock show_status_text(id, player, bool:status_text, message[192])
{
	static temp[35], i;
    
	if (is_user_alive(player)) {
		for (i = 0; i < MAX_REPLACES; i++ ) {
			switch (i)  {
				
				case RE_HP: {
					num_to_str(get_user_health(player), temp, charsmax(temp));
				}

				case RE_RANK: {
					copy(temp, charsmax(temp), g_PlayersData[player][gTitle]);
				}

				case RE_NAME: {
					if(status_text) {
						copy(temp, charsmax(temp), "%p2");
					} else {
						get_user_name(player, temp, charsmax(temp));
					}
				}
			
				default: {
					copy(temp, charsmax(temp), "");
				}
			}

			replace(message, 191, g_Replaces[i], temp);
		}
	}
	trim(message);

	// server_print("message '%s'", message);

	message_begin(MSG_ONE_UNRELIABLE, g_StatusText, _, id);
	write_byte(0);
	write_string(message);
	message_end();
}
#endif

public plugin_natives()
{
	register_library("ServerRanks");

	register_native("server_ranks_inited", "native_inited");
	register_native("server_ranks_player_loaded", "native_player_loaded");
	register_native("server_ranks_get_player_experience", "native_get_player_experience");
	register_native("server_ranks_get_player_points", "native_get_player_points");
	register_native("server_ranks_get_player_level", "native_get_player_level");
	register_native("server_ranks_get_user_rankname", "native_get_user_rankname");
	register_native("server_ranks_get_level_bonus", "native_get_level_bonus");
}

public native_inited(plugin_id, param_nums)
{
	if (param_nums != 1) {
		return 0;
	}

	if (get_param(1) == 1) {
		new players[32], num;
		get_players(players, num, "ch");

		for (new i = 0; i < num; i++) {
			playerLoad(players[i]);
		}

		g_Inited = true;
		ExecuteForward(g_Forwards[fwdInitialized], g_Return);
	} else {
		set_fail_state("Error init storage");
	}

	return 1;
}

public native_player_loaded(plugin_id, param_nums)
{
	static id;

	if (param_nums != 3) {
		return 0;
	}

	id = get_param(1);

	if (!isValidPlayer(id)) {
		return 0;
	}

	g_PlayersData[id][gExp] = get_param(2);
	g_PlayersData[id][gPoints] = get_param(3);
	playerLevel(id, true);
	g_ShowInfo[id] = get_pcvar_num(g_Cvars[cvarShowLoad]);
	SetBit(g_IsLoaded, id);
	return 1;
}

public native_get_player_experience(plugin_id, param_nums)
{
	static id;

	if (param_nums != 1) {
		return 0;
	}

	id = get_param(1);

	if (!isValidPlayer(id)) {
		return 0;
	}

	return g_PlayersData[id][gExp];
}

public native_get_player_points(plugin_id, param_nums)
{
	static id;

	if (param_nums != 1) {
		return 0;
	}

	id = get_param(1);

	if (!isValidPlayer(id)) {
		return 0;
	}

	return g_PlayersData[id][gPoints];
}

public native_get_player_level(plugin_id, param_nums)
{
	static id;

	if (param_nums != 1) {
		return 0;
	}

	id = get_param(1);

	if (!isValidPlayer(id)) {
		return 0;
	}

	return g_PlayersData[id][gLevel];
}

public native_get_user_rankname(plugin_id, param_nums)
{
	static id;

	if (param_nums != 3) {
		return 0;
	}

	id = get_param(1);

	if (!isValidPlayer(id)) {
		return 0;
	}

	set_string(2, g_PlayersData[id][gTitle], get_param(3));
	return 1;
}

public native_get_level_bonus(plugin_id, param_nums)
{
	static level;

	if (param_nums != 1) {
		return 0;
	}

	level = get_param(1);

	if (level < 0 || level >= g_LevelsNum) {
		return 0;
	}

	ArrayGetArray(g_LevelsData, level, g_Level);
	return g_Bonus ? g_Level[gLevelBonus] : 0;
}
