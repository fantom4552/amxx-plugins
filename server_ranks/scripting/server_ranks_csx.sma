#pragma semicolon 1

#include <amxmodx>
#include <csstats>
#include <csx>
#include <server_ranks>

#define PLUGIN "Server Ranks CSX"
#define VERSION "1.0"
#define AUTHOR "F@nt0M"

public plugin_init()
{
	register_plugin(PLUGIN, VERSION, AUTHOR);
}

public plugin_cfg()
{
	server_ranks_inited(1);
}

public server_ranks_player_load(id)
{
	new stats[8], bodyhits[8], stats2[4];
	get_user_stats(id, stats, bodyhits);
	get_user_stats2(id, stats2);

	server_print("LOAD STATS FOR %d", id);
	server_print("%d, %d, %d, %d, %d, %d, %d, %d", stats[0], stats[1], stats[2], stats[3], stats[4], stats[5], stats[6], stats[7]);
	
	// if (g_XPhead > 1 && g_B != 0)
	// 	PlayerXP[id] = ((stats[2] * g_XPhead + ((stats[0] - stats[2]) * g_XPv)) + (stats2[1] * g_Def + stats2[3] * g_Pl))
	// else if (g_XPhead <= 1 && g_B != 0)
	// 	PlayerXP[id] = (stats[0] * g_XPv + (stats2[1] * g_Def + stats2[3] * g_Pl))
	// else if (g_XPhead > 1 && g_B == 0)
	// 	PlayerXP[id] = (stats[2] * g_XPhead + ((stats[0] - stats[2]) * g_XPv))
	// else
	// 	PlayerXP[id] = stats[0] * g_XPv

	server_ranks_player_loaded(id, stats[0]);
}

public server_ranks_player_save(id, exp)
{
	#pragma unused id
	#pragma unused exp
}