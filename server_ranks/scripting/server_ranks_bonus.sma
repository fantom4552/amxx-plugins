#pragma semicolon 1

#include <amxmodx>
#include <cstrike>
#include <fakemeta>
#include <hamsandwich>
#include <server_ranks>

#define PLUGIN "Server Ranks Bonus"
#define VERSION "1.0"
#define AUTHOR "F@nt0M"

#define BONUS_DEFUSE		(1<<0)  /* flag "a" */
#define BONUS_HEGRENADE		(1<<1)  /* flag "b" */
#define BONUS_FLASHBANG		(1<<2)  /* flag "c" */
#define BONUS_FLASHBANG2	(1<<3)  /* flag "d" */
#define BONUS_SMOKEGRENADE	(1<<4)  /* flag "e" */
#define BONUS_ARMOR			(1<<5)  /* flag "f" */
#define BONUS_DEAGLE		(1<<6)  /* flag "g" */

#define isValidWeapon(%1) (1 <= %1 <= 30)

const PRIMARY_WEAPONS_BIT_SUM = (1 << CSW_SCOUT) | (1<< CSW_XM1014) | (1 << CSW_M3) | (1<< CSW_MAC10)|(1<<CSW_AUG)|(1<<CSW_UMP45)|(1<<CSW_SG550)|(1<<CSW_GALIL)|(1<<CSW_FAMAS)|(1<<CSW_AWP)|(1<<CSW_MP5NAVY)|(1<<CSW_M249)|(1<<CSW_M4A1)|(1<<CSW_TMP)|(1<<CSW_G3SG1)|(1<<CSW_SG552)|(1<<CSW_AK47)|(1<<CSW_P90);
const SECONDARY_WEAPONS_BIT_SUM = (1<<CSW_P228)|(1<<CSW_DEAGLE)|(1<<CSW_ELITE)|(1<<CSW_FIVESEVEN)|(1<<CSW_USP)|(1<<CSW_GLOCK18);

new const g_WeaponsBrammo[] = {
	0, 52, 0, 90, 0, 32, 0, 100, 90, 0, 120, 100, 100, 90, 90, 90, 100, 120, 30, 120, 200, 32, 90, 120, 90, 0, 35, 90, 90, 0, 100
};


new bool:g_DefuseInMap = false;
new g_IgnoreFlag;

public plugin_init()
{
	register_plugin(PLUGIN, VERSION, AUTHOR);

	RegisterHam(Ham_Spawn, "player", "EventPlayerSpawn", 1);

	if(engfunc(EngFunc_FindEntityByString,FM_NULLENT, "classname", "func_bomb_target")) {
		g_DefuseInMap = true;
	}

	register_cvar("sr_ignore_flag", "");
}

public server_ranks_started()
{
	new flag[5];
	get_cvar_string("sr_ignore_flag", flag, 4);
	if (flag[0]) {
		g_IgnoreFlag = read_flags(flag);
	} else {
		g_IgnoreFlag = 0;
	}
}

public EventPlayerSpawn(id)
{
	if (!isIgnore(id) && !is_user_bot(id)) {
		new level = server_ranks_get_player_level(id);
		new bonus = server_ranks_get_level_bonus(level);
		
		if (g_DefuseInMap && (bonus & BONUS_DEFUSE)) {
			cs_set_user_defuse(id, 1);
		}

		if (bonus & BONUS_HEGRENADE) {
			give_item_ex(id, "weapon_hegrenade");
		}

		if (bonus & BONUS_FLASHBANG) {
			give_item_ex(id, "weapon_flashbang");
		}

		if (bonus & BONUS_FLASHBANG2) {
			cs_set_user_bpammo(id, CSW_FLASHBANG, 2);
		}

		if (bonus & BONUS_SMOKEGRENADE) {
			give_item_ex(id, "weapon_smokegrenade");
		}

		if (bonus & BONUS_ARMOR) {
			cs_set_user_armor(id, 100, CS_ARMOR_VESTHELM);
		}

		if (bonus & BONUS_DEAGLE) {
			give_weapon_ex(id, "weapon_deagle", true);
		}
	}
}

stock give_item_ex(index, const item[]) 
{
	if (!equal(item, "weapon_", 7) && !equal(item, "ammo_", 5) && !equal(item, "item_", 5) && !equal(item, "tf_weapon_", 10)) {
		return 0;
	}

	new ent = engfunc(EngFunc_CreateNamedEntity, engfunc(EngFunc_AllocString, item));
	if (!pev_valid(ent)) {
		return 0;
	}

	new Float:origin[3];
	pev(index, pev_origin, origin);
	set_pev(ent, pev_origin, origin);
	set_pev(ent, pev_spawnflags, pev(ent, pev_spawnflags) | SF_NORESPAWN);
	dllfunc(DLLFunc_Spawn, ent);

	new save = pev(ent, pev_solid);
	dllfunc(DLLFunc_Touch, ent, index);
	if (pev(ent, pev_solid) != save) {
		return ent;
	}

	engfunc(EngFunc_RemoveEntity, ent);

	return -1;
}

stock give_weapon_ex(id, weapon[], bool:forse = false)
{
	static weapons[32], num, wid, wtype, wname[32], bool:exists, ent, ent_box, i;

	wid = get_weaponid(weapon);
	if ((1 << wid) & PRIMARY_WEAPONS_BIT_SUM) {
		wtype = PRIMARY_WEAPONS_BIT_SUM;
	} else if ((1 << wid) & SECONDARY_WEAPONS_BIT_SUM) {
		wtype = SECONDARY_WEAPONS_BIT_SUM;
	}
	
	num = 0;
	get_user_weapons(id, weapons, num);
	
	exists = false;
	for (i = 0; i < num; i++) {
		if (!forse && wid == weapons[i]) {
			exists = true;
			break;
		}

		if ((1 << weapons[i]) & wtype) {
			get_weaponname(weapons[i], wname, 31);

			ent = -1;
			while ((ent = engfunc(EngFunc_FindEntityByString, ent, "classname", wname)) && pev(ent, pev_owner) != id) {}
			if(!ent) continue;

			engclient_cmd(id, "drop", wname);

			ent_box = pev(ent, pev_owner);
			if (!ent_box || ent_box == id) continue;

			dllfunc(DLLFunc_Think, ent_box);
		}
	}

	if (isValidWeapon(wid)) {
		if (!exists) {
			#if defined DROP_SHIELD
			engclient_cmd(id, "drop", "weapon_shield");
			#endif
			give_item_ex(id, weapon);
		}

		cs_set_user_bpammo(id, wid, g_WeaponsBrammo[wid]);
	}
}

stock bool:isIgnore(id)
{
	if (g_IgnoreFlag > 0) {
		return bool:(get_user_flags(id) & g_IgnoreFlag);
	}

	return false;
}