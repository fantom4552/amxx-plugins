#if defined _server_ranks_core_included
	#endinput
#endif

#define _server_ranks_core_included

#pragma reqlib ServerRanks

#if !defined AMXMODX_NOAUTOLOAD
	#pragma loadlib ServerRanks
#endif

native server_ranks_inited(status);
native server_ranks_player_loaded(id, experience, points);
native server_ranks_get_player_experience(id);
native server_ranks_get_player_points(id);
native server_ranks_get_player_level(id);
native server_ranks_get_user_rankname(id, const rankname[], len);
native server_ranks_get_level_bonus(level);

forward server_ranks_started();
forward server_ranks_initialized();
forward server_ranks_player_load(id, const authid[]);
forward server_ranks_player_save(id, experience, points, const authid[]);
forward server_ranks_player_levelup(id, experience, points);