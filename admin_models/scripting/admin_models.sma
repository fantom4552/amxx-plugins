#pragma semicolon 1

#include <amxmodx>
#include <hamsandwich>
#include <cstrike>

#define PLUGIN "Admin Models"
#define VERSION "1.4.1"
#define AUTHOR "F@nt0M"

#define isValidUser(%1) (is_user_connected(%1) && is_user_alive(%1))
#define CheckFlag(%1,%2) (get_user_flags(%1) & %2)

#define PLAYER_MODEL_CT "admin_ct"
#define PLAYER_MODEL_TE "admin_te"

new pcvar_Enable;
new pcvar_Flag;
new g_Flag;

public plugin_init()
{
	register_plugin(PLUGIN, VERSION, AUTHOR);

	RegisterHam(Ham_Spawn, "player", "player_spawn", 1);

	pcvar_Enable = register_cvar("amx_admin_models", "1");
	pcvar_Flag = register_cvar("amx_admin_models_flag", "z");
}

public plugin_cfg()
{
	flag_cvar_change();

	#if defined hook_cvar_change
	hook_cvar_change(pcvar_Flag, "flag_cvar_change");
	#endif
}

public flag_cvar_change()
{
	new flag[1];
	get_pcvar_string(pcvar_Flag, flag, 1);
	g_Flag = read_flags(flag);
}

public plugin_precache()
{
	new model[128];
	format(model, 127, "models/player/%s/%s.mdl", PLAYER_MODEL_CT, PLAYER_MODEL_CT);
	precache_model(model);
	format(model, 127, "models/player/%s/%s.mdl", PLAYER_MODEL_TE, PLAYER_MODEL_TE);
	precache_model(model);
}

public player_spawn(id)
{
	if (isValidUser(id)) {
		if (get_pcvar_num(pcvar_Enable) && CheckFlag(id, g_Flag)) {
			switch (cs_get_user_team(id)) {
				case CS_TEAM_T: {
					cs_set_user_model(id, PLAYER_MODEL_TE);
				}

				case CS_TEAM_CT: {
					cs_set_user_model(id, PLAYER_MODEL_CT);
				}

				default: {
					cs_reset_user_model(id);
				}
			}
		} else {
			cs_reset_user_model(id);
		}
	}
}