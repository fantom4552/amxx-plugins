#pragma semicolon 1

#include <amxmodx>
#include <hamsandwich>
#include <cstrike>

#define PLUGIN "Admin Models"
#define VERSION "1.4.1"
#define AUTHOR "F@nt0M"

#define isValidUser(%1) (is_user_connected(%1) && is_user_alive(%1))
// #define isValidTeam(%1) (CS_TEAM_T <= cs_get_user_team(%1) <= CS_TEAM_CT)
#define CheckFlag(%1,%2) (get_user_flags(%1) & %2)

#define PLAYER_MODEL_CT "admin_ct"
#define PLAYER_MODEL_TE "admin_te"

new pcvar_Enable;
// new pcvar_Flag;
// new g_Flag;

enum _:AdminModel {
	ModelType,
	ModelAuthData[32],
	ModelCT[32],
	ModelT[32]
};

new g_AdminModel[AdminModel];
new Array:g_AdminModels;

public plugin_init()
{
	register_plugin(PLUGIN, VERSION, AUTHOR);

	RegisterHam(Ham_Spawn, "player", "player_spawn", 1);

	pcvar_Enable = register_cvar("amx_admin_models", "1");
	// pcvar_Flag = register_cvar("amx_admin_models_flag", "z");

	g_AdminModels = ArrayCreate(AdminModel);
}

public plugin_precache()
{
	if (loadConfig()) {
		set_fail_state("Error load config file");
	}
}

public plugin_end()
{
	ArrayDestroy(g_AdminModels);
}

// public plugin_cfg()
// {
// 	flag_cvar_change();

// 	#if defined hook_cvar_change
// 	hook_cvar_change(pcvar_Flag, "flag_cvar_change");
// 	#endif
// }

// public flag_cvar_change()
// {
// 	new flag[1];
// 	get_pcvar_string(pcvar_Flag, flag, 1);
// 	g_Flag = read_flags(flag);
// }

// public plugin_precache()
// {
// 	new model[128];
// 	format(model, 127, "models/player/%s/%s.mdl", PLAYER_MODEL_CT, PLAYER_MODEL_CT);
// 	precache_model(model);
// 	format(model, 127, "models/player/%s/%s.mdl", PLAYER_MODEL_TE, PLAYER_MODEL_TE);
// 	precache_model(model);
// }

public player_spawn(id)
{
	if (isValidUser(id)) {
		if (get_pcvar_num(pcvar_Enable) && CheckFlag(id, ADMIN_USER)) {
			switch (cs_get_user_team(id)) {
				case CS_TEAM_T: {
					cs_set_user_model(id, PLAYER_MODEL_TE);
				}

				case CS_TEAM_CT: {
					cs_set_user_model(id, PLAYER_MODEL_CT);
				}

				default: {
					cs_reset_user_model(id);
				}
			}
		} else {
			cs_reset_user_model(id);
		}
	}
}

bool:loadConfig()
{
	new configFile[128];
	get_localinfo("amxx_configsdir", configFile, charsmax(configFile));
	format(configFile, charsmax(configFile), "%s/admin_models.ini", configFile);

	if (!file_exists(configFile)) {
		log_amx("Config file '%s' not found", configFile);
		return false;
	}

	new line[256], model_ct[32], model_t[32], flags[5], authData[32];

	new file = fopen(configFile, "r");
	if (!file) {
		return false;
	}

	while (!feof(file)) {
		fgets(file, line, charsmax(line));
		trim(line);

		if (!line[0] || line[0] == ';') {
			continue;
		}

		model_ct[0] = '^0';
		model_t[0] = '^0';
		flags[0] = '^0';
		authData[0] = '^0';

		parse(line, model_ct, charsmax(model_ct), model_t, charsmax(model_t), flags, charsmax(flags), authData, charsmax(authData));

		g_AdminModel[ModelType] = read_flags(flags);
		copy(g_AdminModel[ModelAuthData], charsmax(g_AdminModel[ModelAuthData]), authData);
		copy(g_AdminModel[ModelCT], charsmax(g_AdminModel[ModelCT]), model_ct);
		copy(g_AdminModel[ModelT], charsmax(g_AdminModel[ModelT]), model_t);

		ArrayPushArray(g_AdminModels, g_AdminModel);
	}

	fclose(file);
	return true;
}