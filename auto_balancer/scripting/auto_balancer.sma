#pragma semicolon 1

#define BALANCE_IMMUNITY ADMIN_IMMUNITY

// mp_autoteambalance 1
// mp_limitteams 2

#include <amxmodx>
#include <cstrike>

#if (AMXX_VERSION_NUM < 183) || defined NO_NATIVE_COLORCHAT
	#include <colorchat>
#else
	#define DontChange print_team_default
#endif

#define PLUGIN "Auto Team Balancer"
#define VERSION "1.0"
#define AUTHOR "F@nt0M"

#define CheckBit(%1,%2)		(%1 &	(1 << (%2 & 31)))
#define SetBit(%1,%2)		(%1 |=	(1 << (%2 & 31)))
#define ClearBit(%1,%2)		(%1 &= ~(1 << (%2 & 31)))
#define ResetBit(%1)		(%1 = 0)

new Float:g_JoinedTeam[MAX_PLAYERS+1] = {-1.0, ...};
new g_pcvarEnable;
new g_pcvarImmunity;
new g_IsTransfered;

public plugin_init()
{
	register_plugin(PLUGIN, VERSION, AUTHOR);

	register_dictionary("auto_balancer.txt");

	g_pcvarEnable = register_cvar("ab_enable", "1");
	g_pcvarImmunity = register_cvar("ab_immunity", "1");

	register_logevent("LogEvent_JoinTeam", 3, "1=joined team");
	register_event("TextMsg", "Auto_Team_Balance_Next_Round", "a", "1=4", "2&#Auto_Team");
}

public LogEvent_JoinTeam()
{
	new loguser[80], name[32], id;
	read_logargv(0, loguser, 79);
	parse_loguser(loguser, name, 31);
	id = get_user_index(name);

	g_JoinedTeam[id] = get_gametime();
}


public Auto_Team_Balance_Next_Round()
{
	if (get_pcvar_num(g_pcvarEnable)) {
		if (balance_teams()) {
			client_print_color(0, print_team_default,  "%L", LANG_PLAYER, "TEAM_BALANCED");
		}
	}
}

bool:balance_teams()
{
	new players_ct[32], num_ct, players_t[32], num_t;
	get_players(players_ct, num_ct, "eh", "CT");
	get_players(players_t, num_t, "eh", "TERRORIST");

	new count = num_ct - num_t;
	new CsTeams:team;

	if (count > 0) {
		team = CS_TEAM_CT;
	} else if(count < 0) {
		team = CS_TEAM_T;
	} else {
		return false;
	}

	ResetBit(g_IsTransfered);

	count = abs(count / 2);

	new last;
	while (count > 0) {

		if (team == CS_TEAM_CT) {
			last = get_player_move(players_ct, num_ct);
		} else {
			last = get_player_move(players_t, num_t);
		}

		if (!last) {
			return false;
		}

		cs_set_user_team_custom(last, (team == CS_TEAM_CT ? CS_TEAM_T : CS_TEAM_CT));
		SetBit(g_IsTransfered, last);

		count--;
	}

	return true;
}

get_player_move(players[32], num)
{
	new i, id, last = 0;
	new bool:g_Immunity = bool:get_pcvar_num(g_pcvarImmunity);

	for (i = 0; i < num; i++) {
		id = players[i];

		if(CheckBit(g_IsTransfered, id)) {
			continue;
		}

		if (g_Immunity && (get_user_flags(id) & BALANCE_IMMUNITY)) {
			continue;
		}

		if (g_JoinedTeam[id] > g_JoinedTeam[last]) {
			last = id;
		}
	}

	return last;
}

stock cs_set_user_team_custom(id, CsTeams:team)
{
	switch(team) {
		case CS_TEAM_T: {
			if (cs_get_user_defuse(id)) {
				cs_set_user_defuse(id, 0);
			}
		}

		case CS_TEAM_CT: {
			if (user_has_weapon(id, CSW_C4)) {
				engclient_cmd(id, "drop", "weapon_c4");
			}
		}
	}

	cs_set_user_team(id, team);
}