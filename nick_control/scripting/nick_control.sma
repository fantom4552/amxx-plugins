#pragma semicolon 1

#include <amxmodx>
#include <regex>

#define PLUGIN "Nick Control"
#define VERSION "0.1"
#define AUTHOR "F@nt0M"

#define MAX_NICK_CHANGE 3
#define PUNISH_TYPE 1
#define PUNISH_MSG "Bad Nick"
#define BAT_TYME 10080

#define MAX_PLAYERS 32

#define TASK_FQ 60.0
#define TASK_ID 15154

#define CheckBit(%1,%2)		(%1 &	(1 << (%2 & 31)))
#define SetBit(%1,%2)		(%1 |=	(1 << (%2 & 31)))
#define ClearBit(%1,%2)		(%1 &= ~(1 << (%2 & 31)))
#define ResetBit(%1)		(%1 = 0)

new g_IsBanning;

new g_NickChangeNum[MAX_PLAYERS+1];

new Regex:g_SpamRegex;
new g_Return;

public plugin_init()
{
	register_plugin(PLUGIN, VERSION, AUTHOR);

	register_event("SayText", "eventChangeName", "a", "2=#Cstrike_Name_Change");

	arrayset(g_NickChangeNum, 0, MAX_PLAYERS+1);
	ResetBit(g_IsBanning);

	new r_error[128];
	g_SpamRegex = regex_compile("(?:\d{1,3}\.\d{1,3}\.\d{1,3}\.\d{1,3}|http://)", g_Return, r_error, 127);
}

public client_authorized(id)
{
	if (isSpamNick(id)) {
		kickPlayer(id);
	} else {
		remove_task(TASK_ID+id);
		g_NickChangeNum[id] = 0;
		ClearBit(g_IsBanning, id);
	}
}

public eventChangeName()
{
	new id = read_data(1);

	g_NickChangeNum[id]++;
	if (isSpamNick(id)) {
		kickPlayer(id);
	} else if (g_NickChangeNum[id] > MAX_NICK_CHANGE) {
		punishPlayer(id);
	} else if(!task_exists(TASK_ID+id)) {
		set_task(TASK_FQ, "decrementNickChangeNum", TASK_ID+id);
	}
}

public decrementNickChangeNum(id)
{
	id -= TASK_ID;
	g_NickChangeNum[id]--;
}

punishPlayer(id)
{
	if (!CheckBit(g_IsBanning, id)) {
		SetBit(g_IsBanning, id);
		#if PUNISH_TYPE == 1
		banPlayer(id);
		#else
		kickPlayer(id);
		#endif
	}
}

stock bool:isSpamNick(id)
{
	new name[32];
	get_user_name(id, name, 31);
	server_print("name %s", name);
	if (regex_match_c(name, g_SpamRegex, g_Return) > 0) {
		return true;
	}
	return false;
}

stock banPlayer(id)
{
	server_cmd("amx_ban %d #%d ^"%s^"", BAT_TYME, get_user_userid(id), PUNISH_MSG);
}

stock kickPlayer(id)
{
	server_cmd("kick #%d ^"%s^"", get_user_userid(id), PUNISH_MSG);
}