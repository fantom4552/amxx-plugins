CREATE TABLE `users_prefix` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `authid` varchar(255) DEFAULT NULL,
  `prefix` varchar(255) DEFAULT NULL,
  `active` tinyint(1) unsigned DEFAULT '0',
  `created` int(255) unsigned DEFAULT '0',
  `expired` int(255) unsigned DEFAULT '0',
  PRIMARY KEY (`id`),
  UNIQUE KEY `prefix_main_idx` (`authid`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8