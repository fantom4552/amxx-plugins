// executed if the db is initialized
forward prefixes_initialized();

forward prefixes_set_user(const id, const prefix[]);

// native get_user_prefix(id, const prefix[], maxlen);