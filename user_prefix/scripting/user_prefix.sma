#pragma semicolon 1

#define ENABLE_MYSQL
#define ADMINLOAD
// #define COLORED_TRANSLIT
#define COLORED_TRANSLIT_NG

#include <amxmodx>

#if defined ENABLE_MYSQL
#include <sqlx>
#endif // ENABLE_MYSQL

#if defined ENABLE_MYSQL && defined ADMINLOAD
#include <adminload>
#endif // ENABLE_MYSQL && ADMINLOAD

#if defined COLORED_TRANSLIT_NG
#include <colored_translit_ng>
#endif

#if defined COLORED_TRANSLIT
#include <colored_translit>
#endif

#if AMXX_VERSION_NUM == 183
#define client_has_disconnect client_disconnected
#define create_new_cvar create_cvar
#else
#define client_has_disconnect client_disconnect
#define create_new_cvar register_cvar
#endif

#define PLUGIN "User Prefix"
#define VERSION "1.7"
#define AUTHOR "F@nt0M"

#define isValidPlayer(%1) (1 <= %1 <= g_MaxPlayers)

#define CheckBit(%1,%2)		(%1 &	(1 << (%2 & 31)))
#define SetBit(%1,%2)		(%1 |=	(1 << (%2 & 31)))
#define ClearBit(%1,%2)		(%1 &= ~(1 << (%2 & 31)))
#define ResetBit(%1)		(%1 = 0)

new Prefixes_Initialized;

new bool:g_Init;
new Trie:g_Prefixes;
new g_MaxPlayers;
new pcvar_AuthType;
new g_PlayerPrefix[MAX_PLAYERS+1][64];
new g_IsLoaded;

#if defined ENABLE_MYSQL
new Handle:g_DBTuple;
#endif // ENABLE_MYSQL

public plugin_init()
{
	register_plugin(PLUGIN, VERSION, AUTHOR);
	
	register_concmd("amx_reloadprefix", "cmdReload");

	#if defined ENABLE_MYSQL
	register_cvar("prefix_table_name", "users_prefix");
	#endif // ENABLE_MYSQL

	pcvar_AuthType = register_cvar("prefix_auth_type", "0");

	Prefixes_Initialized = CreateMultiForward("prefixes_initialized", ET_IGNORE);

	g_Prefixes = TrieCreate();
	g_MaxPlayers = get_maxplayers();

	g_Init = false;
}

public plugin_end()
{
	TrieDestroy(g_Prefixes);
}

public plugin_natives()
{
	register_native("get_user_prefix", "native_get_user_prefix");
}

#if defined COLORED_TRANSLIT
public ct_message_format(id)
{
	if (g_Init && CheckBit(g_IsLoaded, id)) {
		ct_add_to_msg(CT_MSGPOS_PREFIX, "[^x04%s^x01]", g_PlayerPrefix[id]);
	}
}
#endif

#if defined COLORED_TRANSLIT_NG
public ctng_message_format(id, say_team)
{
	#pragma unused say_team

	if (g_Init && CheckBit(g_IsLoaded, id)) {
		ctng_add_prefix(0, "[^x04%s^x01]", g_PlayerPrefix[id]);
		ctng_block_prefix(1);
	}
}
#endif

public cmdReload(id, level, cid)
{
	if (get_user_flags(id) & ADMIN_CFG) {

		#if defined ENABLE_MYSQL
		if (db_load()) {
			g_Init = true;
		} else if (file_load()) {
			g_Init = true;
		} else {
			log_amx("[%s] Error load prefixes", PLUGIN);
		}
		#else
		if (file_load()) {
			g_Init = true;
		} else {
			log_amx("[%s] Error load prefixes", PLUGIN);
		}
		#endif

		if (g_Init) {
			check_players();
			new ret;
			ExecuteForward(Prefixes_Initialized, ret);
		}

		console_print(id, "[%s] Reloaded complete", PLUGIN);
	}
}

#if defined ENABLE_MYSQL && defined ADMINLOAD
public adminload_sql_initialized(Handle:DBTuple, const dbPrefix[])
{
	if (DBTuple == Empty_Handle) {
		log_amx("[Error] DB Info Tuple from adminload is empty!");
		return PLUGIN_CONTINUE;
	}

	g_DBTuple = DBTuple;

	if (db_load()) {
		g_Init = true;
	} else if (file_load()) {
		g_Init = true;
	} else {
		log_amx("[%s] Error load prefixes", PLUGIN);
	}

	if (g_Init) {
		check_players();
		new ret;
		ExecuteForward(Prefixes_Initialized, ret);
	}

	return PLUGIN_CONTINUE;
}
public adminload_changename(id, name[])
{
	if (g_Init && get_pcvar_num(pcvar_AuthType) == 0) {
		check_player(id, name);
	}
}
#else // ENABLE_MYSQL && ADMINLOAD
public client_infochanged(id) 
{ 
	new newname[32], oldname[32];
	if (g_Init && get_pcvar_num(pcvar_AuthType) == 0) {
		get_user_info(id, "name", newname,31);
		get_user_name(id, oldname,31);

		if (!equali(newname, oldname)) {
			check_player(id, newname);
		}
	}
}

public plugin_cfg()
{
	#if defined ENABLE_MYSQL
	new config_file[128];
	get_localinfo( "amxx_configsdir", config_file, 127);
	server_cmd("exec %s/sql.cfg", config_file);
	server_exec();
	set_task(0.1, "init_db");

	#else // ENABLE_MYSQL

	if (file_load()) {
		g_Init = true;
	} else {
		log_amx("[%s] Error load prefixes", PLUGIN);
	}

	if (g_Init) {
		check_players();
		new ret;
		ExecuteForward(Prefixes_Initialized, ret);
	}

	#endif // ENABLE_MYSQL
}
#endif // ENABLE_MYSQL && ADMINLOAD

#if defined ENABLE_MYSQL && !defined ADMINLOAD
public init_db()
{
	SQL_SetAffinity("mysql");
	g_DBTuple = SQL_MakeStdTuple();

	if (db_load()) {
		g_Init = true;
	} else if (file_load()) {
		g_Init = true;
	} else {
		log_amx("[%s] Error load prefixes", PLUGIN);
	}

	if (g_Init) {
		check_players();
		new ret;
		ExecuteForward(Prefixes_Initialized, ret);
	}
}
#endif

public client_putinserver(id)
{
	ClearBit(g_IsLoaded, id);
	if (g_Init && !is_user_bot(id) && !is_user_hltv(id)) {
		check_player(id);
	}
}

check_players()
{
	for (new id = 1; id <= g_MaxPlayers; id++) {
		if (is_user_connected(id) && !is_user_bot(id) && !is_user_hltv(id)) {
			check_player(id);		
		}
	}
}

check_player(id, const name[] = "")
{
	static authid[32];

	get_user_auth(id, authid, 31, name);

	if (TrieKeyExists(g_Prefixes, authid)) {
		TrieGetString(g_Prefixes, authid, g_PlayerPrefix[id], 63);
		SetBit(g_IsLoaded, id);
	} else {
		g_PlayerPrefix[id][0] = '^0';
		ClearBit(g_IsLoaded, id);
	}
}

get_user_auth(id, authid[], len, const name[] = "")
{
	switch (get_pcvar_num(pcvar_AuthType)) {
		case 1: {
			get_user_authid(id, authid, len);
		}

		case 2: {
			get_user_ip(id, authid, len, 1);
		}

		default: {
			if (name[0]) {
				copy(authid, len, name);
			} else {
				get_user_name(id, authid, len);
			}
		}
	}
}

#if defined ENABLE_MYSQL
bool:db_load()
{
	new table_name[32];
	get_cvar_string("prefix_table_name", table_name, 31);

	if (!table_name[0]) {
		log_amx("[%s] SQL error: Empty table name", PLUGIN);
		return false;
	}

	new pquery[256];
	format(pquery, 255, "SELECT authid, prefix FROM `%s` WHERE active = 1 AND (expired = 0 OR expired > UNIX_TIMESTAMP(NOW()))", table_name);

	new error[512], errornum;
	
	new Handle:SqlConn = SQL_Connect(g_DBTuple, errornum, error, 512);
	if (SqlConn == Empty_Handle) {
		log_amx("[%s] SQL error: can't connect: '%s'", PLUGIN, error);
		return false;
	}
	
	SQL_QueryAndIgnore(SqlConn, "SET NAMES utf8");

	new Handle:query = SQL_PrepareQuery(SqlConn, pquery);
	
	if (!SQL_Execute(query)) {
		errornum = SQL_QueryError(query, error, 511);
		log_amx("[SQLError] Message: %s (%d)", error, errornum);
		log_amx("[SQLError] Query statement: %s", pquery);
		return false;
    }

	TrieClear(g_Prefixes); 
	
	new authid[32], prefix[64];
	new count = 0;

	while (SQL_MoreResults(query)) {
		SQL_ReadResult(query, 0, authid, 31);
		SQL_ReadResult(query, 1, prefix, 63);

		TrieSetString(g_Prefixes, authid, prefix);
					
		SQL_NextRow(query);
		
		count++;
	}
	
	log_amx("[%s] Loaded %d prefixes from DB", PLUGIN, count);

	SQL_FreeHandle(query);
	SQL_FreeHandle(SqlConn);

	return true;
}
#endif // ENABLE_MYSQL

bool:file_load()
{
	new filename[128], config_file[128];
	get_localinfo( "amxx_configsdir", config_file, 127);
	format(filename, 127, "%s/user_prefix.ini", config_file);

	if (!file_exists(filename)) {
		return false;
	}

	new file = fopen(filename, "r");

	if (!file) {
		return false;
	}
	
	new text[512];
	new nickname[32], prefix[64];
	new count = 0;

	TrieClear(g_Prefixes); 
		
	while (!feof(file)) {
		fgets(file, text, 511);
		
		trim(text);
		
		if(!text[0] || text[0] == ';'){
			continue;
		}
		
		nickname[0] = 0;
		prefix[0] = 0;

		parse(text, nickname, 31, prefix, 63);
		TrieSetString(g_Prefixes, nickname, prefix);

		count++;
	}

	fclose(file);

	log_amx("[%s] Loaded %d prefixes from file", PLUGIN, count);

	return true;
}

public native_get_user_prefix(plugin_id, param_nums)
{
	static id;
	
	if (param_nums != 3) {
		return -1;
	}
	
	id = get_param(1);

	if (!isValidPlayer(id) || !CheckBit(g_IsLoaded, id)) {
		return 0;
	}

	set_string(2, g_PlayerPrefix[id], get_param(3));
	return 1;
}