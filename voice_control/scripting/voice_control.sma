#pragma semicolon 1

// TODO: Set PDATA_ADMIN
// TODO: No set_SPECTATOR if ADMIN
// TODO: unmute by time

#define ADMINLOAD
// #define MUTE_INFO
#define MAX_PLAYERS 32
#define TASK_TICK_FQ 5.0
#define ADMIN_VOICE	ADMIN_CHAT

#include <amxmodx>
#include <amxmisc>
#include <engine>
#include <fakemeta>
#include <nvault>
#include <time>

#if (AMXX_VERSION_NUM < 183) || defined NO_NATIVE_COLORCHAT
	#include <colorchat>
#else
	#define DontChange print_team_default
#endif

#if AMXX_VERSION_NUM == 183
#define client_has_disconnect client_disconnected
#define create_new_cvar create_cvar
#else
#define client_has_disconnect client_disconnect
#define create_new_cvar register_cvar
#endif

#if defined ADMINLOAD
#include <adminload>
#endif

#define charmax(%1) (sizeof(%1) - 1)

#define PLUGIN "Voice Control"
#define VERSION "1.3.2"
#define AUTHOR "F@nt0M"

#define TASK_TICK_ID 3248
#define TASK_INFO_ID 3249
#define TEAM_SPECTATOR 3

#define PDATA_DISCONNECTED 0
#define PDATA_CONNECTING 1
#define PDATA_CONNECTED 2
#define PDATA_ADMIN 3
#define PDATA_MUTED 4
#define PDATA_ALIVE 5
#define PDATA_SPECTATOR 6

#define set_user_state(%1,%2) (g_PlayerState[%1] = (1<<%2))
#define add_user_state(%1,%2) (g_PlayerState[%1] |= (1<<%2))
#define get_user_state(%1,%2) (g_PlayerState[%1] & (1<<%2))
#define remove_user_state(%1,%2) (g_PlayerState[%1] &= ~(1<<%2))
#define is_user_disconnected(%1) (get_user_state(%1, PDATA_DISCONNECTED) || (!is_user_connecting(%1) && !get_user_state(%1, PDATA_CONNECTED)))

new g_PlayerState[MAX_PLAYERS+1];
new g_ClientSettings[MAX_PLAYERS+1][2];
new g_Muted[MAX_PLAYERS+1];
new g_PlayerMuted[MAX_PLAYERS+1][MAX_PLAYERS+1];
new g_SysTime;
new g_MuteStorage;
new bool:g_DeadMute = true;
new g_ShowActivity;

public plugin_init()
{
	register_plugin(PLUGIN, VERSION, AUTHOR);
	
	register_dictionary("voice_control.txt");
	register_dictionary("common.txt");
	register_dictionary("time.txt");

	register_concmd("amx_mute", "cmdMute", ADMIN_VOICE, "<steamID or nickname or #authid or IP> <time>");
	register_concmd("amx_unmute", "cmdUnmute", ADMIN_VOICE, "<steamID or nickname or #authid or IP>");

	register_forward(FM_Voice_SetClientListening, "fwdVoiceSetclientlistening");
	register_event("VoiceMask", "EventVoiceMask", "b");

	register_event("ResetHUD", "EventResetHUD", "b");
	register_event("DeathMsg", "EventDeathMsg", "a");

	register_clcmd("say /mute", "say_mute");
	register_clcmd("say_team /mute", "say_mute");

	set_task(TASK_TICK_FQ, "tick", TASK_TICK_ID, _, _, "b");

	arrayset(g_PlayerState, PDATA_DISCONNECTED, MAX_PLAYERS+1);

	g_MuteStorage = nvault_open("mute_list");
	if (g_MuteStorage == INVALID_HANDLE) {
		log_amx("[Error] Error open mute list file");
	}

	create_new_cvar("amx_mute_alives", "1");
}

public plugin_cfg()
{
	g_ShowActivity = get_cvar_num("amx_show_activity");

	g_DeadMute = (get_cvar_num("amx_mute_alives") == 1) ? true : false;
	#if defined MUTE_INFO
	set_task(125.0, "display_mute", TASK_INFO_ID, _, _, "b");
	#endif
}

public plugin_end()
{
	remove_task(TASK_TICK_ID);
	remove_task(TASK_INFO_ID);
	if (g_MuteStorage != INVALID_HANDLE) {
		nvault_close(g_MuteStorage);
	}
}

#if defined MUTE_INFO
public display_mute()
{
	client_print_color(0, print_team_default, "^1[ ^4MUTE ^1] %L ^4/mute", LANG_PLAYER, "SAY_MUTE");
}
#endif

#if defined ADMINLOAD
public client_authorized(id)
{
	set_user_state(id, PDATA_CONNECTING);
}
public adminload_connect(id, flags)
{
	if (flags & ADMIN_VOICE) {
		add_user_state(id, PDATA_ADMIN);
	}
}

public adminload_disconnect(id)
{
	remove_user_state(id, PDATA_ADMIN);
}
#else
public client_authorized(id)
{
	set_user_state(id, PDATA_CONNECTING);
	if (get_user_flags(id) & ADMIN_VOICE) {
		add_user_state(id, PDATA_ADMIN);
	}
}
#endif

public client_putinserver(id)
{
	remove_user_state(id, PDATA_CONNECTING);
	add_user_state(id, PDATA_CONNECTED);
	storage_check(id);
}

public client_has_disconnect(id)
{
	set_user_state(id, PDATA_DISCONNECTED);
}

public tick()
{
	static id;

	g_SysTime = get_systime(0);
	
	for (id = 1; id <= MAX_PLAYERS; id++) {
		if(is_user_disconnected(id)) {
			continue;
		}

		if(get_user_team(id) == TEAM_SPECTATOR) {
			add_user_state(id, PDATA_SPECTATOR);
		} else {
			remove_user_state(id, PDATA_SPECTATOR);
		}

		if (get_user_state(id, PDATA_MUTED) && !player_check(id)) {
			player_unmute(id);
			storage_remove(id);
			time_passed_notify(id);
		}
	}
}

public EventResetHUD(id)
{
	if(is_user_alive(id)) {
		add_user_state(id, PDATA_ALIVE);
	}
}

public EventDeathMsg()
{
	remove_user_state(read_data(2), PDATA_ALIVE);
}

public cmdMute(id, level, cid)
{
	if (!cmd_access(id, level, cid, 3, true)) {
		return PLUGIN_HANDLED;
	}

	new args[64], playerStr[32], muteTimeStr[10], playerName[32], muteTime, expired;
	read_args(args, 63);
	parse(args, playerStr, 31, muteTimeStr, 9);

	if (!is_str_num(muteTimeStr)) {
		console_print(id, "[VoiceControl] Bad mute time");
		return PLUGIN_HANDLED;
	}

	muteTime = str_to_num(muteTimeStr);

	if (muteTime > 0) {
		expired = get_systime(0) + (muteTime * 60);
	} else {
		expired = 0;
	}

	new admin_name[32];
	get_user_name(id, admin_name, 31);

	new player = locate_player(playerStr);

	if (!player || is_user_disconnected(player)) {
		if (is_authid(playerStr) && storage_add_authid(playerStr, expired)) {
			console_print(id, "Player ^"%s^" successfully muted", playerStr);
			log_amx("[MUTE] Admin ^"%s^" muted ^"%s^" for %d minutes", admin_name, playerStr, muteTime);
		} else {
			console_print(id, "[VoiceControl] Player %s not found", playerStr);
		}
		return PLUGIN_HANDLED;
	}

	get_user_name(player, playerName, 31);

	if (get_user_state(player, PDATA_ADMIN)) {
		console_print(id, "[VoiceControl] Player ^"%s^" is admin", playerName);
		return PLUGIN_HANDLED;
	}

	if (get_user_state(player, PDATA_MUTED)) {
		console_print(id, "[VoiceControl] Player ^"%s^" already muted", playerName);
		return PLUGIN_HANDLED;
	}

	player_mute(player, expired);
	storage_add(player, expired);

	console_print(id, "[VoiceControl] Player ^"%s^" successfully muted", playerName);
	mute_notify(player, id, muteTime);

	log_amx("[MUTE] Admin ^"%s^" muted ^"%s^" for %d minutes", admin_name, playerName, muteTime);

	return PLUGIN_HANDLED;
}

public cmdUnmute(id, level, cid)
{
	if (!cmd_access(id, level, cid, 2, true)) {
		return PLUGIN_HANDLED;
	}

	new args[64], playerStr[32], playerName[32];
	read_args(args, 63);
	parse(args, playerStr, 31);

	new admin_name[32];
	get_user_name(id, admin_name, 31);

	new player = locate_player(playerStr);

	if (!player || is_user_disconnected(player)) {
		if (is_authid(playerStr) && storage_remove_authid(playerStr)) {
			console_print(id, "Player ^"%s^" successfully unmuted", playerStr);
			log_amx("[MUTE] Admin ^"%s^" unmuted ^"%s^"", admin_name, playerStr);
		} else {
			console_print(id, "[VoiceControl] Player %s not found", playerStr);
		}
		return PLUGIN_HANDLED;
	}

	get_user_name(player, playerName, 31);

	if (!get_user_state(player, PDATA_MUTED)) {
		console_print(id, "[VoiceControl] Player ^"%s^" not muted", playerName);
		return PLUGIN_HANDLED;
	}

	player_unmute(player);
	storage_remove(player);

	console_print(id, "[VoiceControl] Player ^"%s^" successfully unmuted", playerName);
	unmute_notify(player, id);

	log_amx("[MUTE] Admin ^"%s^" unmuted ^"%s^"", admin_name, playerName);

	return PLUGIN_HANDLED;
}

public EventVoiceMask(id)
{
	g_ClientSettings[id][0] = read_data(1);
	g_ClientSettings[id][1] = read_data(2);
}

public fwdVoiceSetclientlistening(receiver, sender, listen)
{
	if (!g_ClientSettings[receiver][0] || g_ClientSettings[receiver][1] & (1<<(sender-1))) {
		return FMRES_IGNORED;
	}
	
	if (receiver == sender) {
		return FMRES_IGNORED;
	}

	if (g_DeadMute && !get_user_state(sender, PDATA_ALIVE) && get_user_state(receiver, PDATA_ALIVE) && !get_user_state(sender, PDATA_ADMIN) && !get_user_state(receiver, PDATA_ADMIN) && !get_user_state(sender, PDATA_SPECTATOR)) {
		engfunc(EngFunc_SetClientListening, receiver, sender, false);
		forward_return(FMV_CELL, false);
		return FMRES_SUPERCEDE;
	}

	if (get_user_state(sender, PDATA_MUTED)) {
		engfunc(EngFunc_SetClientListening, receiver, sender, false);
		forward_return(FMV_CELL, false);
		return FMRES_SUPERCEDE;
	}

	if (g_PlayerMuted[receiver][sender]) {
		engfunc(EngFunc_SetClientListening, receiver, sender, false);
		forward_return(FMV_CELL, false);
		return FMRES_SUPERCEDE;
	}

	
	engfunc(EngFunc_SetClientListening, receiver, sender, true);
	forward_return(FMV_CELL, true);
	return FMRES_IGNORED;
}

public say_mute(id)
{
	if (!show_user_menu(id)) {
		client_print_color(id, print_team_default, "^1[ ^4MUTE ^1] %L", LANG_PLAYER, "MUTE_DISABLED");
	}

	return PLUGIN_HANDLED;
}

bool:show_user_menu(id)
{
	new menu = menu_create("\rMute:", "menu_handler");
	new callback = menu_makecallback("menu_callback");

	new i, num, players[32], name[32], index[3], title[128];

	formatex(title, charmax(title), "%L", LANG_SERVER, "BACK");
	menu_setprop(menu, MPROP_BACKNAME, title);
	
	formatex(title, charmax(title), "%L", LANG_SERVER, "MORE");
	menu_setprop(menu, MPROP_NEXTNAME, title);

	formatex(title, charmax(title), "%L^n^n\y%s by %s", LANG_SERVER, "EXIT", PLUGIN, AUTHOR);
	menu_setprop(menu, MPROP_EXITNAME, title);

	get_players(players, num, "ch");

	if (num <= 1) {
		return false;
	}

	for (i = 0; i < num; i++ ) {	
		if (players[i] != id) {
			get_user_name(players[i], name, 31);
		
			num_to_str(players[i], index, 2);
			menu_additem(menu, name, index, 0, callback);
		}
	}

	menu_display(id, menu);

	return true;
}

public menu_handler(id, menu, item)
{
	if (item == MENU_EXIT) {
		menu_destroy(menu);
		return PLUGIN_HANDLED;
	}

	new access, info[3], text[64], callback;
	new oldmenu, newmenu, page;

	menu_item_getinfo(menu, item, access, info, 2, text, 63, callback);

	new player = str_to_num(info);
	
	g_PlayerMuted[id][player] = !g_PlayerMuted[id][player];
	display_result(id, player);

	player_menu_info(id, oldmenu, newmenu, page);
	menu_display(id, menu, page);
 	
	return PLUGIN_CONTINUE;
}

public menu_callback(id, menu, item)
{
	new access, info[3], name[32], title[128], callback;
	menu_item_getinfo(menu, item, access, info, 2, title, 127, callback);

	new player = str_to_num(info);

	get_user_name(player, name, 31);
	if (g_PlayerMuted[id][player]) {
		format(title, 127, "\y%s \r[MUTED]", name);
	} else {
		format(title, 127, "\y%s", name);
	}

	menu_item_setname(menu, item, title);

	// if (get_user_state(player, PDATA_ADMIN)) {
	// 	return ITEM_DISABLED;
	// }

	return ITEM_ENABLED;
}

display_result(id, player)
{
	new name[32];
	get_user_name(player, name, 31);
	
	if (g_PlayerMuted[id][player]) {
		client_print_color(id, print_team_default, "^1[ ^4MUTE ^1] %L", LANG_PLAYER, "MUTE_YES", name);
	} else {
		client_print_color(id, print_team_default, "^1[ ^4MUTE ^1] %L", LANG_PLAYER, "MUTE_NO", name);
	}
}

mute_notify(id, admin_id, muteTime)
{
	new muteLength[64], message[192], playerName[32], adminName[32];

	if (muteTime > 0)  {
		get_time_length(id, muteTime, timeunit_minutes, muteLength, 63);
	}

	server_print("MUTE %d - %s", muteTime, muteLength);

	get_user_name(id, playerName, 31);
	get_user_name(admin_id, adminName, 31);

	if (g_ShowActivity == 1) {
		if (muteTime > 0) {
			format(message, 191, "%L", LANG_SERVER, "MUTE_ANNOUNCE", playerName, muteLength);
		} else {
			format(message, 191, "%L", LANG_SERVER, "MUTE_ANNOUNCE_PERM", playerName);
		}
	} else if (g_ShowActivity == 2) {
		if (muteTime > 0) {
			format(message, 191, "%L", LANG_SERVER, "MUTE_ANNOUNCE_2", adminName, playerName, muteLength);
		} else {
			format(message, 191, "%L", LANG_SERVER, "MUTE_ANNOUNCE_2_PERM", adminName, playerName);
		}
	}

	client_print_color(0, print_team_default, message);
}

unmute_notify(id, admin_id)
{
	new message[192], playerName[32], adminName[32];

	get_user_name(id, playerName, 31);
	get_user_name(admin_id, adminName, 31);

	if (g_ShowActivity == 1) {
		format(message, 191, "%L", LANG_SERVER, "UNMUTE_ANNOUNCE", playerName);
	} else if (g_ShowActivity == 2) {
		format(message, 191, "%L", LANG_SERVER, "UNMUTE_ANNOUNCE_2", adminName, playerName);
	}

	client_print_color(0, print_team_default, message);
}

time_passed_notify(id)
{
	new playerName[32];
	get_user_name(id, playerName, 31);
	client_print_color(0, print_team_default, "%L", LANG_SERVER, "UNMUTE_TIME_PASSED", playerName);
}

bool:player_check(id)
{
	if(g_Muted[id] == 0 || (g_Muted[id] > 0 && g_SysTime < g_Muted[id])) {
		return true;
	}
	
	return false;
}

player_mute(id, expired)
{
	add_user_state(id, PDATA_MUTED);
	g_Muted[id] = expired;
	set_speak(id, SPEAK_MUTED);
}

player_unmute(id)
{
	remove_user_state(id, PDATA_MUTED);
	g_Muted[id] = -1;
	set_speak(id, SPEAK_NORMAL);
}

bool:storage_check(id)
{
	new data[32], authid[37], timestamp, expired;
	if (g_MuteStorage == INVALID_HANDLE) {
		return false;
	}

	get_user_authid(id, authid, 36);
	if (!nvault_lookup(g_MuteStorage, authid, data, 31, timestamp)) {
		return false;
	}
		
	if (!is_str_num(data)) {
		return false;
	}
			
	expired = abs(str_to_num(data));

	if(expired == 0 || expired > g_SysTime) {
		player_mute(id, expired);
	} else {
		storage_remove(id);
	}

	return true;
}

bool:storage_add(id, expired) 
{
	new data[32], authid[37];

	if (g_MuteStorage == INVALID_HANDLE) {
		return false;
	}

	num_to_str(expired, data, 31);
	get_user_authid(id, authid, 36);
	nvault_set(g_MuteStorage, authid, data);
	return true;
}

bool:storage_remove(id)
{
	new authid[37];

	if (g_MuteStorage == INVALID_HANDLE) {
		return false;
	}

	get_user_authid(id, authid, 36);
	nvault_remove(g_MuteStorage, authid); 
	return true;
}

bool:storage_add_authid(authid[], expired)
{
	new data[32];

	if (g_MuteStorage == INVALID_HANDLE) {
		return false;
	}

	num_to_str(expired, data, 31);
	nvault_set(g_MuteStorage, authid, data);
	return true;
}

bool:storage_remove_authid(authid[])
{
	new data[32], timestamp;

	if (g_MuteStorage == INVALID_HANDLE) {
		return false;
	}

	if (!nvault_lookup(g_MuteStorage, authid, data, 31, timestamp)) {
		return false;
	}
		
	if (!is_str_num(data)) {
		return false;
	}

	nvault_remove(g_MuteStorage, authid); 
	return true;
}

stock locate_player(const identifier[]) 
{
	new player = 0;

	if (identifier[0]=='#' && identifier[1]) {
		player = find_player("k", str_to_num(identifier[1]));
	}

	if (!player)	{
		player = find_player("c", identifier);
	}

	if (!player) {
		player = find_player("bl", identifier);
	}
	
	if (!player) {
		player = find_player("d", identifier);
	}
	
	return player;
}

stock bool:is_authid(authid[])
{
	return (equal(authid, "STEAM_", 6) || equal(authid, "VALVE_", 6));
}