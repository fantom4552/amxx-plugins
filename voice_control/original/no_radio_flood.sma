#include <amxmodx>

#define PLUGIN "No Radio Flood"
#define VERSION "1.1"
#define AUTHOR "Starsailor"

new Float:gRadio[33]
new pTime,pBlock

new szRadioCommands[][] =
{
	"radio1", "coverme", "takepoint", "holdpos", "regroup", "followme", "takingfire",
	"radio2", "go", "fallback", "sticktog", "getinpos", "stormfront", "report",
	"radio3", "roger", "enemyspot", "needbackup", "sectorclear", "inposition", "reportingin", "getout", "negative", "enemydown"
}


public plugin_init()
{	
	register_plugin(PLUGIN, VERSION, AUTHOR)
	
	for (new i=0; i<sizeof szRadioCommands; i++)
	{
		register_clcmd(szRadioCommands[i], "cmdRadio")
	}
	pTime = register_cvar("nrf_time","5")  //0 Disabled
	pBlock = register_cvar("nrf_block_fith","1")
	
	register_message(get_user_msgid("SendAudio"),"FireInTheHole")
	register_cvar("srf_version",VERSION,FCVAR_SERVER|FCVAR_SPONLY) //Srf = Stop Radio Flooding :D, Last Plugin name
	
}

public cmdRadio(id)
{
	
	new iTime = get_pcvar_num(pTime)
	
	if(!is_user_alive(id))
	{
		return PLUGIN_HANDLED_MAIN
	}
	
	if(iTime > 0)
	{  		
		new Float:fTime = get_gametime()
		
		if(fTime - gRadio[id] < iTime)
		{
			
			client_print(id,print_center,"Sorry, but you cannot abuse this command!")
			
			return PLUGIN_HANDLED_MAIN
		}
		
		gRadio[id] = fTime
	}
	
	return PLUGIN_CONTINUE
}

public FireInTheHole(msgid,msg_dest,msg_entity)
{
	
	if(get_msg_args() < 3 || get_msg_argtype(2) != ARG_STRING)
	{
		return PLUGIN_HANDLED
	}
	
	new szArg[32]
	
	get_msg_arg_string(2,szArg,31)
	
	if(equal(szArg ,"%!MRAD_FIREINHOLE") && get_pcvar_num(pBlock))
	{
		return PLUGIN_HANDLED;
	}
	
	return PLUGIN_CONTINUE;
}
/* AMXX-Studio Notes - DO NOT MODIFY BELOW HERE
*{\\ rtf1\\ ansi\\ deff0{\\ fonttbl{\\ f0\\ fnil Tahoma;}}\n\\ viewkind4\\ uc1\\ pard\\ lang3082\\ f0\\ fs16 \n\\ par }
*/
