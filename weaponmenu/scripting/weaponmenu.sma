#pragma semicolon 1

#define ADMINLOAD
#define AMX_WARMING

#include <amxmodx>
#include <cstrike>
#include <engine>
#include <fakemeta>

#if defined ADMINLOAD
#include <adminload>
#endif

#if defined AMX_WARMING
#include <amx_warming>
#endif

#if (AMXX_VERSION_NUM < 183) || defined NO_NATIVE_COLORCHAT
	#include <colorchat>
#else
	#define DontChange print_team_default
#endif

#define PLUGIN "WEAPONMENU"
#define VERSION "2.0"
#define AUTHOR "F@nt0M"

#define ALLOW_IN_ROUND 3
#define ADMIN_LEVEL ADMIN_LEVEL_A

#define MAX_PLAYERS 32
#define MAX_WEAPONS 30

#define MENU_TASK_FQ 20.0
#define MENU_TASK_ID 4657

#define MAX_SECTION_WEAPONS 10

#define CheckBit(%1,%2)		(%1 &	(1 << (%2 & 31)))
#define SetBit(%1,%2)		(%1 |=	(1 << (%2 & 31)))
#define ClearBit(%1,%2)		(%1 &= ~(1 << (%2 & 31)))
#define ResetBit(%1)		(%1 = 0)

const PRIMARY_WEAPONS_BIT_SUM = (1 << CSW_SCOUT) | (1<< CSW_XM1014) | (1 << CSW_M3) | (1<< CSW_MAC10)|(1<<CSW_AUG)|(1<<CSW_UMP45)|(1<<CSW_SG550)|(1<<CSW_GALIL)|(1<<CSW_FAMAS)|(1<<CSW_AWP)|(1<<CSW_MP5NAVY)|(1<<CSW_M249)|(1<<CSW_M4A1)|(1<<CSW_TMP)|(1<<CSW_G3SG1)|(1<<CSW_SG552)|(1<<CSW_AK47)|(1<<CSW_P90);
const SECONDARY_WEAPONS_BIT_SUM = (1<<CSW_P228)|(1<<CSW_DEAGLE)|(1<<CSW_ELITE)|(1<<CSW_FIVESEVEN)|(1<<CSW_USP)|(1<<CSW_GLOCK18);

new const g_WeaponsBrammo[] = {
	0, 52, 0, 90, 0, 32, 0, 100, 90, 0, 120, 100, 100, 90, 90, 90, 100, 120, 30, 120, 200, 32, 90, 120, 90, 0, 35, 90, 90, 0, 100
};

enum Weapon {
	WeaponTitle[32],
	WeaponName[16],
	bool:WeaponPack
}

new const g_Weapons[][Weapon] = {
	{"", "", false},
	{"Glock 18", "glock18", false},			// 1
	{"USP", "usp", false},					// 2
	{"P228", "p228", false},				// 3
	{"Deagle", "deagle", false},			// 4
	{"FiveSeven", "fiveseven", false},		// 5
	{"Elite", "elite", false},				// 6
	{"Benelli M3 Super", "m3", false},		// 7
	{"Benelli XM1014", "xm1014", false},	// 8
	{"Ingram MAC-10", "mac10", false},		// 9
	{"Steyr TMP", "tmp", false},			// 10
	{"HK MP5", "mp5navy", false},			// 11
	{"HK UMP 45", "ump45", false},			// 12
	{"FN P90", "p90", false},				// 13
	{"M4A1", "m4a1", false},				// 14
	{"AK47", "ak47", false},				// 15
	{"AWP", "awp", false},					// 16
	{"AUG", "aug", false},					// 17
	{"SG552", "sg552", false},				// 18
	{"Famas", "famas", false},				// 19
	{"Galil", "galil", false},				// 20
	{"Scout", "scout", false},				// 21
	{"G3SG1", "g3sg1", false},				// 22
	{"SG550", "sg550", false},				// 23
	{"Кусачки", "DEFUSE", false},			// 24
	{"Всі гранати", "GRENADES", false},		// 25
	{"Бронежилет", "ARMOR", false},			// 26
	{"M4A1 + Pack", "m4a1", true},			// 27
	{"AK47 + Pack", "ak47", true},			// 28
	{"AWP + Pack", "awp", true},			// 29
	{"AUG + Pack", "aug", true},			// 30
	{"Famas + Pack", "famas", true},		// 31
	{"Scout + Pack", "scout", true}			// 32
};

enum Section {
	SectionTitle[32],
	SectionWeapons[MAX_SECTION_WEAPONS]
}

new g_Sections[][Section] = {
	{"Пістолети", {1, 2, 3, 4, 5, 6, 0, 0, 0, 0}},
	{"Автомати", {7, 8, 9, 10, 11, 12, 13, 0, 0, 0}},
	{"Гвинтівки", {14, 15, 16, 17, 18, 19, 20, 21, 22, 23}},
	{"Спорядження", {24, 25, 26, 0, 0, 0, 0, 0, 0, 0}},
	{"Комплекти зброї", {27, 28, 29, 30, 31, 32, 0, 0, 0, 0}}
};

new g_RoundNum = 0;
new bool:g_StartGame = false;
new g_IsAdmin;
new g_IsUsed;


public plugin_init()
{
	register_plugin(PLUGIN, VERSION, AUTHOR);

	register_clcmd("weaponmenu", "cmd_weaponmenu");

	#if !defined AMX_WARMING
	register_event("TextMsg","event_start_game","a","2=#Game_Commencing", "2=#Game_will_restart_in");
	#endif

	register_event("HLTV", "event_round_start", "a", "1=0", "2=0");
}

#if defined ADMINLOAD
public adminload_connect(id, flags)
{
	if (flags & ADMIN_LEVEL) {
		SetBit(g_IsAdmin, id);
		client_cmd(id, "bind ^"-^" ^"weaponmenu^"");
	}
}

public adminload_disconnect(id)
{
	ClearBit(g_IsAdmin, id);
}
#else
public client_putinserver(id)
{
	new flags = get_user_flags(id);
	if (flags & ADMIN_LEVEL) {
		SetBit(g_IsAdmin, id);
		client_cmd(id, "bind ^"-^" ^"weaponmenu^"");
	}
}

public client_disconnect(id)
{
	ClearBit(g_IsAdmin, id);
}
#endif

#if defined AMX_WARMING
public warming_finish()
{
	g_StartGame = true;
}
#else
public event_start_game()
{
	g_StartGame = true;
}
#endif

public event_round_start()
{
	if (g_StartGame) {
		g_RoundNum = 0;
		g_StartGame = false;
	} else {
		g_RoundNum++;
	}

	ResetBit(g_IsUsed);
}

public cmd_weaponmenu(id)
{
	if (!CheckBit(g_IsAdmin, id)) {
		return PLUGIN_HANDLED;
	}

	if (g_RoundNum <= ALLOW_IN_ROUND) {
		client_print_color(id, print_team_default, "^3[^4MENU^3]^1 Доступно з %d-го раунду!", ALLOW_IN_ROUND);
		return PLUGIN_HANDLED;
	}

	if (CheckBit(g_IsUsed, id)) {
		client_print_color(id, print_team_default, "^3[^4MENU^3]^1 Ви вже використовували. Зачекайте...");
		return PLUGIN_HANDLED;
	}

	new menu = menu_create("\rОберіть розділ", "menu_handler");

	new i, numi, stri[3];
	numi = sizeof(g_Sections);
	for (i = 0; i < numi; i++) {
		num_to_str(i, stri, 2);
		menu_additem(menu, g_Sections[i][SectionTitle], stri);
	}
	
	menu_setprop(menu, MPROP_EXITNAME, "Вихід");

	displayMenu(id, menu, 0);
	return PLUGIN_HANDLED;
}

public menu_handler(id, menu, item)
{
	if (item == MENU_EXIT) {
		destroyMenu(id, menu);
		return PLUGIN_HANDLED;
	}

	if (g_RoundNum <= ALLOW_IN_ROUND) {
		destroyMenu(id, menu);
		client_print_color(id, print_team_default, "^3[^4MENU^3]^1 Доступно з %d-го раунду!", ALLOW_IN_ROUND);
		return PLUGIN_HANDLED;
	}

	if (CheckBit(g_IsUsed, id)) {
		destroyMenu(id, menu);
		client_print_color(id, print_team_default, "^3[^4MENU^3]^1 Ви вже використовували. Зачекайте...");
		return PLUGIN_HANDLED;
	}

	new access, data[6], title[64], callback;
	menu_item_getinfo(menu, item, access, data, 5, title, 63, callback);

	new key = str_to_num(data);

	destroyMenu(id, menu);

	menu = menu_create("\rОберіть зброю", "weapon_handler");

	new i, wid, stri[3];
	for (i = 0; i < MAX_SECTION_WEAPONS; i++) {
		wid = g_Sections[key][SectionWeapons][i];
		if (wid > 0) {
			num_to_str(wid, stri, 2);
			menu_additem(menu, g_Weapons[wid][WeaponTitle], stri);
		}
	}
	
	menu_setprop(menu, MPROP_EXITNAME, "Вихід");

	displayMenu(id, menu, 0);

	return PLUGIN_HANDLED;
}

public weapon_handler(id, menu, item)
{
	if (item == MENU_EXIT) {
		destroyMenu(id, menu);
		return PLUGIN_HANDLED;
	}

	new access, data[6], title[64], callback;
	menu_item_getinfo(menu, item, access, data, 5, title, 63, callback);

	new key = str_to_num(data);

	new weapon[32];

	switch(key)
	{
		case 24: {
			cs_set_user_defuse(id, 1);
		}

		case 25: {
			give_item_ex(id, "weapon_hegrenade");
			give_item_ex(id, "weapon_flashbang");
			cs_set_user_bpammo(id, CSW_FLASHBANG, 2);
			give_item_ex(id, "weapon_smokegrenade");
		}

		case 27: {
			cs_set_user_armor(id, 100, CS_ARMOR_VESTHELM);
		}

		default: {

			copy(weapon, 31, g_Weapons[key][WeaponName]);
			format(weapon, 31, "weapon_%s", weapon);
			give_weapon_ex(id, weapon, true);

			if (g_Weapons[key][WeaponPack]) {
				give_item_ex(id, "weapon_hegrenade");
				give_item_ex(id, "weapon_flashbang");
				cs_set_user_bpammo(id, CSW_FLASHBANG, 2);
				give_item_ex(id, "weapon_smokegrenade");

				if (cs_get_user_team(id) == CS_TEAM_CT) {
					cs_set_user_defuse(id, 1);
				}

				cs_set_user_armor(id, 100, CS_ARMOR_VESTHELM);
				give_weapon_ex(id, "weapon_deagle", true);
			}
		}
	}

	SetBit(g_IsUsed, id);

	destroyMenu(id, menu);
	return PLUGIN_HANDLED;
}

stock displayMenu(id, menu, page = 0)
{
	menu_display(id, menu, page);
	if (task_exists(MENU_TASK_ID+id)) {
		cancelMenu(MENU_TASK_ID+id);
		remove_task(MENU_TASK_ID+id);
	}
	set_task(MENU_TASK_FQ, "cancelMenu", MENU_TASK_ID+id);
}

stock resetMenu(id, menu)
{
	new oldmenu, newmenu, page;

	player_menu_info(id, oldmenu, newmenu, page);
	menu_display(id, menu, page);
	change_task(MENU_TASK_ID+id, MENU_TASK_FQ);
}

stock destroyMenu(id, menu)
{
	remove_task(MENU_TASK_ID+id);
	menu_destroy(menu);
}

public cancelMenu(id)
{
	id -= MENU_TASK_ID;
	menu_cancel(id);
}

stock give_item_ex(index, const item[]) 
{
	if (!equal(item, "weapon_", 7) && !equal(item, "ammo_", 5) && !equal(item, "item_", 5) && !equal(item, "tf_weapon_", 10)) {
		return 0;
	}

	new ent = engfunc(EngFunc_CreateNamedEntity, engfunc(EngFunc_AllocString, item));
	if (!pev_valid(ent)) {
		return 0;
	}

	new Float:origin[3];
	pev(index, pev_origin, origin);
	set_pev(ent, pev_origin, origin);
	set_pev(ent, pev_spawnflags, pev(ent, pev_spawnflags) | SF_NORESPAWN);
	dllfunc(DLLFunc_Spawn, ent);

	new save = pev(ent, pev_solid);
	dllfunc(DLLFunc_Touch, ent, index);
	if (pev(ent, pev_solid) != save) {
		return ent;
	}

	engfunc(EngFunc_RemoveEntity, ent);

	return -1;
}

stock give_weapon_ex(id, weapon[], bool:forse = false)
{
	static weapons[32], num, wid, wtype, wname[32], bool:exists, ent, ent_box, i;

	wid = get_weaponid(weapon);
	if ((1 << wid) & PRIMARY_WEAPONS_BIT_SUM) {
		wtype = PRIMARY_WEAPONS_BIT_SUM;
	} else if ((1 << wid) & SECONDARY_WEAPONS_BIT_SUM) {
		wtype = SECONDARY_WEAPONS_BIT_SUM;
	}
	
	num = 0;
	get_user_weapons(id, weapons, num);
	
	exists = false;
	for (i = 0; i < num; i++) {
		if (!forse && wid == weapons[i]) {
			exists = true;
			break;
		}

		if ((1 << weapons[i]) & wtype) {
			get_weaponname(weapons[i], wname, 31);

			ent = -1;
			while ((ent = engfunc(EngFunc_FindEntityByString, ent, "classname", wname)) && pev(ent, pev_owner) != id) {}
			if(!ent) continue;

			engclient_cmd(id, "drop", wname);

			ent_box = pev(ent, pev_owner);
			if (!ent_box || ent_box == id) continue;

			dllfunc(DLLFunc_Think, ent_box);
		}
	}

	if (is_valid_weapon(wid)) {
		if (!exists) {
			give_item_ex(id, weapon);
		}

		cs_set_user_bpammo(id, wid, g_WeaponsBrammo[wid]);
	}
}

stock bool:is_valid_weapon(wid)
{
	return (1 <= wid <= MAX_WEAPONS);
}