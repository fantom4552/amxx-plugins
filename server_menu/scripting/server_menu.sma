#pragma semicolon 1

#define ADMINLOAD

#include <amxmodx>

#if defined ADMINLOAD
#include <adminload>
#endif

#define ENABLE_INFO
// #define ENABLE_CALLFUNC
#define HUD_MESSAGE_FREQ 30.0

#define PLUGIN "Server Menu"
#define VERSION "1.4.1"
#define AUTHOR "F@nt0M"

#if !defined charsmax
#define charsmax(%1) (sizeof(%1) - 1)
#endif

new g_ServerName[128];
new g_ServerAddress[32];

new bool:g_Enable = false;
new g_PlayerMenu;
new g_AdminMenu;

public plugin_init()
{
	register_plugin(PLUGIN, VERSION, AUTHOR);

	register_dictionary("server_menu.txt");
	register_dictionary("common.txt");

	register_clcmd("amxmodmenu", "admin_menu");

	register_clcmd("menu", "player_menu");
	register_clcmd("say /menu", "player_menu");
	register_clcmd("say_team /menu", "player_menu");
	register_concmd("nightvision", "player_menu");
}

public plugin_cfg()
{
	get_pcvar_string(get_cvar_pointer("hostname"), g_ServerName, 127);
	get_user_ip(0, g_ServerAddress, 31);

	g_PlayerMenu = menu_create("player_menu", "menu_handler");
	configMenu(g_PlayerMenu, false);

	g_AdminMenu = menu_create("admin_menu", "menu_handler");
	configMenu(g_AdminMenu, false);

	if (loadMenu()) {
		g_Enable = true;
		#if defined ENABLE_INFO
		set_task(HUD_MESSAGE_FREQ, "task_hudmsg", _, _, _, "b");
		#endif
	}
}

public plugin_end()
{
	menu_destroy(g_PlayerMenu);
	menu_destroy(g_AdminMenu);
}

public client_putinserver(id)
{
	if (g_Enable) {
 		client_cmd(id, "bind ^"F3^" ^"menu^"");
 	}

 	if (get_user_flags(id) & ADMIN_MENU) {
 		client_cmd(id, "bind ^"=^" ^"amxmodmenu^"");
 	}
}

#if defined ENABLE_INFO
public task_hudmsg()
{
	set_hudmessage(0, 255, 0, -1.0, 0.0, 0, 6.0, 6.0);
	show_hudmessage(0, "%L", LANG_SERVER, "SERVER_MENU_HELP");
}
#endif

public player_menu(id)
{
	if (g_Enable) {
		menu_display(id, g_PlayerMenu, 0);
	}
 	
 	return PLUGIN_HANDLED;
}

public admin_menu(id)
{
	if (!(get_user_flags(id) & ADMIN_MENU)) {
		client_print(id, print_console, "You have no right to this command");
		return PLUGIN_HANDLED;
	}

	#if defined ADMINLOAD
	static expired, title[128];
	expired = get_expired_days(id);
	if (expired > 0) {
		format(title, 127, "%L^n%L", LANG_SERVER, "SERVER_MENU_ADMIN", LANG_SERVER, "ADMIN_EXPIRED", expired);
	} else{
		format(title, 127, "%L", LANG_SERVER, "SERVER_MENU_ADMIN");		
	}
	menu_setprop(g_AdminMenu, MPROP_TITLE, title);
	#endif

	menu_display(id, g_AdminMenu, 0);

	return PLUGIN_HANDLED;
}

public menu_handler(id, menu, item)
{
	if (item == MENU_EXIT) {
		return PLUGIN_HANDLED;
	}

	new access, command[128], title[128], callback;
	menu_item_getinfo(menu, item, access, command, charsmax(command), title, charsmax(title), callback);

	// log_amx("Player %d, Cmd: ^"%s^"", id, command);

	#if defined ENABLE_CALLFUNC
	if (contain(command, ":")) {
		strtok(command, title, charsmax(title), command, charsmax(command), ':');
		callfunc(id, command, title);
	} else {
		client_cmd(id, command);
	}
	#else
	client_cmd(id, command);
	#endif

	return PLUGIN_CONTINUE;
}

bool:loadMenu()
{
	new configDir[128], configFile[128];
	new text[512], title[128], command[128], access_str[10], access;

	get_localinfo("amxx_configsdir", configDir, charsmax(configDir));
	format(configFile, charsmax(configFile), "%s/server_menu/server_menu.ini", configDir);

	if (!file_exists(configFile)) {
		log_amx("Could not found config file %s. Menu disable", configFile);
		return false;
	}

	new menuCount = 0;
	new bool:playerMenu = true;

	new file = fopen(configFile, "r");
	if (!file) {
		log_amx("Could not open config file %s. Menu disable", configFile);
		return false;
	}

	while (!feof(file)) {
		fgets(file, text, charsmax(text));
		trim(text);

		if (!text[0] || text[0]==';' || (text[0]=='/' && text[1]=='/')) {
			continue;
		}

		if (equali(text, "[PLAYER]")) {
			playerMenu  = true;
		} else if (equali(text, "[ADMIN]")) {
			playerMenu  = false;
		} else {

			title[0] = '^0';
			command[0] = '^0';
			access_str[0] = '^0';

			if (parse(text, title, charsmax(title), command, charsmax(command), access_str, charsmax(access_str)) > 1) {
				access = access_str[0] ? read_flags(access_str) : 0;

				// log_amx("Flags: ^"%s^", Title: ^"%s^", Command: ^"%s^", Access: %d", flags_str, title, command, access_str);

				if (playerMenu) {
					menu_additem(g_PlayerMenu, title, command, access);
					menuCount++;
				} else {
					menu_additem(g_AdminMenu, title, command, access);
				}
			}
		}
	}

	fclose(file);

	if (menuCount == 0) {
		log_amx("Load zero items from config. Menu disable");
		return false;
		
	}

	log_amx("Load %d items from config. Menu enable", menuCount);
	return true;
}

configMenu(menu, bool:adminmenu = false)
{
	new text[256];

	menu_setprop(menu, MPROP_PERPAGE, 7);

	if (adminmenu) {
		format(text, charsmax(text), "\y%L", LANG_SERVER,  "SERVER_MENU_ADMIN", LANG_SERVER);
	} else {
		format(text, charsmax(text), "\y%L^n\d%L", LANG_SERVER, "SERVER_MENU_TITLE", g_ServerName, LANG_SERVER, "SERVER_MENU_IP", g_ServerAddress);
	}
	menu_setprop(menu, MPROP_TITLE, text);

	format(text, charsmax(text), "%L", LANG_SERVER, "BACK");
	menu_setprop(menu, MPROP_BACKNAME, text);
	
	format(text, charsmax(text), "%L", LANG_SERVER, "MORE");
	menu_setprop(menu, MPROP_NEXTNAME, text);

	format(text, charsmax(text), "%L^n^n\y%s by %s", LANG_SERVER, "EXIT", PLUGIN, AUTHOR);
	menu_setprop(menu, MPROP_EXITNAME, text);
}

#if defined ADMINLOAD
stock get_expired_days(id)
{
	static expired;
	expired = adminload_get_expired(id);

	if (expired == 0) {
		return 0;
	}

	expired -= get_systime(0);

	return (expired / 86400);
}
#endif

#if defined ENABLE_CALLFUNC
stock bool:callfunc(id, function[128], plugin[128])
{
	format(plugin, charsmax(plugin), "%s.amxx", plugin);

	callfunc_begin(function, plugin);
	callfunc_push_int(id);
	callfunc_end();

	return false;
}
#endif