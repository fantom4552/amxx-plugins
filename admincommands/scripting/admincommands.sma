#pragma semicolon 1

#include <amxmodx>
#include <amxmisc>

#if (AMXX_VERSION_NUM < 183) || defined NO_NATIVE_COLORCHAT
#include <colorchat>
#else
#define DontChange print_team_default
#endif

#define PLUGIN "Admin Commands"
#define VERSION "0.2"
#define AUTHOR "F@nt0M"

#define CheckFlag(%1,%2) (get_user_flags(%1) & %2)

new g_ShowActivity;

public plugin_init()
{
	register_plugin(PLUGIN, VERSION, AUTHOR);

	register_concmd("amx_kick", "cmdKick", ADMIN_KICK, "<name or #userid or steamid> [reason]");
	register_concmd("amx_slay", "cmdSlay", ADMIN_SLAY, "<name or #userid or steamid>");
	register_concmd("amx_slap", "cmdSlap", ADMIN_SLAY, "<name or #userid or steamid> [power]");
	register_concmd("amx_nick", "cmdNick", ADMIN_SLAY, "<name or #userid or steamid> <new nick>");
	// register_concmd("amx_pause", "cmdPause", ADMIN_CVAR, "- pause or unpause the game") // TODO
	register_concmd("amx_who", "cmdWho", ADMIN_ADMIN, "- displays who is on server");
	register_concmd("amx_plugins", "cmdPlugins", ADMIN_ADMIN);
	register_concmd("amx_modules", "cmdModules", ADMIN_ADMIN);

	g_ShowActivity = get_cvar_pointer("amx_show_activity");
}

public plugin_cfg()
{
	// Cvars which can be changed only with rcon access
	onlyRcon("rcon_password");
	onlyRcon("amx_show_activity");
	onlyRcon("amx_mode");
	onlyRcon("amx_password_field");
	onlyRcon("amx_default_access");
	onlyRcon("amx_reserved_slots");
	onlyRcon("amx_reservation");
	onlyRcon("amx_sql_table");
	onlyRcon("amx_sql_host");
	onlyRcon("amx_sql_user");
	onlyRcon("amx_sql_pass");
	onlyRcon("amx_sql_db");
	onlyRcon("amx_sql_type");

}

public cmdKick(id, level, cid)
{
	if(!cmd_access(id, level, cid, 2, true)) {
		return PLUGIN_HANDLED;
	}

	new args[70], identifier[62], reason[32];
	read_args(args, charsmax(args));
	parse(args, identifier, charsmax(identifier), reason, charsmax(reason));
	remove_quotes(identifier);
	remove_quotes(reason);

	new player = locate_player(identifier);

	if(!player) {
		console_print(id, "Client not found");
		return PLUGIN_HANDLED;
	}

	if (id != player && CheckFlag(player, ADMIN_IMMUNITY)) {
		console_print(id, "Client can't be kicked");
		return PLUGIN_HANDLED;
	}

	log_activity(player, id, "KICK", "kick (reason '%s')", reason);

	new userid  = get_user_userid(player);

	if(reason[0] && !is_user_bot(player)) {
		server_cmd("kick #%d ^"%s^"", userid, reason);
	} else {
		server_cmd("kick #%d", userid);
	}

	show_activity_players(player, id, "%L", LANG_PLAYER, "CMD_KICK");

	console_print(id, "Client kicked");
	
	return PLUGIN_HANDLED;
}

public cmdSlay(id, level, cid)
{
	if(!cmd_access(id, level, cid, 2, true)) {
		return PLUGIN_HANDLED;
	}

	new identifier[62];
	read_args(identifier, charsmax(identifier));
	remove_quotes(identifier);

	new player = locate_player(identifier);

	if(!player) {
		console_print(id, "Client not found");
		return PLUGIN_HANDLED;
	}

	if (id != player && CheckFlag(player, ADMIN_IMMUNITY)) {
		console_print(id, "Client can't be slayed");
		return PLUGIN_HANDLED;
	}

	log_activity(player, id, "SLAY", "slay");

	user_kill(player);

	show_activity_players(player, id, "%L", LANG_PLAYER, "CMD_SLAY");
	console_print(id, "Client slayed");
	
	return PLUGIN_HANDLED;
}

public cmdSlap(id, level, cid)
{
	if(!cmd_access(id, level, cid, 2, true)) {
		return PLUGIN_HANDLED;
	}

	new args[70], identifier[62], power[32];
	read_args(args, charsmax(args));
	parse(args, identifier, charsmax(identifier), power, charsmax(power));
	remove_quotes(identifier);
	remove_quotes(power);

	new player = locate_player(identifier);
	new damage = (power[0] && is_str_num(power)) ? str_to_num(power) : 0;

	if(!player) {
		console_print(id, "Client not found");
		return PLUGIN_HANDLED;
	}

	if (id != player && CheckFlag(player, ADMIN_IMMUNITY)) {
		console_print(id, "Client can't be slapped");
		return PLUGIN_HANDLED;
	}

	log_activity(player, id, "SLAP", "slap (damage '%d')", damage);

	user_slap(player, damage);

	show_activity_players(player, id, "%L", LANG_PLAYER, "CMD_SLAP", damage);

	console_print(id, "Client slapped");
	
	return PLUGIN_HANDLED;
}

public cmdNick(id, level, cid)
{
	if(!cmd_access(id, level, cid, 2, true)) {
		return PLUGIN_HANDLED;
	}

	new args[70], identifier[62], nick[32];
	read_args(args, charsmax(args));
	parse(args, identifier, charsmax(identifier), nick, charsmax(nick));
	remove_quotes(identifier);
	remove_quotes(nick);

	new player = locate_player(identifier);
	
	if (!nick[0]) {
		copy(nick, charsmax(nick), "Player");
	}

	if(!player) {
		console_print(id, "Client not found");
		return PLUGIN_HANDLED;
	}

	if (id != player && CheckFlag(player, ADMIN_IMMUNITY)) {
		console_print(id, "Client nick can't be changed");
		return PLUGIN_HANDLED;
	}

	log_activity(player, id, "NICK", "nick (new '%s')", nick);

	client_cmd(player, "name ^"%s^"", nick);

	show_activity_players(player, id, "%L ^4%s", LANG_SERVER, "CMD_NICK", nick);

	console_print(id, "Client nick changed");

	return PLUGIN_HANDLED;
}

public cmdWho(id, level, cid)
{
	if (!cmd_access(id, level, cid, 1)) {
		return PLUGIN_HANDLED;
	}

	new players[32], inum, cl_on_server[64], authid[32], name[32], flags, sflags[32];
	new lImm[16], lRes[16], lAccess[16], lYes[16], lNo[16];
	
	format(lImm, 15, "%L", id, "IMMU");
	format(lRes, 15, "%L", id, "RESERV");
	format(lAccess, 15, "%L", id, "ACCESS");
	format(lYes, 15, "%L", id, "YES");
	format(lNo, 15, "%L", id, "NO");
	
	get_players(players, inum);
	format(cl_on_server, 63, "%L", id, "CLIENTS_ON_SERVER");
	console_print(id, "^n%s:^n #  %-16.15s %-20s %-8s %-4.3s %-4.3s %s", cl_on_server, "nick", "authid", "userid", lImm, lRes, lAccess);
	
	for (new a = 0; a < inum; ++a) {
		get_user_authid(players[a], authid, 31);
		get_user_name(players[a], name, 31);
		flags = get_user_flags(players[a]);
		get_flags(flags, sflags, 31);
		console_print(id, "%2d  %-16.15s %-20s %-8d %-6.5s %-6.5s %s", players[a], name, authid, get_user_userid(players[a]), (flags&ADMIN_IMMUNITY) ? lYes : lNo, (flags&ADMIN_RESERVATION) ? lYes : lNo, sflags);
	}
	
	console_print(id, "%L", id, "TOTAL_NUM", inum);
	get_user_authid(id, authid, 31);
	get_user_name(id, name, 31);

	log_amx("Cmd: ^"%s<%d><%s><>^" ask for players list", name, get_user_userid(id), authid);
	
	return PLUGIN_HANDLED;
}

public cmdPlugins(id, level, cid)
{
	if (!cmd_access(id, level, cid, 1)) {
		return PLUGIN_HANDLED;
	}
		
	// If server executes redirect this to "amxx plugins" for more in depth output
	if (id==0) {
		server_cmd("amxx plugins");
		server_exec();
		return PLUGIN_HANDLED;
	}

	new name[32], version[32], author[32], filename[32], status[32];
	new lName[32], lVersion[32], lAuthor[32], lFile[32], lStatus[32];

	format(lName, 31, "%L", id, "NAME");
	format(lVersion, 31, "%L", id, "VERSION");
	format(lAuthor, 31, "%L", id, "AUTHOR");
	format(lFile, 31, "%L", id, "FILE");
	format(lStatus, 31, "%L", id, "STATUS");

	new StartPLID=0;
	new EndPLID;

	new Temp[96];

	new num = get_pluginsnum();
	
	if (read_argc() > 1) {
		read_argv(1,Temp,sizeof(Temp)-1);
		StartPLID=str_to_num(Temp)-1; // zero-based
	}

	EndPLID=min(StartPLID + 10, num);
	
	new running = 0;
	
	console_print(id, "----- %L -----", id, "LOADED_PLUGINS");
	console_print(id, "%-18.17s %-11.10s %-17.16s %-16.15s %-9.8s", lName, lVersion, lAuthor, lFile, lStatus);

	new i=StartPLID;
	while (i <EndPLID) {
		get_plugin(i++, filename, 31, name, 31, version, 31, author, 31, status, 31);
		console_print(id, "%-18.17s %-11.10s %-17.16s %-16.15s %-9.8s", name, version, author, filename, status);
		
		// "debug" or "running"
		if (status[0]=='d' || status[0]=='r') {
			running++;
		}
	}

	console_print(id, "%L", id, "PLUGINS_RUN", EndPLID-StartPLID, running);
	console_print(id, "----- %L -----", id, "HELP_ENTRIES", StartPLID + 1, EndPLID, num);
	
	if (EndPLID < num) {
		formatex(Temp,sizeof(Temp)-1,"----- %L -----",id,"HELP_USE_MORE", EndPLID + 1);
		replace_all(Temp,sizeof(Temp)-1,"amx_help","amx_plugins");
		console_print(id,"%s",Temp);
	} else {
		formatex(Temp,sizeof(Temp)-1,"----- %L -----",id,"HELP_USE_BEGIN");
		replace_all(Temp,sizeof(Temp)-1,"amx_help","amx_plugins");
		console_print(id,"%s",Temp);
	}

	return PLUGIN_HANDLED;
}

public cmdModules(id, level, cid)
{
	if (!cmd_access(id, level, cid, 1)) {
		return PLUGIN_HANDLED;
	}

	new name[32], version[32], author[32], status, sStatus[16];
	new lName[32], lVersion[32], lAuthor[32], lStatus[32];

	format(lName, 31, "%L", id, "NAME");
	format(lVersion, 31, "%L", id, "VERSION");
	format(lAuthor, 31, "%L", id, "AUTHOR");
	format(lStatus, charsmax(lStatus), "%L", id, "STATUS");

	new num = get_modulesnum();
	
	console_print(id, "%L:", id, "LOADED_MODULES");
	console_print(id, "%-23.22s %-11.10s %-20.19s %-11.10s", lName, lVersion, lAuthor, lStatus);
	
	for (new i = 0; i < num; i++) {
		get_module(i, name, 31, author, 31, version, 31, status);
		
		switch (status) 
		{
			case module_loaded: {
				copy(sStatus, 15, "running");
			}

			default: {
				copy(sStatus, 15, "bad load");
				copy(name, charsmax(name), "unknown");
				copy(author, charsmax(author), "unknown");
				copy(version, charsmax(version), "unknown");
			}
		}
		
		console_print(id, "%-23.22s %-11.10s %-20.19s %-11.10s", name, version, author, sStatus);
	}

	console_print(id, "%L", id, "NUM_MODULES", num);

	return PLUGIN_HANDLED;
}

stock show_activity_players(id, admin_id, const msg[], {Float,_}:...)
{
	new name[32], adminName[32];

	new action[192];
	vformat(action, 191, msg, 4);

	get_user_name(id, name, 31);
	get_user_name(admin_id, adminName, 31);

	new color = color_activity_players(id);

	switch (get_pcvar_num(g_ShowActivity)) {
		case 1: {
			client_print_color(0, color, "%L", LANG_PLAYER, "ADMIN_CMD", action, name);
		}

		case 2: {
			client_print_color(0, color, "%L", LANG_PLAYER, "ADMIN_CMD_NAME", adminName, action, name);
		}

		case 3: {
			send_activity_players(true, color, "%L", LANG_PLAYER, "ADMIN_CMD_NAME", adminName, action, name);
			send_activity_players(false, color, "%L", LANG_PLAYER, "ADMIN_CMD", action, name);
		}

		case 4: {
			send_activity_players(true, color, "%L", LANG_PLAYER, "ADMIN_CMD_NAME", adminName, action, name);
		}

		case 5: {
			send_activity_players(true, color, "%L", LANG_PLAYER, "ADMIN_CMD", action, name);
		}
	}
}

stock send_activity_players(bool:showAdmins, color, const msg[], {Float,_}:...)
{
	new message[192], players[32], num;

	vformat(message, 191, msg, 4);

	get_players(players, num, "ch");
	for (new i = 0; i < num; i++) {
		if (showAdmins && is_user_admin(players[i])) {
			client_print_color(players[i], color, message);
		} else if(!showAdmins && !is_user_admin(players[i])) {
			client_print_color(players[i], color, message);
		}
	}
}

stock color_activity_players(id)
{
	new print_color;
	switch (get_user_team(id))
	{
		case 1: {
			print_color = print_team_red;
		}

		case 2: {
			print_color = print_team_blue;
		}

		default: {
			print_color = print_team_grey;
		}
	}

	return print_color;
}

stock log_activity(id, admin_id, const action[], const msg[], {Float,_}:...)
{
	new playerName[32], playerAuthid[32], playerIP[32];
	new adminName[32], adminAuthid[32], adminIP[32];

	new message[192];
	vformat(message, 191, msg, 5);

	get_user_name(id, playerName, 31);
	get_user_authid(id, playerAuthid, 31);
	get_user_ip(id, playerIP, 31, 1);

	get_user_name(admin_id, adminName, 31);
	get_user_authid(admin_id, adminAuthid, 31);
	get_user_ip(admin_id, adminIP, 31, 1);

	log_amx("[%s] Admin <%s><%s><%s> %s player <%s><%s><%s>", action, adminName, adminAuthid, adminIP, message, playerName, playerAuthid, playerIP);
}

stock locate_player(const identifier[]) 
{
	new player = 0;

	if (identifier[0]=='#' && identifier[1]) {
		player = find_player("k", str_to_num(identifier[1]));
	}

	if (!player) {
		player = find_player("c", identifier);
	}

	if (!player) {
		player = find_player("bl", identifier);
	}
	
	if (!player) {
		player = find_player("d", identifier);
	}
	
	return player;
}

stock onlyRcon(const name[])
{
	new pointer;
	if ((pointer=get_cvar_pointer(name)) != 0) {
		new flags=get_pcvar_flags(pointer);
		
		if (!(flags & FCVAR_PROTECTED)) {
			set_pcvar_flags(pointer,flags | FCVAR_PROTECTED);
		}
	}
}