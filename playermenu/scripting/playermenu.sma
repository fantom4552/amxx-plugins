// TODO: Group Variables
// TODO: Team callback when is not Cstrike

#pragma semicolon 1

#include <amxmodx>
#include <amxmisc>
#include <cstrike>
#include <fakemeta>

#if (AMXX_VERSION_NUM < 183) || defined NO_NATIVE_COLORCHAT
#include <colorchat>
#else
#define DontChange print_team_default
#endif

#define PLUGIN "Player menu"
#define VERSION "0.1"
#define AUTHOR "F@nt0M"

#define isPlayerConnected(%1) (is_user_connected(g_PlayersMenuPlayer[%1]))

new const m_iMenu = 205;
new const m_bTeamChanged = 501;
new const Menu_ChooseAppearance = 3;

new const CsTeams:g_CSTeamNumbers[3] = {
	CS_TEAM_T,
	CS_TEAM_CT,
	CS_TEAM_SPECTATOR
};

new const g_CSTeamNames[3][] = {
	"TERRORIST",
	"CT",
	"SPECTATOR"
};

new const g_CSTeamNumbersStr[3][] = {
	"1",
	"2",
	"6"
};

new g_MenuGetPlayersCallback;
new g_ShowActivity;

new g_PlayersMenu[MAX_PLAYERS+1];
new g_PlayersMenuPage[MAX_PLAYERS+1];
new g_PlayersMenuTeams[MAX_PLAYERS+1];
new g_PlayersMenuPlayer[MAX_PLAYERS+1];
new g_PlayersReason[MAX_PLAYERS+1][32];

new g_KickMenu;
new g_SlapMenu;
new g_TeamMenu;

new bool:g_ModuleCstrike = false;
new bool:g_ModuleFakemeta = false;

new pcvar_AllowSpectators;
new pcvar_LimitTeams;

new g_CSPlayerCanSwitchFromSpec[MAX_PLAYERS + 1];
new bool:g_Silent[MAX_PLAYERS + 1];

new g_TransferingAdmin;

public plugin_init()
{
	register_plugin(PLUGIN, VERSION, AUTHOR);

	register_dictionary("playermenu.txt");
	register_dictionary("admincommands.txt");
	register_dictionary("common.txt");

	register_clcmd("amx_kickmenu", "cmdKickMenu", ADMIN_KICK, "- displays kick menu");
	register_clcmd("amx_slapmenu", "cmdSlapMenu", ADMIN_SLAY, "- displays slap/slay menu");
	register_clcmd("amx_teammenu", "cmdTeamMenu", ADMIN_LEVEL_A, "- displays team menu");

	register_clcmd("Reason", "msgEnterReason");

	new modname[9];
	get_modname(modname, charsmax(modname));
	if (equal(modname, "cstrike") || equal(modname, "czero")) {
		register_event("TeamInfo", "Event_TeamInfo", "a", "2=TERRORIST", "2=CT");
		register_event("TextMsg", "Event_TextMsg", "b", "1=4", "2=#Only_1_Team_Change");
	}

	g_MenuGetPlayersCallback = menu_makecallback("callback_MenuGetPlayers");

	g_ShowActivity = get_cvar_pointer("amx_show_activity");

	if (LibraryExists("cstrike", LibType_Library)) {
		g_ModuleCstrike = true;
	}

	if (LibraryExists("fakemeta", LibType_Library)) {
		g_ModuleFakemeta = true;
	}

	pcvar_AllowSpectators = get_cvar_pointer("allow_spectators");
	pcvar_LimitTeams = get_cvar_pointer("mp_limitteams");
}

public plugin_cfg()
{
	makeKickConfirmMenu();
	makeSlapConfirmMenu();
	makeTeamConfirmMenu();
}

public client_putinserver(id)
{
	g_CSPlayerCanSwitchFromSpec[id] = false;
	g_Silent[id] = false;
}

public Event_TeamInfo()
{
	new id = read_data(1);
	if (is_user_connected(id)) {
		g_CSPlayerCanSwitchFromSpec[id] = true;
	}
}

public Event_TextMsg(id) // #Only_1_Team_Change
{
	if (g_TransferingAdmin && is_user_connected(id) && (id == g_TransferingAdmin || is_user_connected(g_TransferingAdmin))) {
		new name[MAX_NAME_LENGTH];
		get_user_name(id, name, charsmax(name));
		client_print(g_TransferingAdmin, print_chat, "%L", g_TransferingAdmin, "PM_CANT_PERF_PLAYER", name);
	}
}

public cmdKickMenu(id, level, cid)
{
	if(!cmd_access(id, level, cid, 1, true)) {
		return PLUGIN_HANDLED;
	}

	new menu = menu_create("menu_kick", "actionKickMenu");
	MenuSetProps(id, menu, "PM_KICK_MENU");
	MenuGetPlayers(menu);
	
	g_PlayersMenu[id] = menu;
	g_PlayersMenuPage[id] = 0;
	g_PlayersMenuPlayer[id] = 0;
	g_PlayersReason[id][0] = '^0';
	menu_display(id, menu, 0);

	return PLUGIN_HANDLED;
}

public actionKickMenu(id, menu, item)
{
	if(item == MENU_EXIT) {
		g_PlayersMenu[id] = 0;
		g_PlayersMenuPage[id] = 0;
		g_PlayersMenuPlayer[id] = 0;
		g_PlayersReason[id][0] = '^0';
		menu_destroy(menu);
		return PLUGIN_HANDLED; 
	}

	new access, data[3], title[64], callback;
	menu_item_getinfo(menu, item, access, data, 2, title, 63, callback);

	new oldmenu, newmenu;
	player_menu_info(id, oldmenu, newmenu, g_PlayersMenuPage[id]);
	
	g_PlayersMenuPlayer[id] = str_to_num(data);

	if (!isPlayerConnected(id)) {
		g_PlayersMenuPlayer[id] = 0;
		g_PlayersReason[id][0] = '^0';
		menu_display(id, g_PlayersMenu[id], g_PlayersMenuPage[id]);
		return PLUGIN_HANDLED;
	}

	menu_display(id, g_KickMenu);
	
	return PLUGIN_HANDLED;
}

makeKickConfirmMenu()
{
	g_KickMenu = menu_create("menu_kick", "actionKickConfirmMenu");
	MenuSetProps(0, g_KickMenu, "PM_KICK_MENU");
	menu_setprop(g_KickMenu, MPROP_EXIT, MEXIT_NEVER);

	new callback = menu_makecallback("callback_KickReason");

	new title[32];
	format(title, 31, "%L", LANG_SERVER, "YES");
	menu_additem(g_KickMenu, title, "1");
	format(title, 31, "%L", LANG_SERVER, "NO");
	menu_additem(g_KickMenu, "No", "2");
	format(title, 31, "%L", LANG_SERVER, "PM_REASON");
	menu_additem(g_KickMenu, "Change Reason", "3", 0 , callback);
}

public actionKickConfirmMenu(id, menu, item)
{
	new access, data[3], title[64], callback;
	menu_item_getinfo(menu, item, access, data, 2, title, 63, callback);

	if (!isPlayerConnected(id)) {
		g_PlayersMenuPlayer[id] = 0;
		g_PlayersReason[id][0] = '^0';
		menu_display(id, g_PlayersMenu[id], g_PlayersMenuPage[id]);
		return PLUGIN_HANDLED;
	}

	switch (str_to_num(data)) {
		case 1: {
			log_activity(g_PlayersMenuPlayer[id], id, "KICK", "kick (reason %s)", g_PlayersReason[id]);

			new userid = get_user_userid(g_PlayersMenuPlayer[id]);
			if(g_PlayersReason[id][0] && !is_user_bot(g_PlayersMenuPlayer[id])) {
				server_cmd("kick #%d ^"%s^"", userid, g_PlayersReason[id]);
			} else {
				server_cmd("kick #%d", userid);
			}

			show_activity_players(g_PlayersMenuPlayer[id], id, "%L", LANG_PLAYER, "CMD_KICK");

			g_PlayersMenuPlayer[id] = 0;
			g_PlayersReason[id][0] = '^0';
			menu_display(id, g_PlayersMenu[id], g_PlayersMenuPage[id]);
		}

		case 2: {
			g_PlayersMenu[id] = menu;
			g_PlayersMenuPage[id] = 0;
			g_PlayersMenuPlayer[id] = 0;
			g_PlayersReason[id][0] = '^0';
			menu_display(id, g_PlayersMenu[id], g_PlayersMenuPage[id]);
		}

		case 3: {
			client_cmd(id, "messagemode ^"Reason^"");
		}
	}
	
	return PLUGIN_HANDLED;
}

public callback_KickReason(id, menu, item)
{
	new title[64];
	if (g_PlayersReason[id][0]) {
		format(title, 63, "%L: \r%s", id, "PM_REASON", g_PlayersReason[id]);
	} else {
		format(title, 63, "%L: %L", id, "PM_REASON", id, "NONE");
	}

	menu_item_setname(menu, item, title);
	
	return ITEM_ENABLED;
}

public msgEnterReason(id)
{
	new typed[32];
	read_args(typed, charsmax(typed));
	remove_quotes(typed);

	copy(g_PlayersReason[id], 31, typed);

	if (!isPlayerConnected(id)) {
		g_PlayersMenuPlayer[id] = 0;
		g_PlayersReason[id][0] = '^0';
		menu_display(id, g_PlayersMenu[id], g_PlayersMenuPage[id]);
	} else {
		menu_display(id, g_KickMenu);
	}
}

public cmdSlapMenu(id, level, cid)
{
	if(!cmd_access(id, level, cid, 1, true)) {
		return PLUGIN_HANDLED;
	}

	new menu = menu_create("menu_slap", "actionSlapMenu");
	MenuSetProps(id, menu, "PM_SLAP_MENU");
	MenuGetPlayers(menu);
	
	g_PlayersMenu[id] = menu;
	g_PlayersMenuPage[id] = 0;
	g_PlayersMenuPlayer[id] = 0;
	menu_display(id, menu, 0);

	return PLUGIN_HANDLED;
}

public actionSlapMenu(id, menu, item)
{
	if(item == MENU_EXIT) {
		g_PlayersMenu[id] = 0;
		g_PlayersMenuPage[id] = 0;
		g_PlayersMenuPlayer[id] = 0;
		menu_destroy(menu);
		return PLUGIN_HANDLED; 
	}

	new access, data[3], title[64], callback;
	menu_item_getinfo(menu, item, access, data, 2, title, 63, callback);

	new oldmenu, newmenu;
	player_menu_info(id, oldmenu, newmenu, g_PlayersMenuPage[id]);
	
	g_PlayersMenuPlayer[id] = str_to_num(data);

	if (!isPlayerConnected(id)) {
		g_PlayersMenuPlayer[id] = 0;
		menu_display(id, g_PlayersMenu[id], g_PlayersMenuPage[id]);
		return PLUGIN_HANDLED;
	}

	menu_display(id, g_SlapMenu);
	
	return PLUGIN_HANDLED;
}

makeSlapConfirmMenu()
{
	g_SlapMenu = menu_create("menu_slap", "actionSlapConfirmMenu");
	MenuSetProps(0, g_SlapMenu, "PM_SLAP_MENU");
	menu_setprop(g_SlapMenu, MPROP_EXIT, MEXIT_ALL);

	new title[32];
	format(title, charsmax(title), "%L", LANG_SERVER, "PM_SLAP_ITEM", 0);
	menu_additem(g_SlapMenu, title, "0");
	format(title, charsmax(title), "%L", LANG_SERVER, "PM_SLAP_ITEM", 1);
	menu_additem(g_SlapMenu, title, "1");
	format(title, charsmax(title), "%L", LANG_SERVER, "PM_SLAP_ITEM", 5);
	menu_additem(g_SlapMenu, title, "5");
	format(title, charsmax(title), "%L", LANG_SERVER, "PM_SLAY_ITEM");
	menu_additem(g_SlapMenu, title, "SLAY");
}

public actionSlapConfirmMenu(id, menu, item)
{
	if(item == MENU_EXIT) {
		g_PlayersMenuPlayer[id] = 0;
		menu_display(id, g_PlayersMenu[id], g_PlayersMenuPage[id]);
		return PLUGIN_HANDLED;
	}

	new access, data[5], title[64], callback;
	menu_item_getinfo(menu, item, access, data, 4, title, 63, callback);

	if (!isPlayerConnected(id)) {
		g_PlayersMenuPlayer[id] = 0;
		menu_display(id, g_PlayersMenu[id], g_PlayersMenuPage[id]);
		return PLUGIN_HANDLED;
	}

	if (equal(data, "SLAY")) {
		log_activity(g_PlayersMenuPlayer[id], id, "SLAY", "slay");
		user_kill(g_PlayersMenuPlayer[id]);
		show_activity_players(g_PlayersMenuPlayer[id], id, "%L", LANG_PLAYER, "CMD_SLAY");
		g_PlayersMenuPlayer[id] = 0;
		menu_display(id, g_PlayersMenu[id], g_PlayersMenuPage[id]);
	} else {
		new damage = str_to_num(data);
		log_activity(g_PlayersMenuPlayer[id], id, "SLAP", "slap (damage %d)", damage);
		user_slap(g_PlayersMenuPlayer[id], damage);
		show_activity_players(g_PlayersMenuPlayer[id], id, "%L", LANG_PLAYER, "CMD_SLAP", damage);
		menu_display(id, g_SlapMenu);
	}

	return PLUGIN_HANDLED;
}

public cmdTeamMenu(id, level, cid)
{
	if(!cmd_access(id, level, cid, 1, true)) {
		return PLUGIN_HANDLED;
	}

	new menu = menu_create("menu_team", "actioTeamMenu");
	MenuSetProps(id, menu, "PM_TEAM_MENU");
	MenuGetPlayers(menu);
	
	g_PlayersMenu[id] = menu;
	g_PlayersMenuPage[id] = 0;
	g_PlayersMenuPlayer[id] = 0;
	menu_display(id, menu, 0);

	return PLUGIN_HANDLED;
}

public actioTeamMenu(id, menu, item)
{
	if(item == MENU_EXIT) {
		g_PlayersMenu[id] = 0;
		g_PlayersMenuPage[id] = 0;
		g_PlayersMenuPlayer[id] = 0;
		menu_destroy(menu);
		return PLUGIN_HANDLED; 
	}

	new access, data[3], title[64], callback;
	menu_item_getinfo(menu, item, access, data, 2, title, 63, callback);
	
	new oldmenu, newmenu;
	player_menu_info(id, oldmenu, newmenu, g_PlayersMenuPage[id]);

	g_PlayersMenuPlayer[id] = str_to_num(data);

	if (!isPlayerConnected(id)) {
		g_PlayersMenuPlayer[id] = 0;
		menu_display(id, g_PlayersMenu[id], g_PlayersMenuPage[id]);
		return PLUGIN_HANDLED;
	}

	menu_display(id, g_TeamMenu);
	
	return PLUGIN_HANDLED;
}

makeTeamConfirmMenu()
{
	g_TeamMenu = menu_create("menu_slap", "actionTeamConfirmMenu");
	MenuSetProps(0, g_TeamMenu, "PM_TEAM_MENU");
	menu_setprop(g_TeamMenu, MPROP_EXIT, MEXIT_ALL);

	new callbackTeam = menu_makecallback("callback_TeamMenuTeam");
	new callbackSilent = menu_makecallback("callback_TeamMenuSilent");

	new title[64];
	format(title, 63, "%L", LANG_SERVER, "PM_T");
	menu_additem(g_TeamMenu, title, "0", 0, callbackTeam);
	format(title, 63, "%L", LANG_SERVER, "PM_CT");
	menu_additem(g_TeamMenu, title, "1", 0, callbackTeam);
	format(title, 63, "%L", LANG_SERVER, "PM_SPEC");
	menu_additem(g_TeamMenu, title, "2", 0, callbackTeam);
	menu_addblank(g_TeamMenu, 0);
	menu_additem(g_TeamMenu, "", "SILENT", 0, callbackSilent);
}

public actionTeamConfirmMenu(id, menu, item)
{
	if(item == MENU_EXIT) {
		g_PlayersMenuPlayer[id] = 0;
		menu_display(id, g_PlayersMenu[id], g_PlayersMenuPage[id]);
		return PLUGIN_HANDLED;
	}

	new access, data[10], title[64], callback;
	menu_item_getinfo(menu, item, access, data, 9, title, 63, callback);

	if (!isPlayerConnected(id)) {
		g_PlayersMenuPlayer[id] = 0;
		menu_display(id, g_PlayersMenu[id], g_PlayersMenuPage[id]);
		return PLUGIN_HANDLED;
	}
	
	if (equal(data, "SILENT")) {
		g_Silent[id] = !g_Silent[id];
		menu_display(id, menu);
	} else {
		new key = str_to_num(data);
		new player = g_PlayersMenuPlayer[id];

		log_activity(g_PlayersMenuPlayer[id], id, "TEAM", "change team (team %s)", g_CSTeamNames[key]);

		g_TransferingAdmin = id;

		if (key == 2) {
			if (g_ModuleFakemeta) {
				if (get_pdata_int(player, m_iMenu) == Menu_ChooseAppearance) {
					// works for both vgui and old style menus, and send menuselect could close other menus (and since get_user_menu fails to return VGUI and old style classes menus...)
					engclient_cmd(player, "joinclass", "6");
				}
			} else {
				// force
				engclient_cmd(player, "joinclass", "6");
			}
		}

		if (g_CSPlayerCanSwitchFromSpec[player] && g_ModuleCstrike && (CS_TEAM_T <= cs_get_user_team(player) <= CS_TEAM_CT)) {
			if (is_user_alive(player) && (!g_Silent[id] || key == 2)) {
				new deaths = cs_get_user_deaths(player);
				user_kill(player, 1);
				cs_set_user_deaths(player, deaths);
			}

			cs_set_user_team(player, key + 1);
		} else {
			if (is_user_alive(player) && (!g_Silent[id] || key == 2)) {
				user_kill(player, 1);
			}

			if (g_ModuleFakemeta) {
				set_pdata_bool(player, m_bTeamChanged, true);
			}

			new limit_setting;
			if (pcvar_LimitTeams) {
				limit_setting = get_pcvar_num(pcvar_LimitTeams);

				set_pcvar_num(pcvar_LimitTeams, 0);
			}

			if (key == 2) {
				new Float:allow_spectators_setting;
				if (pcvar_AllowSpectators) {
					allow_spectators_setting = get_pcvar_float(pcvar_AllowSpectators);
					if (allow_spectators_setting != 1.0) {
						set_pcvar_float(pcvar_AllowSpectators, 1.0);
					}
				}

				engclient_cmd(player, "jointeam", g_CSTeamNumbersStr[key]);

				if (pcvar_AllowSpectators && allow_spectators_setting != 1.0) {
					set_pcvar_float(pcvar_AllowSpectators, allow_spectators_setting);
				}
			} else {
				engclient_cmd(player, "jointeam", g_CSTeamNumbersStr[key]);
				engclient_cmd(player, "joinclass", "1");
			}

			if (pcvar_LimitTeams && limit_setting != 0) {
				set_pcvar_num(pcvar_LimitTeams, limit_setting);
			}
		}

		if (g_ModuleCstrike) {
			cs_reset_user_model(player);
		}

		if (g_ModuleFakemeta) {
			set_pdata_bool(player, m_bTeamChanged, true);
		}

		switch (key) {
			case 0: {
				copy(data, charsmax(data), "PM_T");
			}

			case 1: {
				copy(data, charsmax(data), "PM_CT");
			}

			default: {
				copy(data, charsmax(data), "PM_SPEC");
			}
		}

		show_activity_players(g_PlayersMenuPlayer[id], id, "%L ^4%L", LANG_PLAYER, "CMD_TEAM", LANG_PLAYER, data);

		g_TransferingAdmin = 0;
		g_PlayersMenuPlayer[id] = 0;
		menu_display(id, g_PlayersMenu[id], g_PlayersMenuPage[id]);
	}
	
	return PLUGIN_HANDLED;
}

public callback_TeamMenuTeam(id, menu, item)
{

	new access, data[3], title[64], callback;
	menu_item_getinfo(menu, item, access, data, 2, title, 63, callback);

	new key = str_to_num(data);
	new CsTeams:team = cs_get_user_team(g_PlayersMenuPlayer[id]);

	if(team == g_CSTeamNumbers[key]) {
		return ITEM_DISABLED;
	}
	
	return ITEM_ENABLED;
}

public callback_TeamMenuSilent(id, menu, item)
{
	new title[64];
	format(title, 63, "%L: %L", id, "PM_SILENT", id, (g_Silent[id] ? "ON" : "OFF"));
	menu_item_setname(menu, item, title);
	
	return ITEM_ENABLED;
}

stock MenuGetPlayers(menu) 
{
	new i, pid, playerId[3], players[32], num;

	get_players(players, num);

	for (i = 0; i < num; i++) {
		pid = players[i];
		g_PlayersMenuTeams[pid] = get_user_team(pid);
	}

	SortCustom1D(players, num, "_PlayersMenuSortTeam");

	for (i = 0; i <num; i++) {
		pid = players[i];
		num_to_str(pid, playerId, 2);
		menu_additem(menu, "", playerId, 0, g_MenuGetPlayersCallback);
	}
}

public _PlayersMenuSortTeam(const elem1, const elem2)
{
	if (elem1 == 0 || elem2 == 0) {
		return 0;
	}
		
	if (elem1 == elem2) {
		return 0;
	}

	if (g_PlayersMenuTeams[elem1] > g_PlayersMenuTeams[elem2]) {
		return 1;
	} else if (g_PlayersMenuTeams[elem1] < g_PlayersMenuTeams[elem2]) {
		return -1;
	}

	return 0;
}

public callback_MenuGetPlayers(id, menu, item) 
{
	new access, data[3], title[64], callback;
	menu_item_getinfo(menu, item, access, data, 2, title, 63, callback);
	
	new pid = str_to_num(data);
	
	new name[32];
	get_user_name(pid, name, 31);
	new len = format(title, charsmax(title), "%s", name);

	if (!is_user_connected(pid)) {
		len += format(title[len], charsmax(title) - len, " \r(%L)", id, "PM_NOT_CONN");
	} else if (id == pid) {
		len += format(title[len], charsmax(title) - len, " \r(%L)", id, "PM_YOU");
	} else if (get_user_flags(pid) & ADMIN_IMMUNITY) {
		len += format(title[len], charsmax(title) - len, " \r*");
	} else if (is_user_bot(pid)) {
		len += format(title[len], charsmax(title) - len, " \r(BOT)");
	} if (is_user_hltv(pid)) {
		len += format(title[len], charsmax(title) - len, " (HLTV)");
	}

	switch (g_PlayersMenuTeams[pid]) {
		case 1: {
			len += format(title[len], charsmax(title) - len, " \w(T)");
		}

		case 2: {
			len += format(title[len], charsmax(title) - len, " \w(CT)");
		}

		default: {
			len += format(title[len], charsmax(title) - len, " \w(SPEC)");
		}
	}

	menu_item_setname(menu, item, title);
	
	if (is_user_hltv(pid) || ((get_user_flags(pid) & ADMIN_IMMUNITY) && id != pid)) {
		return ITEM_DISABLED;
	}
	
	return ITEM_ENABLED;
}

stock MenuSetProps(id, menu, const title[]) 
{
	new text[64];

	menu_setprop(menu, MPROP_PERPAGE, 7);

	formatex(text, 63, "\y%L\w", id, title);
	menu_setprop(menu, MPROP_TITLE, text);

	formatex(text, 63, "%L", id, "BACK");
	menu_setprop(menu, MPROP_BACKNAME, text);
	
	formatex(text, 63, "%L", id, "MORE");
	menu_setprop(menu, MPROP_NEXTNAME, text);
	
	formatex(text, 63, "%L^n^n\y%s by %s", id, "EXIT", PLUGIN, AUTHOR);
	menu_setprop(menu, MPROP_EXITNAME, text);
	
	return 1;
}

stock show_activity_players(id, admin_id, const msg[], {Float,_}:...)
{
	new name[32], adminName[32];

	new action[192];
	vformat(action, 191, msg, 4);

	get_user_name(id, name, 31);
	get_user_name(admin_id, adminName, 31);

	new color = color_activity_players(id);

	switch (get_pcvar_num(g_ShowActivity)) {
		case 1: {
			client_print_color(0, color, "%L", LANG_PLAYER, "ADMIN_CMD", action, name);
		}

		case 2: {
			client_print_color(0, color, "%L", LANG_PLAYER, "ADMIN_CMD_NAME", adminName, action, name);
		}

		case 3: {
			send_activity_players(true, color, "%L", LANG_PLAYER, "ADMIN_CMD_NAME", adminName, action, name);
			send_activity_players(false, color, "%L", LANG_PLAYER, "ADMIN_CMD", action, name);
		}

		case 4: {
			send_activity_players(true, color, "%L", LANG_PLAYER, "ADMIN_CMD_NAME", adminName, action, name);
		}

		case 5: {
			send_activity_players(true, color, "%L", LANG_PLAYER, "ADMIN_CMD", action, name);
		}
	}
}

stock send_activity_players(bool:showAdmins, color, const msg[], {Float,_}:...)
{
	new message[192], players[32], num;

	vformat(message, 191, msg, 4);

	get_players(players, num, "ch");
	for (new i = 0; i < num; i++) {
		if (showAdmins && is_user_admin(players[i])) {
			client_print_color(players[i], color, message);
		} else if(!showAdmins && !is_user_admin(players[i])) {
			client_print_color(players[i], color, message);
		}
	}
}

stock color_activity_players(id)
{
	new print_color;
	switch (get_user_team(id))
	{
		case 1: {
			print_color = print_team_red;
		}

		case 2: {
			print_color = print_team_blue;
		}

		default: {
			print_color = print_team_grey;
		}
	}

	return print_color;
}

stock log_activity(id, admin_id, const action[], const msg[], {Float,_}:...)
{
	new playerName[32], playerAuthid[32], playerIP[32];
	new adminName[32], adminAuthid[32], adminIP[32];

	new message[192];
	vformat(message, 191, msg, 5);

	get_user_name(id, playerName, 31);
	get_user_authid(id, playerAuthid, 31);
	get_user_ip(id, playerIP, 31, 1);

	get_user_name(admin_id, adminName, 31);
	get_user_authid(admin_id, adminAuthid, 31);
	get_user_ip(admin_id, adminIP, 31, 1);

	log_amx("[%s] Admin <%s><%s><%s> %s player <%s><%s><%s>", action, adminName, adminAuthid, adminIP, message, playerName, playerAuthid, playerIP);
}