[ua]
MM_MAP_NOMINATE_ALREADY = ^1Карту ^4%s ^1вже номіновано
MM_MAP_NOMINATE = ^1Гравець ^3%s ^1номінував карту ^3%s
MM_MAP_SWITCH = ^1Гравець ^3%s ^1замінив номіновану раніше карту ^4%s ^1на ^4%s
MM_LAST_ROUND = Останній раунд
MM_TIME_LEFT = ^1До кінця карти залишилося^4
MM_TIME_LEFT_DISABLED = Карта не обмежена за часом
MM_THE_TIME = ^1Поточний час:^4
MM_CURRENT_MAP = ^1Поточна карта:^4
MM_MAP_EXTEND = Продовження %s
MM_MAP_CHANGE = ^1Гравець ^3%s ^1проголосував за ^4%s
MM_MAP_CHANGE_EXTEND = ^1Гравець ^3%s ^1проголосував за ^4продовження ^1карти
MM_MAP_REVOTE = ^1Голосування буде продовжено в ^4наступному раунді
MM_MAP_REVOTE_HUD = Голосування буде продовжено в наступному раунді
MM_MAP_EXTENDED_TIME = ^1Голосування ^4завершенно. ^1Карту продовжено на ^4%.0f хв.
MM_MAP_EXTENDED_TIME_HUD = Голосування завершенно.^n Карту продовжено на %.0f хв.
MM_MAP_FINISH = ^1Голосування завершенно. Наступна карта ^4%s
MM_MAP_FINISH_HUD = Голосування завершенно.^n Наступна карта %s
MM_CHOOSE_THE_NEXT_MAP = Виберіть наступну карту
MM_CHOOSE_THE_NEXT_MAP_TIME = Голосування почнеться через %d секунд
MM_CHOOSE_THE_NEXT_MAP_END_TIME = Голосування завершиться через %d секунд
MM_VOTED_PLAYERS_COUNT = Проголосувало %d з %d гравців
MM_NO_MAPS = Недостатньо карт
MM_MAP_TITLE = Виберіть карту:
MM_MAP_CONFIRM = Підтвердіть вибір:
MM_START_VOTE = Почати голосування
MM_VOTE_MAP_DISABLE = ^1Голосування за зміну карти ^4ЗАБОРОНЕНО
MM_VOTE_MAP_ENABLE = ^1Голосування за зміну карти ^4ДОЗВОЛЕНО
MM_VOTE_MAP_ALREADY = Ти вже проголосував за зміну карти
MM_VOTE_MAP_CHANGE = ^1Гравець ^3%s ^1проголосував за зміну карти.
MM_VOTE_MAP_CHANGE_ALL = ^4Набрана потрібна кількість голосів для зміни карти
MM_VOTE_MAP_PLAYERS = ^4%d з ^4%d ^1гравців необхідних проголосувало за зміну карти
MM_VOTE_MAP_TIMER = Голосування за зміну карти буде доступне через
MM_VOTE_MAP_MAX = Номіновано максимум карт
MM_VOTING_PROGRESS = Голосування запущене, операція недоступна
MM_ADMIN_CHANGE_MAP = ^1Адміністратор ^3%s ^1поставив карту ^4%s
MM_ADMIN_VOTE_MAP = ^1Адміністратор ^3%s ^1запустив голосування в наступному раунді

[en]
MM_MAP_NOMINATE_ALREADY = ^1Карту ^4%s ^1вже номіновано
MM_MAP_NOMINATE = ^1Гравець ^3%s ^1номінував карту ^3%s
MM_MAP_SWITCH = ^1Гравець ^3%s ^1замінив номіновану раніше карту ^4%s ^1на ^4%s
MM_LAST_ROUND = Останній раунд
MM_TIME_LEFT = ^1До кінця карти залишилося^4
MM_TIME_LEFT_DISABLED = Карта не обмежена за часом
MM_THE_TIME = ^1Поточний час:^4
MM_CURRENT_MAP = ^1Поточна карта:^4
MM_MAP_EXTEND = Продовження %s
MM_MAP_CHANGE = ^1Гравець ^3%s ^1проголосував за ^4%s
MM_MAP_CHANGE_EXTEND = ^1Гравець ^3%s ^1проголосував за ^4продовження ^1карти
MM_MAP_REVOTE = ^1Голосування буде продовжено в ^4наступному раунді
MM_MAP_REVOTE_HUD = Голосування буде продовжено в наступному раунді
MM_MAP_EXTENDED_TIME = ^1Голосування ^4завершенно. ^1Карту продовжено на ^4%.0f хв.
MM_MAP_EXTENDED_TIME_HUD = Голосування завершенно.^n Карту продовжено на %.0f хв.
MM_MAP_FINISH = ^1Голосування завершенно. Наступна карта ^4%s
MM_MAP_FINISH_HUD = Голосування завершенно.^n Наступна карта %s
MM_CHOOSE_THE_NEXT_MAP = Виберіть наступну карту
MM_CHOOSE_THE_NEXT_MAP_TIME = Голосування почнеться через %d секунд
MM_CHOOSE_THE_NEXT_MAP_END_TIME = Голосування завершиться через %d секунд
MM_VOTED_PLAYERS_COUNT = Проголосувало %d з %d гравців
MM_NO_MAPS = Недостатньо карт
MM_MAP_TITLE = Виберіть карту:
MM_MAP_CONFIRM = Підтвердіть вибір:
MM_START_VOTE = Почати голосування
MM_VOTE_MAP_DISABLE = ^1Голосування за зміну карти ^4ЗАБОРОНЕНО
MM_VOTE_MAP_ENABLE = ^1Голосування за зміну карти ^4ДОЗВОЛЕНО
MM_VOTE_MAP_ALREADY = Ти вже проголосував за зміну карти
MM_VOTE_MAP_CHANGE = ^1Гравець ^3%s ^1проголосував за зміну карти.
MM_VOTE_MAP_CHANGE_ALL = ^4Набрана потрібна кількість голосів для зміни карти
MM_VOTE_MAP_PLAYERS = ^4%d з ^4%d ^1гравців необхідних проголосувало за зміну карти
MM_VOTE_MAP_TIMER = Голосування за зміну карти буде доступне через
MM_VOTE_MAP_MAX = Номіновано максимум карт
MM_VOTING_PROGRESS = Голосування запущене, операція недоступна
MM_ADMIN_CHANGE_MAP = ^1Адміністратор ^3%s ^1поставив карту ^4%s
MM_ADMIN_VOTE_MAP = ^1Адміністратор ^3%s ^1запустив голосування в наступному раунді