#pragma semicolon 1

#include <amxmodx>
#include <map_manager>

#if (AMXX_VERSION_NUM < 183) || defined NO_NATIVE_COLORCHAT
	#include <colorchat>
#else
	#define DontChange print_team_default
#endif

#if AMXX_VERSION_NUM == 183
#define client_has_disconnect client_disconnected
#define create_new_cvar create_cvar
#else
#define client_has_disconnect client_disconnect
#define create_new_cvar register_cvar
#endif

#define PLUGIN "Map Manager"
#define VERSION "3.4.2"
#define AUTHOR "F@nt0M"

#include "map_manager/defines.inl"
#include "map_manager/global_vars.inl"
#include "map_manager/cfg.inl"
#include "map_manager/cmd.inl"
#include "map_manager/events.inl"
#include "map_manager/votemap.inl"
#include "map_manager/admin.inl"
#include "map_manager/menu.inl"
#include "map_manager/stocks.inl"

public plugin_init()
{
	register_plugin(PLUGIN, VERSION, AUTHOR);

	register_dictionary("map_manager.txt");
	register_dictionary("common.txt");

	register_event("TextMsg", "eventStartGame", "a", "2=#Game_Commencing", "2=#Game_will_restart_in");
	register_event("HLTV", "eventRoundStart", "a", "1=0", "2=0");

	set_task(1.0, "taskLastRound", LATS_ROUND_TASK_ID, _, _, "b");

	#if defined DEBUG && DEBUG >= 2
	register_concmd("mm_lastround", "cmdLastRound");
	#endif

	register_clcmd("amx_mapmenu", "cmdMapMenu", _, "- displays votemap menu");
	register_clcmd("amx_votemapmenu", "cmdVoteMapMenu", _, "- displays changelevel menu");

	register_clcmd("say rtv", "cmdRockTheVote", 0, "- vote map change");
	register_clcmd("say /rtv", "cmdRockTheVote", 0, "- vote map change");
	register_clcmd("say_team rtv", "cmdRockTheVote", 0, "- vote map change");
	register_clcmd("say_team /rtv", "cmdRockTheVote", 0, "- vote map change");

	register_clcmd("say /maps", "cmdMaps", 0, "- nominate map");
	register_clcmd("say_team /maps", "cmdMaps", 0, "- nominate map");

	register_clcmd("say timeleft", "cmdTimeLeft", 0, "- displays timeleft");
	register_clcmd("say_team timeleft", "cmdTimeLeft", 0, "- displays timeleft");

	register_clcmd("say thetime", "cmdTheTime", 0, "- displays current time");
	register_clcmd("say_team thetime", "cmdTheTime", 0, "- displays current time");

	register_clcmd("say currentmap", "cmdCurrentMap", 0, "- display current map");
	register_clcmd("say_team currentmap", "cmdCurrentMap", 0, "- display current map");

	g_Maps = ArrayCreate(MapConfig);

	get_localinfo("amxx_configsdir", g_ConfigDir, charmax(g_ConfigDir));

	pcv_amx_nextmap		= register_cvar("amx_nextmap", "", FCVAR_SERVER | FCVAR_EXTDLL | FCVAR_SPONLY);
	pcv_amx_timeleft	= register_cvar("amx_timeleft", "00:00", FCVAR_SERVER | FCVAR_EXTDLL | FCVAR_UNLOGGED | FCVAR_SPONLY);

	pcv_mp_timelimit 	= get_cvar_pointer("mp_timelimit");
	pcv_mp_chattime		= get_cvar_pointer("mp_chattime");
	pcv_mp_roundtime	= get_cvar_pointer("mp_roundtime");
	pcv_mp_freezetime	= get_cvar_pointer("mp_freezetime");
	pcv_mp_c4timer		= get_cvar_pointer("mp_c4timer");

	pcv_voteratio			= create_new_cvar("mm_voteratio", "0.6");
	pcv_timeout_nominate	= create_new_cvar("mm_timeout_nominate", "60");
	pcv_users_nominate		= create_new_cvar("mm_users_nominate", "3");
	pcv_timelimit_step		= create_new_cvar("mm_timelimit_step", "10");
	pcv_timelimit_max		= create_new_cvar("mm_timelimit_max", "40");

	set_pcvar_num(pcv_mp_chattime, check_cvar_num(pcv_mp_chattime, 3, 30));

	g_MsgScrenFade	= get_user_msgid("ScreenFade");
	g_HudSync 		= CreateHudSyncObj();

	g_VotemapEnable	= false;
	g_VotemapStarted = false;
	g_RockTheVote = false;

	g_StartTime = get_systime(0);

	g_FwdVoteStarted = CreateMultiForward("mp_vote_started", ET_IGNORE);
	g_FwdVoteFinished = CreateMultiForward("mp_vote_finished", ET_IGNORE, FP_CELL);

	server_cmd("exec %s/map_manager/mapmanager.cfg", g_ConfigDir);

	if (g_PlayersNum <= 0 || !checkPlayersNum()) {
		#if defined DEBUG && DEBUG >= 1
		log_amx("Start check players num. Players num: %d", g_PlayersNum);
		#endif
		set_task(EMPTY_CHECK_TASK_FQ, "checkPlayersNumTask", EMPTY_CHECK_TASK_ID);
	}
}

public plugin_cfg()
{
	cmd_pause_plugin("nextmap.amxx");
	cmd_pause_plugin("timeleft.amxx");
	cmd_pause_plugin("mapchooser.amxx");

	if (loadConfig()) {
		menuMapChangeCreate();
		menuMapConfirmCreate();
		menuMapVoteCreate();
		menuMapNominateCreate();
	} else {
		set_fail_state("Error open config file");
	}
}

public plugin_end()
{
	ArrayDestroy(g_Maps);
	
	if (g_FreezetimeRepare) {
		set_pcvar_num(pcv_mp_freezetime, g_Freezetime);
		g_FreezetimeRepare = false;
	}

	if (g_TimelimitRepare) {
		set_pcvar_float(pcv_mp_timelimit, g_Timelimit);
		g_TimelimitRepare = false;
	}

	remove_task(LATS_ROUND_TASK_ID);

	menuMapChangeDestroy();
	menuMapConfirmCDestroy();
	menuMapVoteDestroy();
	menuMapNominateDestroy();
}

public client_authorized(id)
{
	if (!is_user_bot(id) && !is_user_hltv(id)) {
		g_PlayersNum++;
		ClearBit(g_FlagVotemap, id);
		ClearBit(g_FlagNominate, id);
	}

	if (!checkPlayersNum()) {
		if (!task_exists(EMPTY_CHECK_TASK_ID)) {
			#if defined DEBUG && DEBUG >= 1
			log_amx("Start check players num. Players num: %d", g_PlayersNum);
			#endif
			set_task(EMPTY_CHECK_TASK_FQ, "checkPlayersNumTask", EMPTY_CHECK_TASK_ID);
		}
	} else if (task_exists(EMPTY_CHECK_TASK_ID)) {
		#if defined DEBUG && DEBUG >= 1
		log_amx("Cancel check players num. Players num: %d", g_PlayersNum);
		#endif
		remove_task(EMPTY_CHECK_TASK_ID);
	}
}

public client_has_disconnect(id)
{
	if (!is_user_bot(id) && !is_user_hltv(id)) {
		g_PlayersNum--;
		ClearBit(g_FlagVotemap, id);
		ClearBit(g_FlagNominate, id);
	}

	if (g_PlayersNum <= 0 || !checkPlayersNum()) {
		if (!task_exists(EMPTY_CHECK_TASK_ID)) {
			#if defined DEBUG && DEBUG >= 1
			log_amx("Start check players num. Players num: %d", g_PlayersNum);
			#endif
			set_task(EMPTY_CHECK_TASK_FQ, "checkPlayersNumTask", EMPTY_CHECK_TASK_ID);
		}
	} else if (task_exists(EMPTY_CHECK_TASK_ID)) {
		#if defined DEBUG && DEBUG >= 1
		log_amx("Cancel check players num. Players num: %d", g_PlayersNum);
		#endif
		remove_task(EMPTY_CHECK_TASK_ID);
	}
}

#if defined DEBUG && DEBUG >= 2
public cmdLastRound()
{
	g_MapExendEnable = true;
	g_MapRevoted = false;
	get_maps();
	set_last_round();
}
#endif



changeMap(mapname[])
{
	#if defined DEBUG && DEBUG >= 2
	log_amx("Change map %s", mapname);
	#endif
	server_cmd("changelevel %s", mapname);
}

set_last_round()
{
	if (!g_MapRevoted && !g_TimelimitRepare) {
		g_Timelimit = get_pcvar_float(pcv_mp_timelimit);
		g_TimelimitRepare = true;
	}

	new Float:timelimit = get_pcvar_float(pcv_mp_timelimit);
	g_Addtime = (get_pcvar_float(pcv_mp_roundtime) * 60 + get_pcvar_num(pcv_mp_freezetime) + get_pcvar_num(pcv_mp_c4timer) + get_pcvar_num(pcv_mp_chattime) + VOTE_TIME + LAST_MINUTE + 10) / 60 - g_Addtime;

	set_pcvar_float(pcv_mp_timelimit, timelimit + g_Addtime);

	if (!g_MapRevoted && !g_FreezetimeRepare) {
		g_Freezetime = get_pcvar_num(pcv_mp_freezetime);
		g_FreezetimeRepare = true;
	}

	if (!g_MapRevoted) {
		set_pcvar_num(pcv_mp_freezetime, VOTE_TIME + 10);
	}

	state lastround;
}

remove_last_round()
{
	if (g_FreezetimeRepare) {
		set_pcvar_num(pcv_mp_freezetime, g_Freezetime);
		g_FreezetimeRepare = false;
	}
	
	state none;
}

change_rand_map()
{
	new i, j;
	new Array:maps = ArrayCreate(1);
	new mapsNum = 0;

	for (i = 0; i < g_MapsNum; i++) {
		if (GetMapData(i) && i != g_CurrentMapId && !g_MapConfig[MapIsLast]) {
			for (j = 0; j < g_MapConfig[MapRate]; j++) {
				ArrayPushCell(maps, i);
				mapsNum++;
			}
		}
	}

	i = random_num(0, mapsNum - 1);
	new mapId = ArrayGetCell(maps, i);
	ArrayDestroy(maps);

	if (GetMapData(mapId)) {
		set_pcvar_string(pcv_amx_nextmap, g_MapConfig[MapName]);
		message_begin(MSG_ALL, SVC_INTERMISSION);
		message_end();
		new taskTime = get_pcvar_num(pcv_mp_chattime) + 1;
		set_task(float(taskTime), "taskChangeMap");

		state changemap;

		#if defined DEBUG && DEBUG >= 1
		log_amx("Random map will be changed #%d %s", mapId, g_MapConfig[MapName]);
		#endif
	}
}

get_maps()
{
	new i, j;
	new Array:mapsRated = ArrayCreate(1);
	new mapsRatedNum = 0;
	new bool:addMap = false;

	for (i = 0; i < g_MapsNum; i++) {
		if (GetMapData(i) && i != g_CurrentMapId && !g_MapConfig[MapIsLast] && checkPlayersNum()) {
			mapsRatedNum++;
		}
	}

	new includeLastMaps = (mapsRatedNum >= MIN_VOTE_MAPS) ? false : true;

	mapsRatedNum = 0;
	for (i = 0; i < g_MapsNum; i++) {
		if (GetMapData(i) && i != g_CurrentMapId && checkPlayersNum()) {
			addMap = true;

			if (!includeLastMaps && g_MapConfig[MapIsLast]) {
				addMap = false;
			}

			if (addMap) {
				for (j = 0; j < g_MapConfig[MapRate]; j++) {
					ArrayPushCell(mapsRated, i);
					mapsRatedNum++;
				}
			}
		}
	}

	g_SelectedMapsNum = 0;
	for (i = 0; i <= MAX_VOTE_MAPS; i++) {
		g_SelectedMaps[i][VoteId] = -1;
	}

	for (i = 1; i <= MAX_PLAYERS; i++) {
		if (is_user_connected(i) && CheckBit(g_FlagNominate, i)) {
			g_SelectedMaps[g_SelectedMapsNum][VoteId] = g_NominatedMap[i];
			g_SelectedMapsNum++;
		}
	}

	j = 0;
	while (g_SelectedMapsNum < MAX_VOTE_MAPS && j < mapsRatedNum) {
		i = random_num(0, mapsRatedNum - 1);
		i = ArrayGetCell(mapsRated, i);

		if (!isMapSelected(i)) {
			g_SelectedMaps[g_SelectedMapsNum][VoteId] = i;
			g_SelectedMapsNum++;
		}

		j++;
	}

	if (g_MapExendEnable) {
		new timelimit_max = get_pcvar_num(pcv_timelimit_max) * 60;
		if ((get_systime(0) - g_StartTime) > timelimit_max) {
			g_MapExendEnable = false;
		}
	}

	if (g_MapExendEnable) {
		g_SelectedMaps[MAX_VOTE_MAPS][VoteId] = g_CurrentMapId;
	}

	#if defined DEBUG  && DEBUG >= 1
	log_amx("GET MAPS ===");
	for (i = 0; i < g_SelectedMapsNum; i++) {
		GetMapData(g_SelectedMaps[i][VoteId]);
		log_amx("Add map: #%d %s (menu #%d)", g_SelectedMaps[i][VoteId], g_MapConfig[MapName], i);
	}
	if (g_MapExendEnable) {
		log_amx("Add map extend: #%d (menu #%d)", g_SelectedMaps[MAX_VOTE_MAPS][VoteId], MAX_VOTE_MAPS);
	} else {
		log_amx("Map extend disabled");
	}
	log_amx("END GET MAPS ===");
	#endif

	ArrayDestroy(mapsRated);
}

get_maps_revote(max_val)
{
	new i, revoteMaps[MAX_VOTE_MAPS+1], revoteMapsNum;
	for (i = 0; i < g_SelectedMapsNum; i++) {
		if (g_SelectedMaps[i][VoteCount] == max_val){
			revoteMaps[revoteMapsNum++] = g_SelectedMaps[i][VoteId];
		}
	}

	g_MapExendEnable = (max_val == g_SelectedMaps[MAX_VOTE_MAPS][VoteCount]) ? true : false;

	g_SelectedMapsNum = 0;
	for (i = 0; i <= MAX_VOTE_MAPS; i++) {
		g_SelectedMaps[i][VoteId] = -1;
	}

	for (i = 0; i < revoteMapsNum; i++) {
		g_SelectedMaps[g_SelectedMapsNum][VoteId] = revoteMaps[i];
		g_SelectedMapsNum++;
	}

	if (g_MapExendEnable) {
		g_SelectedMaps[MAX_VOTE_MAPS][VoteId] = g_CurrentMapId;
	}

	#if defined DEBUG  && DEBUG >= 1
	log_amx("GET MAPS REVOTE ===");
	for (i = 0; i < g_SelectedMapsNum; i++) {
		GetMapData(g_SelectedMaps[i][VoteId]);
		log_amx("Add map: #%d %s (menu #%d)", g_SelectedMaps[i][VoteId], g_MapConfig[MapName], i);
	}
	if (g_MapExendEnable) {
		log_amx("Add map extend: #%d (menu #%d)", g_SelectedMaps[MAX_VOTE_MAPS][VoteId], MAX_VOTE_MAPS);
	} else {
		log_amx("Map extend disabled");
	}
	log_amx("END GET MAPS REVOTE ===");
	#endif
}

get_maps_votemap(id)
{
	g_SelectedMapsNum = 0;
	for (new i = 0; i < g_AdminVoteMapNum[id]; i++) {
		g_SelectedMaps[g_SelectedMapsNum][VoteId] = g_AdminVoteMap[id][i];
		g_SelectedMapsNum++;
	}

	if ((get_systime(0) - g_StartTime) > get_pcvar_num(pcv_timelimit_max)) {
		g_MapExendEnable = false;
	}

	if (g_MapExendEnable) {
		g_SelectedMaps[MAX_VOTE_MAPS][VoteId] = g_CurrentMapId;
	}
}