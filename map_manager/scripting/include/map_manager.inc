#if defined _map_manager_included
	#endinput
#endif

#define _map_manager_included

#define MP_STATUS_EXTEND 0
#define MP_STATUS_CHANGE 1
#define MP_STATUS_REVOTE 2

forward mp_vote_started();
forward mp_vote_finished(status);
