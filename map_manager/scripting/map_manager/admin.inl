#if defined _admin_included
	#endinput
#endif

#define _admin_included

public cmdMapMenu(id) <lastround>
{
	if (!(get_user_flags(id) & ADMIN_CHANGE_FLAG)) {
		return PLUGIN_HANDLED;
	}
	client_print_color(id, print_team_default, "%L", id, "MM_VOTING_PROGRESS");
	return PLUGIN_HANDLED;
}

public cmdMapMenu(id) <changemap>
{
	if (!(get_user_flags(id) & ADMIN_CHANGE_FLAG)) {
		return PLUGIN_HANDLED;
	}

	client_print_color(id, print_team_default, "%L", id, "MM_VOTING_PROGRESS");
	return PLUGIN_HANDLED;
}

public cmdMapMenu(id) <>
{
	if (!(get_user_flags(id) & ADMIN_CHANGE_FLAG)) {
		return PLUGIN_HANDLED;
	}
	
	if (!g_MapsNum) {
		client_print_color(id, print_team_default, "%L", id, "MM_NO_MAPS");
		return PLUGIN_HANDLED;
	}
	
	menu_display(id, g_MenuMapChange, 0);

	return PLUGIN_HANDLED;
}

public cmdMapMenuHandler(id, menu, item) <lastround>
{
	#pragma unused item
	client_print_color(id, print_team_default, "%L", id, "MM_VOTING_PROGRESS");
	return PLUGIN_HANDLED;
}

public cmdMapMenuHandler(id, menu, item) <changemap>
{
	#pragma unused item
	client_print_color(id, print_team_default, "%L", id, "MM_VOTING_PROGRESS");
	return PLUGIN_HANDLED;
}

public cmdMapMenuHandler(id, menu, item) <>
{
	if (item == MENU_EXIT) {
		return PLUGIN_HANDLED;
	}

	new data[6], name[64], access, callback;
	menu_item_getinfo(menu, item, access, data, charmax(data), name, charmax(name), callback);

	g_ChangeMapId = str_to_num(data);
	menu_display(id, g_MenuMapConfirm);

	return PLUGIN_CONTINUE;
}

public cmdVoteMapConfirmHandler(id, menu, item)
{
	if (item == 0) {
		state changemap;
		new name[32];
		get_user_name(id, name, 31);
		if (GetMapData(g_ChangeMapId)) {
			client_print_color(0, print_team_default, "%L", id, "MM_ADMIN_CHANGE_MAP", name, g_MapConfig[MapName]);
			sendHudmessage(100, 200, 0, -1.0, 0.1, 0, 0.0, 8.0, 1.0, 1.0, 4, "%L", LANG_SERVER, "MM_LAST_ROUND");

			log_amx("Admin %s change maps %s. Players %d", name, g_MapConfig[MapName], get_playersnum(-1));
		} else {
			g_ChangeMapId = -1;
		}
	} else {
		g_ChangeMapId = -1;
	}
}

public cmdVoteMapMenu(id) <lastround>
{
	if (!(get_user_flags(id) & ADMIN_VOTE_FLAG)) {
		return PLUGIN_HANDLED;
	}
	
	client_print_color(id, print_team_default, "%L", id, "MM_VOTING_PROGRESS");
	return PLUGIN_HANDLED;
}

public cmdVoteMapMenu(id) <changemap>
{
	if (!(get_user_flags(id) & ADMIN_VOTE_FLAG)) {
		return PLUGIN_HANDLED;
	}

	client_print_color(id, print_team_default, "%L", id, "MM_VOTING_PROGRESS");
	return PLUGIN_HANDLED;
}

public cmdVoteMapMenu(id) <>
{
	if (!(get_user_flags(id) & ADMIN_VOTE_FLAG)) {
		return PLUGIN_HANDLED;
	}

	if (!g_MapsNum) {
		client_print_color(id, print_team_default, "%L", id, "MM_NO_MAPS");
		return PLUGIN_HANDLED;
	}

	g_AdminVoteMapNum[id] = 0;

	menu_display(id, g_MenuMapVote, 0);

	return PLUGIN_CONTINUE;
}

public cmdVoteMapMenuHandler(id, menu, item) <lastround>
{
	#pragma unused item
	client_print_color(id, print_team_default, "%L", id, "MM_VOTING_PROGRESS");
	return PLUGIN_HANDLED;
}

public cmdVoteMapMenuHandler(id, menu, item) <changemap>
{
	#pragma unused item
	client_print_color(id, print_team_default, "%L", id, "MM_VOTING_PROGRESS");
	return PLUGIN_HANDLED;
}

public cmdVoteMapMenuHandler(id, menu, item) <>
{
	if (item == MENU_EXIT) {
		return PLUGIN_HANDLED;
	}

	new data[6], name[64], access, callback;

	menu_item_getinfo(menu, item, access, data, charmax(data), name, charmax(name), callback);
	if (equal(data, "START")) {
		if (g_AdminVoteMapNum[id] > 0) {

			g_MapExendEnable = true;
			get_maps_votemap(id);
			set_last_round();
			g_MapRevoted = false;

			new name[32];
			get_user_name(id, name, 31);
			client_print_color(0, print_team_default, "%L", id, "MM_ADMIN_VOTE_MAP", name);
			sendHudmessage(100, 200, 0, -1.0, 0.1, 0, 0.0, 8.0, 1.0, 1.0, 4, "%L", LANG_SERVER, "MM_LAST_ROUND");

			new len, msg[256];
			len = format(msg, 255, "Admin %s nominate maps:", name);
			for (new i = 0; i < g_AdminVoteMapNum[id]; i++) {
				if (GetMapData(g_AdminVoteMap[id][i])) {
					len += format(msg[len], 255 - len, " %s", g_MapConfig[MapName]);
				}
			}
			len += format(msg[len], 255 - len, ". Players %d", g_PlayersNum);
			log_amx(msg);
		} else {
			resetMenu(id, menu);
		}
	#if !defined menu_addblank2
	} else if (equal(data, "EMPTY")) {
		resetMenu(id, menu);
	#endif
	} else {
		new vote = str_to_num(data);
		setMapAdmin(id, vote);
		resetMenu(id, menu);
	}

	return PLUGIN_CONTINUE;
}

public cmdVoteMapMenuCallback(id, menu, item)
{
	new data[6], title[64], access, callback;
	menu_item_getinfo(menu, item, access, data, charmax(data), title, charmax(title), callback);

	new vote = str_to_num(data);

	GetMapData(vote);
	if(isMapAdminVote(id, vote)) {
		format(title, 31, "\r%s", g_MapConfig[MapName]);
	} else {
		format(title, 31, "%s", g_MapConfig[MapName]);
	}

	menu_item_setname(menu, item, title);

	return ITEM_ENABLED;
}