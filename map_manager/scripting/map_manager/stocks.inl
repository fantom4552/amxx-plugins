#if defined _stocks_included
	#endinput
#endif

#define _stocks_included

stock bool:GetMapData(mapId) 
{
	if (0 <= mapId <= g_MapsNum) {
		ArrayGetArray(g_Maps, mapId, g_MapConfig);
		return true;
	}

	return false;
}

stock bool:checkPlayersNum() 
{
	return (g_MapConfig[MapMin] <= g_PlayersNum <= g_MapConfig[MapMax]);
}

stock check_cvar_num(cvar, min, max)
{
	new value = get_pcvar_num(cvar);

	if(value < min) {
		value = min;
	} else if (value > max) {
		value = max;
	}

	return 	value;
}

stock bool:isMapSelected(id) {
	for (new i = 0; i < g_SelectedMapsNum; i++) {
		if(g_SelectedMaps[i][VoteId] == id){
			return true;
		}
	}

	return false;
}

stock bool:isMapNominated(mapId) {
	for (new id = 1; id <= MAX_PLAYERS; id++) {
		if (is_user_connected(id) && CheckBit(g_FlagNominate, id) && g_NominatedMap[id] == mapId) {
			return true;
		}
	}

	return false;
}

stock numMapNominated()
{
	new id, num;
	for (id = 1; id <= MAX_PLAYERS; id++) {
		if (is_user_connected(id) && CheckBit(g_FlagNominate, id)) {
			num++;
		}
	}

	return num;
}

stock bool:isMapAdminVote(id, mapId) {
	for (new i = 0; i < g_AdminVoteMapNum[id]; i++) {
		if (g_AdminVoteMap[id][i] == mapId) {
			return true;
		}
	}

	return false;
}

stock bool:setMapAdmin(id, mapId) {
	new i, bool:action_add = true;
	for (i = 0; i < g_AdminVoteMapNum[id]; i++) {
		if (!action_add) {
			g_AdminVoteMap[id][i] = g_AdminVoteMap[id][i+1];
		} else if(g_AdminVoteMap[id][i] == mapId) {
			g_AdminVoteMap[id][i] = g_AdminVoteMap[id][i+1];
			action_add = false;
		}
	}

	if(action_add) {
		if(g_AdminVoteMapNum[id] < MAX_VOTE_MAPS) {
			g_AdminVoteMap[id][g_AdminVoteMapNum[id]++] = mapId;
		}
	} else {
		g_AdminVoteMapNum[id]--;
	}

	return action_add;
}

stock bool:isLastMap(mapname[])
{
	for (new i = 0; i < g_LastMapsNum; i++) {
		if(equali(mapname, g_LastMaps[i])) {
			return true;
		}
	}

	return false;
}

stock bool:isValidMap(mapname[])
{
	if (is_map_valid(mapname)) {
		return true;
	}

	new len = strlen(mapname) - 4;
	if (len < 0) {
		return false;
	}

	if (equali(mapname[len], ".bsp")) {
		// If the ending was .bsp, then cut it off.
		// the string is byref'ed, so this copies back to the loaded text.
		mapname[len] = '^0';
		if (is_map_valid(mapname)) {
			return true;
		}
	}

	return false;
}

stock sendHudmessage(red=200, green=100, blue=0, Float:x=-1.0, Float:y=0.35, effects=0, Float:fxtime=6.0, Float:holdtime=12.0, Float:fadeintime=0.1, Float:fadeouttime=0.2, channel=4, msg[], any:...)
{
	new players[32], num;
	get_players(players, num, "ch");
	
	new message[192];
	vformat(message, charmax(message), msg, 13);
	
	set_hudmessage(red, green, blue, x, y, effects, fxtime, holdtime, fadeintime, fadeouttime, channel);
	
	for (new i = 0; i < num; i++) {
		if (is_user_connected(players[i])) {
			ShowSyncHudMsg(players[i], g_HudSync, message);
		}
	}
}

stock screenFade(time, hold, flags)
{
	message_begin(MSG_ALL, g_MsgScrenFade, {0,0,0}, 0);
	write_short(time);
	write_short(hold);
	write_short(flags);
	write_byte(0);
	write_byte(0);
	write_byte(0);
	write_byte(255);
	message_end();
}

stock cmd_pause_plugin(arg[32])
{
	new len = strlen(arg);

	if (len && (cmd_find_plugin(arg, len) != -1) && pause("dc", arg)) {
		log_amx("Plugin '%s' stopped", arg);
	}
}

stock cmd_find_plugin(arg[32], &len)
{
	new name[32], title[32], status[2];
	new pluginsnum = get_pluginsnum();

	for (new i = 0; i < pluginsnum; ++i)
	{
		get_plugin(i, name, charmax(name), title, charmax(title), status, 0, status, 0, status, 1);
		if (equali(name, arg, len) && (
			status[0] == 'r' ||	/*running*/
			status[0] == 'p' ||	/*paused*/
			status[0] == 's' ||	/*stopped*/
			status[0] == 'd' 	/*debug*/
		)) {
			len = copy(arg, charmax(arg), name);
			return i;
		}
	}
	
	return -1;
}