#if defined _global_vars_included
	#endinput
#endif

#define _global_vars_included

// Constants
new const g_Speak[5][] = {"one", "two", "three", "four", "five"};
new g_ConfigDir[128];

// CVars
new pcv_amx_nextmap;
new pcv_amx_timeleft;

new pcv_mp_timelimit;
new pcv_mp_chattime;
new pcv_mp_roundtime;
new pcv_mp_freezetime;
new pcv_mp_c4timer;

new pcv_voteratio;
new pcv_timeout_nominate;
new pcv_users_nominate;
new pcv_timelimit_step;
new pcv_timelimit_max;

new g_FwdVoteStarted;
new g_FwdVoteFinished;

new Float:g_Addtime = 0.0;
new g_PlayersNum = 0;

// CVars buffer
new bool:g_FreezetimeRepare	= false;
new g_Freezetime;
new bool:g_TimelimitRepare	= false;
new Float:g_Timelimit;

// Last maps
new g_LastMaps[LAST_MAP_SIZE + 1][32];
new g_LastMapsNum;

// Map Config
enum _:MapConfig {
	MapName[32],
	bool:MapIsLast,
	MapRate,
	MapMin,
	MapMax
};

new Array:g_Maps;
new g_MapsNum;
new g_MapConfig[MapConfig];

new g_CurrentMapId;

// Votemap
enum _:VoteMap {
	VoteId,
	VoteCount
};

new g_SelectedMaps[MAX_VOTE_MAPS+1][VoteMap];
new g_SelectedMapsNum;

// Admin Votemap
new g_AdminVoteMap[MAX_PLAYERS + 1][MAX_VOTE_MAPS];
new g_AdminVoteMapNum[MAX_PLAYERS + 1];

// Nominated
new g_NominatedMap[MAX_PLAYERS + 1];

// Changed
new g_ChangeMapId;
new g_NumPlayersVoted;

// Flags
new g_FlagVotemap;
new g_FlagNominate;
new g_FlagMenuSelect;

// Booleans
new bool:g_VotemapEnable;
new bool:g_MapExendEnable;
new bool:g_MapRevoted;
new bool:g_VotemapStarted;
new bool:g_RockTheVote;

// Messages
new g_MsgScrenFade;
new g_HudSync;

// Timers
new g_Timer;
new g_StartTime;

// Menus
new g_MenuMapChange;
new g_MenuMapConfirm;
new g_MenuMapVote;
new g_MenuMapNominate;