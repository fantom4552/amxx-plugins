#if defined _menu_included
	#endinput
#endif

#define _menu_included

menuMapChangeCreate()
{
	new i, item[4], title[64];

	format(title, charmax(title), "\r%L", LANG_SERVER, "MM_MAP_TITLE");
	g_MenuMapChange = menu_create(title, "cmdMapMenuHandler");
	
	format(title, charmax(title), "%L", LANG_SERVER, "BACK");
	menu_setprop(g_MenuMapChange, MPROP_BACKNAME, title);
	format(title, charmax(title), "%L", LANG_SERVER, "MORE");
	menu_setprop(g_MenuMapChange, MPROP_NEXTNAME, title);
	format(title, charmax(title), "%L^n^n\y%s by %s", LANG_SERVER, "EXIT", PLUGIN, AUTHOR);
	menu_setprop(g_MenuMapChange, MPROP_EXITNAME, title);
	
	for (i = 0; i < g_MapsNum; i++) {
		if (GetMapData(i)) {
			num_to_str(i, item, charmax(item));
			menu_additem(g_MenuMapChange, g_MapConfig[MapName], item);
		}
	}
}

menuMapChangeDestroy()
{
	menu_destroy(g_MenuMapChange);
}

menuMapConfirmCreate()
{
	new title[64];

	format(title, charmax(title), "\r%L^n\y%s by %s", LANG_SERVER, "MM_MAP_CONFIRM", PLUGIN, AUTHOR);
	g_MenuMapConfirm = menu_create(title, "cmdVoteMapConfirmHandler");
	menu_setprop(g_MenuMapConfirm, MPROP_EXIT, MEXIT_NEVER);

	format(title, charmax(title), "%L", LANG_SERVER, "YES");
	menu_additem(g_MenuMapConfirm, title, "");
	format(title, charmax(title), "%L", LANG_SERVER, "NO");
	menu_additem(g_MenuMapConfirm, title, "");
}

menuMapConfirmCDestroy()
{
	menu_destroy(g_MenuMapConfirm);
}

menuMapVoteCreate()
{
	new i, item[4], title[64];

	format(title, charmax(title), "\r%L", LANG_SERVER, "MM_MAP_TITLE");

	g_MenuMapVote = menu_create(title, "cmdVoteMapMenuHandler");
	new callback = menu_makecallback("cmdVoteMapMenuCallback");

	menu_setprop(g_MenuMapVote, MPROP_EXIT, MEXIT_ALL);
	menu_setprop(g_MenuMapVote, MPROP_PERPAGE, 7);
	
	format(title, charmax(title), "%L", LANG_SERVER, "BACK");
	menu_setprop(g_MenuMapVote, MPROP_BACKNAME, title);
	format(title, charmax(title), "%L", LANG_SERVER, "MORE");
	menu_setprop(g_MenuMapVote, MPROP_NEXTNAME, title);
	format(title, charmax(title), "%L^n^n\y%s by %s", LANG_SERVER, "EXIT", PLUGIN, AUTHOR);
	menu_setprop(g_MenuMapVote, MPROP_EXITNAME, title);

	for (i = 0; i < g_MapsNum; i++) {

		if (i > 0 && (i % 6) == 0) {
			menu_addblank(g_MenuMapVote, 0);
			format(title, charmax(title), "\r%L", LANG_SERVER, "MM_START_VOTE");
			menu_additem(g_MenuMapVote, title, "START");
		}

		if (GetMapData(i)) {
			num_to_str(i, item, charmax(item));
			menu_additem(g_MenuMapVote, g_MapConfig[MapName], item, 0, callback);
		}
	}
	
	if ((g_MapsNum % 6) > 0) {
		new temp_c = 6 - (g_MapsNum  % 6);
		for (i = 0; i < temp_c; i++) {
			#if defined menu_addblank2
			menu_addblank2(g_MenuMapVote);
			#else
			menu_additem(g_MenuMapVote, "", "EMPTY");
			#endif
		}

		menu_addblank(g_MenuMapVote, 0);
		format(title, charmax(title), "\r%L", LANG_SERVER, "MM_START_VOTE");
		menu_additem(g_MenuMapVote, title, "START");
	}
}

menuMapVoteDestroy()
{
	menu_destroy(g_MenuMapVote);
}

menuMapNominateCreate()
{
	new title[64];

	format(title, charmax(title), "\r%L", LANG_SERVER, "MM_MAP_TITLE");
	g_MenuMapNominate = menu_create(title, "cmdMapsHandler");

	new callback = menu_makecallback("cmdMapsCalback");

	new i, item[4];
	for (i = 0; i < g_MapsNum; i++) {
		if (GetMapData(i)) {
			num_to_str(i, item, charmax(item));
			menu_additem(g_MenuMapNominate, g_MapConfig[MapName], item, 0, callback);
		}
	}
}

menuMapNominateDestroy()
{
	menu_destroy(g_MenuMapNominate);
}

stock resetMenu(id, menu)
{
	new oldmenu, newmenu, page;

	player_menu_info(id, oldmenu, newmenu, page);
	menu_display(id, menu, page);
}