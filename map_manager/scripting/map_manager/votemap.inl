#if defined _votemap_included
	#endinput
#endif

#define _votemap_included

voteMenu()
{
	if (!g_VotemapStarted) {

		new i, item[4];
		new title[64];

		for (i = 0; i <= MAX_VOTE_MAPS; i++) {
			g_SelectedMaps[i][VoteCount] = 0;
		}

		format(title, charmax(title), "\r%L^n\y%s by %s", LANG_PLAYER, "MM_MAP_TITLE", PLUGIN, AUTHOR);
		new menu = menu_create(title, "voteMenuHandler");

		menu_setprop(menu, MPROP_PERPAGE, 0);
		menu_setprop(menu, MPROP_EXIT, MEXIT_NEVER);

		new callback = menu_makecallback("voteMenuCallback");
		
		for (i = 0; i < g_SelectedMapsNum; i++) {
			if (GetMapData(g_SelectedMaps[i][VoteId])) {
				num_to_str(i, item, charmax(item));
				menu_additem(menu, g_MapConfig[MapName], item, 0, callback);
			}
		}

		for (i = g_SelectedMapsNum; i <= MAX_VOTE_MAPS; i++) {
			#if defined menu_addblank2
			menu_addblank2(menu);
			#else
			menu_additem(menu, "", "EMPTY");
			#endif
		}

		if (g_MapExendEnable && GetMapData(g_SelectedMaps[MAX_VOTE_MAPS][VoteId])) {
			format(title, charmax(title), "%L", LANG_PLAYER, "MM_MAP_EXTEND", g_MapConfig[MapName]);
			num_to_str(MAX_VOTE_MAPS, item, charmax(item));
			menu_additem(menu, title, item, 0, callback);
		}

		g_FlagMenuSelect = 0;
		g_NumPlayersVoted = 0;

		#if defined DEBUG && DEBUG >= 1
		new players[32], num;
		get_players(players, num, "ch");
		log_amx("VOTE START ===");
		log_amx("Players online %d", num);
		#endif

		voteMenuDisplay(menu);

		new data[1];
		data[0] = menu;

		g_Timer = VOTE_TIME;
		set_task(1.0, "taskVotemap", _, data, 1, "a", g_Timer + 1);

		g_VotemapStarted = true;
	}
}

public voteMenuHandler(id, menu, item)
{
	if (item == MENU_EXIT) {
		return PLUGIN_HANDLED;
	}

	#if !defined DEBUG || DEBUG <= 1
	if (CheckBit(g_FlagMenuSelect, id)) {
		menu_display(id, menu, 0);
		return PLUGIN_CONTINUE;
	}
	#endif

	new data[6], name[64], access, callback;
	menu_item_getinfo(menu, item, access, data, charmax(data), name, charmax(name), callback);

	#if !defined menu_addblank2
	if (equal(data, "EMPTY")) {
		menu_display(id, menu, 0);
	} else {
	#endif

	new vote = str_to_num(data);
	
	g_SelectedMaps[vote][VoteCount]++;
	SetBit(g_FlagMenuSelect, id);

	get_user_name(id, name, charmax(name));

	new print_color;
	switch (get_user_team(id))
	{
		case 1: {
			print_color = print_team_red;
		}

		case 2: {
			print_color = print_team_blue;
		}

		default: {
			print_color = print_team_grey;
		}
	}

	if (vote != MAX_VOTE_MAPS) {
		GetMapData(g_SelectedMaps[vote][VoteId]);
		client_print_color(0, print_color, "%L", LANG_PLAYER, "MM_MAP_CHANGE", name, g_MapConfig[MapName]);
		#if defined DEBUG && DEBUG >= 1
		log_amx("Player %s choose #%d %s (menu #%d)", name, g_SelectedMaps[vote][VoteId], g_MapConfig[MapName], vote);
		#endif
	} else {
		client_print_color(0, print_color, "%L", LANG_PLAYER, "MM_MAP_CHANGE_EXTEND", name);
		#if defined DEBUG && DEBUG >= 1
		GetMapData(g_SelectedMaps[MAX_VOTE_MAPS][VoteId]);
		log_amx("Player %s choose extend #%d %s (menu #%d)", name, g_SelectedMaps[MAX_VOTE_MAPS][VoteId], g_MapConfig[MapName], vote);
		#endif
	}

	g_NumPlayersVoted++;

	voteMenuDisplay(menu);

	#if !defined menu_addblank2
	}
	#endif

	return PLUGIN_CONTINUE;
}

public voteMenuCallback(id, menu, item) 
{
	static access, data[6], name[128], callback;
	menu_item_getinfo(menu, item, access, data, charmax(data), name, charmax(name), callback);

	new vote = str_to_num(data);

	if (vote != MAX_VOTE_MAPS) {
		GetMapData(g_SelectedMaps[vote][VoteId]);
		format(name, charmax(name), "%s%s [%d]", (CheckBit(g_FlagMenuSelect, id) ? "\d" : ""), g_MapConfig[MapName], g_SelectedMaps[vote][VoteCount]);
	} else {
		get_mapname(name, charmax(name));
		format(name, charmax(name), "%s%L [%d]", (CheckBit(g_FlagMenuSelect, id) ? "\d" : ""), LANG_PLAYER, "MM_MAP_EXTEND", name, g_SelectedMaps[MAX_VOTE_MAPS][VoteCount]);
	}

	menu_item_setname(menu, item, name);

	// if (CheckBit(g_FlagMenuSelect, id)) {
	// 	return ITEM_DISABLED;
	// }

	return ITEM_ENABLED;
}

public voteMenuEnd(menu)
{
	new i, max_id, max_val;
	new bool:revote = false;
	new ret;

	voteMenuClose(menu);

	#if defined DEBUG && DEBUG >= 1
	log_amx("VOTE FINISH ===");
	for (i = 0; i < g_SelectedMapsNum; i++) {
		GetMapData(g_SelectedMaps[i][VoteId]);
		log_amx("Map voted: #%d %s (menu #%d). Count: %d", g_SelectedMaps[i][VoteId], g_MapConfig[MapName], i, g_SelectedMaps[i][VoteCount]);
	}
	if (g_MapExendEnable) {
		GetMapData(g_SelectedMaps[MAX_VOTE_MAPS][VoteId]);
		log_amx("Map voted extend: #%d %s (menu #%d). Count: %d", g_SelectedMaps[MAX_VOTE_MAPS][VoteId], g_MapConfig[MapName], MAX_VOTE_MAPS, g_SelectedMaps[MAX_VOTE_MAPS][VoteCount]);
	} else {
		log_amx("Map extend disabled");
	}
	#endif

	max_id = g_SelectedMaps[MAX_VOTE_MAPS][VoteId];
	max_val = g_SelectedMaps[MAX_VOTE_MAPS][VoteCount];
	for (i = 0; i < g_SelectedMapsNum; i++) {
		if (!g_MapRevoted && g_SelectedMaps[i][VoteCount] == max_val){
			max_id = g_SelectedMaps[i][VoteId];
			revote = true;
		} else if (g_SelectedMaps[i][VoteCount] > max_val){
			max_id = g_SelectedMaps[i][VoteId];
			max_val = g_SelectedMaps[i][VoteCount];
			revote = false;
		}
	}

	#if defined DEBUG
	GetMapData(max_id);
	log_amx("Votemap choose #%d %s (revote %s). Max count %d", max_id, g_MapConfig[MapName], (revote ? "YES" : "NO"), max_val);
	#endif

	if (!g_MapRevoted && revote && max_id != g_CurrentMapId) {
		ExecuteForward(g_FwdVoteFinished, ret, MP_STATUS_REVOTE);
		g_MapRevoted = true;
		set_last_round();
		get_maps_revote(max_val);
		sendHudmessage(255, 255, 255, -1.0, 0.1, 0, 0.0, 3.00, 0.0, 0.0, 4, "%L", LANG_PLAYER, "MM_MAP_REVOTE_HUD");
		client_print_color(0, print_team_default, "%L", LANG_PLAYER, "MM_MAP_REVOTE");
		screenFade(4096, 1024, 2);
		#if defined DEBUG  && DEBUG >= 1
		log_amx("Map will be revoted");
		#endif
	} else if (g_MapExendEnable && max_id == g_CurrentMapId) {
		ExecuteForward(g_FwdVoteFinished, ret, MP_STATUS_EXTEND);
		new Float:steptime = get_pcvar_float(pcv_timelimit_step);
		set_pcvar_float(pcv_mp_timelimit, get_pcvar_float(pcv_mp_timelimit) + steptime - g_Addtime);
		g_Addtime = 0.0;
		remove_last_round();
		sendHudmessage(255, 255, 255, -1.0, 0.1, 0, 0.0, 3.00, 0.0, 0.0, 4, "%L", LANG_PLAYER, "MM_MAP_EXTENDED_TIME_HUD", steptime);
		client_print_color(0, print_team_default, "%L", LANG_PLAYER, "MM_MAP_EXTENDED_TIME", steptime);
		screenFade(4096, 1024, 2);
		state none;
		#if defined DEBUG && DEBUG >= 1
		log_amx("Map will be extended %.0f minutes", steptime);
		#endif
	} else {
		ExecuteForward(g_FwdVoteFinished, ret, MP_STATUS_CHANGE);
		GetMapData(max_id);
		set_pcvar_string(pcv_amx_nextmap, g_MapConfig[MapName]);
		sendHudmessage(255, 255, 255, -1.0, 0.1, 0, 0.0, 3.00, 0.0, 0.0, 4, "%L", LANG_PLAYER, "MM_MAP_FINISH_HUD", g_MapConfig[MapName]);
		client_print_color(0, print_team_default, "%L", LANG_PLAYER, "MM_MAP_FINISH", g_MapConfig[MapName]);
		message_begin(MSG_ALL, SVC_INTERMISSION);
		message_end();
		new taskTime = get_pcvar_num(pcv_mp_chattime) + 1;
		set_task(float(taskTime), "taskChangeMap");
		state none;
		#if defined DEBUG && DEBUG >= 1
		log_amx("Map will be changed #%d %s", max_id, g_MapConfig[MapName]);
		#endif
	}

	g_VotemapStarted = false;

	if (g_RockTheVote) {
		ResetBit(g_FlagVotemap);
	}
	g_RockTheVote = false;
}

voteMenuDisplay(menu)
{
	new i, players[32], num;
	get_players(players, num, "ch");
	for (i = 0; i < num; i++) {
		menu_display(players[i], menu, 0);
	}
}

voteMenuClose(menu)
{
	new i, players[32], num;
	get_players(players, num, "ch");
	for (i = 0; i < num; i++) {
		menu_cancel(players[i]);
		client_cmd(players[i], "slot1");
	}

	menu_destroy(menu);
}