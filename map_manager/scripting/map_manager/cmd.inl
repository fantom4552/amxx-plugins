#if defined _cmd_included
	#endinput
#endif

#define _cmd_included

public cmdRockTheVote(id) <lastround>
{
	client_print_color(id, print_team_default, "%L", id, "MM_VOTING_PROGRESS");
	return PLUGIN_CONTINUE;
}

public cmdRockTheVote(id) <changemap>
{
	client_print_color(id, print_team_default, "%L", id, "MM_VOTING_PROGRESS");
	return PLUGIN_CONTINUE;
}

public cmdRockTheVote(id) <>
{
	new i, voted = 0;
	new Float:voteratio = get_pcvar_float(pcv_voteratio);

	if (voteratio == 0.0) {
		client_print_color(id, print_team_default, "%L", id, "MM_VOTE_MAP_DISABLE");
		return PLUGIN_CONTINUE;
	}

	if (!g_VotemapEnable) {
		new timer = get_pcvar_num(pcv_timeout_nominate) - (get_systime(0) - g_StartTime);
		server_print("RTV get_systime %d, g_StartTime %d, pcv_timeout_nominate %d, time %d", get_systime(0), g_StartTime, get_pcvar_num(pcv_timeout_nominate), timer);
		client_print_color(id, print_team_default, "%L %02d:%02d", id, "MM_VOTE_MAP_TIMER", timer / 60, timer % 60);
		return PLUGIN_CONTINUE;
	}

	if (CheckBit(g_FlagVotemap, id)) {
		client_print_color(id, print_team_default, "%L", id, "MM_VOTE_MAP_ALREADY");
	} else {
		SetBit(g_FlagVotemap, id);

		new name[32];
		get_user_name(id, name, 31);
		client_print_color(0, print_team_default, "%L", LANG_PLAYER, "MM_VOTE_MAP_CHANGE", name);
	}

	for (i = 1; i <= MAX_PLAYERS; i++) {
		if (is_user_connected(i) && CheckBit(g_FlagVotemap, i)) {
			voted++;
		}
	}

	if (g_PlayersNum > 0 && floatround(voteratio * 100.0) > (voted * 100 / g_PlayersNum)) {
		client_print_color(0, print_team_default, "%L", LANG_PLAYER, "MM_VOTE_MAP_PLAYERS", voted, floatround(voteratio * g_PlayersNum + 0.49));
	} else {
		g_MapExendEnable = true;
		g_MapRevoted = false;
		g_RockTheVote = true;
		get_maps();
		set_last_round();
		client_print_color(0, print_team_default, "%L", id, "MM_VOTE_MAP_CHANGE_ALL");
		sendHudmessage(100, 200, 0, -1.0, 0.1, 0, 0.0, 8.0, 1.0, 1.0, 4, "%L", LANG_SERVER, "MM_LAST_ROUND");
	}

	return PLUGIN_CONTINUE;
}

public cmdMaps(id) <lastround>
{
	client_print_color(id, print_team_default, "%L", id, "MM_VOTING_PROGRESS");
	return PLUGIN_CONTINUE;
}

public cmdMaps(id) <changemap>
{
	client_print_color(id, print_team_default, "%L", id, "MM_VOTING_PROGRESS");
	return PLUGIN_CONTINUE;
}

public cmdMaps(id) <>
{
	if (numMapNominated() > get_pcvar_num(pcv_users_nominate)) {
		client_print_color(id, print_team_default, "%L", id, "MM_VOTE_MAP_MAX");
		return PLUGIN_CONTINUE;
	}

	menu_display(id, g_MenuMapNominate);

	return PLUGIN_CONTINUE;
}

public cmdMapsHandler(id, menu, item) <lastround>
{
	#pragma unused item
	client_print_color(id, print_team_default, "%L", id, "MM_VOTING_PROGRESS");
	return PLUGIN_HANDLED;
}

public cmdMapsHandler(id, menu, item) <changemap>
{
	#pragma unused item
	client_print_color(id, print_team_default, "%L", id, "MM_VOTING_PROGRESS");
	return PLUGIN_HANDLED;
}

public cmdMapsHandler(id, menu, item) <>
{
	new mapname[32];

	if (numMapNominated() > get_pcvar_num(pcv_users_nominate)) {
		client_print_color(id, print_team_default, "%L", id, "MM_VOTE_MAP_MAX");
		return PLUGIN_HANDLED;
	}
	
	if (item == MENU_EXIT) {
		return PLUGIN_HANDLED;
	}

	new data[6], name[64], access, callback;
	menu_item_getinfo(menu, item, access, data, charmax(data), name, charmax(name), callback);

	new vote = str_to_num(data);

	GetMapData(vote);
	copy(mapname, charmax(mapname), g_MapConfig[MapName]);

	if (isMapNominated(vote)) {
		client_print_color(id, print_team_default, "%L", id, "MM_MAP_NOMINATE_ALREADY", mapname);
		return PLUGIN_HANDLED;
	}

	get_user_name(id, name, charmax(name));

	if (CheckBit(g_FlagNominate, id)) {
		GetMapData(g_NominatedMap[id]);
		g_NominatedMap[id] = vote;
		client_print_color(0, print_team_default, "%L", LANG_PLAYER, "MM_MAP_SWITCH", name, g_MapConfig[MapName], mapname);
	} else {
		g_NominatedMap[id] = vote;
		SetBit(g_FlagNominate, id);
		client_print_color(0, print_team_default, "%L", LANG_PLAYER, "MM_MAP_NOMINATE", name, mapname);
	}
	
	return PLUGIN_CONTINUE;
}

public cmdMapsCalback(id, menu, item)
{
	new data[6], title[64], access, callback;
	menu_item_getinfo(menu, item, access, data, charmax(data), title, charmax(title), callback);

	new mapId = str_to_num(data);

	if (!GetMapData(mapId) || mapId == g_CurrentMapId || g_MapConfig[MapIsLast] || !checkPlayersNum()) {
		return ITEM_DISABLED;
	}

	if (isMapNominated(mapId)) {
		return ITEM_DISABLED;
	}

	return ITEM_ENABLED;
}

public cmdTimeLeft(id) <lastround>
{
	client_print_color(id, print_team_default, "%L", id, "MM_VOTING_PROGRESS");
	return PLUGIN_CONTINUE;
}

public cmdTimeLeft(id) <changemap>
{
	client_print_color(id, print_team_default, "%L", id, "MM_VOTING_PROGRESS");
	return PLUGIN_CONTINUE;
}

public cmdTimeLeft(id) <>
{
	new timeleft = get_timeleft();

	if (timeleft == 0) {
		client_print_color(id, print_team_default, "%L", id, "MM_TIME_LEFT_DISABLED");
	} else {
		client_print_color(id, print_team_default, "%L %02d:%02d", id, "MM_TIME_LEFT", (timeleft / 60), (timeleft % 60));
	}

	return PLUGIN_CONTINUE;
}

public cmdTheTime(id)
{
	new thetime[64];
	get_time("%Y/%m/%d - %H:%M:%S", thetime, charmax(thetime));
	client_print_color(id, print_team_default, "%L %s", id, "MM_THE_TIME", thetime);
	return PLUGIN_CONTINUE;
}

public cmdCurrentMap(id)
{
	new mapname[32];
	get_mapname(mapname, charmax(mapname));
	client_print_color(id, print_team_default, "%L %s", id,  "MM_CURRENT_MAP", mapname);
	return PLUGIN_CONTINUE;
}