#if defined _events_included
	#endinput
#endif

#define _events_included

public eventStartGame()
{
	g_StartTime = get_systime(0);
	g_VotemapEnable = false;
}

public eventRoundStart() <none> {}
public eventRoundStart() <> {}

public eventRoundStart() <lastround>
{
	new ret;
	ExecuteForward(g_FwdVoteStarted, ret);

	screenFade(1, 1, 4);

	g_Timer = 3;
	set_task(1.0, "taskTimeToChoose", _, _, _, "a", g_Timer + 1);
}

public eventRoundStart() <changemap>
{
	screenFade(1, 1, 4);

	if (GetMapData(g_ChangeMapId)) {
		set_pcvar_string(pcv_amx_nextmap, g_MapConfig[MapName]);
			
		message_begin(MSG_ALL, SVC_INTERMISSION);
		message_end();

		new taskTime = get_pcvar_num(pcv_mp_chattime) + 1;
		set_task(float(taskTime), "taskChangeMap");
	} else {
		state none;
	}

	// set_task(Float:get_pcvar_num(pcv_mp_chattime), "changeMap", _, g_MapConfig[MapName], charmax(g_MapConfig[MapName]));
}

public taskLastRound() <lastround> 
{
	#if defined DEBUG && DEBUG >= 2
	new timeleft = get_timeleft();
	client_print(0, print_center, "%02d:%02d", (timeleft / 60), (timeleft % 60));
	#endif

	set_pcvar_string(pcv_amx_timeleft, "00:00");
}

public taskLastRound() <changemap> 
{
	#if defined DEBUG && DEBUG >= 2
	new timeleft = get_timeleft();
	client_print(0, print_center, "%02d:%02d", (timeleft / 60), (timeleft % 60));
	#endif

	set_pcvar_string(pcv_amx_timeleft, "00:00");
}

public taskLastRound() <>
{
	static timeleft, string[12];

	timeleft = get_timeleft();
	format(string, 11, "%02d:%02d", (timeleft / 60), (timeleft % 60));
	set_pcvar_string(pcv_amx_timeleft, string);

	#if defined DEBUG && DEBUG >= 2
	client_print(0, print_center, "%02d:%02d", (timeleft / 60), (timeleft % 60));
	#endif

	if (0 < timeleft <= LAST_MINUTE) {
		if (g_PlayersNum > 0) {
			g_MapExendEnable = true;
			g_MapRevoted = false;
			get_maps();
			set_last_round();
			sendHudmessage(100, 200, 0, -1.0, 0.1, 0, 0.0, 8.0, 1.0, 1.0, 4, "%L", LANG_PLAYER, "MM_LAST_ROUND");
		} else {
			#if defined DEBUG && DEBUG >= 1
			log_amx("Last round. Empty Server");
			#endif

			change_rand_map();
		}
	}

	if (!g_VotemapEnable && (get_systime(0) - g_StartTime) > get_pcvar_num(pcv_timeout_nominate)) {
		client_print_color(0, print_team_default, "%L", LANG_PLAYER, "MM_VOTE_MAP_ENABLE");
		g_VotemapEnable = true;
	}
}

public taskTimeToChoose()
{
	if(g_Timer > 0) {
		sendHudmessage(220, 160, 0, -1.0, 0.1, 0, 0.0, 1.03, 0.0, 0.0, 4, "%L", LANG_PLAYER, "MM_CHOOSE_THE_NEXT_MAP_TIME", g_Timer);
		if (g_Timer < 6) {
			client_cmd(0 ,"spk ^"fvox/%s^"", g_Speak[g_Timer - 1]);
		}

		g_Timer--;
	} else {
		sendHudmessage(100, 200, 0, -1.0, 0.1, 0, 0.0, 3.0, 1.0, 1.0, 4, "%L", LANG_PLAYER, "MM_CHOOSE_THE_NEXT_MAP");
		client_cmd(0, "spk Gman/Gman_Choose%d", random_num(1, 2));

		voteMenu();
	}
}

public taskVotemap(data[1])
{
	if(g_Timer > 0) {
		if (g_Timer < VOTE_TIME - 3) {
			sendHudmessage( 220, 160, 0, -1.0, 0.1, 0, 0.0, 1.03, 0.0, 0.0, 4, "%L", LANG_PLAYER, "MM_CHOOSE_THE_NEXT_MAP_END_TIME", g_Timer);

			if (g_Timer < 6) {
				client_cmd(0 ,"spk ^"fvox/%s^"", g_Speak[g_Timer - 1]);
			}
		}

		if (g_NumPlayersVoted > 0) {
			new plNum = get_playersnum(-1);
			set_hudmessage(0, 255, 0, -1.0, 0.2, 0, 0.0, 1.03, 0.0, 0.0, 4);
			show_hudmessage(0, "%L", LANG_PLAYER, "MM_VOTED_PLAYERS_COUNT", (g_NumPlayersVoted > plNum ? plNum : g_NumPlayersVoted), plNum);
			// sendHudmessage(g_HudSync2, 0, 255, 0, -1.0, 0.2, 0, 0.0, 1.03, 0.0, 0.0, 4, "%L", LANG_PLAYER, "MM_VOTED_PLAYERS_COUNT", (g_NumPlayersVoted > plNum ? plNum : g_NumPlayersVoted), plNum);
		}

		g_Timer--;
	} else {
		voteMenuEnd(data[0]);
	}
}

public taskChangeMap()
{
	new mapname[32];
	screenFade(4096, 1024, 2);
	get_pcvar_string(pcv_amx_nextmap, mapname, 31);
	#if defined DEBUG  && DEBUG >= 1
	log_amx("Task change map %s", mapname);
	#endif
	changeMap(mapname);
}

public checkPlayersNumTask() <changemap> {}
public checkPlayersNumTask() <lastround> {}

public checkPlayersNumTask() <>
{
	#if defined DEBUG && DEBUG >= 1
	log_amx("Start check players num. Players num: %d", g_PlayersNum);
	#endif

	if (g_PlayersNum <= 0) {
		change_rand_map();
	} else if (!checkPlayersNum()) {
		g_MapExendEnable = true;
		g_MapRevoted = false;
		get_maps();
		set_last_round();
		sendHudmessage(100, 200, 0, -1.0, 0.1, 0, 0.0, 8.0, 1.0, 1.0, 4, "%L", LANG_PLAYER, "MM_LAST_ROUND");
	}
}