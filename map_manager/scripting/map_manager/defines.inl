#if defined _defines_included
	#endinput
#endif

#define _defines_included

#define charmax(%1) (sizeof(%1) - 1)

#define CheckBit(%1,%2)  ( %1 &   (1 << (%2 & 31)))
#define SetBit(%1,%2)    (%1 |=  (1 << (%2 & 31)))
#define ClearBit(%1,%2)  (%1 &= ~(1 << (%2 & 31)))
#define ResetBit(%1)  (%1 = 0)

#define LATS_ROUND_TASK_ID 43546

#define EMPTY_CHECK_TASK_FQ 300.0
#define EMPTY_CHECK_TASK_ID 43548

#define ADMIN_VOTE_FLAG ADMIN_VOTE
#define ADMIN_CHANGE_FLAG ADMIN_MAP

#define MAX_PLAYERS 	32
#define MIN_RATE 		1
#define MAX_RATE 		6
#define MIN_VOTE_MAPS 	2
#define MAX_VOTE_MAPS 	4

#define LAST_MINUTE		60
#define VOTE_TIME 		15
#define LAST_MAP_SIZE 	5

#define DEBUG 1