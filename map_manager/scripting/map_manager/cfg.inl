#if defined _cfg_included
	#endinput
#endif

#define _cfg_included

bool:loadConfig()
{
	new configFile[128];
	new line[128];
	new mapname[32], maprateStr[5], minStr[5], maxStr[5];
	new maprate, plMin, plMax;
	new curentMap[32];
	
	ArrayClear(g_Maps);
	g_MapsNum = 0;

	get_mapname(curentMap, charmax(curentMap));

	loadLastMaps();

	formatex(configFile, charmax(configFile), "%s/map_manager/mapconfig.ini", g_ConfigDir);
	if (!file_exists(configFile)) {
		log_amx("FIle %s not found", configFile);
		return false;
	}

	new file = fopen(configFile, "r");

	if (!file) {
		return false;
	}
	
	while (!feof(file)) {
		fgets(file, line, charmax(line));
		trim(line);

		if (line[0] == ';' || line[0] == '^0') {
			continue;
		}

		parse(line, mapname, charmax(mapname), maprateStr, charmax(maprateStr), minStr, charmax(minStr), maxStr, charmax(maxStr));

		if (!isValidMap(mapname)) {
			continue;
		}

		maprate = str_to_num(maprateStr);
		plMin = str_to_num(minStr);
		plMax = str_to_num(maxStr);

		if (plMax < 1) {
			plMax = 1;
		}

		if(maprate > MAX_RATE) {
			maprate = MAX_RATE;
		} else if (maprate < MIN_RATE) {
			maprate = MIN_RATE;
		}

		copy(g_MapConfig[MapName], charmax(g_MapConfig[MapName]), mapname);
		g_MapConfig[MapIsLast] = isLastMap(mapname);
		g_MapConfig[MapRate] = maprate;
		g_MapConfig[MapMin] = plMin;
		g_MapConfig[MapMax] = plMax;

		ArrayPushArray(g_Maps, g_MapConfig);

		if (equal(mapname, curentMap)) {
			g_CurrentMapId = g_MapsNum;
		}

		#if defined DEBUG && DEBUG >= 2
		log_amx("Load map #%d: %s, Rate: %d, Min: %d, Max %d, IsLast %s", g_MapsNum, g_MapConfig[MapName], g_MapConfig[MapRate], g_MapConfig[MapMin], g_MapConfig[MapMax], (g_MapConfig[MapIsLast] ? "YES" : "NO"));
		#endif

		g_MapsNum++;
	}

	fclose(file);

	saveLastMaps(curentMap);

	return true;
}

loadLastMaps()
{
	new configFile[128];
	new line[128];

	g_LastMapsNum = 0;
	formatex(configFile, charmax(configFile), "%s/map_manager/maplast.ini", g_ConfigDir);
	if (!file_exists(configFile)) {
		return false;
	}

	new file = fopen(configFile, "r");
	while (!feof(file)) {
		fgets(file, line, charmax(line));
		trim(line);
		if (line[0] == ';' || line[0] == '^0') {
			continue;
		}

		copy(g_LastMaps[g_LastMapsNum], 31, line);
		g_LastMapsNum++;
	}

	fclose(file);
	return true;
}

saveLastMaps(curentMap[])
{
	new configFile[128];
	new startPos; //, voteNumNextMap;

	// voteNumNextMap = g_MapsNum - g_LastMapsNum - 1;
	startPos = (g_LastMapsNum >= LAST_MAP_SIZE) ? 1 : 0;
	// if (voteNumNextMap >= MAX_VOTE_MAPS) {
	// 	 startPos = (g_LastMapsNum >= LAST_MAP_SIZE) ? 1 : 0;
	// } else {
	// 	startPos = MAX_VOTE_MAPS - voteNumNextMap;
	// }

	formatex(configFile, charmax(configFile), "%s/map_manager/maplast.ini", g_ConfigDir);

	new file = fopen(configFile, "w");
	for (new i = startPos; i < g_LastMapsNum; i++) {
		fprintf(file, "%s^n", g_LastMaps[i]);
	}
	
	// if (!isLastMap(curentMap)) {
	fprintf(file, "%s^n", curentMap);

	fclose(file);
	return true;
}