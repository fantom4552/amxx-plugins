#pragma semicolon 1

// #define HAM_DAMAGE
// #define SHOW_BULLET_DAMAGE

#include <amxmodx>
#include <cstrike>

#include <fun>

#if defined HAM_DAMAGE
#include <hamsandwich>
#endif

#if defined SHOW_BULLET_DAMAGE
#include <fakemeta>
#endif

#define PLUGIN "Amx Killer"
#define VERSION "3.0"
#define AUTHOR "F@nt0M"

#if (AMXX_VERSION_NUM < 183) || defined NO_NATIVE_COLORCHAT
	#include <colorchat>
#else
	#define DontChange print_team_default
#endif

#define isValidPlayer(%1) (1 <= %1 <= g_MaxPlayers)
#define IsValidTeam(%1) (CS_TEAM_T <= cs_get_user_team(%1) <= CS_TEAM_CT)
#define CheckKillerFlag(%1) (%1 &	g_KillerFlags)

#define EXTRAOFFSET 5
#define OFFSET_MONEY 115

#define MAX_BUFFER_LEN 512

#define SCREENFADE 		(1<<0)  /* flag "a" */
#define BESTPLAYER		(1<<1)  /* flag "b" */
#define KILL_STATS		(1<<2)  /* flag "c" */
#define ROUND_STATS		(1<<3)  /* flag "d" */
#define CMD_RS			(1<<4)  /* flag "e" */
#define CMD_ME			(1<<5)  /* flag "f" */
#define CMD_HP			(1<<6)  /* flag "g" */
#define KILL_ASSIST		(1<<7)  /* flag "h" */
#define BULLET_DAMAGE	(1<<8)  /* flag "i" */

new g_ForwardAssist;
new bool:g_GameStarted = false;
new bool:g_Round;

new g_MaxPlayers;
new g_SyncHud;

new g_MsgScreenFade;
new g_MsgMoney;
new g_MsgScoreInfo;

new g_Buffer[MAX_BUFFER_LEN+1], g_BufferLen;

enum _:CVars {
	cvarKillerFlags,
	cvarAssistDamage,
	cvarAssistMoney,
	cvarAssisMaxMoney
}

new g_CVars[CVars];
new g_KillerFlags;

enum _:Score {
	gKills,
	gHeads,
	gTotalDamage
};

new g_Score[MAX_PLAYERS+1][Score];

enum _:Stats {
	gHits,
	gDamage,
	bool:gKill
};
new g_Stats[MAX_PLAYERS+1][MAX_PLAYERS+1][Stats];

new g_Hp[MAX_PLAYERS+1][3];
new g_Names[MAX_PLAYERS+1][32];

public plugin_init()
{
	register_plugin(PLUGIN, VERSION, AUTHOR);

	register_dictionary("amx_killer.txt");

	#if defined HAM_DAMAGE
	RegisterHamPlayer(Ham_TakeDamage, "EventDamage", 1);
	#else
	register_event("Damage", "EventDamage", "b", "2!0");
	#endif

	register_event("DeathMsg", "EventDeath", "a", "1>0");

	register_event("HLTV", "EventRoundStart", "a", "1=0", "2=0");
	register_logevent("EventRoundEnd", 2, "1=Round_End");

	register_event("TextMsg", "EventStartGame", "a", "2=#Game_Commencing");

	register_clcmd("say /rs", "CmdRs");
	register_clcmd("say_team /rs", "CmdRs");

	register_clcmd("say /me", "CmdMe");
	register_clcmd("say_team /me", "CmdMe");

	register_clcmd("say /hp", "CmdHp");
	register_clcmd("say_team /hp", "CmdHp");

	g_CVars[cvarKillerFlags] = register_cvar("amx_killer_flags", "abcdefgh");
	g_CVars[cvarAssistDamage] = register_cvar("amx_killer_assist_damage", "60");
	g_CVars[cvarAssistMoney] = register_cvar("amx_killer_assist_money", "300");
	g_CVars[cvarAssisMaxMoney] = register_cvar("amx_killer_assist_maxmoney", "16000");

	g_MsgScreenFade = get_user_msgid("ScreenFade");
	g_MsgMoney = get_user_msgid("Money");
	g_MsgScoreInfo = get_user_msgid("ScoreInfo");

	g_MaxPlayers = get_maxplayers();
	g_SyncHud = CreateHudSyncObj();

	g_ForwardAssist	= CreateMultiForward("amx_killer_assist", ET_IGNORE, FP_CELL, FP_CELL, FP_CELL);

	new configDir[128];
	get_localinfo("amxx_configsdir", configDir, charsmax(configDir));
	server_cmd("exec %s/amx_killer/config.cfg", configDir);
}

public plugin_cfg()
{
	new flags[10];
	get_pcvar_string(g_CVars[cvarKillerFlags], flags, 9);
	g_KillerFlags = read_flags(flags);
}

#if defined HAM_DAMAGE
public EventDamage(victim, weapon, attacker, Float:damage, damage_type)
{
	if (isValidPlayer(victim) && isValidPlayer(attacker) && victim != attacker && cs_get_user_team(victim) != cs_get_user_team(attacker)) {
		g_Stats[attacker][victim][gHits]++;
		g_Stats[attacker][victim][gDamage] += floatround(damage);

		g_Score[attacker][gTotalDamage] += floatround(damage);

		#if defined SHOW_BULLET_DAMAGE
		bulletDamage(attacker, victim, floatround(damage));
		#endif
	}
}
#else
public EventDamage(victim)
{
	static attacker, damage;

	attacker = get_user_attacker(victim);
	damage = read_data(2);

	if (isValidPlayer(victim) && isValidPlayer(attacker) && victim != attacker && cs_get_user_team(victim) != cs_get_user_team(attacker)) {
		g_Stats[attacker][victim][gHits]++;
		g_Stats[attacker][victim][gDamage] += damage;

		g_Score[attacker][gTotalDamage] += damage;

		#if defined SHOW_BULLET_DAMAGE
		bulletDamage(attacker, victim, damage);
		#endif
	}
}
#endif

public EventDeath()
{
	static killer, victim, head, health, armor, CsArmorType:armor_type, assist, tmp, damage, assist_damage;

	killer = read_data(1);
	victim = read_data(2);
	head = read_data(3);

	if (is_user_connected(killer) && is_user_connected(victim) && killer != victim  && cs_get_user_team(killer) != cs_get_user_team(victim)) {

		g_Stats[killer][victim][gKill] = true;
		g_Score[killer][gKills]++;

		health = get_user_health(killer);
		armor = cs_get_user_armor(killer, armor_type);
		
		if (CheckKillerFlag(KILL_STATS)) {
			client_print_color(victim, getTeamColor(killer), "%L", victim, "KILLER_INFO", g_Names[killer], health, armor);
			client_print_color(victim, getTeamColor(killer), "%L", victim, "KILLER_DAMAGE", g_Names[killer], g_Stats[victim][killer][gDamage]);
		}

		if (head == 1) {
			g_Score[killer][gHeads]++;
			if (CheckKillerFlag(SCREENFADE)) {
				screen_fade(killer);
			}
		}

		if (CheckKillerFlag(ROUND_STATS)) {
			showScore(victim);
		}

		if (CheckKillerFlag(KILL_ASSIST)) {
			assist = 0;
			damage = 0;

			assist_damage = get_pcvar_num(g_CVars[cvarAssistDamage]);

			for (tmp = 1; tmp <= g_MaxPlayers; tmp++) {
				if (tmp != killer && g_Stats[tmp][victim][gDamage] >= assist_damage && g_Stats[tmp][victim][gDamage] > damage) {
					assist = tmp;
					damage = g_Stats[tmp][victim][gDamage];
				}
			}
			
			if (assist > 0 && damage > assist_damage) {
				assitsBonus(killer, victim, assist);
				ExecuteForward(g_ForwardAssist, tmp, assist, killer, victim);
			}
		}

		g_Hp[victim][0] = killer;
		g_Hp[victim][1] = health;
		g_Hp[victim][2] = armor;
	}
}

public EventRoundStart()
{
	new id, player;
	for (id = 1; id <= g_MaxPlayers; id++) {
		for (player = 0; player <= g_MaxPlayers; player++) {
			g_Stats[id][player][gHits] = 0;
			g_Stats[id][player][gDamage] = 0;
			g_Stats[id][player][gKill] = false;

		}
		g_Score[id][gKills] = 0;
		g_Score[id][gHeads] = 0;
		g_Score[id][gTotalDamage] = 0;

		arrayset(g_Hp[id], 0, 3);

		if (is_user_connected(id)) {
			get_user_name(id, g_Names[id], 31);
		} else {
			g_Names[id][0] = '^0';
		}
	}

	if (g_GameStarted) {
		g_Round = true;
	}
}

public EventRoundEnd()
{
	if (g_GameStarted && g_Round) {
		if (CheckKillerFlag(BESTPLAYER)) {
			set_task(1.0, "TaskShowBestPlayer");
		}

		if (CheckKillerFlag(ROUND_STATS)) {
			set_task(1.0, "TaskShowRoundStats");
		}
	}

	g_Round = false;
}

public EventStartGame()
{
	if (!g_GameStarted) {
		g_GameStarted = true;
	}
}

public CmdRs(id)
{
	if (!CheckKillerFlag(CMD_RS)) {
		return PLUGIN_CONTINUE;
	}

	if (is_user_connected(id)) {
		cs_set_user_deaths(id, 0);
		set_user_frags(id, 0);
		cs_set_user_deaths(id, 0);
		set_user_frags(id, 0);
		client_print_color(id, print_team_default, "%L", id, "RESET_SCORE");
	}

	return PLUGIN_CONTINUE;
}

public CmdMe(id)
{
	if (!CheckKillerFlag(CMD_ME)) {
		return PLUGIN_CONTINUE;
	}

	if (g_Round && is_user_alive(id)) {
		client_print_color(id, print_team_default, "%L", id, "ME_DISABLED");
		return PLUGIN_CONTINUE;
	}

	new hits = 0;
	for (new player = 1; player <= g_MaxPlayers; player++) {
		hits += g_Stats[id][player][gHits];
	}

	client_print_color(id, print_team_default, "%L", id, "ME_INFO", hits, g_Score[id][gTotalDamage]);

	return PLUGIN_CONTINUE;
}

public CmdHp(id)
{
	if (!CheckKillerFlag(CMD_HP)) {
		return PLUGIN_CONTINUE;
	}

	if (is_user_alive(id)) {
		client_print_color(id, print_team_default, "%L", id, "HP_ALIVE");
		return PLUGIN_CONTINUE;
	}

	if (g_Hp[id][0] == 0) {
		client_print_color(id, print_team_default, "%L", id, "HP_EMPTY");
		return PLUGIN_CONTINUE;	
	}

	new killer = g_Hp[id][0];
	client_print_color(id, print_team_default, "%L", id, "HP_INFO", g_Names[killer], g_Hp[id][1], g_Hp[id][2]);

	return PLUGIN_CONTINUE;
}

public TaskShowBestPlayer()
{
	new bestPlayer = getBestPlayer();
	set_hudmessage(0, 255, 0, -1.0, 0.17, 0, 5.0);
	ShowSyncHudMsg(0, g_SyncHud, "%L^n%s^n%L", LANG_SERVER, "BEST_PLAYER", g_Names[bestPlayer], LANG_SERVER, "BEST_SUMMARY", g_Score[bestPlayer][gKills], g_Score[bestPlayer][gHeads], g_Score[bestPlayer][gTotalDamage]);
}

public TaskShowRoundStats()
{
	new players[32], num, i;
	get_players(players, num, "ach");
	for(i = 0; i < num; i++) {
		if (IsValidTeam(players[i]) && is_user_alive(players[i])) {
			showScore(players[i]);
		}
	}	
}

getBestPlayer()
{
	new players[32], num;  
	get_players(players, num);  
	SortCustom1D(players, num, "SortBestPlayer");
	return players[0];
}

public SortBestPlayer(id1, id2)  
{
	static result;

	result = 0;
	if(g_Score[id1][gKills] > g_Score[id2][gKills]) {
		result = -1;
	} else if(g_Score[id1][gKills] < g_Score[id2][gKills]) {
		result = 1;
	} else {
		if(g_Score[id1][gTotalDamage] > g_Score[id2][gTotalDamage]) {
			result = -1;
		} else if(g_Score[id1][gTotalDamage] < g_Score[id2][gTotalDamage]) {
			result = 1;
		} else {
			result = 0;
		}
	}

	return result;
}

showScore(id)
{
	static players[32], num, player, damage;
	get_players(players, num, "h");

	damage = 0;
	for (player = 1; player <= g_MaxPlayers; player++) {
		if (player != id) {
			damage += g_Stats[player][id][gDamage];
		}
	}

	if (g_Score[id][gTotalDamage] > 0) {
		g_BufferLen = format(g_Buffer, MAX_BUFFER_LEN, "%L^n", LANG_SERVER, "VICTIM_SUMARY", g_Score[id][gTotalDamage]);
		for (player = 1; player <= g_MaxPlayers; player++) {
			if (player != id && g_Stats[id][player][gDamage] > 0) {
				g_BufferLen += format(g_Buffer[g_BufferLen], MAX_BUFFER_LEN - g_BufferLen, "%L^n", LANG_SERVER, "KILL_VICTIM", g_Names[player], g_Stats[id][player][gHits], g_Stats[id][player][gDamage]);
			}
		}

		set_hudmessage(0, 80, 220, 0.55, 0.60, 0, 6.0, 3.0, 1.0, 1.0, -1);
		show_hudmessage(id, "%s", g_Buffer);
	}

	if (damage > 0) {
		g_BufferLen = format(g_Buffer, MAX_BUFFER_LEN, "%L^n", LANG_SERVER, "ATTACKER_SUMARY", damage);
		for (player = 1; player <= g_MaxPlayers; player++) {
			if (player != id && g_Stats[player][id][gDamage] > 0) {
				g_BufferLen += format(g_Buffer[g_BufferLen], MAX_BUFFER_LEN - g_BufferLen, "%L^n", LANG_SERVER, "KILL_ATTACKER", g_Names[player], g_Stats[player][id][gHits], g_Stats[player][id][gDamage]);
			}
		}

		set_hudmessage(220, 80, 0, 0.55, 0.35, 0, 6.0, 3.0, 1.0, 1.0, -1);
		show_hudmessage(id, "%s", g_Buffer);
	}
}

assitsBonus(killer, victim, assist)
{
	// new money = get_pdata_int(assist, OFFSET_MONEY, EXTRAOFFSET);
	new money = cs_get_user_money(assist);

	new give_money = get_pcvar_num(g_CVars[cvarAssistMoney]);
	new max_money = get_pcvar_num(g_CVars[cvarAssisMaxMoney]);

	if (money < max_money) {
		if (money + give_money > max_money) {
			money = max_money;
		} else {
			money += give_money;
		}
	
		cs_set_user_money(assist, money);
		// set_pdata_int(assist, OFFSET_MONEY, money, EXTRAOFFSET);
		
		// no reason to send a money message when the player has no hud
		if (is_user_alive(assist)) {
			message_begin(MSG_ONE_UNRELIABLE, g_MsgMoney, _, assist);
			write_long(money);
			write_byte(1);
			message_end();
		}
	}

	// set_pev(assist, pev_frags, float(frags));
	
	new flags = get_user_frags(assist) + 1;
	
	set_user_frags(assist, flags);
	
	message_begin(MSG_ALL, g_MsgScoreInfo);
	write_byte(assist);
	write_short(flags);
	write_short(get_user_deaths(assist));
	write_short(0);
	write_short(get_user_team(assist));
	message_end();

	client_print_color(assist, print_team_default, "%L", assist, "KILL_ASSIST", g_Names[killer], g_Names[victim]);
}

#if defined SHOW_BULLET_DAMAGE
bulletDamage(attacker, victim, damage)
{
	if (CheckKillerFlag(BULLET_DAMAGE) && is_ent_visible(attacker, victim)) {
		set_hudmessage(0, 100, 200, -1.0, 0.55, 2, 0.1, 4.0, 0.02, 0.02, -1);
		ShowSyncHudMsg(attacker, g_SyncHud, "%i^n", damage);
	}
}
#endif

stock screen_fade(id)
{
	message_begin(MSG_ONE, g_MsgScreenFade, {0,0,0}, id);
	write_short(1<<10);
	write_short(1<<10);
	write_short(0x0000);
	write_byte(0);
	write_byte(0);
	write_byte(200);
	write_byte(75);
	message_end();
}

stock getTeamColor(id)
{
	switch (cs_get_user_team(id)) {
		case CS_TEAM_T: {
			return print_team_red;
		}

		case CS_TEAM_CT: {
			return print_team_blue;
		}
	}

	return print_team_default;
}

#if defined SHOW_BULLET_DAMAGE
stock bool:is_ent_visible(index, entity, ignoremonsters = 0) {
	new Float:start[3], Float:dest[3];
	pev(index, pev_origin, start);
	pev(index, pev_view_ofs, dest);
	vec_add(start, dest, start);

	pev(entity, pev_origin, dest);
	engfunc(EngFunc_TraceLine, start, dest, ignoremonsters, index, 0);

	new Float:fraction;
	get_tr2(0, TR_flFraction, fraction);
	if (fraction == 1.0 || get_tr2(0, TR_pHit) == entity)
		return true;

	return false;
}

stock vec_add(const Float:in1[], const Float:in2[], Float:out[])
{
	out[0] = in1[0] + in2[0];
	out[1] = in1[1] + in2[1];
	out[2] = in1[2] + in2[2];
}
#endif