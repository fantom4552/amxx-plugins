#include <amxmodx>
#include <amxmisc>
#include <cstrike>
#include <fun>

#if (AMXX_VERSION_NUM < 183) || defined NO_NATIVE_COLORCHAT
	#include <colorchat>
#else
	#define DontChange print_team_default
#endif

#define PLUGIN "Auto Demo Recorder"
#define VERSION "2.0"
#define AUTHOR "F@nt0M"

#define TASK_ID 2345

new g_pcvarEnable, g_pcvarFreq, g_pcvarFormat;

public plugin_init() 
{
	register_plugin(PLUGIN, VERSION, AUTHOR);

	register_dictionary("demo_recorder.txt");
	
	g_pcvarEnable = register_cvar("amx_demo_enable", "1");
	g_pcvarFreq = register_cvar("amx_demo_freq", "5.0");
	g_pcvarFormat = register_cvar("amx_demo_format", "Demo-%mapname%-%date%-%time%");
}

public client_putinserver(id)
{
	if (get_pcvar_num(g_pcvarEnable)) {
		set_task(get_pcvar_float(g_pcvarFreq), "start_record", TASK_ID+id);
	}
}

public start_record(id)
{
	id -= TASK_ID;

	new demo_name[64], map_name[32], date[32], time[32];
	get_pcvar_string(g_pcvarFormat, demo_name, 63);
	get_mapname(map_name, 31);
	get_time("%m_%d_%Y", date, 31);
	get_time("%H_%M", time, 31);
	replace(demo_name, 63, "%mapname%", map_name);
	replace(demo_name, 63, "%date%", date);
	replace(demo_name, 63, "%time%", time);

	client_cmd(id, "stop"); 
	client_cmd(id, "record ^"%s^"", demo_name);
	set_task(5.0, "showMessage", TASK_ID+id, demo_name, 64);
}

public showMessage(demo_name[64], id)
{
	id -= TASK_ID;

	new user_name[33], Time[9], Date[64];
	get_user_name(id, user_name, 32);
	
	get_time("%H:%M:%S", Time, 8);
	get_time("%d.%m.%Y", Date, 63);

	client_print_color(id, print_team_default,  "%L", id, "WARNING1", user_name);
	client_print_color(id, print_team_default,  "%L", id, "WARNING2", demo_name);
	client_print_color(id, print_team_default,  "%L", id, "WARNING3", Time, Date);
}