#pragma semicolon 1

#include <amxmodx>
#include <amxmisc>

#define PLUGIN "Colored Translit NG"
#define VERSION "2.1"
#define AUTHOR "F@nt0M"

#include "colored_translit_ng/defines.inl"

#if defined SPAM_FILTER
#include <regex>
#endif

#if defined ENABLE_GAG && defined GAG_SAVE_LIST
#include <nvault>
#endif

#if defined ADMINLOAD
#include <adminload>
#endif

#if AMXX_VERSION_NUM == 183
#define client_has_disconnect client_disconnected
#define create_new_cvar create_cvar
#else
#define client_has_disconnect client_disconnect
#define create_new_cvar register_cvar
#endif

#include "colored_translit_ng/vars.inl"
#include "colored_translit_ng/cfg.inl"
#include "colored_translit_ng/init.inl"
#include "colored_translit_ng/say.inl"
#include "colored_translit_ng/menu.inl"
#include "colored_translit_ng/natives.inl"
#include "colored_translit_ng/stocks.inl"

#if defined ENABLE_GAG
#include "colored_translit_ng/gag.inl"
#endif

#if defined SWEAR_FILTER
#include "colored_translit_ng/swear.inl"
#endif

#if defined LOG_TO_FILE
#include "colored_translit_ng/log.inl"
#endif