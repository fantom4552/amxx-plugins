#if defined _colored_translit_ng_included
	#endinput
#endif

#define _colored_translit_ng_included

#pragma reqlib ColoredTranslitNG

#if !defined AMXMODX_NOAUTOLOAD
	#pragma loadlib ColoredTranslitNG
#endif

forward ctng_message_format(id, say_team);
native ctng_add_prefix(replace, const message[100], any:...);
native ctng_block_prefix(status);

