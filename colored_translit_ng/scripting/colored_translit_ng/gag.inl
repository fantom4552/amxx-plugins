#if defined _gag_included
	#endinput
#endif

#define _gag_included

stock bool:is_gaged(id)
{
	if (CheckFlag(g_IsGaged, id)) {
		if (g_PlayersData[id][PlayerGagExpired] > 0 && g_PlayersData[id][PlayerGagExpired] < g_SysTime) {
			ungag_player(id);
			#if defined GAG_SAVE_LIST
			gag_remove_list(id);
			#endif
			return false;
		}

		return true;
	}

	return false;
}

public adm_cmd_gag(id, level, cid)
{
	if (!cmd_access(id, level, cid, 3, true)) {
		return PLUGIN_HANDLED;
	}

	new args[64], player_str[32], gag_time_str[10], gag_time, expired;
	read_args(args, 63);
	parse(args, player_str, 21, gag_time_str, 9);

	if (!is_str_num(gag_time_str)) {
		console_print(id, "Bad time");
		return PLUGIN_HANDLED;
	}
	
	gag_time = str_to_num(gag_time_str);

	if (gag_time > 0) {
		expired = g_SysTime + (gag_time * 60);
	} else {
		expired = 0;
	}

	new admin_name[32];
	get_user_name(id, admin_name, 31);

	new player = locate_player(player_str);
	// !CheckFlag(g_IsConnected, player)
	if (!player) {
		#if defined GAG_SAVE_LIST
		if (is_authid(player_str) && gag_add_authid(player_str, expired)) {
			console_print(id, "Player ^"%s^" successfully gaged", player_str);
			log_amx("[GAG] Admin ^"%s^" gagged ^"%s^" for %d minutes", admin_name, player_str, gag_time);
		} else {
			console_print(id, "Player %s not found", player_str);
		}
		#else
		console_print(id, "Player %s not found", player_str);
		#endif
		return PLUGIN_HANDLED;
	}

	get_user_name(player, g_PlayerName, 31);

	#if !defined ENABLE_ADMIN_GAG
	if (CheckFlag(g_IsAdmin, player)) {
		console_print(id, "Player ^"%s^"is ADMIN", g_PlayerName);
		return PLUGIN_HANDLED;
	}
	#endif

	if (CheckFlag(g_IsGaged, player)) {
		console_print(id, "Player ^"%s^" already gaged", g_PlayerName);
		return PLUGIN_HANDLED;
	}

	gag_player(player, expired);

	#if defined GAG_SAVE_LIST
	gag_add_list(player, expired);
	#endif

	console_print(id, "Player ^"%s^" successfully gaged", g_PlayerName);

	log_amx("[GAG] Admin ^"%s^" gagged ^"%s^" for %d minutes", admin_name, g_PlayerName, gag_time);

	if (gag_time > 0) {
		#if defined HIDE_ADMIN_GAG
		format(g_Output, charmax(g_Output), "%L", LANG_SERVER, "CTNG_GAGGED_ADMIN_HIDE", g_PlayerName, gag_time);
		#else
		format(g_Output, charmax(g_Output), "%L", LANG_SERVER, "CTNG_GAGGED_ADMIN", admin_name, g_PlayerName, gag_time);
		#endif
	} else {
		#if defined HIDE_ADMIN_GAG
		format(g_Output, charmax(g_Output), "%L", LANG_SERVER, "CTNG_GAGGED_ADMIN_P_HIDE", g_PlayerName);
		#else
		format(g_Output, charmax(g_Output), "%L", LANG_SERVER, "CTNG_GAGGED_ADMIN_P", admin_name, g_PlayerName);
		#endif
	}

	replace_color();

	SendMessageAll(0, COLOR_RED);
	
	return PLUGIN_HANDLED;
}

public adm_cmd_ungag(id, level, cid)
{
	if (!cmd_access(id, level, cid, 2, true)) {
		return PLUGIN_HANDLED;
	}

	new args[64], player_str[32], player;
	read_args(args, 63);
	parse(args, player_str, 31);

	new admin_name[32];
	get_user_name(id, admin_name, 31);

	player = locate_player(player_str);

	if (!player) {
		#if defined GAG_SAVE_LIST
		if (is_authid(player_str) && gag_remove_authid(player_str)) {
			console_print(id, "Player ^"%s^" successfully ungaged", player_str);
			log_amx("[GAG] Admin ^"%s^" ungagged ^"%s^"", admin_name, player_str);
		} else {
			console_print(id, "Player %s not found", player_str);
		}
		#else
		console_print(id, "Player %s not found", player_str);
		#endif
		return PLUGIN_HANDLED;
	}

	get_user_name(player, g_PlayerName, 31);

	if (!CheckFlag(g_IsGaged, player)) {
		console_print(id, "Player %s not gaged", g_PlayerName);
		return PLUGIN_HANDLED;
	}

	ungag_player(player);

	#if defined GAG_SAVE_LIST
	gag_remove_list(player);
	#endif

	console_print(id, "Player ^"%s^" successfully ungaged", g_PlayerName);

	log_amx("[GAG] Admin ^"%s^" ungagged ^"%s^"", admin_name, g_PlayerName);

	#if defined HIDE_ADMIN_GAG
	format(g_Output, charmax(g_Output), "%L", LANG_SERVER, "CTNG_UNGAGGED_ADMIN_HIDE", g_PlayerName);
	#else
	format(g_Output, charmax(g_Output), "%L", LANG_SERVER, "CTNG_UNGAGGED_ADMIN", admin_name, g_PlayerName);
	#endif

	replace_color();
	
	SendMessageAll(0, COLOR_RED);
	
	return PLUGIN_HANDLED;
}

#if defined GAG_SAVE_LIST
bool:gag_check_list(id)
{
	new data[20], timestamp, expired;
	if (g_GagList == INVALID_HANDLE) {
		return false;
	}

	if (!nvault_lookup(g_GagList, g_PlayersData[id][PlayerAuthID], data, 19, timestamp)) {
		return false;
	}
		
	if (!is_str_num(data)) {
		return false;
	}
			
	expired = abs(str_to_num(data));

	if (expired == 0) {
		gag_player(id, expired);
	} else if(expired > g_SysTime) {
		gag_player(id, expired);
	} else {
		gag_remove_list(id);
	}

	return true;
}

bool:gag_add_list(id, expired)
{
	new data[32];
	if (g_GagList == INVALID_HANDLE) {
		return false;
	}

	num_to_str(expired, data, 31);
	nvault_set(g_GagList, g_PlayersData[id][PlayerAuthID], data);
	return true;
}

bool:gag_remove_list(id)
{
	if (g_GagList == INVALID_HANDLE) {
		return false;
	}

	nvault_remove(g_GagList, g_PlayersData[id][PlayerAuthID]);
	return true;
}

bool:gag_add_authid(authid[], expired)
{
	new data[32];

	if (g_GagList == INVALID_HANDLE) {
		return false;
	}

	num_to_str(expired, data, 31);
	nvault_set(g_GagList, authid, data);
	return true;
}

bool:gag_remove_authid(authid[])
{
	new data[20], timestamp;

	if (g_GagList == INVALID_HANDLE) {
		return false;
	}

	if (!nvault_lookup(g_GagList, authid, data, 19, timestamp)) {
		return false;
	}
		
	if (!is_str_num(data)) {
		return false;
	}

	nvault_remove(g_GagList, authid);
	return true;
}
#endif

gag_player(id, expired)
{
	SetFlag(g_IsGaged, id);
	g_PlayersData[id][PlayerGagExpired] = expired;
}

ungag_player(id)
{
	ClearFlag(g_IsGaged, id);
	g_PlayersData[id][PlayerGagExpired] = 0;
}

stock bool:is_authid(authid[])
{
	return (equal(authid, "STEAM_", 6) || equal(authid, "VALVE_", 6));
}