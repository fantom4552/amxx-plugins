#if defined _vars_included
	#endinput
#endif

#define _vars_included

#define CheckFlag(%1,%2)  ( %1 &   (1 << (%2 & 31)))
#define SetFlag(%1,%2)    (%1 |=  (1 << (%2 & 31)))
#define ClearFlag(%1,%2)  (%1 &= ~(1 << (%2 & 31)))

#define charmax(%1) (sizeof(%1) - 1)

#define COLOR_NONE 0
#define COLOR_RED 1
#define COLOR_BLUE 2
#define COLOR_WHITE 3

#define HANDLE_NONE 0
#define HANDLE_SPAM 1
#define HANDLE_SWEAR 2
#define HANDLE_FLOOD 3

new const g_Colors[][15] = {
	"UNASSIGNED",
	"TERRORIST",
	"CT",
	"SPECTATOR"
};

new g_msgChannel;

new g_MaxPlayers;
new g_msgSayText;
new g_msgTeamInfo;

new g_PlayerTeam;
new g_PlayerAlive;
new g_Color;
new g_PlayerName[32];
new g_Message[192];
new g_Output[192];
new g_Console[192];

new g_TeamName[10];

#if defined TRANSLIT
new bool:g_Translit;
new g_NumTranslitSimbols;
new g_OriginalSimbol[128][5];
new g_TranslitSimbol[128][5];
#endif

new bool:g_Ignore;
new Array:g_Ignores;
new g_IgnoresNum;

#if defined SWEAR_FILTER
new g_SwearMsg[256];
new bool:g_Swear;
new Array:g_Swears;
new g_SwearsNum;
#endif

#if defined SPAM_FILTER
new Regex:g_SpamRegex;
#endif

new g_IsAdmin;
new g_IsVip;
new g_IsConnected;

#if defined ENABLE_GAG
new g_IsGaged;
#endif

#if defined TRANSLIT
new g_IsTranslit;
#endif

new g_SysTime;
new g_HudSync;

#if defined LOG_TO_FILE
new g_LogFileChat;
new g_LogMessage[512];
new g_LogDir[128];
#endif

#if defined ENABLE_GAG && defined GAG_SAVE_LIST
new g_GagList;
#endif

enum _:Teams
{
	TEAM_UNASSIGNED = 0,
	TEAM_T = 1,
	TEAM_CT = 2,
	TEAM_SPECTATOR = 3
};

enum _:playerData
{
	PlayerIP[32],
	PlayerAuthID[37],
	PlayersGagged,
	#if defined ENABLE_GAG
	PlayerGagExpired,
	#endif
	#if defined ANTI_FLOOD
	PlayerLastMessage,
	#endif
	PlayerWarns
}

new g_PlayersData[MAX_PLAYERS+1][playerData];

new g_FwdFormat;
new g_Prefix[100];
new bool:g_BlockPrefix;

new g_Return;