#if defined _swear_included
	#endinput
#endif

#define _swear_included

stock ReplaceSwear()
{
	copy(g_SwearMsg, 255, g_Message);
	strtolower(g_SwearMsg);
	replace_all(g_SwearMsg, 255, " ", "");
	replace_all(g_SwearMsg, 255, "{", "[");
	replace_all(g_SwearMsg, 255, "}", "]");
	replace_all(g_SwearMsg, 255, "<", ",");
	replace_all(g_SwearMsg, 255, ">", ".");
	replace_all(g_SwearMsg, 255, "~", "`");
	replace_all(g_SwearMsg, 255, "*", "");
	replace_all(g_SwearMsg, 255, "_", "");
}

stock bool:is_swear(id)
{
	new tmp[64];

	ReplaceSwear();

	#if !defined ADMIN_SWEAR_FILTER
	if(CheckFlag(g_IsAdmin, id))
	{	
		return false;
	}
	#endif

	for(new i = 0; i < g_SwearsNum; i++)
	{
		ArrayGetString(g_Swears, i, tmp, 63);
		if(containi(g_SwearMsg, tmp) != -1)
		{
			g_PlayersData[id][PlayerWarns]++;
			return true;
		}
	}

	return false;
}