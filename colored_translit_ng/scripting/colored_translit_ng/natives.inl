public plugin_natives() 
{
	register_library("ColoredTranslitNG");
	register_native("ctng_add_prefix", "native_add_prefix", 0);
	register_native("ctng_block_prefix", "native_block_prefix", 0);
}

public native_add_prefix(plugin, paramsnum)
{
	new message[100];

	if (paramsnum < 2) {
		return 0;
	}

	vdformat(message, charmax(message), 2, 3);

	if (get_param(1) == 0) {
		add(g_Prefix, charmax(g_Prefix), message, charmax(message));
	} else {
		copy(g_Prefix, charmax(g_Prefix), message);
	}
	return 1;
}

public native_block_prefix(plugin, paramsnum)
{
	if (paramsnum != 1) {
		return 0;
	}

	g_BlockPrefix = (get_param(1) == 1) ? true : false;
	return 1;
}