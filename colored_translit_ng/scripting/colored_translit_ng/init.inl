#if defined _init_included
	#endinput
#endif

#define _init_included

public plugin_init()
{
	register_plugin(PLUGIN, VERSION, AUTHOR);

	register_dictionary("colored_translit_ng.txt");

	#if defined ENABLE_GAG
	register_concmd("amx_gag", "adm_cmd_gag", ADMIN_FLAG, "<steamID or nickname or #authid or IP> <time>");
	register_concmd("amx_ungag", "adm_cmd_ungag", ADMIN_FLAG, "<steamID or nickname or #authid or IP>");
	#endif

	g_MaxPlayers = get_maxplayers();

	g_msgSayText = get_user_msgid("SayText");
	g_msgTeamInfo = get_user_msgid("TeamInfo");

	register_clcmd("say", "hook_say");
	register_clcmd("say_team", "hook_say_team");

	#if defined TRANSLIT
	register_clcmd("amx_lang_switch", "language_switch");
	#endif

	g_FwdFormat = CreateMultiForward("ctng_message_format", ET_IGNORE, FP_CELL);

	new r_ret, r_error[128];
	g_SpamRegex = regex_compile("(?:\d{1,3}\.\d{1,3}\.\d{1,3}\.\d{1,3}|http://)", r_ret, r_error, 127);

	g_SysTime = get_systime(0);
	set_task(1.0, "systime", 5455, _, _, "b");

	g_HudSync = CreateHudSyncObj();

	g_Ignores = ArrayCreate(32, 1);
	#if defined SWEAR_FILTER
	g_Swears = ArrayCreate(32, 1);
	#endif

	#if defined ENABLE_GAG && defined GAG_SAVE_LIST
	g_GagList = nvault_open("gag_list");
	if (g_GagList == INVALID_HANDLE) {
		log_amx("[Error] Error open gag list file");
	}
	#endif
}

public plugin_end()
{
	regex_free(g_SpamRegex);
	ArrayDestroy(g_Ignores);
	#if defined SWEAR_FILTER
	ArrayDestroy(g_Swears);
	#endif
	#if defined LOG_TO_FILE
	log_end();
	#endif

	#if defined ENABLE_GAG && defined GAG_SAVE_LIST
	if (g_GagList != INVALID_HANDLE) {
		nvault_close(g_GagList);
	}
	#endif
}

public client_putinserver(id)
{
	SetFlag(g_IsConnected, id);
	
	#if defined TRANSLIT
	SetFlag(g_IsTranslit, id);
	set_task(INFO_TASK_FQ, "show_info", INFO_TASK_ID+id);
	#endif

	g_PlayersData[id][PlayerWarns] = 0;

	#if defined ANTI_FLOOD
	g_PlayersData[id][PlayerLastMessage] = g_SysTime;
	#endif

	get_user_ip(id, g_PlayersData[id][PlayerIP], 31, 1);
	get_user_authid(id, g_PlayersData[id][PlayerAuthID], 36);

	#if defined ENABLE_GAG && defined GAG_SAVE_LIST
	gag_check_list(id);
	#endif
}

public client_has_disconnect(id)
{
	ClearFlag(g_IsConnected, id);
	ClearFlag(g_IsVip, id);
	ClearFlag(g_IsAdmin, id);
	
	#if defined ENABLE_GAG
	ClearFlag(g_IsGaged, id);
	#endif

	#if defined TRANSLIT
	ClearFlag(g_IsTranslit, id);
	#endif

	for (new i = 0; i <= MAX_PLAYERS; i++) {
		ClearFlag(g_PlayersData[i][PlayersGagged], id);
	}
}

#if defined ADMINLOAD
public adminload_connect(id, flags)
{
	if (flags & ADMIN_FLAG) {
		SetFlag(g_IsAdmin, id);
	} else if(flags & VIP_FLAG) {
		SetFlag(g_IsVip, id);
	}
}

public adminload_disconnect(id)
{
	ClearFlag(g_IsAdmin, id);
	ClearFlag(g_IsVip, id);
}
#else
public client_authorized(id)
{
	new flags = get_user_flags(id);
	if (flags & ADMIN_FLAG) {
		SetFlag(g_IsAdmin, id);
	} else if(flags & VIP_FLAG) {
		SetFlag(g_IsVip, id);
	}
}
#endif

public systime()
{
	g_SysTime = get_systime(0);
}

#if defined TRANSLIT
public show_info(id)
{
	id -= INFO_TASK_ID;

	if (CheckFlag(g_IsConnected, id)) {
		format(g_Output, 255, "^x01%L", id, "CTNG_INFO_UKR");
		replace_color();
		SendMessageOne(COLOR_NONE, id);
		format(g_Output, 255, "^x01%L", id, "CTNG_INFO_ENG");
		replace_color();
		SendMessageOne(COLOR_NONE, id);
	}
}

public language_switch(id)
{
	if (CheckFlag(g_IsTranslit, id)) {
		ClearFlag(g_IsTranslit, id);
		format(g_Output, charmax(g_Output), "%L", LANG_SERVER, "CTNG_ENG");
	} else {
		SetFlag(g_IsTranslit, id);
		format(g_Output, charmax(g_Output), "%L", LANG_SERVER, "CTNG_UKR");
	}

	replace_color();
	SendMessageOne(COLOR_NONE, id);

	return PLUGIN_HANDLED;
}
#endif