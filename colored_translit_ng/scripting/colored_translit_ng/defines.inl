#if defined _defines_included
	#endinput
#endif

#define _defines_included

#define INFO_TASK_FQ 5.0
#define INFO_TASK_ID 13266

#define TRANSLIT

#define SPAM_FILTER

#define ANTI_FLOOD
#define ADMIN_ANTI_FLOOD
#define FLOOD_TIME 1
#define MAX_WARNS 5

#define WARN_DECREMENT_FREQ 15.0
#define WARN_DECREMENT_TASK 63645

#define SWEAR_FILTER
#define ADMIN_SWEAR_FILTER

#define HIDE_CMD

#define ENABLE_GAG
// #define ENABLE_ADMIN_GAG
// #define HIDE_ADMIN_GAG
#define GAG_SAVE_LIST

#define LOG_TO_FILE
// #define LOG_TO_AMXX

#define ADMINLOAD

#define MAX_PLAYERS 32

#define ADMIN_FLAG ADMIN_CHAT
#define VIP_FLAG ADMIN_LEVEL_H

// #define OLD_COMPABOLITY