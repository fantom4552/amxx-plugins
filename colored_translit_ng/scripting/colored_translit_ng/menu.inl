#if defined _menu_included
	#endinput
#endif

#define _menu_included

public gag_menu(id)
{
	server_print("Say Gag");
	new menu = menu_create("\rGag:", "gag_menu_handler");
	new callback = menu_makecallback("menu_callback");

	new i, num, players[32], name[32], index[3], title[128];

	formatex(title, charmax(title), "%L", LANG_SERVER, "BACK");
	menu_setprop(menu, MPROP_BACKNAME, title);
	
	formatex(title, charmax(title), "%L", LANG_SERVER, "MORE");
	menu_setprop(menu, MPROP_NEXTNAME, title);

	formatex(title, charmax(title), "%L^n^n\y%s by %s", LANG_SERVER, "EXIT", PLUGIN, AUTHOR);
	menu_setprop(menu, MPROP_EXITNAME, title);

	get_players(players, num);

	if (num <= 1) {
		return PLUGIN_HANDLED;
	}

	for (i = 0; i < num; i++ ) {	
		if (players[i] != id) {
			get_user_name(players[i], name, 31);
		
			num_to_str(players[i], index, 2);
			menu_additem(menu, name, index, 0, callback);
		}
	}

	menu_display(id, menu);

	return PLUGIN_HANDLED;
}

public gag_menu_handler(id, menu, item)
{
	if (item == MENU_EXIT) {
		menu_destroy(menu);
		return PLUGIN_HANDLED;
	}

	new access, info[3], text[64], callback;
	new oldmenu, newmenu, page;

	menu_item_getinfo(menu, item, access, info, 2, text, 63, callback);

	new player = str_to_num(info);
	
	if (isPlayerGagged(id, player)) {
		ClearFlag(g_PlayersData[id][PlayersGagged], player);
		// client_print_color(id, print_team_default, "^1[ ^GAG ^1] Player %s gagged", name);
	} else {
		SetFlag(g_PlayersData[id][PlayersGagged], player);
		// client_print_color(id, print_team_default, "^1[ ^4MUTE ^1] Player %s unagged", name);
	}

	player_menu_info(id, oldmenu, newmenu, page);
	menu_display(id, menu, page);
 	
	return PLUGIN_CONTINUE;
}

public menu_callback(id, menu, item)
{
	new access, info[3], name[32], title[128], callback;
	menu_item_getinfo(menu, item, access, info, 2, title, 127, callback);

	new player = str_to_num(info);

	get_user_name(player, name, 31);
	if (isPlayerGagged(id, player)) {
		format(title, 127, "\y%s \r[GAGGED]", name);
	} else {
		format(title, 127, "\y%s", name);
	}

	menu_item_setname(menu, item, title);

	return ITEM_ENABLED;
}