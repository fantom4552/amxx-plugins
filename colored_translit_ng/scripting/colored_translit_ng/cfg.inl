#if defined _cfg_included
	#endinput
#endif

#define _cfg_included

public plugin_cfg()
{
	new s_ConfigsDir[128], s_File[128];
	new Line, Len;
	new Input[64];

	get_configsdir(s_ConfigsDir, 127);

	#if defined TRANSLIT
	format(s_File, 127, "%s/colored_translit_ng/translit.ini", s_ConfigsDir);
	if (file_exists(s_File)) {
		while ((Line = read_file(s_File, Line, Input, 31, Len)) != 0) {
			strtok(Input, g_OriginalSimbol[g_NumTranslitSimbols], 16, g_TranslitSimbol[g_NumTranslitSimbols], 16, ' ');
			g_NumTranslitSimbols++;
		}

		if (g_NumTranslitSimbols > 0) {
			g_Translit = true;
		} else {
			g_Translit = false;
		}
	}
	else
	{
		g_Translit = false;
	}
	#endif

	format(s_File, 127, "%s/colored_translit_ng/ignores.ini", s_ConfigsDir);
	if (file_exists(s_File)) {
		g_IgnoresNum = 0;
		while ((Line = read_file(s_File, Line, Input, 63, Len)) != 0) {
			if (Input[0] != ';' && Len > 0) {
				ArrayPushString(g_Ignores, Input);
				g_IgnoresNum++;
			}
		}

		if (g_IgnoresNum) {
			g_Ignore = true;
		} else {
			g_Ignore = false;
		}
	}
	else
	{
		g_Ignore = false;
	}

	#if defined SWEAR_FILTER
	format(s_File, 127, "%s/colored_translit_ng/swears.ini", s_ConfigsDir);
	if (file_exists(s_File)) {
		g_SwearsNum = 0;
		while ((Line = read_file(s_File, Line, Input, 63, Len)) != 0) {
			if (Input[0] != ';' && Len > 0) {
				ArrayPushString(g_Swears, Input);
				g_SwearsNum++;
			}
		}

		if (g_SwearsNum > 0) {
			g_Swear = true;
		} else {
			g_Swear = false;
		}
	} else {
		g_Swear = false;
	}
	#endif

	#if defined LOG_TO_FILE
	log_start();
	#endif
}