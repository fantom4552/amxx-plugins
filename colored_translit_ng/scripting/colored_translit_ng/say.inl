#if defined _say_included
	#endinput
#endif

#define _say_included

public hook_say(id)
{
	if (is_user_hltv(id) || is_user_bot(id)) {
		return PLUGIN_CONTINUE;
	}

	read_args(g_Message, 255);
	handle_say(id, false);

	return PLUGIN_HANDLED;
}

public hook_say_team(id)
{
	if (is_user_hltv(id) || is_user_bot(id)) {
		return PLUGIN_CONTINUE;
	}

	read_args(g_Message, charmax(g_Message));
	handle_say(id, true);

	return PLUGIN_HANDLED;
}

handle_say(id, bool:say_team)
{
	static result;

	remove_quotes(g_Message);
	replace_all(g_Message, charmax(g_Message), "%", "");
	trim(g_Message);

	get_user_name(id, g_PlayerName, 31);

	if (CheckFlag(g_IsAdmin, id) && g_Message[0] == '!') {
		#if defined TRANSLIT
		if (g_Translit && CheckFlag(g_IsTranslit, id)) {
			TranslitMessage();
		}
		#endif

		format_admin(say_team);

		if (say_team) {
			SendMessageAdmin(COLOR_WHITE);
		} else {
			SendHudMessage();
		}
	} else {
		result = make_say(id, say_team);

		#if defined SPAM_FILTER || defined SWEAR_FILTER || defined ANTI_FLOOD
		switch (result) {
			#if defined SPAM_FILTER
			case HANDLE_SPAM: {
				g_PlayersData[id][PlayerWarns]++;
				if (g_PlayersData[id][PlayerWarns] > MAX_WARNS) {
					player_punish(id, "Spam Detected!");
					format(g_Output, charmax(g_Output), "%L", LANG_SERVER, "CTNG_SPAM_PUNISH", g_PlayerName);
					replace_color();
					SendMessageAll(0, COLOR_RED);
				}
			}
			#endif

			#if defined SWEAR_FILTER
			case HANDLE_SWEAR: {
				g_PlayersData[id][PlayerWarns] += 3;
				if (g_PlayersData[id][PlayerWarns] > MAX_WARNS) {
					player_punish(id, "Swear Detected!");
					format(g_Output, charmax(g_Output), "%L", LANG_SERVER, "CTNG_SWEAR_PUNISH", g_PlayerName);
					replace_color();
					SendMessageAll(0, COLOR_RED);
				}
			}
			#endif

			#if defined ANTI_FLOOD
			case HANDLE_FLOOD: {
				g_PlayersData[id][PlayerWarns]++;
				if (g_PlayersData[id][PlayerWarns] > MAX_WARNS) {
					player_punish(id, "Flood Detected!");
					format(g_Output, charmax(g_Output), "%L", LANG_SERVER, "CTNG_FLOOD_PUNISH", g_PlayerName);
					replace_color();
					SendMessageAll(0, COLOR_RED);
				} else {
					remove_task(WARN_DECREMENT_TASK+id);
					set_task(WARN_DECREMENT_FREQ, "warn_decrement", WARN_DECREMENT_TASK+id);
				}
			}
			#endif
		}
		#endif
	}
}

make_say(id, bool:say_team)
{
	static gag_time_remaining, bool:message_cmd;

	g_PlayerTeam = get_user_team(id);
	g_PlayerAlive = is_user_alive(id);

	g_Color = get_color(id);

	if (is_empty()) {
		return HANDLE_NONE;
	}

	#if defined SPAM_FILTER
	if (is_spam()) {
		format(g_Output, charmax(g_Output), "^x03[SPAM] ^x01%s: ^x04%s", g_PlayerName, g_Message);
		SendMessageAdmin(COLOR_RED);

		#if defined LOG_TO_FILE
		format_log(COLOR_RED);
		write_log(id);
		#endif

		return HANDLE_SPAM;
	}
	#endif

	#if defined ANTI_FLOOD
	if (is_flood(id)) {
		return HANDLE_FLOOD;
	}
	
	g_PlayersData[id][PlayerLastMessage] = g_SysTime;
	#endif

	#if defined TRANSLIT
	if (is_switch_lang(id)) {
		if (CheckFlag(g_IsTranslit, id)) {
			format(g_Output, charmax(g_Output), "%L", LANG_SERVER, "CTNG_UKR");
		} else {
			format(g_Output, charmax(g_Output), "%L", LANG_SERVER, "CTNG_ENG");
		}
		replace_color();
		SendMessageOne(COLOR_NONE, id);

		return HANDLE_NONE;
	}
	#endif

	if (is_gagcmd()) {
		gag_menu(id);
		return HANDLE_NONE;
	}
	
	if (is_cmd()) {
		message_cmd = true;
	} else {
		#if defined TRANSLIT
		if (g_Translit && CheckFlag(g_IsTranslit, id)) {
			TranslitMessage();
		}
		#endif

		message_cmd = false;
	}

	#if defined HIDE_CMD
	if (message_cmd) {
		return HANDLE_NONE;
	}
	#endif

	if (is_gaged(id)) {
		gag_time_remaining = g_PlayersData[id][PlayerGagExpired] - g_SysTime;
		if (gag_time_remaining > 60) {
			format(g_Output, charmax(g_Output), "%L", LANG_SERVER, "CTNG_GAGGED_MIN", (gag_time_remaining / 60));
		} else if (0 < gag_time_remaining < 60) {
			format(g_Output, charmax(g_Output), "%L", LANG_SERVER, "CTNG_GAGGED_SEC", gag_time_remaining);
		} else {
			format(g_Output, charmax(g_Output), "%L", LANG_SERVER, "CTNG_GAGGED_PERMANENT");
		}
		replace_color();

		SendMessageOne(COLOR_NONE, id);

		return HANDLE_NONE;
	}

	#if defined SWEAR_FILTER
	if (g_Swear && is_swear(id)) {
		format(g_Output, charmax(g_Output), "^x03[CENSURE] ^x01%s: ^x04%s", g_PlayerName, g_Message);
		SendMessageAdmin(COLOR_RED);

		#if defined LOG_TO_FILE
		format_log(COLOR_RED);
		write_log(id);
		#endif

		format(g_Output, charmax(g_Output), "^%L", LANG_SERVER, "CTNG_SWEAR");
		replace_color();

		SendMessageOne(COLOR_RED, id);

		return HANDLE_SWEAR;
	}
	#endif

	format_output(id, say_team);
	format_console(id, say_team);

	if (strlen(g_Output) >= 190) {
		format(g_Output, charmax(g_Output), "%L", LANG_SERVER, "CTNG_LONGMSG");
		replace_color();

		SendMessageOne(COLOR_NONE, id);

		return HANDLE_NONE;
	}

	if(say_team) {
		SendMessageTeam(id, g_Color, g_PlayerTeam);
		SendConsoleTeam(g_PlayerTeam);
	} else {
		SendMessageAll(id, g_Color);
		SendConsoleAll();
	}

	#if defined LOG_TO_FILE
	format_log(g_Color);
	write_log(id);
	#endif

	#if defined LOG_TO_AMXX
	static authid[34];
	get_user_authid(id, authid, 33);
	log_message("^"%s<%d><%s><%s>^" %s ^"%s^"%s", 
		g_PlayerName, 
		get_user_userid(id),
		authid,
		g_Colors[g_PlayerTeam],
		(!say_team ? "say" : "say_team"),
		g_Message,
		(g_PlayerAlive ? "" : " (dead)"));
	#endif

	return HANDLE_NONE;
}