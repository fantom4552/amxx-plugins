#if defined _log_included
	#endinput
#endif

#define _log_included

stock log_start()
{
	new p_FilePath[128], p_LogFileTime[16], p_LogFile[128], p_LogTitle[512];
	new p_MapName[32];

	get_localinfo("amxx_logs", p_FilePath, 127);

	format(g_LogDir, 127, "%s/colored_translit", p_FilePath);
	if(!dir_exists(g_LogDir))
	{
		mkdir(g_LogDir);
	}

	get_time("20%y.%m.%d", p_LogFileTime, 15);
	format(p_LogFile, 127, "%s/chat_%s.htm", g_LogDir, p_LogFileTime);
	if(!file_exists(p_LogFile))
	{
		format(p_LogTitle, 511, "<title>Colored Translit Next Generation by F@nt0M - %s</title>^n\
							<META http-equiv=Content-Type content='text/html;charset=UTF-8'>^n\
							<h2 align=center>Colored Translit Next Generation by F@nt0M</h2>", p_LogFileTime);
		write_file(p_LogFile, p_LogTitle);
	}

	get_mapname(p_MapName, 31);
	format(p_LogTitle, 511, "<hr><h3 align=center>Map - %s</h3><font face=^"Verdana^" size=2>", p_MapName);
	write_file(p_LogFile, p_LogTitle);

	g_LogFileChat = fopen(p_LogFile, "at");
}

stock log_end()
{
	fputs(g_LogFileChat, "</font>");
	fclose(g_LogFileChat);
}

stock format_log(color)
{
	copy(g_LogMessage, 511, g_Output);
	replace_all(g_LogMessage, 511, "^x01", "</font><font color=^"black^">");
	replace_all(g_LogMessage, 511, "^x04", "</font><font color=^"green^">");
	switch(color)
	{
		case COLOR_RED:
		{
			replace_all(g_LogMessage, 511, "^x03", "</font><font color=^"red^">");
		}

		case COLOR_BLUE:
		{
			replace_all(g_LogMessage, 511, "^x03", "</font><font color=^"blue^">");
		}

		case COLOR_WHITE:
		{
			replace_all(g_LogMessage, 511, "^x03", "</font><font color=^"gray^">");
		}
	}
}

stock write_log(id)
{
	static p_LogMessage[512], p_LogInfo[256], p_LogTime[16];

	get_time("%H:%M:%S", p_LogTime, 15);
	
	format(p_LogInfo, 255, "<font color=^"black^">%s &lt;%s&gt;&lt;%s&gt;</font>", p_LogTime, g_PlayersData[id][PlayerAuthID], g_PlayersData[id][PlayerIP]);
	format(p_LogMessage, 511, "%s - <font color=^"black^">%s</font><br>^n", p_LogInfo, g_LogMessage);

	fputs(g_LogFileChat, p_LogMessage);
	fflush(g_LogFileChat);
}