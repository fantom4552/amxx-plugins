stock bool:is_empty()
{
	return (equal(g_Message, "")) ? true : false;
}

#if defined TRANSLIT
stock bool:is_switch_lang(id)
{
	if (equal(g_Message, "/ukr")) {
		SetFlag(g_IsTranslit, id);
		return true;
	} else if (equal(g_Message, "/eng")) {
		ClearFlag(g_IsTranslit, id);
		return true;
	}

	return false;
}
#endif

stock bool:is_gagcmd()
{
	return equal(g_Message, "/gag") ? true : false;
}

#if defined HIDE_CMD
stock bool:is_cmd()
{
	if(g_Message[0] == '/') {
		return true;
	} else if (g_Ignore && is_ignore()) {
		return true;
	}
	
	return false;
}
#endif

#if defined SPAM_FILTER
stock bool:is_spam()
{
	if (regex_match_c(g_Message, g_SpamRegex, g_Return) > 0) {
		return true;
	}
	return false;
}
#endif

#if defined ANTI_FLOOD
stock bool:is_flood(id)
{
	#if !defined ADMIN_ANTI_FLOOD
	if (CheckFlag(g_IsAdmin, id)) {
		return false;
	}
	#endif

	if ((g_SysTime - g_PlayersData[id][PlayerLastMessage]) > FLOOD_TIME) {
		return false;
	}

	return true;
}

public warn_decrement(id)
{
	id -= WARN_DECREMENT_TASK;

	if (g_PlayersData[id][PlayerWarns] > 0) {
		g_PlayersData[id][PlayerWarns]--;
		set_task(15.0, "warn_decrement", WARN_DECREMENT_TASK+id);
	}
}
#endif

stock bool:isPlayerGagged(id, player)
{
	return CheckFlag(g_PlayersData[id][PlayersGagged], player) ? true : false;
}

stock player_punish(id, reason[])
{
	server_cmd("kick #%d ^"%s^"", get_user_userid(id), reason);
}

stock bool:is_ignore()
{
	new tmp[64];

	for (new i = 0; i < g_IgnoresNum; i++) {
		ArrayGetString(g_Ignores, i, tmp, 63);
		if (containi(g_Message, tmp) != -1) {
			return true;
		}
	}

	return false;
}

stock get_color(id)
{
	if (CheckFlag(g_IsAdmin, id)) {
		return COLOR_WHITE;
	}

	return g_PlayerTeam;
}

stock format_output(id, bool:say_team)
{
	new len = format(g_Output, charmax(g_Output), "^x01");

	if (!g_PlayerAlive) {
		if (g_PlayerTeam == TEAM_T || g_PlayerTeam == TEAM_CT) {
			len += format(g_Output[len], charmax(g_Output) - len, "*%L* ", LANG_PLAYER, "CTNG_DEAD");
		} else {
			len += format(g_Output[len], charmax(g_Output) - len, "*%L* ", LANG_PLAYER, "CTNG_SPECTATOR");
		}
	}

	if (say_team) {
		switch (g_PlayerTeam) {
			case TEAM_T: {
				len += format(g_Output[len], charmax(g_Output) - len, "(%L) ", LANG_PLAYER, "CTNG_TT_TEAM");
			}

			case TEAM_CT: {
				len += format(g_Output[len], charmax(g_Output) - len, "(%L) ", LANG_PLAYER, "CTNG_CT_TEAM");
			}

			default: {
				len += format(g_Output[len], charmax(g_Output) - len, "(%L) ", LANG_PLAYER, "CTNG_SPECTATOR_TEAM");
			}
		}
	}

	g_Prefix[0] = '^0';
	g_BlockPrefix = false;
	ExecuteForward(g_FwdFormat, g_Return, id);

	if (!g_BlockPrefix) {
		if (CheckFlag(g_IsAdmin, id)) {
			len += format(g_Output[len], charmax(g_Output) - len, "[^x04%L^x01]", LANG_PLAYER, "CTNG_ADMIN");
		} else if (CheckFlag(g_IsVip, id)) {
			len += format(g_Output[len], charmax(g_Output) - len, "[^x04%L^x01]", LANG_PLAYER, "CTNG_VIP");
		}
	}

	len += format(g_Output[len], charmax(g_Output) - len, g_Prefix);

	if (CheckFlag(g_IsAdmin, id)) {
		len += format(g_Output[len], charmax(g_Output) - len, " ^x04%s: ^x03%s", g_PlayerName, g_Message);
	} else if (CheckFlag(g_IsVip, id)) {
		len += format(g_Output[len], charmax(g_Output) - len, " ^x03%s: ^x04%s", g_PlayerName, g_Message);
	} else {
		len += format(g_Output[len], charmax(g_Output) - len, " ^x03%s: ^x01%s", g_PlayerName, g_Message);
	}
}

stock format_console(id, bool:say_team)
{
	new len = format(g_Console, charmax(g_Console), "[CHAT]");

	if (!g_PlayerAlive) {
		if (g_PlayerTeam == TEAM_T || g_PlayerTeam == TEAM_CT) {
			len += format(g_Console[len], charmax(g_Console) - len, "[%L]", LANG_PLAYER, "CTNG_DEAD");
		} else {
			len += format(g_Console[len], charmax(g_Console) - len, "[%L]", LANG_PLAYER, "CTNG_SPECTATOR");
		}
	}

	if (say_team) {
		switch (g_PlayerTeam) {
			case TEAM_T: {
				len += format(g_Console[len], charmax(g_Console) - len, "[%L] ", LANG_PLAYER, "CTNG_TT_TEAM");
			}

			case TEAM_CT: {
				len += format(g_Console[len], charmax(g_Console) - len, "[%L] ", LANG_PLAYER, "CTNG_CT_TEAM");
			}

			default: {
				len += format(g_Console[len], charmax(g_Console) - len, "[%L] ", LANG_PLAYER, "CTNG_SPECTATOR_TEAM");
			}
		}
	}

	if (CheckFlag(g_IsAdmin, id)) {
		len += format(g_Console[len], charmax(g_Console) - len, "[%L] ", LANG_PLAYER, "CTNG_ADMIN");
	} else if (CheckFlag(g_IsVip, id)) {
		len += format(g_Console[len], charmax(g_Console) - len, "[%L] ", LANG_PLAYER, "CTNG_VIP");
	}

	len += format(g_Console[len], charmax(g_Console) - len, " %s: %s", g_PlayerName, g_Message);
}

stock format_admin(bool:say_team)
{
	new len = 0;
	g_Output[len] = '^0';

	if (say_team) {
		len += format(g_Output[len], charmax(g_Output) - len, "^x01(%L) ", LANG_PLAYER, "CTNG_ADMINS");
		len += format(g_Output[len], charmax(g_Output) - len, "^x04%s: ^x03%s", g_PlayerName, g_Message[1]);
	} else {
		len += format(g_Output[len], charmax(g_Output) - len, "%s: %s", g_PlayerName, g_Message[1]);
	}

}

#if defined TRANSLIT
stock TranslitMessage()
{
	static i;
	for (i = 0; i < g_NumTranslitSimbols; i++) {
		replace_all(g_Message, charmax(g_Message), g_OriginalSimbol[i], g_TranslitSimbol[i]);
	}
}
#endif

stock SendMessageAll(id, color)
{
	for (new player = 1; player <= g_MaxPlayers; player++) {
		if (CheckFlag(g_IsConnected, player) && !isPlayerGagged(id, player)) {
			get_user_team(player, g_TeamName, 9);
			ChangeTeamInfo(player, g_Colors[color]);
			WriteMessage(player, g_Output);
			ChangeTeamInfo(player, g_TeamName);
		}
	}
}

stock SendMessageTeam(id, color, team)
{
	for (new player = 1; player <= g_MaxPlayers; player++) {
		if (CheckFlag(g_IsConnected, player) && (CheckFlag(g_IsAdmin, player) || get_user_team(player) == team) && !isPlayerGagged(id, player)) {
			get_user_team(player, g_TeamName, 9);
			ChangeTeamInfo(player, g_Colors[color]);
			WriteMessage(player, g_Output);
			ChangeTeamInfo(player, g_TeamName);
		}
	}
}

stock SendMessageAdmin(color)
{
	for (new player = 1; player <= g_MaxPlayers; player++) {
		if (CheckFlag(g_IsConnected, player) && CheckFlag(g_IsAdmin, player)) {
			if (color != COLOR_NONE) {
				get_user_team(player, g_TeamName, 9);
				ChangeTeamInfo(player, g_Colors[color]);
			}
			WriteMessage(player, g_Output);
			if (color != COLOR_NONE) {
				ChangeTeamInfo(player, g_TeamName);
			}
		}
	}
}

stock SendMessageOne(color, player)
{
	if(CheckFlag(g_IsConnected, player))
	{
		if(color != COLOR_NONE)
		{
			get_user_team(player, g_TeamName, 9);
			ChangeTeamInfo(player, g_Colors[color]);
		}
		WriteMessage(player, g_Output);
		if(color != COLOR_NONE)
		{
			ChangeTeamInfo(player, g_TeamName);
		}
	}
}

stock ChangeTeamInfo(player, team[])
{
	message_begin(MSG_ONE, g_msgTeamInfo, _, player);
	write_byte(player);
	write_string(team);
	message_end();
}


stock WriteMessage(player, message[])
{
	message_begin(MSG_ONE, g_msgSayText, _, player);
	write_byte(player);
	write_string(message);
	message_end();
}


stock SendConsoleAll()
{
	for (new player = 1; player <= g_MaxPlayers; player++)
	{
		if (CheckFlag(g_IsConnected, player)) {
			client_print(player, print_console, g_Console);
		}
	}
}

stock SendConsoleTeam(team)
{
	for (new player = 1; player <= g_MaxPlayers; player++)
	{
		if (CheckFlag(g_IsConnected, player) && (CheckFlag(g_IsAdmin, player) || get_user_team(player) == team)) {
			client_print(player, print_console, g_Console);
		}
	}
}

stock SendConsoleAdmin()
{
	for (new player = 1; player <= g_MaxPlayers; player++) {
		if (CheckFlag(g_IsConnected, player) && CheckFlag(g_IsAdmin, player)) {
			client_print(player, print_console, g_Console);
		}
	}
}

stock locate_player(const identifier[]) 
{
	new player = 0;

	if (identifier[0]=='#' && identifier[1]) {
		player = find_player("k", str_to_num(identifier[1]));
	}

	if (!player)	{
		player = find_player("c", identifier);
	}

	if (!player) {
		player = find_player("bl", identifier);
	}
	
	if (!player) {
		player = find_player("d", identifier);
	}
	
	return player;
}

stock replace_color()
{
	replace_all(g_Output, charsmax(g_Output), "!g", "^x04");
	replace_all(g_Output, charsmax(g_Output), "!t", "^x03");
	replace_all(g_Output, charsmax(g_Output), "!n", "^x01");
}

stock SendHudMessage()
{
	if (++g_msgChannel > 6 || g_msgChannel < 3) {
		g_msgChannel = 3;
	}

	new Float:verpos = 0.55 + float(g_msgChannel) / 35.0;	
	set_hudmessage(255, 255, 255, 0.05, verpos, 0, 6.0, 6.0, 0.5, 0.15, -1);

	for (new player = 1; player <= g_MaxPlayers; player++) {
		if (CheckFlag(g_IsConnected, player)) {
			ShowSyncHudMsg(player, g_HudSync, g_Output);
		}
	}
}