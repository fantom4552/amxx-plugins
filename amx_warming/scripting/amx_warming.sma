#pragma semicolon 1

#define KNIFE_ENABLE
#define TIMERINFO

#define MAX_MONEY 16000

#include <amxmodx>
#include <cstrike>
#include <hamsandwich>

#if AMXX_VERSION_NUM != 183
#include <dhudmessage>
#endif

#define PLUGIN "Amx Warming"
#define VERSION "1.6"
#define AUTHOR "F@nt0M"

#define InTeam(%1) ( CS_TEAM_UNASSIGNED < cs_get_user_team(%1) < CS_TEAM_SPECTATOR ) 

#define TASK_ID_COUNTDOWN 9131
#define TASK_ID_RESPAWN 8845

#define HUD_TIME 220, 160, 0, -1.0, 0.8, 0, 0.0, 0.8, 0.0, 0.0
#define HUD_GO	50, 250, 50, -1.0, 0.2, 2, 4.0, 3.0, 0.05, 0.01

#define RESPAWN_TIMER 2.0
#define COUNTDOWN_SPEC 4

new g_RestartForward;

new g_pcvarRestart;
new g_pcvarStartMoney;
new g_pcvarBuytime;

new g_pcvarEanble;
new g_pcvarTime;

new g_Countdown;

new g_StartMoney;
new Float:g_BuyTime;

#if defined KNIFE_ENABLE
new g_pcvarKnife;
new bool:g_Knife;
#endif

#if defined TIMERINFO
new g_MsgShowTimer;
new g_MsgRoundTime;
#endif

public plugin_init()
{
	register_plugin(PLUGIN, VERSION, AUTHOR);

	register_dictionary("amx_warming.txt");

	register_event("TextMsg", "event_start_game", "a", "2=#Game_Commencing");

	register_event("DeathMsg", "kill", "a");

	#if defined KNIFE_ENABLE
	register_event("CurWeapon", "knife", "b", "1=1", "2!29");
	#endif

	#if defined TIMERINFO
	g_MsgShowTimer = get_user_msgid("ShowTimer");
	g_MsgRoundTime = get_user_msgid("RoundTime");
	#endif
	
	g_pcvarRestart = get_cvar_pointer("sv_restart");
	g_pcvarStartMoney = get_cvar_pointer("mp_startmoney");
	g_pcvarBuytime = get_cvar_pointer("mp_buytime");

	g_RestartForward = CreateMultiForward("amx_warming_finish", ET_IGNORE);

	state enabled;
}

public plugin_precache()
{
	g_pcvarEanble = register_cvar("amx_warming", "1");
	g_pcvarTime = register_cvar("amx_warming_time", "60");

	#if defined KNIFE_ENABLE
	g_pcvarKnife = register_cvar("amx_warming_knife", "1");
	#endif
}

public event_start_game() <enabled>
{
	if (get_pcvar_num(g_pcvarEanble)) {
		new time = get_pcvar_num(g_pcvarTime);
		g_Countdown = time + COUNTDOWN_SPEC + 1;

		if (task_exists(TASK_ID_COUNTDOWN)) {
			remove_task(TASK_ID_COUNTDOWN);
		} else {
			g_StartMoney = get_pcvar_num(g_pcvarStartMoney);
			g_BuyTime = get_pcvar_float(g_pcvarBuytime);

			set_pcvar_num(g_pcvarStartMoney, MAX_MONEY);
			set_pcvar_float(g_pcvarBuytime, Float:(float(time) / 60));
		}

		#if defined KNIFE_ENABLE
		g_Knife = get_pcvar_num(g_pcvarKnife) ? true : false;
		#endif

		set_task(1.0, "task_countdown", TASK_ID_COUNTDOWN, _, _, "a", g_Countdown);
	}
}

public event_start_game() <disabled> {}

public kill()  <enabled>
{
	static id;

	id = read_data(2);

	if (!is_user_connected(id) || is_user_alive(id) || is_user_bot(id) || !InTeam(id)) {
		return PLUGIN_CONTINUE;
	}

	remove_task(TASK_ID_RESPAWN+id);
	set_task(RESPAWN_TIMER, "respawn", TASK_ID_RESPAWN+id);

	return PLUGIN_CONTINUE;
}

public kill() <disabled> { return PLUGIN_CONTINUE; }

public task_countdown()
{
	g_Countdown--;

	if (g_Countdown > COUNTDOWN_SPEC) {
		#if defined TIMERINFO
		update_timer(g_Countdown-COUNTDOWN_SPEC);
		#else
		set_dhudmessage(HUD_TIME);
		show_dhudmessage(0, "%L", LANG_SERVER, "WARMING_TIME", g_Countdown-COUNTDOWN_SPEC);
		#endif
	} else if(g_Countdown == COUNTDOWN_SPEC) {
		set_dhudmessage(HUD_GO);
		show_dhudmessage(0, "%L", LANG_SERVER, "WARMING_GO");
	} else if(g_Countdown == 2) {
		set_pcvar_num(g_pcvarStartMoney, g_StartMoney);
		set_pcvar_float(g_pcvarBuytime, g_BuyTime);

		set_pcvar_num(g_pcvarRestart, 1);

		new ret;
		ExecuteForward(g_RestartForward, ret);
	} else if(g_Countdown <= 0) {
		state disabled;
	}
}

public respawn(id) <enabled>
{
	id -= TASK_ID_RESPAWN;
	
	if (is_user_connected(id) && !is_user_alive(id) && InTeam(id)) {
		ExecuteHamB(Ham_CS_RoundRespawn, id);
	}
}

public respawn(id) <disabled> {}

#if defined KNIFE_ENABLE
public knife(id)  <enabled>
{
	if (g_Knife) {
		engclient_cmd(id, "weapon_knife");
	}
}

public knife(id)  <disabled> {}
#endif

#if defined TIMERINFO
update_timer(countdown)
{
	static players[32], num, i;
	get_players(players, num, "ac");
	for (i = 0; i < num; ++i) {
		message_begin(MSG_ONE_UNRELIABLE, g_MsgShowTimer, _, players[i]);
		message_end();
		
		message_begin(MSG_ONE_UNRELIABLE, g_MsgRoundTime, _, players[i]);
		write_short(countdown + 1);
		message_end();
	}
}
#endif